BETA-VERSION !!! - Dib2x/ Dib2QM/ etc.

HELP - HILFE - ...

(Deutscher Text: weiter unten)

QUESTIONS (Q) AND ANSWERS (A)

Q: I have the settings right, but I keep getting messaging errors.
A: Some e-mail providers block unknown apps from accessing the account. This is one of the reasons, why we recommend to use an extra account (like mail.de) for Dib2x. Anyway, you have to check the provider settings (i.e. smaller apps like
Dib2Qm need to get access and be allowed to use IMAP and SMTP services).
E.g. for a GMail account: log in with your browser, go to 'Settings', 'Forwarding...IMAP', 'Enable IMAP', 'Save', then to 'Accounts', 'Other ... settings', '...Security', '2-Step...', 'Off', and 'Connected apps', 'Allow less secure', 'On'.

Q: How can I make use of exported files?
A: The important thing is to use them for backups and, if needed, for transferring the data to a new device. Exported data keeps its encoding. The source files are online (or saved together with the exported files), so that experts can enable other types of devices or apps.

Q: The messages arrive with quite a delay ...
A: Depending on the brand of the device, various system settings and missing permissions can interfere, e.g. status, notifications, background, screen lock, protected apps etc.

Q: Why is the e-mail password 'less protected' than the data?
A: Well, it actually depends on your willingness to type long passwords for the 'access code' ... If your Android system is trustworthy, it will keep your app-specific data protected. Then a short 'access code' should be sufficient. The data itself is protected by a combination of 'access code' plus e-mail password.

(German)

HILFE - FRAGEN (F) UND ANTWORTEN (A)

F: Meine Einstellungen sind richtig, aber ich kriege nur irgendwelche Verbindungs-Fehler.
A: Manche E-Mail-Anbieter blockieren unbekannte Apps. Das ist einer der Gründe, warum wir empfehlen, für Dib2x eine extra E-Mail-Adresse zu verwenden (z.B. bei mail.de). Auf jeden Fall müssen dort die Einstellungen geprüft werden:
Z.B. bei einer GMail-Adresse: mit dem Browser einloggen, dann zu 'Einstellungen', 'Weiterl...IMAP', 'IMAP aktivieren', 'Speichern', dann zu 'Konto', 'Weitere ...', '...Sicherheit', '2-Step/2-Stufen...', 'Off/Aus', und '... Apps', '...allow/erlauben', 'On/Ein'.

F: Wie kann ich exportierte Daten weiterbearbeiten?
A: Das wichtigste ist, dass mit dem Export Daten gesichert (Backup!) und auch auf ein neues Gerät übertragen werden können. Beim Exportieren bleiben die Daten kodiert. Die Source-Dateien sind online (oder werden mitgespeichert), sodass Experten auch andere Gerätetypen oder Apps nutzbar machen könnten.

F: Nachrichten kommen nur verzögert an ...
A: Bei den System-Einstellungen müssen der App die nötigen Berechtigungen zugewiesen werden, insbesondere Status-Anzeige/ Notifications, Hintergrund-Arbeiten/ Background, auch ohne aktivem Bildschirm/ Protected Apps/ Screen Lock, SD lesen und schreiben usw.

F: Warum wird das E-Mail-Passwort nicht 'besser' geschützt?
A: Naja, das hängt vorwiegend an der Bereitschaft der Nutzer, jedes Mal einen langen 'Zugangscode' einzutippen ... Wenn Ihr Android-Gerät vertrauenswürdig ist, wird es die App-spezifischen Daten schützen. Dann wird auch kein langer Zugangscode ('access code') benötigt. Auf jeden Fall sind die Daten durch eine Kombination aus Zugangscode und E-Mail-Passwort gesichert.
