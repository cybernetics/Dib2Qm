// Copyright (C) 2017,2019 Roland Horsch and others:
// -- Changes: Copyright (C) 2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// -- Structure/API: Copyright (C) 2014/2015  Jeroen Vreeken.
// All under GNU license:
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// This part is based on 'pgp' from net.vreeken.quickmsg, and many valuable hints about
// frugal crypto, e.g. crypto.stackexchange.com:
// BC usage had to be replaced (RSA/ DH/ ECDH (RFC4880_EXP2)). Sorry for not keeping a list ...
// Due to the former dependency on an outdated BC, backwards compatibility had to be
// dropped temporarily.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package localhost.dibdib.chat;

import com.gitlab.dibdib.dib2qm.*;
import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.util.Arrays;
import javax.activation.*;
import javax.crypto.KeyAgreement;
import javax.mail.util.ByteArrayDataSource;
import localhost.dibdib.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.func.*;
import net.sf.dibdib.store.Codec;
import net.sf.dibdib.struc.*;
import net.vreeken.quickmsg.attachment;

/** Static keys for authentication, session keys for encryption.
 * Old key info for alternating keys (1/2/0): CAAU = Count, Added/Include, Acknowledge, Use.
 * Now used as C0A0 (Count + Acknowledge).
 */
public class EcDhQm implements PrivProvIf {

protected net.sf.dibdib.join.ContextIf_OLD prefs;

private final byte DER_TAG_SEQUENCE = 0x30;
private final byte PGP_TAG_PK = (byte) (0x80 | 0x40 | 6);
private final byte PGP_TAG_UID = (byte) (0x80 | 0x40 | 0x13);
private final byte PGP_PK_VERSION = 4;
private final byte PGP_PK_ALGO_X_ECDH = 101;
private final byte[] PGP_PK_PRE = new byte[] { PGP_PK_VERSION, 0, 0, 0, 1, PGP_PK_ALGO_X_ECDH };

private static boolean errorMsg = false;

@Override
public void init( net.sf.dibdib.join.ContextIf_OLD context ) {
	Dib2Config.log( "ecdh init", "a" );
	prefs = context; // Dib2Config.qContextQm;
	if (null == prefs.get( "KEY.0.SIG.ECDSA256.S", null )) {
		KeyPairGenerator kpg;
		try {
			kpg = KeyPairGenerator.getInstance( "EC" );
		} catch (NoSuchAlgorithmException e) {
			Dib2Config.log( "ecdh init", "err1 " + e );
			return;
		}
		// Waiting for Java/BC/Android to support other curves ...
		kpg.initialize( 256 );
		KeyPair kp = kpg.generateKeyPair();
		byte[] sigPk = kp.getPublic().getEncoded();
		byte[] sigSk = kp.getPrivate().getEncoded();
		prefs.set( "KEY.0.SIG.ECDSA256.P", sigPk );
		prefs.set( "KEY.0.SIG.ECDSA256.S", sigSk );
		Dib2Config.log( "ecdh init", "created " + fingerprint( "0" ) );
	} else {
		Dib2Config.log( "ecdh init", "fi " + fingerprint( "0" ) );
	}
}

/** FNV-1a, also for short keys. From UtilMisc ...
 * Cmp. http://www.isthe.com/chongo/tech/comp/fnv.
 */
private static int hash32_fnv1a( byte[] data ) {
	int hash = 0x811c9dc5;
	for (byte b0 : data) {
		hash ^= 0xff & b0;
		// hash *= prime;
		hash += (hash << 1) + (hash << 4) + (hash << 7) + (hash << 8) + (hash << 24);
	}
	return hash;
}

@Override
public boolean load_keys() {
	return null != prefs.get( "KEY.0.SIG.ECDSA256.S", null );
}

protected byte[] getPkSignatureEncoded( String addr ) {
	final String which = addr.equalsIgnoreCase( UtilString.string4Utf8( prefs.get( "email_address", new byte[ 0 ] ) ) ) ? "0" : addr;
	final byte[] pk = prefs.get( "KEY." + which + ".SIG.ECDSA256.P", null );
	if (null == pk) {
		return null;
	}
	return new X509EncodedKeySpec( pk ).getEncoded();
}

@Override
public String fingerprint( String addr ) {
	byte[] pk = getPkSignatureEncoded( addr );
	return UtilCodec.fingerprint( pk, true );
}

@Override
public byte[] public_keyring_add_key( byte[] xInputStream, String[] xyAddr ) {
	byte[] old = null;
	if ((null == xInputStream) || (10 >= xInputStream.length)) {
		return null;
	}
	byte[] pk = new byte[ 0 ];
	if (xyAddr[ 0 ].contains( "@" ) && (DER_TAG_SEQUENCE == xInputStream[ 0 ])) {
		///// Deprecated format
		pk = xInputStream.clone();
	} else if (PGP_TAG_PK == xInputStream[ 0 ]) {
		int offs = UtilMisc.getPacketHeaderLen( xInputStream, 0 );
		int len = UtilMisc.getPacketBodyLen( xInputStream, 1 );
		if ((1 >= offs) || (6 < offs) || (PGP_PK_PRE.length >= len)
			|| ((offs + len) > xInputStream.length) //,
			|| (xInputStream[ offs ] != PGP_PK_PRE[ 0 ])
			|| (xInputStream[ offs + PGP_PK_PRE.length - 1 ] != PGP_PK_PRE[ PGP_PK_PRE.length - 1 ])) {
			return null;
		}
		pk = Arrays.copyOfRange( xInputStream, offs + PGP_PK_PRE.length, offs + len );
		offs += len;
		if (offs < xInputStream.length) {
			len = UtilMisc.getPacketBodyLen( xInputStream, offs + 1 );
			offs += UtilMisc.getPacketHeaderLen( xInputStream, offs );
			if ((offs + len) > xInputStream.length) {
				return null;
			}
			String uid = UtilString.string4Utf8( Arrays.copyOfRange( xInputStream, offs, offs + len ) );
			int iL = uid.indexOf( '<' );
			int iR = uid.lastIndexOf( '>' );
			if ((0 <= iL) && (iL < iR)) {
				uid = uid.substring( iL + 1, iR ).replace( " ", "" );
			} else {
				return null;
			}
			if (xyAddr[ 0 ].contains( "@" ) && !uid.equalsIgnoreCase( xyAddr[ 0 ] ) && (3 < uid.length())) {
				old = prefs.get( "KEY." + uid + ".SIG.ECDSA256.P", null );
				if (null != old) {
					return null;
				}
			}
			xyAddr[ 0 ] = uid;
		}
	}
	if ((0 < pk.length) && xyAddr[ 0 ].contains( "@" ) && !xyAddr[ 0 ].equalsIgnoreCase( Pgp.my_user_id )) {
		old = prefs.get( "KEY." + xyAddr[ 0 ] + ".SIG.ECDSA256.P", null );
		prefs.set( "KEY." + xyAddr[ 0 ] + ".SIG.ECDSA256.P", pk );
		///// DEPRECATED: Use it as extra session key:
		prefs.set( "KEY." + xyAddr[ 0 ] + ".ENC.ECDH.P1", pk ); // P1/S1
		prefs.set( "KEY.0.ECDH." + xyAddr[ 0 ] + ".P1", prefs.get( "KEY.0.SIG.ECDSA256.P", null ) );
		prefs.set( "KEY.0.ECDH." + xyAddr[ 0 ] + ".S1", prefs.get( "KEY.0.SIG.ECDSA256.S", null ) );
		// Use it as initial session key:
		prefs.set( "KEY.0.ECDH." + xyAddr[ 0 ] + ".CAAU", "0000".getBytes( UtilString.STR256 ) ); //0111" );
		if (Arrays.equals( pk, old )) {
			return null;
		} else if (null == old) {
			old = pk;
		}
		prefs.remove( "KEY." + xyAddr[ 0 ] + ".CIPHER" );
	}
	return old;
}

@Override
public attachment key_attachment( String keyuserAddr ) {
	Dib2Config.log( "key_attachment", "generate public key" );
	attachment attachment = new attachment();
	byte[] pk = getPkSignatureEncoded( keyuserAddr );
	if (null == pk) {
		return null;
	}
	if ((null != keyuserAddr) && (1 < keyuserAddr.length()) && !keyuserAddr.equals( Pgp.my_user_id )) {
		int secs = (int) (UtilMisc.currentTimeMillisLinearized() / 1000);
		byte[] pkPack = Arrays.copyOf( PGP_PK_PRE, pk.length + PGP_PK_PRE.length );
		pkPack[ 1 ] = (byte) (secs >>> 24);
		pkPack[ 2 ] = (byte) (secs >>> 16);
		pkPack[ 3 ] = (byte) (secs >>> 8);
		// pkPack[4] = variant;
		System.arraycopy( pk, 0, pkPack, PGP_PK_PRE.length, pk.length );
		pkPack = UtilMisc.packet4880X( PGP_TAG_PK, null, pkPack, 0, pkPack.length );
		byte[] uid = UtilString.bytesUtf8( "<" + keyuserAddr + ">" );
		byte[] uidPack = UtilMisc.packet4880X( PGP_TAG_UID, null, uid, 0, uid.length );
		pk = Arrays.copyOf( pkPack, pkPack.length + uidPack.length );
		System.arraycopy( uidPack, 0, pk, pkPack.length, uidPack.length );
	} else {
		// Use deprecated format for the time being.
	}
	attachment.datahandler = new DataHandler( new ByteArrayDataSource( pk, "application/pgp-keys" ) );
	attachment.name = fingerprint( keyuserAddr ) + ".asc";
	prefs.set( "KEY.0.ECDH." + keyuserAddr + ".CAAU", "0000".getBytes( UtilString.STR256 ) ); // 0101" );
	prefs.remove( "KEY." + keyuserAddr + ".CIPHER" );
//	prefs.remove( "KEY." + keyuserAddr + ".ENC.ECDH.P1" );
//	prefs.remove( "KEY." + keyuserAddr + ".ENC.ECDH.P2" );
	return attachment;
}

@Override
public attachment pgpmime_id() {
	attachment a = new attachment();
	a.disposition = "inline";
	DataSource ds;
	try {
		ds = new ByteArrayDataSource( "Version: 1\n", "application/pgp-encrypted" );
	} catch (IOException e) {
		return null;
	}
	a.datahandler = new DataHandler( ds );
	a.name = "pgp_mime_id";
	return a;
}

private byte[] calcAesKey( byte[] mySk, byte[] myPk, byte[] otherPk ) {
	byte[] out = null;
	try {
		X509EncodedKeySpec pkSpec = new X509EncodedKeySpec( otherPk );
		PKCS8EncodedKeySpec skSpec = new PKCS8EncodedKeySpec( mySk );
		KeyFactory kfp = KeyFactory.getInstance( "EC" );
		KeyFactory kfs = KeyFactory.getInstance( "EC" );
		KeyAgreement ka = KeyAgreement.getInstance( "ECDH" );
		ka.init( kfs.generatePrivate( skSpec ) );
		ka.doPhase( kfp.generatePublic( pkSpec ), true );
		byte[] sharedSecret = ka.generateSecret();
		///// Derive a key from shared secret.
		boolean meFirst = false;
		for (int i0 = 0; (i0 < myPk.length) && (i0 < otherPk.length); ++ i0) {
			if (myPk[ i0 ] != otherPk[ i0 ]) {
				meFirst = (myPk[ i0 ] > otherPk[ i0 ]);
				break;
			}
		}
		MessageDigest hash = MessageDigest.getInstance( "SHA-256" );
		hash.update( sharedSecret );
		hash.update( meFirst ? myPk : otherPk );
		hash.update( meFirst ? otherPk : myPk );
		out = hash.digest();
	} catch (Exception e) {
		return null;
	}
	return out;
}

public byte[] getAesKey_OLD( String who, char usedKeyNumber ) { //, byte[] optTempKey ) {
	byte[] out = null;
	try {
		if ('0' < usedKeyNumber) {
			out = prefs.get( "KEY." + who //,
				+ (".ENC.AES.K" + usedKeyNumber), //,
				new byte[ 0 ] );
			if ((null != out) && (32 == out.length)) {
				return out;
			}
		}
		byte[] xPk = //optTempKey;
		prefs.get( "KEY." + who //,
			+ ((usedKeyNumber <= '0') ? ".SIG.ECDSA256.P" : (".ENC.ECDH.P" + usedKeyNumber)), //,
			new byte[ 0 ] );
		if ((null == xPk) || (10 >= xPk.length)) {
			return null;
		}
		byte[] mySk = prefs.get( (usedKeyNumber <= '0')//,
		? ("KEY.0.SIG.ECDSA256.S") //,
			: ("KEY.0.ECDH." + who + ".S" + usedKeyNumber), //,
			null );
		byte[] myPk = prefs.get( (usedKeyNumber <= '0')//,
		? ("KEY.0.SIG.ECDSA256.P") //,
			: ("KEY.0.ECDH." + who + ".P" + usedKeyNumber), //,
			null );
		out = calcAesKey( mySk, myPk, xPk );
		if ('0' < usedKeyNumber) {
			prefs.set( "KEY." + who + ".ENC.AES.K" + usedKeyNumber, out );
			prefs.remove( "KEY." + who + ".ENC.ECDH.P" + usedKeyNumber );
		}
	} catch (Exception e) {
		return null;
	}
	return out;
}

/** Use hash of public key for checking in case of lost packages.
 * @param who
 * @param mine
 * @param xHash
 * @param offs
 * @return
 */
private byte[] getKey4Hash( String who, boolean mine, int hash, boolean senderIsMe ) {
	byte[] check;
	final String which = mine ? ("KEY.0.ECDH." + who + ".P")
		: "KEY." + who + ".ENC.ECDH.P";
	for (int n0 = 1; n0 <= 2; ++ n0) {
		check = prefs.get( which + n0, null );
		if ((null != check) && (hash32_fnv1a( check ) == hash)) {
			if (mine && !senderIsMe && (n0 == 1)) {
				String caau = new String( prefs.get( "KEY.0.ECDH." + who + ".CAAU", "0000".getBytes( UtilString.STR256 ) ),
					UtilString.STR256 );
				if ('9' < caau.charAt( 0 )) {
					// Is acknowledged. Start count-down.
					prefs.set( "KEY.0.ECDH." + who + ".CAAU", ("9" + caau.substring( 1 )).getBytes( UtilString.STR256 ) );
				}
			}
			return check;
		}
	}
	check = prefs.get( mine ? "KEY.0.SIG.ECDSA256.P" : ("KEY." + who + ".SIG.ECDSA256.P"), null );
	if ((null != check) && (hash32_fnv1a( check ) == hash)) {
		return check;
	}
	return null;
}

private byte[] getMatchingSk( byte[] pk, String who ) {
	final String which = "KEY.0.ECDH." + who + ".P";
	for (int n0 = 1; n0 <= 2; ++ n0) {
		if (Arrays.equals( pk, prefs.get( which + n0, null ) )) {
			return prefs.get( "KEY.0.ECDH." + who + ".S" + n0, null );
		}
	}
	if (Arrays.equals( pk, prefs.get( "KEY.0.SIG.ECDSA256.P", null ) )) {
		return prefs.get( "KEY.0.SIG.ECDSA256.S", null );
	}
	return null;
}

private boolean setNewKey( String who, byte[] key, byte[] matchingSk ) {
	int hash = hash32_fnv1a( key );
	String which = (null != matchingSk) ? ("KEY.0.ECDH." + who + ".P")
		: "KEY." + who + ".ENC.ECDH.P";
	byte[] key2 = getKey4Hash( who, (null != matchingSk), hash, (null != matchingSk) );
	if (null != key2) {
		if (null != matchingSk) {
			return false;
		} else if (Arrays.equals( key, key2 )) {
			return true;
		}
		prefs.remove( "KEY." + who + ".ENC.AES" );
		prefs.set( which + 2, key2 );
		prefs.set( "KEY.0.ECDH." + who + ".CAAU", "0000".getBytes( UtilString.STR256 ) );
		return true;
	}
	prefs.remove( "KEY." + who + ".ENC.AES" );
	prefs.set( which + 2, prefs.get( which + 1, null ) );
	prefs.set( which + 1, key );
	if (null == matchingSk) {
		String caau = new String( prefs.get( "KEY.0.ECDH." + who + ".CAAU", "X000".getBytes( UtilString.STR256 ) ), UtilString.STR256 );
		prefs.set( "KEY.0.ECDH." + who + ".CAAU", ("" + caau.charAt( 0 ) + "010").getBytes( UtilString.STR256 ) );
	} else {
		prefs.set( "KEY.0.ECDH." + who + ".S2", prefs.get( "KEY.0.ECDH." + who + ".S1", null ) );
		prefs.set( "KEY.0.ECDH." + who + ".S1", matchingSk );
		prefs.set( "KEY.0.ECDH." + who + ".CAAU", "X000".getBytes( UtilString.STR256 ) );
	}
	return true;
}

private boolean createDhKey( String who, char id ) throws NoSuchAlgorithmException {
	KeyPairGenerator kpg;
	kpg = KeyPairGenerator.getInstance( "EC" );
	kpg.initialize( 256 );
	KeyPair kp = kpg.generateKeyPair();
	byte[] pk = kp.getPublic().getEncoded();
	byte[] sk = kp.getPrivate().getEncoded();
	if (0 == id) {
		prefs.remove( "KEY." + who + ".ENC.AES.K1" );
		prefs.remove( "KEY." + who + ".ENC.AES.K2" );
		return setNewKey( who, pk, sk );
	}
	prefs.remove( "KEY." + who + ".ENC.AES.K" + id );
	prefs.set( "KEY.0.ECDH." + who + ".P" + id, pk );
	prefs.set( "KEY.0.ECDH." + who + ".S" + id, sk );
	prefs.remove( "KEY." + who + ".ENC.AES" );
	return true;
}

private byte[] getPkInfo4Sending( String who ) {
	byte[] myPk = prefs.get( "KEY.0.ECDH." + who + ".P1", null );
	if (null == myPk) {
		try {
			if (!createDhKey( who, (char) 0 )) {
				createDhKey( who, '1' );
			}
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
		myPk = prefs.get( "KEY.0.ECDH." + who + ".P1", null );
	}
	String caau = new String( prefs.get( "KEY.0.ECDH." + who + ".CAAU", "X000".getBytes( UtilString.STR256 ) ), UtilString.STR256 );
	byte[] otherPk = prefs.get( "KEY." + who + ".SIG.ECDSA256.P", null );
	boolean full = true;
	if ('1' == caau.charAt( 2 )) {
		otherPk = prefs.get( "KEY." + who + ".ENC.ECDH.P1", otherPk );
		full = '9' <= caau.charAt( 0 );
	}
	if (null == otherPk) {
		return null;
	}
	int len = myPk.length;
	byte[] out = new byte[ 10 ];
	if (full) {
		out = Arrays.copyOf( myPk, 5 + 2 + myPk.length );
		out[ len ] = (byte) len;
		out[ ++ len ] = QVal.TAG_BYTES_L1;
		++ len;
	} else {
		UtilMisc.bytes4Long( out, 0, hash32_fnv1a( myPk ), 4 );
		out[ 4 ] = (byte) (QVal.TAG_BYTES_L0 + 4);
		len = 5;
	}
	UtilMisc.bytes4Long( out, len, hash32_fnv1a( otherPk ), 4 );
	out[ len + 4 ] = (byte) (QVal.TAG_BYTES_L0 + 4);
	return out;
}

private byte[] getAesKey( String who, boolean senderIsMe, byte[] pkInfo, int offsTag2 ) {
	long offsLen2 = QVal.getTcvOffsetLength( pkInfo, 2, offsTag2, 1 );
	long offsLen1 = QVal.getTcvOffsetLength( pkInfo, 2, -1 + (int) offsLen2, 1 );
	byte[] myKeyOrHash = senderIsMe
		? Arrays.copyOfRange( pkInfo, (int) offsLen1, (int) (offsLen1 >>> 32) + (int) offsLen1 )
		: Arrays.copyOfRange( pkInfo, (int) offsLen2, (int) (offsLen2 >>> 32) + (int) offsLen2 );
	byte[] otherKeyOrHash = !senderIsMe
		? Arrays.copyOfRange( pkInfo, (int) offsLen1, (int) (offsLen1 >>> 32) + (int) offsLen1 )
		: Arrays.copyOfRange( pkInfo, (int) offsLen2, (int) (offsLen2 >>> 32) + (int) offsLen2 );
	if (4 == myKeyOrHash.length) {
		myKeyOrHash = getKey4Hash( who, true, (int) UtilMisc.long4Bytes( myKeyOrHash, 0, 4 ), senderIsMe );
	}
	if (4 == otherKeyOrHash.length) {
		otherKeyOrHash = getKey4Hash( who, false, (int) UtilMisc.long4Bytes( otherKeyOrHash, 0, 4 ), senderIsMe );
	}
	if ((null == myKeyOrHash) || (null == otherKeyOrHash)) {
		return null;
	}
	boolean isLatest = false;
	byte[] out;
	byte[] check = prefs.get( "KEY." + who + ".ENC.ECDH.P1", null );
	String caau = new String( prefs.get( "KEY.0.ECDH." + who + ".CAAU", "0000".getBytes( UtilString.STR256 ) ), UtilString.STR256 );
	if ((null != check) && Arrays.equals( otherKeyOrHash, check )) {
		check = prefs.get( "KEY.0.ECDH." + who + ".P1", null );
		if ((null != check) && Arrays.equals( myKeyOrHash, check )) {
			isLatest = true;
			out = prefs.get( "KEY." + who + ".ENC.AES", null );
			if (null != out) {
				if (('0' < caau.charAt( 0 )) && ('9' >= caau.charAt( 0 ))) {
					// Count down.
					prefs.set( "KEY.0.ECDH." + who + ".CAAU",
						("" + (char) (-1 + '0' + (0xf & caau.charAt( 0 ))) + caau.substring( 1 )).getBytes( UtilString.STR256 ) );
				}
				return out;
			}
		}
	}
	if (!isLatest) {
		prefs.remove( "KEY." + who + ".ENC.AES" );
//		prefs.set( "KEY.0.ECDH." + who + ".CAAU", ("" + caau.charAt( 0 ) + "000").getBytes( UtilString.STR256 ) );
	}
	out = calcAesKey( getMatchingSk( myKeyOrHash, who ), myKeyOrHash, otherKeyOrHash );
	if ((isLatest) && (null != out)) {
		prefs.set( "KEY." + who + ".ENC.AES", out );
	}
	return out;
}

private attachment encrypt_sign_OLD( attachment unenc, String to ) {
	String filename = unenc.name;
	byte[] enc;
	String caau = new String( prefs.get( "KEY.0.ECDH." + to + ".CAAU", new byte[ 0 ] ) );
	final int hdVersion = (prefs.get( "KEY." + to + ".CIPHER", new byte[ 1 ] )[ 0 ] == 'C')
		? Dib2Constants.FILE_STRUC_VERSION_CMPAT : Dib2Constants.FILE_STRUC_VERSION_MIN;
	try {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		unenc.datahandler.writeTo( os );
		unenc = null;
		byte[] dat = os.toByteArray();
		if (('0' >= caau.charAt( 0 )) && ('0' >= caau.charAt( 1 ))) { // || (caau.charAt( 1 ) != caau.charAt( 2 ))) {
			///// Countdown done. Need a new key.
			char id = ('1' == caau.charAt( 3 )) ? '2' : '1';
			createDhKey( to, id );
			caau = "X" + id + caau.charAt( 2 ) + caau.charAt( 3 );
			prefs.set( "KEY.0.ECDH." + to + ".CAAU", caau.getBytes( UtilString.STR256 ) );
			Dib2Config.log( "encrypt sign", "new key, caau " + caau );
		} else if (((caau.charAt( 1 ) == caau.charAt( 3 )) || (caau.charAt( 2 ) == caau.charAt( 3 ))) //,
			&& ('0' != caau.charAt( 3 ))) {
			caau = "" + caau.charAt( 0 ) + '0' + caau.substring( 2 );
			prefs.set( "KEY.0.ECDH." + to + ".CAAU", caau.getBytes( UtilString.STR256 ) );
		} else if ('0' != caau.charAt( 1 )) {
			caau = "X" + caau.substring( 1 );
			prefs.set( "KEY.0.ECDH." + to + ".CAAU", caau.getBytes( UtilString.STR256 ) );
		} else if (('9' == caau.charAt( 0 )) && ('0' >= caau.charAt( 2 ))) {
			caau = "800" + caau.charAt( 3 );
			prefs.set( "KEY.0.ECDH." + to + ".CAAU", caau.getBytes( UtilString.STR256 ) );
		}
		byte[] aesKey = getAesKey_OLD( to, //
			caau.charAt( 3 ) // ('0' == caau.charAt( 3 )) ? caau.charAt( 1 ) : caau.charAt( 3 )
		); // , null );
		Codec.HeaderInfo hi = new Codec.HeaderInfo( hdVersion, (char) 0, 'A',
			prefs.get( "email_address", null ), caau, prefs.get( "KEY.0.ECDH." + to + ".P" + caau.charAt( 1 ), null ) );
		enc = Codec.instance.pack( dat, 0, dat.length, aesKey, null, hi, prefs.get( "KEY.0.SIG.ECDSA256.S", new byte[ 0 ] ) );
		if (null == enc) {
			return null;
		}
	} catch (Exception e) {
		Dib2Config.log( "encrypt sign", "error " + caau + " " + e + e.getMessage() );
		return null;
	}
	attachment a_out = new attachment();
	a_out.name = filename + ".asc";
	DataSource ds = new ByteArrayDataSource( enc, "application/octet-stream" );
	a_out.datahandler = new DataHandler( ds );
	return a_out;
}

@Override
public attachment encrypt_sign( attachment unenc, String to ) {
	String filename = unenc.name;
	byte[] enc;
	String caau = new String( prefs.get( "KEY.0.ECDH." + to + ".CAAU", new byte[ 0 ] ), UtilString.STR256 );
//	String hint = "xxxxx" + UtilString.hex4Bytes( prefs.get( "KEY.0.ECDH." + to + ".P" + caau.charAt( 3 ), null ), false );
	final int hdVersion = (prefs.get( "KEY." + to + ".CIPHER", new byte[ 1 ] )[ 0 ] == 'C')
		? Dib2Constants.FILE_STRUC_VERSION_CMPAT : Dib2Constants.FILE_STRUC_VERSION_MIN;
//	Dib2Config.log( "encrypt sign", to + ' ' + hdVersion + " caau " + caau + " " + hint.substring( hint.length() - 5 ) );
	if (4 != caau.length()) {
		return null;
	}
	if (60 > hdVersion) {
		return encrypt_sign_OLD( unenc, to );
	}
	try {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		unenc.datahandler.writeTo( os );
		unenc = null;
		byte[] dat = os.toByteArray();
		if ('0' >= caau.charAt( 0 )) { // && (null != prefs.get( "KEY." + to + ".ENC.AES", null ))) {
			///// Countdown done. Need a new key.
			if (createDhKey( to, (char) 0 )) {
				prefs.set( "KEY.0.ECDH." + to + ".CAAU", ("X" + caau.substring( 1 )).getBytes( UtilString.STR256 ) );
				prefs.remove( "KEY." + to + ".ENC.AES" );
//			Dib2Config.log( "encrypt sign", "new key " + hash.. );
			}
		}
		byte[] pkInfo = getPkInfo4Sending( to );
		if (null == pkInfo) {
			return null;
		}
		byte[] aesKey = getAesKey( to, true, pkInfo, pkInfo.length - 1 );
		Codec.HeaderInfo hi = new Codec.HeaderInfo( hdVersion, 'C', 'A',
			prefs.get( "email_address", null ), caau,
			pkInfo ); //prefs.get( "KEY.0.ECDH." + to + ".P" + caau.charAt( 1 ), null ) );
		enc = Codec.instance.pack( dat, 0, dat.length, aesKey, null, hi, prefs.get( "KEY.0.SIG.ECDSA256.S", new byte[ 0 ] ) );
		if (null == enc) {
			return null;
		}
	} catch (Exception e) {
		Dib2Config.log( "encrypt sign", "error " + caau + " " + e + e.getMessage() );
		return null;
	}
	attachment a_out = new attachment();
	a_out.name = filename + ".asc";
	DataSource ds = new ByteArrayDataSource( enc, "application/octet-stream" );
	a_out.datahandler = new DataHandler( ds );
	return a_out;
}

private byte[] decrypt_verify_OLD( byte[] dat ) {
	String addr = "";
	int caauInfo = 0;
	byte[] newKey = new byte[ 0 ];
	byte[] aesKey = null;
	int offs = 0;
	try {
		offs += UtilMisc.getPacketHeaderLen( dat, offs );
		if ('A' != dat[ offs + 3 ]) {
			Dib2Config.log( "decrypt_verify", "unexpected format " + offs );
			return null;
		}
		int offsVersion = offs + 2;
		int hdlen = 16 * (dat[ offs + 4 ] & 0xff);
		caauInfo = dat[ offs + 6 ] & 0x3f;
		for (int i0 = 16; i0 < hdlen; ++ i0) {
			if ((' ' > dat[ offs + i0 + 1 ]) || (':' == dat[ offs + i0 ])) {
				++ i0;
				addr = new String( Arrays.copyOfRange( dat, offs + 16, offs + i0 ) );
				offs = ((offs + i0) / 16) * 16;
				while (addr.endsWith( ":" )) {
					addr = addr.substring( 0, addr.length() - 1 );
				}
				addr = addr.trim();
				break;
			}
		}
		if ((16 >= hdlen) || (32 > offs) || (0 >= addr.length())) {
			Dib2Config.log( "decrypt_verify", "address not found/ " + hdlen );
			return null;
		}
		if (0 < addr.indexOf( ' ' )) {
			// New key arrived.
			newKey = UtilString.bytes4Hex( addr.substring( 1 + addr.indexOf( ' ' ) ) );
			addr = addr.substring( 0, addr.indexOf( ' ' ) );
		}
		addr = UtilString.string4HexUtf8( addr ); // new String( UtilMisc.fromHexString( addr ), "UTF-8" );
		if (((Dib2Constants.FILE_STRUC_VERSION_CMPAT / 10) <= dat[ offsVersion ])
			&& ((Dib2Constants.FILE_STRUC_VERSION / 10) >= dat[ offsVersion ])) {
			// Switch to new format:
			prefs.set( "KEY." + addr + ".CIPHER", new byte[] { 'C' } );
			prefs.set( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) );
		}
		{
			String caau = new String( prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) ) );
			String hint = "xxxxx" + UtilString.hex4Bytes( prefs.get( "KEY.0.ECDH." + addr + ".P" + (caauInfo & 3) // caau.charAt( 3 )
			, new byte[ 0 ] ), false );
			Dib2Config
				.log(
					"decrypt_verify",
					addr + " " + caauInfo + " " + dat.length + " " + newKey.length + " caau " + caau + " "
						+ hint.substring( hint.length() - 5 ) );
		}
		if ((3 << 4) <= caauInfo) {
			// Bad format.
			newKey = new byte[ 0 ];
		} else if ((1 << 4) > caauInfo) {
			newKey = new byte[ 0 ];
		} else if (0 == (caauInfo & 3)) {
			Dib2Config.log( "decrypt_verify", "sync " + caauInfo );
			aesKey = getAesKey_OLD( addr, '0' // (char) ('0' + (caauInfo & 3))
			);
		}
	} catch (Exception e) {
		Dib2Config.log( "decrypt_verify", "error " + e + e.getMessage() );
		return null;
	}
	try {
		if (null == aesKey) {
			aesKey = getAesKey_OLD( addr, (char) ('0' + (caauInfo & 3)) ); // , null );
		}
		byte[] sigKey = prefs.get( "KEY." + addr + ".SIG.ECDSA256.P", null );
		dat = CsvCodecAes.instance.decode( dat, 0, dat.length, aesKey, sigKey );
		if (null == dat) {
			//if (16 < key.length) ...;
			Dib2Config.log( "decrypt_verify", "error regarding signature?" );
			prefs.set( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) );
			prefs.remove( "KEY." + addr + ".CIPHER" );
			return null;
		}
		dat = CsvCodecAes.instance.decompress( dat, dat.length );
		///// After having checked the signature.
		String caau = new String( prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) ) );
		int included = (caauInfo >>> 4) & 3;
		int acknowledged = (caauInfo >>> 2) & 3;
		int used = caauInfo & 3;
		if ((16 < newKey.length) && (0 != included)) {
			prefs.set( "KEY." + addr + ".ENC.ECDH.P" + ((caauInfo >>> 4) & 3), newKey );
			prefs.remove( "KEY." + addr + ".ENC.AES.K" + ((caauInfo >>> 4) & 3) );
		} else {
			included = 0;
		}
		// To be acknowledged.
		caau = caau.substring( 0, 2 ) + included + caau.charAt( 3 ); // ((caauInfo >>> 2) & 3) );
		if (included == used) {
			caau = "0000";
		} else if (acknowledged == used) {
			caau = ((0 == used) ? "00" : "90") + caau.charAt( 2 ) + used;
		} else if ((included == acknowledged) && (0 != acknowledged) && (acknowledged == (caau.charAt( 1 ) & 3))) {
			caau = ("90" + acknowledged) + acknowledged;
		} else if ((0 == used) && (included == acknowledged)) {
			caau = ("90" + included) + included;
		} else if (('8' >= caau.charAt( 0 )) && ('1' <= caau.charAt( 0 )) && (used == (caau.charAt( 3 ) & 3))) {
			// Countdown:
			caau = "" + ((char) (caau.charAt( 0 ) - 1)) + caau.substring( 1 ); // + caau.charAt( 2 ) + used;
//		} else if (used != caau.charAt( 3 )) {
//			caau = "000" + used;
		}
		prefs.set( "KEY.0.ECDH." + addr + ".CAAU", caau.getBytes( UtilString.STR256 ) );
	} catch (Exception e) {
		Dib2Config.log( "decrypt_verify", "(key sync?) error " + e + e.getMessage() );
		String caau = new String( prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) ) );
		byte[] pk = prefs.get( "KEY." + addr + ".SIG.ECDSA256.P", null );
		///// Verification might have succeeded => let the info pass a single time.
		if (('0' == caau.charAt( 3 )) || (null == pk) || (5 >= pk.length)) {
			return null;
		}
		dat = "(KEY SYNC ERROR)".getBytes( UtilString.STR256 );
		// Fallback/ Try to revert.
		prefs.set( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) ); //((caauInfo & 3) == 1) ? "0120" : "0210" );
		prefs.remove( "KEY." + addr + ".CIPHER" );
	}
	return dat;
}

@Override
public attachment decrypt_verify( attachment att ) {
	QResult addrPk = QResult.get8Pool();
	String filename = att.name;
	byte[] dat;
	try {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		att.datahandler.writeTo( os );
		att = null;
		dat = os.toByteArray();
		long offsPk0 = Codec.instance.unpackAddrPk( addrPk, dat, CsvCodecAes.instance );
		if (0 >= offsPk0) {
			if (0 > offsPk0) {
				return null;
			}
			dat = decrypt_verify_OLD( dat );
			if (null == dat) {
				return null;
			}
			attachment a_out = new attachment();
			a_out.name = filename + ".asc";
			DataSource ds = new ByteArrayDataSource( dat, "application/octet-stream" );
			a_out.datahandler = new DataHandler( ds );
			return a_out;
		}
	} catch (Exception e) {
		Dib2Config.log( "decrypt_verify", "error " + e + e.getMessage() );
		return null;
	}
	String addr = (String) addrPk.object0;
	byte[] newKey = (byte[]) addrPk.object1;
	int offsPkTag = (int) addrPk.long0;
	if (1 < addr.length()) {
		prefs.set( "KEY." + addr + ".CIPHER", new byte[] { 'C' } );
	}
	try {
		byte[] aesKey = getAesKey( addr, false, dat, offsPkTag );
		if (null == aesKey) {
			dat = null;
		} else {
			byte[] sigKey = prefs.get( "KEY." + addr + ".SIG.ECDSA256.P", null );
			dat = CsvCodecAes.instance.decode( dat, 0, dat.length, aesKey, sigKey );
		}
		if (null == dat) {
			//if (16 < key.length) ...;
			Dib2Config.log( "decrypt_verify", "error regarding signature?" );
//			prefs.set( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) );
//			prefs.remove( "KEY." + addr + ".CIPHER" );
//			return null;
		} else {
			dat = CsvCodecAes.instance.decompress( dat, dat.length );
			///// After having checked the signature ...!
//		    String caau = new String( prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) ) );
			if (16 < newKey.length) {
				setNewKey( addr, newKey, null );
			}
		}
	} catch (Exception e) {
		Dib2Config.log( "decrypt_verify", "(key sync?) error " + e + e.getMessage() );
//		String caau = new String( prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) ) );
		byte[] pk = prefs.get( "KEY." + addr + ".SIG.ECDSA256.P", null );
		///// Verification might have succeeded => let the info pass a single time.
		if ((null == pk) || (5 >= pk.length)) {
			return null;
		}
		dat = null;
	}
	if (null == dat) {
		dat = "(ERROR - KEY SYNC?)\nRe-sync by sending message or invitation.".getBytes( UtilString.STR256 );
		// Fallback/ Force new key on our side.
		prefs.set( "KEY.0.ECDH." + addr + ".CAAU", "0000".getBytes( UtilString.STR256 ) );
		prefs.set( "KEY." + addr + ".CIPHER", new byte[] { 'C' } );
		if (errorMsg) {
			return null;
		}
		errorMsg = true;
	}
	attachment a_out = new attachment();
	a_out.name = filename + ".asc";
	DataSource ds = new ByteArrayDataSource( dat, "application/octet-stream" );
	a_out.datahandler = new DataHandler( ds );
	return a_out;
}

@Override
public void public_keyring_remove_by_address( String addr ) {
	if (addr.equalsIgnoreCase( new String( prefs.get( "email_address", null ), UtilString.STRX16U8 ) ) //.equalsIgnoreCase( addr )
		|| (1 >= addr.length())) {
		// Must not remove myself.
		return;
	}
//	prefs.preference_set( "KEY." + addr + ".SIG.ECDSA256.P", null );
	prefs.remove( "KEY." + addr + ".SIG.ECDSA256.P" );
	prefs.remove( "KEY." + addr + ".CIPHER" );
	prefs.remove( "KEY." + addr + ".ENC.ECDH.P1" );
	prefs.remove( "KEY." + addr + ".ENC.ECDH.P2" );
}
}
