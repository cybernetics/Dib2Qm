// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.join;

import java.io.File;

public interface PlatformIf {
//=====

///// On UI thread

//String init( char platform, Object... parameters );

void invalidate();

int getTextRow( int xYReal, int xRange );

int getTextColumn( int xXReal, String xText );

void setTouched4TextPos();

String[] getLicense( String pre );

boolean pushClipboard( String label, String text );

String getClipboard();

///// On worker thread

File getFilesDir( String... parameters );

void log( String... aMsg );

void toast( String msg );

//=====
}
