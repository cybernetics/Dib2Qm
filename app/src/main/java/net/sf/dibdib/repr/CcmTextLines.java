// Copyright (C) 2018,2019 Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.repr;

import net.sf.dibdib.struc.QBaton;

//TODO
public final class CcmTextLines extends QBaton {
//=====

public String[] lines = { "", "Welcome.", "", "ABSOLUTELY NO WARRANTY.", "See license." };

public int count = lines.length;
public int textLineInx = 0; // 2
public int textLineCharInx = -1;

public void posHome() {
	textLineInx = 0;
	textLineCharInx = -1;
}

//=====
}
