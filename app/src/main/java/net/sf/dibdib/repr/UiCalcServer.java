// Copyright (C) 2018,2019 Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.repr;

import java.io.File;
import java.util.Arrays;
import net.sf.dibdib.config.*;
import net.sf.dibdib.disp.*;
import net.sf.dibdib.func.*;
import net.sf.dibdib.store.*;
import net.sf.dibdib.struc.*;

import static net.sf.dibdib.func.QCalc.*;
import static net.sf.dibdib.func.UtilString.*;
import static net.sf.dibdib.store.UiSwitches.*;

/** For platform's UI thread, singleton gateway. */
public enum UiCalcServer {
//=====

INSTANCE;

///// Threaded (in/ out/ trigger)

public static volatile boolean wxExitRequest = false;

public QDispatcher qGateIn;
public QDispatcher qGateOut;

/** For non-UI thread. */
public void invalidate() {
	Dib2Config.platform.invalidate();
}

/////

public final AboutVw qAboutVw = new AboutVw();
public final CcmCalcVw qCalcVw = new CcmCalcVw();
public final CcmListVw qListVw = new CcmListVw();

public CcmTemplates qTemplates = CcmTemplates.Dib2UiP_Templates_Default();

/** Latest values, continuously changing according to progress. */
public CcmTextLines qPageLinesFeed = new CcmTextLines();

/** Stable values, used for cursor etc. */
public CcmTextLines qPageLinesStable = new CcmTextLines();

public int qShiftScale = PIXR_SHIFT_DEFAULT;
public int qShiftView = PIXR_SHIFT_DEFAULT;
public final int WIN_X_MARGIN = 8;

/** Lines instead of real pixels */
public int qPYLineOffs;

public int qWinWidthPixL;
public int qWinHeightPixL;

///// Size in real pixels

public int qWinXPixR;
public int qWinYPixR;
public int qPixRDpiX;
public int qPixRDpiY;
public int qWinY1TopBarsPixR;
public int qWinEYDialogBarPixR;
/** Last three, real pixels */
public int[] qWinTouchedX3PixR = new int[ 3 ];
/** Last three, real pixels */
public int[] qWinTouchedY3PixR = new int[ 3 ];
public int qWinOffsetXPixR = -WIN_X_MARGIN;

/////

public void init( QDispatcher xmGateIn, QDispatcher xmGateOut ) {
//	kPlatformUi = xmCallback;
	qGateIn = xmGateIn;
	qGateOut = xmGateOut;
	qSwitches = SW_DEFAULT.clone();
//	if (0 >= CsvDb.instance.stackSize() && QMap.main.isFresh() && (null == CsvCodec0.getPassFull())) {
	if (!CsvDb.instance.isInitialized()) {
//		CcmRunner.initLoad( true );
		///// Prepare setup (loading data or intro).
		UiSwitches.pathInitDataFile = CcmRunner.check4Load();
		///// Try with dummy phrase -- maybe good enough for simple calculator etc.
		if (Codec.instance.setDummyPhrase( false )) {
			if (null == UiSwitches.pathInitDataFile) {
				CcmRunner.INSTANCE.addSamples();
				qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 600;
				// Mark as initialized:
				CsvDb.instance.load( null );
//			} else if (CsvCodec0.checkAccessCode( null )) {
//				qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 1;
//			} else {
//				qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 100;
			}
		} else {
			// Access code required:
			qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 200;
		}
	} else { // if (1000 <= qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
		qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 999;
	}

}

public static String findLatest( File dir ) {
	File file = null;
	String[] names = dir.list();
	if (null != names) {
		String name = null;
		long time = -1;
		for (String x0 : names) {
			if (x0.matches( Dib2Config.dbFileName.replace( ".", ".*" ) )
				|| (x0.startsWith( Dib2Config.dbFileName )
				&& (x0.endsWith( ".bak" ) || x0.endsWith( ".old" )))) {
				long x1 = new File( dir, x0 ).lastModified();
				if (time < x1) {
					time = x1;
					name = x0;
				}
			}
		}
		if (0 < time) {
			file = new File( dir, name );
		}
	}
	return (null == file) ? null : file.getAbsolutePath();
}

private QAbstractViewIf[] findView_map = null;

public QAbstractViewIf findView() {
	int swVal = UiSwitches.qSwitches[ UiSwitches.SWI_VIEW ];
	if (1000 > qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
		swVal = SW_VAL_VIEW_Intro;
	} else if (wxExitRequest) {
		return qAboutVw.qLicenseVw;
	}
	if (null == findView_map) {
		findView_map = new QAbstractViewIf[ UiSwitches.SW_VAL_VIEWs ];
		findView_map[ UiSwitches.SW_VAL_VIEW_Help ] = qAboutVw.qCcmHelpVw;
		findView_map[ UiSwitches.SW_VAL_VIEW_Intro ] = qAboutVw.qCcmInitialVw;
		findView_map[ UiSwitches.SW_VAL_VIEW_License ] = qAboutVw.qLicenseVw;
		findView_map[ UiSwitches.SW_VAL_VIEW_List ] = qListVw;
		findView_map[ UiSwitches.SW_VAL_VIEW_Standard ] = qCalcVw;
	}
	return findView_map[ swVal ];
}

public boolean consolidateTextLines() {
//	boolean changed = false;
	int len = qPageLinesFeed.lines.length - 1;
	for (; len >= 0; -- len) {
		if (null == qPageLinesFeed.lines[ len ]) {
			continue;
		}
		if ((len < qPageLinesFeed.count) && (0 < qPageLinesFeed.lines[ len ].trim().length())) {
			break;
		}
		qPageLinesFeed.lines[ len ] = null;
	}
	++ len;
	qPageLinesFeed.count = len;
	if (qPageLinesFeed.lines.length >= (10 + len * 4)) {
		qPageLinesFeed.lines = Arrays.copyOf( qPageLinesFeed.lines, 10 + 2 * len );
	}
	if (qPageLinesFeed.textLineInx >= len) {
		qPageLinesFeed.textLineInx = len;
		qPageLinesFeed.textLineCharInx = (0 > len) ? -1 : 0;
	} else if (qPageLinesFeed.textLineCharInx > qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].length()) {
		qPageLinesFeed.textLineCharInx = qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].length();
	}
	if (qPageLinesStable.lines.length < len) {
		qPageLinesStable.lines = new String[ len * 2 ];
	} else if (qPageLinesStable.lines.length >= (10 + len * 4)) {
		qPageLinesStable.lines = Arrays.copyOf( qPageLinesStable.lines, 10 + 2 * len );
	}
	qPageLinesStable.count = qPageLinesFeed.count;
	for (int i0 = qPageLinesStable.lines.length - 1; i0 >= 0; -- i0) {
//		String old = qPageLinesStable.lines[ i0 ];
		qPageLinesStable.lines[ i0 ] = (len > i0) ? qPageLinesFeed.lines[ i0 ] : null;
//		changed = changed || ((null == old) ? (null != qxPageLines.lines[ i0 ])
//			: !old.equals( qPageLinesStable.lines[ i0 ] ));
	}
	qPageLinesStable.setTimeStamp();
	if (qPageLinesStable.textLineInx >= len) {
		qPageLinesStable.textLineInx = len;
		qPageLinesStable.textLineCharInx = (0 > len) ? -1 : 0;
	} else if (qPageLinesStable.textLineCharInx > qPageLinesStable.lines[ qPageLinesStable.textLineInx ].length()) {
		qPageLinesStable.textLineCharInx = qPageLinesStable.lines[ qPageLinesStable.textLineInx ].length();
	}
	return true; // changed;
}

public void prepareTextLines( boolean is4Console ) {
	QAbstractViewIf vw = findView();
	//Log.d( "prepView", "a " + qSwitches[ Dib2Ui.SWI_ACCESS_MIU_OR_INTRO ] );
	if (1000 > qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
		qPageLinesFeed.count = vw.prepareTextLines( false, qBarDialog[ BARI_TEXTFIELD ] );
		qPageLinesFeed.lines = vw.getTextLines(); //((AboutVw.CcmIntroVw) vw).qLines;
		if (990 < qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
			switchView8Init( qSwitches, qBarTitle, qBarTool );
			vw = findView();
			qPageLinesFeed.lines[ 0 ] = qBarDialog[ BARI_TEXTFIELD ];
			qBarDialog[ BARI_TEXTFIELD ] = "";
			qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
		} else {
			return;
		}
	}
	qTemplates = CcmTemplates.Dib2UiP_Templates_Default(); // ...
	final String txt = (null == qBarDialog[ BARI_TEXTFIELD ]) ? null
		: UtilString.makePrintable( UtilString.string4Mnemonics( qBarDialog[ BARI_TEXTFIELD ] ) );
	int len = vw.prepareTextLines( is4Console, txt );
	qPageLinesFeed.lines = vw.getTextLines();
	qPageLinesFeed.count = len;
	if (null == qPageLinesFeed.lines) {
		qPageLinesFeed.lines[ 0 ] = "";
		if (0 < qBarDialog[ BARI_TEXTFIELD ].length()) {
			qPageLinesFeed.lines[ 0 ] = "   " + qBarDialog[ BARI_TEXTFIELD ].substring( 0, qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] )
				+ '|' + qBarDialog[ BARI_TEXTFIELD ].substring( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] );
		}
	}
}

private long handleEventInitial_lastAttempt = 0;

/**
 * @return 1 if done, 0 if partial, -1 if n/a
 */
private int handleEventIntro( String command, char key ) {
	if (1000 <= qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
		return -1;
	}
	final int curMin = (int) (UtilMisc.currentTimeMillisLinearized() / 1000 / 60);
	if (command.equals( UiSwitches.SW_NAME_LANGUAGE__LANG )) {
		qSwitches[ SWI_LANGUAGE ] = (qSwitches[ SWI_LANGUAGE ] + 1) % Dib2Local.kLanguages.length;
		return 1;
	} else if (610 > qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
		qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = 601;
	} else if (('0' <= key) && (key <= '9')) {
		qBarDialog[ BARI_TEXTFIELD ] += "" + key;
		return 1;
	}
	qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = qAboutVw.qCcmInitialVw.getNextStep( qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] );
	switch (key) {
		case '+':
		case '*':
		case '\u00b1':
			if (0 < qBarDialog[ BARI_TEXTFIELD ].length()) {
				CsvDb.instance.stackPush( new QVal( qBarDialog[ BARI_TEXTFIELD ], false ) );
				qBarDialog[ BARI_TEXTFIELD ] = "";
			}
			QCalc op = QCalc.getOperator( "" + key );
			if (null == op) {
				return 0;
			}
			int cArgs = op.cArgs;
			QVal[] vals = op.calc( CsvDb.instance.stackPop( cArgs, op ) );
			CcmRunner.INSTANCE.pushResult( vals, -1 );
			break;
		case CR:
			if (0 < qBarDialog[ BARI_TEXTFIELD ].length()) {
				CsvDb.instance.stackPush( new QVal( qBarDialog[ BARI_TEXTFIELD ], false ) );
				qBarDialog[ BARI_TEXTFIELD ] = "";
			}
			break;
		case SEND:
			break;
		case ESCAPE:
			CsvDb.instance.stackClear( false );
			if (600 <= qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
				qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = (990 > qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) ? 990 : curMin;
				CsvDb.instance.load( null );
			}
			break;
		default:
			return 1;
	}
	if (990 < qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
		qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = curMin;
		CsvDb.instance.load( null );
	}
	return 1; // (100 >= qSwitches[ SWI_INTRO_STEP ]);
}

/**
 * @return 1 if done, 0 if partial, -1 if n/a
 */
private int handleEventInitial( String command, char key ) {
	final int accessStatus = qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ];
	final byte[] pass = Codec.instance.getPassFull();
	final boolean proper = (null != pass) && Codec.instance.setHexPhrase( "" ); // (4 < pass.length);
	final boolean intro = (500 <= accessStatus); //(null != pass) && !proper;
	if (CsvDb.instance.isInitialized()) {
		if (intro) {
			return handleEventIntro( command, key );
		}
//		if (900 <= qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
//			return -1;
//		}
		if (200 <= qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
			return -1;
		}
		// Checking AC in the following steps.
	}
	if ((null != command) && (0 < command.length())) {
		return 0;
	}
	final int curMin = (int) (UtilMisc.currentTimeMillisLinearized() / 1000 / 60);
	if ((300 < accessStatus) && (0 < handleEventInitial_lastAttempt)) {
		if ((ESCAPE == key) && "0".equals( qBarDialog[ BARI_TEXTFIELD ] )) {
			// Give up on loading, as fallback.
			qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = curMin;
			CsvDb.instance.load( null );
			return 0;
		}
	}
	if ((SEND != key) && (DONE != key) && (CR != key)) {
		return -1;
	}
	int next4Fail = accessStatus + ((90 > (accessStatus % 100)) ? 5 : 0);

	///// Enforce some delay.
	int deltaMsec = 1 * 1000;
//	if (350 < accessStatus) {
//		deltaMsec = 10 * 3600 * 1000;
//	} else 
	if (50 <= (accessStatus % 100)) {
		deltaMsec = 60 * 1000;
	}
	if (handleEventInitial_lastAttempt + deltaMsec > UtilMisc.currentTimeMillisLinearized()) {
		return 1;
	}

	handleEventInitial_lastAttempt = UtilMisc.currentTimeMillisLinearized();
	if (200 > accessStatus) {
		if (CsvDb.instance.isInitialized()) {
			qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = next4Fail;
			if (proper //&& CsvDb.instance.checkAccessCode( null )) {
				&& (130 < next4Fail)) {
				///// Retry count kicks in.
				File filePw = new File( Dib2Config.platform.getFilesDir( "safe" ), Dib2Config.PASS_FILENAME );
				if (filePw.exists()) {
					filePw.delete();
				}
				return 1; // Until user exits ...
			} else if (Codec.instance.checkAccessCode( UtilString.bytesUtf8( qBarDialog[ BARI_TEXTFIELD ] ) )) {
				qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = curMin;
			}
		} else {
//			if (null != UiSwitches.pathInitDataFile) {
			qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = 200;
		}
		return 0;//1
	} else if (300 > accessStatus) {
		Codec.instance.setAccessCode( UtilString.bytesUtf8( qBarDialog[ BARI_TEXTFIELD ] ) );
		next4Fail += 100;
		File filePw = new File( Dib2Config.platform.getFilesDir( "safe" ), Dib2Config.PASS_FILENAME );
		if (!filePw.exists()) {
		} else if (230 <= accessStatus) {
			filePw.delete();
		} else {
			Codec.instance.setHexPhrase( null );
		}
	} else {
		next4Fail -= 100;
		Codec.instance.setHexPhrase( UtilString.hex4Bytes( UtilString.bytesUtf8( qBarDialog[ BARI_TEXTFIELD ] ), false ) );
	}
	qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
	qBarDialog[ BARI_TEXTFIELD ] = "";
	qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = next4Fail;
	return 0;
}

private void handleEventDialogText( int xiField, int xiText ) {
	if (0 > handleEventInitial( "", (char) 0 )) {
//		qSwitches[ SWI_KEYPAD ] &= ~0x4;
		setBar4Layers(); //switchLayers( SW_LAYER_KEYPAD );
	}
}

private void handleEventCanvas( int xX, int xY ) {
	if (0 <= handleEventInitial( "", (char) 0 )) {
		return;
	}
//	wqRefreshNeeded = true;
	setBar4Layers(); //switchLayers( qSwitches, qBarTitle, qBarTool, SW_LAYER_CANVAS );
	qWinTouchedX3PixR[ 2 ] = qWinTouchedX3PixR[ 1 ];
	qWinTouchedX3PixR[ 1 ] = qWinTouchedX3PixR[ 0 ];
	qWinTouchedX3PixR[ 0 ] = xX;
	qWinTouchedY3PixR[ 2 ] = qWinTouchedY3PixR[ 1 ];
	qWinTouchedY3PixR[ 1 ] = qWinTouchedY3PixR[ 0 ];
	qWinTouchedY3PixR[ 0 ] = xY;
	qPageLinesFeed.textLineInx = Dib2Config.platform.getTextRow( xY, qPageLinesFeed.count );
	qPageLinesFeed.textLineCharInx = Dib2Config.platform.getTextColumn( qWinTouchedX3PixR[ 0 ],
		qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ] );
}

private CommandBaton evaluateEventKey( char key, long xbClickclickLong ) {

	CommandBaton commandData = new CommandBaton();
	// Request refresh at the end:
	commandData.operator = QCalc.DUMP;
	commandData.uiKeyOrButton = key;
	int done = handleEventInitial( "", key );
	if (0 <= done) {
		return (0 < done) ? null : commandData;
	}
	boolean calcMode = (1 == qSwitches[ UiSwitches.SWI_KEYPAD_INDEX ]);
	if ((UiSwitches.qPadKeys.length - 1) <= qSwitches[ UiSwitches.SWI_KEYPAD_INDEX ]) {
		qSwitches[ UiSwitches.SWI_KEYPAD_INDEX ] = UiSwitches.keys_UniBlock_FromPad;
	}
	commandData.operator = QCalc.NOP;
	String cmd = "" + key;
	if ((CR == key) && (UiSwitches.SW_LAYER_CANVAS == (0xf & qSwitches[ UiSwitches.SWI_LAYERS ]))) {
		qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
		qBarDialog[ BARI_TEXTFIELD ] = "";
		if (0 <= qPageLinesFeed.textLineCharInx) {
			qPageLinesFeed.lines[ qPageLinesFeed.textLineInx + 1 ] = qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ]
				.substring( qPageLinesFeed.textLineCharInx )
				+ qPageLinesFeed.lines[ qPageLinesFeed.textLineInx + 1 ];
			qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ] = qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].substring( 0,
				qPageLinesFeed.textLineCharInx );
			qPageLinesFeed.textLineCharInx = 0;
			++ qPageLinesFeed.textLineInx;
			Dib2Config.platform.setTouched4TextPos();
		}
		return commandData;
	} else if (XPASTE == key) {
		cmd = Dib2Config.platform.getClipboard();
		cmd = (null == cmd) ? "" : cmd;
		if ((0 >= qBarDialog[ BARI_TEXTFIELD ].length()) && (0 < cmd.length())) {
//			commandData.operator = QCalc.NOP;
			commandData.uiParameter = cmd;
			return commandData;
		}
	} else if (' ' > key) {
		String txt = qBarDialog[ BARI_TEXTFIELD ];
		if ((null == txt) || (CR != key)) {
			return null;
		} else if ((UiSwitches.SW_VAL_VIEW_Standard == qSwitches[ UiSwitches.SWI_VIEW ])
			&& (1 == qSwitches[ UiSwitches.SWI_KEYPAD_INDEX ])
			&& (1 < txt.length()) && (' ' == txt.charAt( 0 ))) {
			// Allow entry of symbols as text via leading ' '.
			qBarDialog[ BARI_TEXTFIELD ] = txt.substring( 1 );
			return null;
		} else if (txt.startsWith( "{" ) && !txt.endsWith( "}" )) {
			// Unfinished quote.
			cmd = "\t";
		} else if (txt.startsWith( "[" ) && !txt.endsWith( "]" )) {
			// Unfinished list.
			cmd = "\t";
		} else {
			return null;
		}
	}
	if ((UiSwitches.SW_VAL_VIEW_Standard == qSwitches[ UiSwitches.SWI_VIEW ])
		&& calcMode && (0 >= qBarDialog[ BARI_TEXTFIELD ].length())) {
		///// Entry of symbols: as operators.
		if (('A' <= key) && (key <= 'F')) {
			///// For calculator.
			switch (key) {
				case 'A':
					cmd = "XOR";
					break;
				case 'B':
					cmd = "SQRT";
					break;
				case 'C':
					cmd = "CLEAR";
					break;
				case 'D':
					cmd = "RADD";
					break;
				case 'E':
					cmd = "E";
					break;
				case 'F':
					cmd = "PI";
					break;
				default:
					;
			}
			key = 0x80;
		}
		if ((0 <= "+-*/%_~!&|^<>=".indexOf( key )) || (0x80 <= key)) {
			commandData.operator = QCalc.getOperator( (0 <= "<>".indexOf( key )) ? ("" + key + key) : cmd );
			if ((null != commandData.operator) && (NOP != commandData.operator)) {
				CsvDb.instance.variable_force( "T", new QVal( commandData.operator.name(), true ) );
				commandData.uiParameter = qBarDialog[ BARI_TEXTFIELD ];
				qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
				qBarDialog[ BARI_TEXTFIELD ] = "";
				return commandData;
			}
		}
		cmd = ('#' == key) ? "0x" : cmd;
	} else if (0 != (2 & xbClickclickLong)) {
		cmd += "" + key + key + key + key;
		cmd += cmd;
	}
	qBarDialog[ BARI_TEXTFIELD ] = qBarDialog[ BARI_TEXTFIELD ].substring( 0, qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] )
		+ cmd
		+ qBarDialog[ BARI_TEXTFIELD ].substring( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] );
	qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] += cmd.length();
	if ((UiSwitches.SW_LAYER_CANVAS == (0xf & qSwitches[ UiSwitches.SWI_LAYERS ])) && (0 <= qPageLinesFeed.textLineCharInx)) {
		qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ] = qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].substring( 0,
			qPageLinesFeed.textLineCharInx )
			+ cmd
			+ qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].substring( qPageLinesFeed.textLineCharInx );
		qPageLinesFeed.textLineCharInx += cmd.length();
		Dib2Config.platform.setTouched4TextPos();
	}
	return commandData;
}

private CommandBaton evaluateEventTools( String tool, char key, long xbClickclickLong ) {
	int done = handleEventInitial( tool, key );
	CommandBaton commandData = new CommandBaton();
	commandData.operator = QCalc.NOP;
	if (0 <= done) {
		return (0 < done) ? null : commandData;
	}
//	qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
//	qBarDialog[ BARI_TEXTFIELD ] = "";
	if ((null == tool) || (0 >= tool.length())) {
		commandData.operator = QCalc.getOperator( "" + key );
		if (' ' > key) {
			return evaluateEventKey( key, xbClickclickLong );
		} else {
			switch (key) {
				case '=':
					return null; // menu
				case 'X':
				case 'A': {
					switchBackOrCircle( xbClickclickLong, SWI_LAYERS, true );
					commandData.operator = QCalc.NOP;
//					setBar4Layers(); //switchLayers( qSwitches, qBarTitle, qBarTool );
					break;
				}
				default:
					return null;
			}
		}
		return commandData;
	}
	commandData.uiParameter = qBarDialog[ BARI_TEXTFIELD ];
	qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
	qBarDialog[ BARI_TEXTFIELD ] = "";
	commandData.operator = QCalc.NOP;
	key = 0;
//	boolean doDraw = true;
	if (tool.startsWith( "LA" )) {
		if (0 != (2 & xbClickclickLong)) {
			// TODO
			// switch keyboard
		} // else ...
		switchBackOrCircle( xbClickclickLong, SWI_LANGUAGE, true );
		setToolBar( qBarTool, "LA" + Dib2Local.kLanguages[ qSwitches[ SWI_LANGUAGE ] ], 0 );
//		qDoDraw = true;
//		return commandData;
	} else if (tool.startsWith( "LY" )) {
		final int level = 0xf & tool.charAt( 2 );
//		shiftLayer( qSwitches, qBarTitle, qBarTool, level );
		switchLayers( level == 1 );
//		qBarTitle[ 2 * 4 ] = "";
	} else if (tool.startsWith( "VW" )) {
		switchBackOrCircle( xbClickclickLong, SWI_VIEW, true );
		setToolBar( qBarTool, UiSwitches.SW_NAMES_VIEW[ qSwitches[ SWI_VIEW ] ], 0 );
		qPageLinesFeed.posHome();
		qPYLineOffs = 0;
	} else {
//		doDraw = false;
		commandData.operator = QCalc.getOperator( tool.trim() );
	}
//	if (doDraw) {
//		wqRefreshNeeded = true;
//		Dib2Config.qPlatform.invalidate();
	return commandData;
}

private String handleUiEvent__getPar( String par ) {
	QVal[] args;
	if ((null == par) || (0 >= par.length())) {
		args = CsvDb.instance.stackPop( 1, null );
		if (null != args) {
			par = args[ 0 ].toString();
		}
	}
	if ((null == par) || (0 >= par.length())) {
		return null;
	}
	return par;
}

public QCalc handleUiEvent( QCalc op, String parameter ) {
	if (null == op) {
		return null;
	}
	String par = parameter;

	///// UI related functions first - concurrent setting of switches ...!
	switch (op) {
		case ABOUT:
			UiSwitches.qSwitches[ UiSwitches.SWI_VIEW ] = UiSwitches.SW_VAL_VIEW_License;
			return null;
		case EXEC:
			// TODO: for [...] take whole stack
			if (null == (par = handleUiEvent__getPar( par ))) {
				return null;
			}
			QCalc opx = QCalc.getOperator( par );
			return handleUiEvent( opx, null );
		case EXIT:
			UiCalcServer.wxExitRequest = true; //...
//			feedProgress( new String[] { "Program is about to stop ... (Please press ENTER)." } );
			Thread.yield();
			return null;
		case HELP:
			UiSwitches.qSwitches[ UiSwitches.SWI_VIEW ] = UiSwitches.SW_VAL_VIEW_Help;
			return null;
		case PW:
		case PWAC:
			if (null == (par = handleUiEvent__getPar( par ))) {
				return null;
			}
			if (PW == op) {
				Codec.instance.setHexPhrase( UtilString.hexUtf8( par, false ) );
			} else {
				Codec.instance.setAccessCode( par.getBytes( UtilString.STRX16U8 ) );
			}
			return null;
		case QFILTER: {
			int flags = UiSwitches.qSwitches[ UiSwitches.SWI_MAPPING_CATS ] << 1;
			if ((Mapping.Cats.OTHERS.flag > flags) || (Mapping.Cats._MAX.flag <= flags)) {
				flags = (int) Mapping.Cats.OTHERS.flag;
			}
			UiSwitches.qSwitches[ UiSwitches.SWI_MAPPING_CATS ] = flags;
			UiSwitches.qSwitches[ UiSwitches.SWI_VIEW ] = UiSwitches.SW_VAL_VIEW_List;
		}
			return null;
		case UICOD:
			if (null == (par = handleUiEvent__getPar( par ))) {
				return null;
			}
			UiSwitches.setUnicodeBlockOffset( par );
			return null;
		case VWCAT:
			if (null == (par = handleUiEvent__getPar( par ))) {
				return null;
			}
			if ('.' < par.charAt( 0 )) {
				int flags = 0;
				try {
					flags = (int) Mapping.Cats.valueOf( par ).flag;
				} catch (Exception e) {
				}
				if (0 == flags) {
					par = UtilString.toUpperCase( par );
					try {
						flags = (int) Mapping.Cats.valueOf( par ).flag;
					} catch (Exception e) {
					}
				}
				UiSwitches.qSwitches[ UiSwitches.SWI_MAPPING_CATS ] = flags;
				UiSwitches.qSwitches[ UiSwitches.SWI_VIEW ] = UiSwitches.SW_VAL_VIEW_List;
			}
			return null;
		default:
	}
	return op;
}

private CommandBaton handleUiEventKey( char key ) {

	CommandBaton commandData = new CommandBaton();
	commandData.operator = null;
	commandData.uiKeyOrButton = key;
	switch (key) {
		case ALT:
		case SHIFT:
			if ((UiSwitches.qPadKeys.length - 1) <= qSwitches[ UiSwitches.SWI_KEYPAD_INDEX ]) {
				if ((UiSwitches.keys_UniBlock_Current <= UiSwitches.keys_UniBlock_Offset) && (ALT == key)) {
					qSwitches[ UiSwitches.SWI_KEYPAD_INDEX ] = UiSwitches.keys_UniBlock_FromPad;
					break;
				}
				UiSwitches.setUnicodeBlock( -1, (ALT == key) ? -1 : 1 );
				break;
			}
			switchBackOrCircle( 0, SWI_KEYPAD_INDEX, (key != ALT) );
			break;
		case SEND:
			commandData.operator = QCalc.getOperator( qBarDialog[ BARI_TEXTFIELD ] );
			if (null != commandData.operator) {
				qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
				qBarDialog[ BARI_TEXTFIELD ] = "";
				break;
			}
			// Fall through.
		case CR:
		case DONE:
		case XCOPY:
		case XCUT:
			commandData.operator = QCalc.NOP; //getOperator( "" + ((DONE != key) ? CR : key) );
			String txt = qBarDialog[ BARI_TEXTFIELD ];
			if (null != txt) {
				// No list processing yet
				txt = UtilString.makePrintable( txt );
			}
			if ((null != txt) && (1 < txt.length())
				&& (('@' == txt.charAt( 0 )) || (':' == txt.charAt( 0 )))) {
				commandData.operator = ('@' == txt.charAt( 0 )) ? LOAD : STORE;
				txt = txt.substring( 1 );
			} else if (null != txt) {
				txt = UtilString.string4Mnemonics( txt );
			}
			commandData.uiParameter = txt;
			final boolean fill = 0 >= txt.length();
			if (fill && (DONE != key)) {
				QVal arg = CsvDb.instance.stackPeek();
				if (null != arg) {
					qBarDialog[ BARI_TEXTFIELD ] = arg.toString();
					if (XCUT == key) {
						commandData.operator = QCalc.CLEAR;
					}
				}
			} else if (XCOPY != key) {
				qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
				qBarDialog[ BARI_TEXTFIELD ] = "";
			}
			if ((XCUT == key) || (XCOPY == key)) {
				Dib2Config.platform.pushClipboard( "edit",
					fill ? qBarDialog[ BARI_TEXTFIELD ] : commandData.uiParameter );
				commandData.uiParameter = null;
			} else {
//				if (commandData.uiParameter.startsWith( "[" )) {
//					return null;
				if ((SEND == key) && fill) {
					Dib2Config.platform.pushClipboard( "edit", qBarDialog[ BARI_TEXTFIELD ] );
					qBarDialog[ BARI_TEXTFIELD ] = "";
					commandData.operator = QCalc.DUP;
				}
			}
			break;
		case PSHIFT:
			char sel = (0 >= qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ]) ? ' '
				: qBarDialog[ BARI_TEXTFIELD ].charAt( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] - 1 );
//			if (0 < qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ]) {
			UiSwitches.setUnicodeSelection( sel );
//				qBarDialog[ BARI_TEXTFIELD ] = qBarDialog[ BARI_TEXTFIELD ].substring( 0, qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] - 1 )
//					+ Rfc1345.next4Rfc( qBarDialog[ BARI_TEXTFIELD ].charAt( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] - 1 ) )
//					+ qBarDialog[ BARI_TEXTFIELD ].substring( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] );
			// Fall through
		case BACKSP:
			if (0 < qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ]) {
				-- qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ];
				qBarDialog[ BARI_TEXTFIELD ] = qBarDialog[ BARI_TEXTFIELD ].substring( 0, qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] )
					+ qBarDialog[ BARI_TEXTFIELD ].substring( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] + 1 );
			}
			break;
		case ESCAPE:
			qWinOffsetXPixR = -WIN_X_MARGIN;
			qPYLineOffs = 0;
			qShiftScale = qShiftView; //PIXR_SHIFT_DEFAULT;
			break;
		case MOVE_LEFT:
			qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] -= (0 < qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ]) ? 1 : 0;
			if (0 != (UiSwitches.SW_LAYER_CANVAS & qSwitches[ SWI_LAYERS ])) {
				qPageLinesFeed.textLineCharInx -= (0 < qPageLinesFeed.textLineCharInx) ? 1 : 0;
				Dib2Config.platform.setTouched4TextPos();
			}
			break;
		case MOVE_RIGHT:
			qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] += (qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] < qBarDialog[ BARI_TEXTFIELD ].length()) ? 1
				: 0;
			if (0 != (UiSwitches.SW_LAYER_CANVAS & qSwitches[ SWI_LAYERS ])) {
				if (0 <= qPageLinesFeed.textLineCharInx) {
					if (qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].length() <= qPageLinesFeed.textLineCharInx) {
						qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ] += ' ';
					}
					++ qPageLinesFeed.textLineCharInx;
					Dib2Config.platform.setTouched4TextPos();
				}
			}
			break;
		case SCROLL_DOWN:
			if (10 < qPageLinesFeed.lines.length) {
				qPYLineOffs = (qPYLineOffs >= (qPageLinesFeed.lines.length - 12 - 5)) ? (qPageLinesFeed.lines.length - 5)
					: (qPYLineOffs + 12);
			}
			break;
		case SCROLL_LEFT:
			qWinOffsetXPixR = (0 >= qWinOffsetXPixR) ? -qWinXPixR / 2
				: (qWinOffsetXPixR - qWinXPixR / 2);
			if ((qWinOffsetXPixR > -2 * WIN_X_MARGIN) && (qWinOffsetXPixR < WIN_X_MARGIN)) {
				qWinOffsetXPixR = -WIN_X_MARGIN;
			}
			break;
		case SCROLL_RIGHT:
			qWinOffsetXPixR += qWinXPixR / 2;
			break;
		case SCROLL_UP:
			qPYLineOffs = (qPYLineOffs >= 12) ? (qPYLineOffs - 12) : 0;
			break;
		case ZOOM_IN:
			if (0 < qShiftScale) {
				qShiftScale -= (1 + qShiftScale / PIXL_SHIFT);
				qWinOffsetXPixR *= (WIN_X_MARGIN < qWinOffsetXPixR) ? 2 : 1;
			}
			break;
		case ZOOM_OUT:
			if (PIXL_SHIFT > qShiftScale) {
				++ qShiftScale;
				qWinOffsetXPixR /= (WIN_X_MARGIN < qWinOffsetXPixR) ? 2 : 1;
			}
			break;
		default:
			commandData.uiKeyOrButton = 0;
			;
	}
	return commandData;
}

/** Handle touch or click event. 
 * @param xXReal X in window.
 * @param xYReal Y in window.
 * @param xXSubOffset X window offset.
 * @param xYSubOffset Y window offset.
 * @param xbClickclickLong b0 = confirmed click (for selections), b1 = long click.
 * @return Matching key value or 0.
 */
public char touch( int xXReal, int xYReal, int xXSubOffset, int xYSubOffset, long xbClickclickLong ) {
	CommandBaton cmdBaton = null;
	char key = 0;
//	boolean doDraw = true;
	String sCommand = "" + CR;

	if (xYReal >= (qWinYPixR - qWinEYDialogBarPixR)) {
		if ( // (xYReal > (qWinYPixR - qWinEYDialogBarPixR / 2)) && 
		(xXReal > qWinXPixR / 2)) {
			key = SEND;
		} else {
			handleEventDialogText( 0, 0 );
//			qDoDraw = true;
			return 0;
		}
	} else if (xYReal <= qWinY1TopBarsPixR) {
		// Roughly, should match for WIN_X_MARGIN:
		int ix = (xXReal * 3 * BAR_MAX_PER_SIDE) / qWinXPixR;
		key = (char) 0;
		sCommand = "";
		if (xYReal <= qWinY1TopBarsPixR / 3) {
			String sKey;
			if ((ix < BAR_MAX_PER_SIDE) || (ix >= 2 * BAR_MAX_PER_SIDE)) {
				sKey = qBarTitle[ 2 * ix + 1 ];
			} else {
				return 0;
			}
			key = (0 < sKey.length()) ? sKey.charAt( 0 ) : (char) 0;
		} else if (xYReal <= 2 * qWinY1TopBarsPixR / 3) {
			ix = 2 * ix + 1;
			if (1 >= ix) {
				sCommand = UiSwitches.SW_NAME_LANGUAGE__LANG;
			} else if (ix < qBarTool.length) {
				sCommand = qBarTool[ ix ];
				if (sCommand.startsWith( "LY" )) {
					sCommand = (5 >= ix) ? "LY1" : "LY2";
				}
				key = DONE;
			} else {
				return 0;
			}
		} else if (2 * BAR_MAX_PER_SIDE <= ix) {
			key = SEND;
		} else {
			return 0;
		}
		cmdBaton = evaluateEventTools( sCommand, key, xbClickclickLong );
		if ((null == cmdBaton) || (null == cmdBaton.operator)) {
			cmdBaton = handleUiEventKey( key );
			if (null == cmdBaton) {
				return 0;
			}
		}
		key = 0;
		if (null != cmdBaton.operator) {
			String op = cmdBaton.operator.getOperatorOrName();
			if (null != op) {
				key = op.charAt( 0 );
			}
		}
	} else if ((UiSwitches.SW_LAYER_CANVAS == (0xf & qSwitches[ UiSwitches.SWI_LAYERS ])) //(0 == (SW_FLAG_KEYPAD_Active & qSwitches[ SWI_LAYERS ]))
		|| (2 == xbClickclickLong)) {
		handleEventCanvas( xXReal + xXSubOffset - 0, xYReal + xYSubOffset - qWinY1TopBarsPixR );
		return 0;
	} else {
		///// 7x7 fields with 1/8 padding.
		int row = (xYReal - qWinY1TopBarsPixR) * qcKeys4Win * 8
			/ (qWinYPixR - qWinY1TopBarsPixR - qWinEYDialogBarPixR);
		// Double margin for middle position, roughly:
		int col = (xXReal + WIN_X_MARGIN) * qcKeys4Win * 8 / qWinXPixR;
		int rem = row % 8;
		if (((0 == rem) || (qcKeys4Win == rem)) && (8 <= row) && (48 >= row)) {
			return 0;
		}
		rem = col % 8;
		if (((0 == rem) || (qcKeys4Win == rem)) && (8 <= col) && (48 >= col)) {
			return 0;
		}
		row /= 8;
		col /= 8;
		key = qPadKeys[ qSwitches[ SWI_KEYPAD_INDEX ] ][ row * qcKeys4Win + col ];
	}
	if (null == cmdBaton) {
		cmdBaton = evaluateEventKey( key, xbClickclickLong );
		if (null == cmdBaton) {
			cmdBaton = handleUiEventKey( key );
		}
	}
	if ((null != cmdBaton) && (null != cmdBaton.uiParameter) && (0 < cmdBaton.uiParameter.length()) && (0 < cmdBaton.uiKeyOrButton)) {
		CsvDb.instance.stackPush( new QVal( cmdBaton.uiParameter, false ) );
		cmdBaton.uiParameter = "";
	}
	if ((null == cmdBaton) || (null == cmdBaton.operator)) {
//		doDraw = false;
//	} else if (QCalc.NOP == cmdBaton.operator) {
//		return QCalc.DONE;
	} else { //if (0 < outCmd.uiCommand.length()) {
		cmdBaton.operator = handleUiEvent( cmdBaton.operator, cmdBaton.uiParameter );
		if ((null != cmdBaton.operator) && (NOP != cmdBaton.operator)) {
			qGateOut.take( cmdBaton );
		}
		// Provide feedback: text field/ other thread showing as busy:
		invalidate();
		// Possibly '0' for some platforms, depending on 'restart':
		return key;
	}
	return 0;
}

public static String getShortDay() {
	return UtilMisc.dateShort4Millis( UtilMisc.currentTimeMillisLinearized() ).substring( 0, 6 );
}

public void start() {
	QResult.getThreadIndex();
	UiCalcServer.wxExitRequest = false;
	UtilMisc.timeZoneDone = false;
	// TODO
	// qPadKeys = ...QWERTZ/ABC
	int minute = UiSwitches.qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ];
	if (1100 < minute) {
		if ((minute * 60L * 1000L + Dib2Constants.MAX_DELTA_ACCESS_CHECK) < UtilMisc.currentTimeMillisLinearized()) {
//			byte[] pass = CsvCodec0.getPassFull();
//			UiSwitches.pathInitDataFile = CcmRunner.check4Load();
//			if (null != UiSwitches.pathInitDataFile) {
			if (Codec.instance.setHexPhrase( "" )) { //((null != pass) && (4 < pass.length)) {
				UiSwitches.qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 110;
			}
		}
//	} else {
//		qAboutVw.qCcmInitialVw.setStep( qSwitches[ SWI_ACCESS_MIU_OR_INTRO ] );
	}
}

public boolean stop( boolean fast ) {
	boolean out = true;
	if (1000 <= UiSwitches.qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
		wxExitRequest = true;
		if (null == Codec.instance.getPassFull()) {
			Codec.instance.setDummyPhrase( true );
		}
		if (fast) {
			out = false;
			CcmRunner.wxSave = true;
		} else {
//			wxExitRequest = true;
			while (true) {
				Thread rx = CcmRunner.qActive;
				if (null == rx) {
					break;
				}
				rx.interrupt();
			}
			CcmRunner.saveAll( 0 );
		}
	}
//	kPlatformUi = null;
	QResult.drop8Pool();
	return out;
}

//=====
}
