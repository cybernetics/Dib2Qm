// Copyright (C) 2019 Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.repr;

public interface QAbstractViewIf {
//=====

public int prepareTextLines( boolean is4Console, String... param );

public String[] getTextLines();

//=====
}
