// Copyright (C) 2018,2019 Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.repr;

import net.sf.dibdib.func.UtilString;
import net.sf.dibdib.store.CsvDb;

public final class CcmCalcVw implements QAbstractViewIf {
//=====

String[] out = new String[ 100 ];

@Override
public String[] getTextLines() {
	return out;
}

@Override
public int prepareTextLines( boolean is4Console, String... param ) {
	long xbHexMemory = 3;
	int xOffset = 1;
	boolean partial = false;
	out = (null == out) ? new String[ 30 ] : out;
	int len = CsvDb.instance.stackRead( xbHexMemory, out, xOffset, partial );
	// Need some 10 lines for possible memory values.
	if ((0 <= len) && !partial && ((len + 10) >= out.length)) {
		len = -len / 2 - 10 / 2 - 2;
	}
	if (0 > len) {
		out = new String[ -len * 2 + 2 ];
		len = CsvDb.instance.stackRead( xbHexMemory, out, xOffset, true );
	}
	out[ 0 ] = ((null == param) || (0 >= param.length)) ? "" : ("\t" + param[ 0 ]);
	for (int i0 = out.length - 1; i0 >= len; -- i0) {
		out[ i0 ] = null;
	}
	for (int i0 = len - 1; i0 >= 0; -- i0) {
		out[ i0 ] = UtilString.makePrintable( out[ i0 ] );
	}
	return len;
}

//=====
}
