// Copyright (C) 2018,2019 Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.repr;

import java.util.Arrays;
import net.sf.dibdib.config.*;
import net.sf.dibdib.disp.CcmRunner;
import net.sf.dibdib.func.QCalc;
import net.sf.dibdib.store.*;

import static net.sf.dibdib.store.UiSwitches.qSwitches;

public final class AboutVw { //implements QAbstractViewIf {
//=====

public static final class CcmHelpVw implements QAbstractViewIf {

String[] out;

@Override
public String[] getTextLines() {
	return out;
}

private static String[] getHelp_lines = null;

public static String[] getHelp( boolean is4CmdLine ) {
	if (null == getHelp_lines) {
		int count = 0;
		String[] lines = new String[ 3 + QCalc.values().length + 1 ];
		lines[ 0 ] = "Version " + Dib2Constants.VERSION_STRING + ". " + Dib2Constants.NO_WARRANTY[ 0 ];
		lines[ 1 ] = "List of available FUNCTIONS (see below, e.g.:"
			+ (is4CmdLine ? " type '\\' + file name, press ENTER, ':EXPORT', ENTER):"
				: " ^FileName, ^ENTER, 'EXPORT', GO):");
		lines[ 2 ] = "(Not fully implemented yet !)";
		count = 3;
		for (QCalc funct : QCalc.values()) {
			if (0 <= funct.cArgs) {
				lines[ count ++ ] = funct.getDescription();
			}
		}
		lines[ count ++ ] = is4CmdLine
			? "(Use with preceding ':' or ',' for commands, '\\' for data."
			: "(E.g. type '3', press > or ENTER, '4', > or ENTER,";
		lines[ count ++ ] = is4CmdLine
			? "E.g.: press '\\', type file name, press ENTER, type ':EXPORT', press ENTER)"
			: "type 'ADD', (press > or ENTER,) press GO)";
		getHelp_lines = Arrays.copyOf( lines, count );
	}
	return getHelp_lines;
}

@Override
public int prepareTextLines( boolean is4Console, String... param ) {
	out = getHelp( is4Console );
	return out.length;
}
}

//=====

public static final class CcmInitialVw implements QAbstractViewIf {

public String[] lines = { "", "Welcome.", "", "ABSOLUTELY NO WARRANTY.", "See license." };
public int count = lines.length;

@Override
public String[] getTextLines() {
	return lines; // qLines.lines;
}

public int getNextStep( int step ) {
	if (500 > step) { //(null != UiSwitches.pathInitDataFile) {
		return (80 < (step % 100)) ? step : (step + 1);
	} else if (670 > step) {
		return (610 > step) ? 610 : (step + 10);
	}
	return (step < 990) ? 990 : 999;
}

private int prepare4Access() {
	int step = qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ];
	lines = (300 <= step) ? Dib2Local.kUiStepPw
		: ((20 > step) ? ((null != CcmRunner.qActive) ? Dib2Local.kUiStepAcLoad : Dib2Local.kUiStepAcStart)
			: ((200 > step) ? Dib2Local.kUiStepAcWait
				: ((250 > step) ? Dib2Local.kUiStepAc
					: Dib2Local.kUiStepAcWait)));
	count = lines.length;
	return count;
}

private static final String[][] prepare4Intro_list = new String[][] { null, //Dib2Local.kUiStep0
	Dib2Local.kUiStep610, Dib2Local.kUiStep620, Dib2Local.kUiStep630, Dib2Local.kUiStep640, Dib2Local.kUiStep650,
	Dib2Local.kUiStep660, Dib2Local.kUiStep670, null, null };

private int prepare4Intro() {
	count = 0;
	String[] txt = null;
	int step = qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ];
	if (610 > step) {
		txt = Dib2Local.kUiStep0;
		lines = txt;
		count = txt.length;
//		stepNext = (5 > step) ? 5 : 10;
		return txt.length;
	} else if (700 > step) {
		txt = prepare4Intro_list[ (step / 10) % 10 ];
	}
	txt = (null == txt) ? Dib2Local.kUiAgree : txt;
	int locLines = 1 + txt.length / Dib2Local.kLanguages.length;
	if (630 <= step) {
		if (lines.length < (5 + locLines)) {
			lines = new String[ 5 + locLines ];
		}
		count = CsvDb.instance.stackRead( 0, lines, 0, true );
		if (0 > count) {
			lines = new String[ -count ];
			count = CsvDb.instance.stackRead( 0, lines, 0, true );
		}
		if (lines.length < (count + locLines)) {
			lines = Arrays.copyOf( lines, count + locLines );
		}
	} else {
		lines = new String[ locLines ];
	}
	lines[ lines.length - 1 ] = "";
	for (int i0 = UiSwitches.qSwitches[ UiSwitches.SWI_LANGUAGE ]; i0 < txt.length; i0 += Dib2Local.kLanguages.length) {
		String line = txt[ i0 ];
		line = (!line.startsWith( "$" )) /*...*/? line : Dib2Local.kWelcome_CALC[ UiSwitches.qSwitches[ UiSwitches.SWI_LANGUAGE ] ];
		lines[ count ++ ] = line;
	}
//	stepNext = ((70 <= step) && (step <= 990)) ? 991 : (step + 1);
	return count;
}

@Override
public int prepareTextLines( boolean is4Console, String... param ) {
	lines[ 0 ] = "";
//	byte[] pass = CsvDb.instance.getPassFull();
//	if ((null == pass) || CsvDb.instance.setHexPhrase( "" )) { //(null != UiSwitches.pathInitDataFile) {
//		return prepare4Access();
//	}
	if (500 > qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
		return prepare4Access();
	}
	return prepare4Intro();
}

}

//=====

public static final class LicenseVw implements QAbstractViewIf {

String[] out;
String[] exit = new String[] { "Exit", "", Dib2Constants.NO_WARRANTY[ 0 ], Dib2Constants.NO_WARRANTY[ 1 ] };

@Override
public String[] getTextLines() {
	if (UiCalcServer.wxExitRequest) {
		return exit;
	}
	return out;
}

@Override
public int prepareTextLines( boolean is4Console, String... param ) {
	if (UiCalcServer.wxExitRequest) {
		return exit.length;
	}
	out = Dib2Config.platform.getLicense( Dib2Local.kLicensePre[ UiSwitches.qSwitches[ UiSwitches.SWI_LANGUAGE ] ] );
	return out.length;
}
}

//=====

public final CcmHelpVw qCcmHelpVw = new CcmHelpVw();
public final CcmInitialVw qCcmInitialVw = new CcmInitialVw();
public final LicenseVw qLicenseVw = new LicenseVw();

//=====
}
