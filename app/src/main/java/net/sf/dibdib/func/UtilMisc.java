// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.func;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.zip.*;
import net.sf.dibdib.config.*;

public final class UtilMisc {
//=====

public static int processMaxTimer = 1000;
public static int processMicroSteps4Timer = 30000;

/** Keep sort order and hex onset, skip char 'O' for potential confusion with '0'. */
public static final String base64XString = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ^_abcdefghijklmnopqrstuvwxyz~";
private static final char[] base64XChars = base64XString.toCharArray();

private static long minTimeVal = Dib2Config.TIME_MIN_2017_01_01;
private static long minTimeLast = Dib2Config.TIME_MIN_2017_01_01;
protected static int idStamp = (((2017 - 2000) & 0xf0) << 14) | (((2017 - 2000) & 0xf) << 12);
protected static long idCount = 1;
public static String qLastId;

/*
=====
Note:
W3 recommendation for ISO date format (www.w3.org/TR/NOTE-datetime):
Complete date:
  YYYY-MM-DD (eg 1997-07-16)
Complete date plus hours and minutes:
  YYYY-MM-DDThh:mmTZD (eg 1997-07-16T19:20+01:00)
Complete date plus hours, minutes and seconds:
  YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)
Complete date plus hours, minutes, seconds and a decimal fraction of a
second
  YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)

where:
 YYYY = four-digit year
 MM   = two-digit month (01=January, etc.)
 DD   = two-digit day of month (01 through 31)
 hh   = two digits of hour (00 through 23) (am/pm NOT allowed)
 mm   = two digits of minute (00 through 59)
 ss   = two digits of second (00 through 59)
 s    = one or more digits representing a decimal fraction of a second
 TZD  = time zone designator (Z or +hh:mm or -hh:mm)
=====
*/

// SimpleDateFormat is not thread-safe! 'X' works only for Java 7
private static final String DATE_FORMAT_ISO_Z = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
// Not needed here:
//	private static final String DATE_FORMAT_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
//	private static final String DATE_FORMAT_SHORT = "yyMMdd'.'HHmmss";
//	private static final String DATE_FORMAT_SHORT_TZ = "yyMMdd'.'HHmmss'.'SSSZ";

// Do not waste time by constantly accessing TZ data:
private static final SimpleDateFormat DATE_SDF = new SimpleDateFormat( DATE_FORMAT_ISO_Z );
private static int timeZoneOffset = 0;

/** Might need a fresh start later on, e.g. for daylight saving. */
public static boolean timeZoneDone = false;

public final static StringBuffer logBuffer = new StringBuffer( 22000 );

//public static void checkPlatform() {
// Check:
//	BigInteger P;
//	P = new BigInteger( 64, 1, new Random( 42 ) );
//	Dib2Config.log( "receiver", "random check > 16: " + P );
//	P = new BigInteger( 64, 1, new Random( 42 ) );
//	Dib2Config.log( "receiver", "random check > 16: " + P );
//	P = new BigInteger( 8, 1, new Random( 42 ) );
//	Dib2Config.log( "receiver", "random check < 16: " + P );
//	P = new BigInteger( 8, 1, new Random( 42 ) );
//	Dib2Config.log( "receiver", "random check < 16: " + P );
//}

public static void initLastId( String id ) {
	String current = createId( "" );
	if (current.compareTo( id ) <= 0) {
		long count = id.charAt( 4 ) & 0x3;
		for (int i0 = 5; i0 < (id.length() - 5); ++ i0) {
			int c0 = id.charAt( i0 ) - '0';
			c0 = (c0 >= 10) ? (c0 + '0' - 'A') : c0;
			count = (count << 6) + c0;
		}
		idCount = (count >>> 2) + 4;
		current = createId( "" );
		if (current.compareTo( id ) <= 0) {
			// Fallback.
			count = currentTimeMillisLinearized() / 1000;
			idCount = count % (12 * 60 * 60);
		}
	}
}

public static String readStream( InputStream is ) throws IOException {
	byte[] text = new byte[ 90000 ];
	int cnt = 0;
	while (cnt < (text.length - 200)) {
		int cx = is.read( text, cnt, text.length - cnt - 100 );
		if (0 >= cx) {
			break;
		}
		cnt += cx;
	}
	is.close();
	text[ cnt ++ ] = '\n';
	return new String( text, 0, cnt );
}

public static String getLicense( String xAdditionalVersionInfo, String... resources ) {
//	final URL[] urls = new URL[] { getClass().getResource( "/license.txt" ), ...
	String license = "(Version " //,
		+ ((null == xAdditionalVersionInfo) ? "" : (xAdditionalVersionInfo + "/ ")) //,
		+ Dib2Constants.VERSION_STRING + ")\n";
	try {
		for (String rsc : resources) {
			InputStream is;
			if (null == rsc) {
				continue;
			}
			is = new BufferedInputStream( UtilMisc.class.getResourceAsStream( rsc ) ); //url.openStream();
			license += readStream( is );
		}
	} catch (Exception e) {
		if (!license.contains( "Could not access" )) {
			license = Dib2Constants.NO_WARRANTY[ 0 ] + "\n(Could not access license files.)\n\n" + license;
		}
	}
	return license;
}

public static void checkTimeZone() {
	if (timeZoneDone) {
		// Leave the time zone for the current run, rather than messing up the internal time line.
		return;
	}
	timeZoneDone = true;
	timeZoneOffset = TimeZone.getDefault().getOffset( System.currentTimeMillis() ); // DATE_SDF.getTimeZone().getRawOffset();
	DATE_SDF.setTimeZone( TimeZone.getTimeZone( "UTC" ) );
}

public static void keepLog( String tag, String msg ) {
	int size = 0;
	while (42000 < logBuffer.length()) {
		size = logBuffer.indexOf( "\n", 200 );
		size = (0 >= size) ? 100 : size;
		logBuffer.delete( 0, size );
	}
	logBuffer.append( tag ).append( "@" + dateShort4Millis() + ": " ).append( msg ).append( "\n" );
}

public static long currentTimeMillisLinearized() {
	final long out = System.currentTimeMillis();
	if (out >= Dib2Config.TIME_MAX) {
		///// Something is really bad ... This version was not meant to be used for more than 30 years :-)
		minTimeVal = (minTimeLast >= minTimeVal) ? minTimeVal : minTimeLast;
		if (out > minTimeLast) {
			///// Keep 'ticking'.
			++ minTimeVal;
			minTimeLast = out;
		}
	} else if (minTimeVal <= out) {
		///// Looks good.
		minTimeVal = out;
		minTimeLast = out;
		return out & ~Dib2Constants.TIME_SHIFTED;
	} else {
		///// Bad timer or bad alignment?
		if (out > (minTimeLast + Dib2Constants.TIME_SHIFTED)) {
			///// Keep 'ticking'.
			++ minTimeVal;
			minTimeLast = out;
		} else if ((Dib2Config.TIME_MAX <= minTimeLast) && (Dib2Config.TIME_MIN_2017_01_01 < out)) {
			///// The system seems to have corrected previous faulty values, but maybe not quite.
			minTimeVal = (minTimeLast >= minTimeVal) ? minTimeVal : minTimeLast;
			minTimeVal = (minTimeVal + out) / 2 - (24 + 12) * 3600 * 1000;
			if (out <= minTimeVal) {
				return (out & ~Dib2Constants.TIME_SHIFTED) | Dib2Constants.TIME_SHIFTED_HOUR;
			}
		}
		// Potential time zone shifting:
		if (out >= (minTimeVal - (24 + 12) * 3600 * 1000)) {
			minTimeLast = out;
			return (minTimeVal & ~Dib2Constants.TIME_SHIFTED) | Dib2Constants.TIME_SHIFTED_HOUR;
		}
	}
	return (minTimeVal & ~Dib2Constants.TIME_SHIFTED) | Dib2Constants.TIME_SHIFTED_UNKNOWN;
}

/** Set time value according to current time, or adjust overall timer.
 * @param value The original value or -1 for current time.
 * @param minimum -1 for unmarked time value, >1 for potential change of overall timer.
 * @return
 */
public static long alignTime( long value, long minimum ) {
	final int hour = 3600 * 1000;
	final int minute = 60 * 1000;
	final int delta = 2 * minute;
	long current = currentTimeMillisLinearized();
	long val = (0 <= value) ? value : current;
	long ref = (1 <= minimum) ? minimum : current;
	if (ref > (current + Dib2Constants.TIME_SHIFTED)) {
		// Bad ref?
		if (Dib2Config.TIME_MAX <= ref) {
			ref = minTimeVal;
		}
		// Adjust overall timer?
		if (ref > current) {
			minTimeVal = (ref > minTimeVal) ? ref : minTimeVal;
		}
	}
	if (val < (ref - delta)) {
		// Time zone changed?
		if (val >= (ref - 25 * hour)) {
			long out = val & ~Dib2Constants.TIME_SHIFTED;
			while (out < (ref - delta)) {
				out += hour;
			}
			if (out <= current) {
				return out | Dib2Constants.TIME_SHIFTED_HOUR;
			}
		}
		return (ref & ~Dib2Constants.TIME_SHIFTED_HOUR) | Dib2Constants.TIME_SHIFTED_UNKNOWN;
	} else if (val > (current + delta)) {
		if (val > (current + 25 * hour)) {
			return current | Dib2Constants.TIME_SHIFTED_UNKNOWN;
		}
		long out = val & ~Dib2Constants.TIME_SHIFTED;
		while (out > (current + delta)) {
			out -= hour;
		}
		return out | Dib2Constants.TIME_SHIFTED_HOUR;
	}
	return (0 <= minimum) ? val : (val & ~Dib2Constants.TIME_SHIFTED);
}

/** @param msec UTC (optional, default: current time)
 * @return
 */
public static String date4Millis( boolean forceGood, long... msec ) {
	long v0 = ((null == msec) || (0 >= msec.length) || (0 > msec[ 0 ])) ? currentTimeMillisLinearized() : msec[ 0 ];
	if (!timeZoneDone) {
		checkTimeZone();
	}
	// Has "+0000":
	String date = DATE_SDF.format( new Date( v0 ) );
	if (!forceGood && (0 != (v0 & Dib2Constants.TIME_SHIFTED))) {
		date = date.substring( 0, date.length() - 9 );
		if (0 != (v0 & Dib2Constants.TIME_SHIFTED_UNKNOWN)) {
			date = date.substring( 0, date.length() - 3 ) + "???";
		}
		if (0 != (v0 & Dib2Constants.TIME_SHIFTED_HOUR)) {
			date = date.substring( 0, date.length() - 8 ) + "??" + date.substring( date.length() - 6 );
		}
		date += ".000+0000";
	}
	return date.substring( 0, date.length() - 5 ).concat( "+00:00" );
}

public static String dateLocal4Millis( boolean forceGood, long... msec ) {
	long v0 = ((null == msec) || (0 >= msec.length) || (0 > msec[ 0 ])) ? currentTimeMillisLinearized() : msec[ 0 ];
	// Local time (without sdf.setTimeZone(TimeZone.getTimeZone()):
	String date = date4Millis( forceGood, v0 + timeZoneOffset );
	// Has "+00:00":
	date = date.substring( 0, date.length() - 6 );
	String tzone = "+";
	int tzMin = timeZoneOffset / (1000 * 60);
	if (0 > tzMin) {
		tzone = "-";
		tzMin = -tzMin;
	}
	tzone += String.format( "%02d:%02d", (tzMin / 60), tzMin % 60 );
	return date + tzone;
}

public static String dateShort4Millis( long... msec ) {
	long v0 = ((null == msec) || (0 >= msec.length) || (0 > msec[ 0 ])) ? currentTimeMillisLinearized() : msec[ 0 ];
	if (!timeZoneDone) {
		checkTimeZone();
	}
	// Local time (without sdf.setTimeZone(TimeZone.getTimeZone()):
	String dx = DATE_SDF.format( new Date( v0 + timeZoneOffset ) );
	dx = dx.substring( 2, dx.length() - 9 ).replaceAll( "[^0-9T\\:]", "" ).replace( 'T', '.' );
	return dx;
}

/** Include TIME_SHIFTED info!
 * @param msec
 * @param len
 * @return
 */
public static String dateShort4Millis( long msec, int len ) {
	String out = dateShort4Millis( msec );
	if (0 != (msec & Dib2Constants.TIME_SHIFTED)) {
		out = out.substring( 0, out.length() - 3 );
		if (0 != (msec & Dib2Constants.TIME_SHIFTED_UNKNOWN)) {
			out = out.substring( 0, out.length() - 3 ) + "???";
		}
		if (0 != (msec & Dib2Constants.TIME_SHIFTED_HOUR)) {
			out = out.substring( 0, out.length() - 5 ) + "??" + out.substring( out.length() - 3 );
		}
		out += ":00";
	}
	if (len >= 0) {
		return out.substring( 0, len );
	}
	// Cut for negative len:
	return out.substring( 0, out.length() + len );
}

public static String prependCentury( String shortDate ) {
	String yyyy = UtilMisc.date4Millis( true ).substring( 0, 4 );
	int pp = (yyyy.charAt( 0 ) & 0xf) * 10 + (yyyy.charAt( 1 ) & 0xf);
	if (yyyy.charAt( 2 ) > ((shortDate.charAt( 0 ) & 0xf) + 5)) {
		-- pp;
	} else if (yyyy.charAt( 2 ) < ((shortDate.charAt( 0 ) & 0xf) - 5)) {
		++ pp;
	}
	return "" + pp + shortDate;
}

public static long millis4Date( String date ) {
	int offs = 0;
	if (!timeZoneDone) {
		checkTimeZone();
	}
	long shifted = (0 > date.indexOf( '?' )) ? 0 : (date.contains( "???" ) ? Dib2Constants.TIME_SHIFTED_UNKNOWN
		: Dib2Constants.TIME_SHIFTED_HOUR);
	date = date.replace( "???", ":00" ).replace( "??", "00" );
	// Short format?
	if ((date.length() <= 6) || date.matches( "[0-9][0-9][0-9][0-9][0-9][0-9]\\..*" )) {
		date = date.replace( ":", "" );
		// Use local time:
		int itz = date.indexOf( '+' );
		if (itz < 0) {
			itz = date.lastIndexOf( '-' );
		}
		if (itz < 0) {
			offs = timeZoneOffset;
			date = date.concat( "110101.120000.000+0000".substring( date.length() ) );
		} else {
			date = date.substring( 0, itz ).concat( "110101.120000.000".substring( itz ) ) //.
				.concat( date.substring( itz ) );
		}
		date = prependCentury( date.substring( 0, 2 ) ) // (date.charAt( 0 ) == '9') ? "19" : "20") + date.substring( 0, 2 )
			+ "-" + date.substring( 2, 4 ) + "-" + date.substring( 4, 6 ) //.
			+ "T" + date.substring( 7, 9 ) + ":" + date.substring( 9, 11 ) + ":" + date.substring( 11, 13 ) //.
			+ date.substring( 13 );
	}
	if (date.matches( "[0-9][0-9][^0-9].*" )) {
		date = prependCentury( date );
	}
	if (date.matches( "[0-9][0-9][0-9][0-9]\\-[0-9][0-9].*" )) {
		// TZ info?
		if (date.charAt( date.length() - 3 ) == ':') {
			char tzi = date.charAt( date.length() - 6 );
			if ((tzi == '+') || (tzi == '-')) {
				date = date.substring( 0, date.length() - 3 ) + date.substring( date.length() - 2 );
			}
		}
		try {
			final Date dx = DATE_SDF.parse( date );
			return ((dx.getTime() - offs) & ~Dib2Constants.TIME_SHIFTED) | shifted;
		} catch (ParseException e) {
			{
			} // NOP
		}
	}
	date = date.replaceAll( "[^0-9]", "" );
	date = (4 >= date.length()) ? date : (date.substring( 0, 4 ) + '-' + date.substring( 4 ));
	date = (7 >= date.length()) ? date : (date.substring( 0, 7 ) + '-' + date.substring( 7 ));
	date = (10 >= date.length()) ? date : (date.substring( 0, 10 ) + 'T' + date.substring( 10 ));
	date = (13 >= date.length()) ? date : (date.substring( 0, 13 ) + ':' + date.substring( 13 ));
	date = (16 >= date.length()) ? date : (date.substring( 0, 16 ) + ':' + date.substring( 16 ));
	date = (19 >= date.length()) ? date : date.substring( 0, 19 );
	date = date.concat( "2000-01-01T12:00:00.000+0000".substring( date.length() ) );
	try {
		final Date dx = DATE_SDF.parse( date );
		return ((dx.getTime() - offs) & ~Dib2Constants.TIME_SHIFTED) | shifted;
	} catch (ParseException e) {
		{
		} // NOP
	}
	return Dib2Config.TIME_MIN_2017_01_01 | Dib2Constants.TIME_SHIFTED_UNKNOWN | (minTimeVal & 0xffff0);
}

/** Calculate limit or steps to limit.
 * 
 * @param count
 * @param previousLimit
 * @return
 */
public static long processTimer( int count, long previousLimit ) {
	if (previousLimit <= 0) {
		return currentTimeMillisLinearized() + processMaxTimer;
	}
	long now = currentTimeMillisLinearized() + 2;
	if (now >= previousLimit) {
		return Long.MIN_VALUE + 1;
	}
	return (processMaxTimer / 2 * count) / (processMaxTimer + (int) (now - previousLimit));
}

private static final byte[] toLeapMinute_months = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
private static final int[] toLeapMinute_months_tot = new int[ 12 ];
private static final byte[] toDate4LeapMinute_monthsDelta = new byte[ 12 ];

public static long leapMinute256ForDate( String xDate ) {
	boolean neg = '-' == xDate.charAt( 0 );
	String date = neg ? (xDate.substring( 1 )) : xDate;
	if (0 == toLeapMinute_months_tot[ 1 ]) {
		for (int i0 = 0; i0 < 11; ++ i0) {
			toLeapMinute_months_tot[ i0 + 1 ] = toLeapMinute_months_tot[ i0 ] + toLeapMinute_months[ i0 ];
		}
	}
	if (QStr.PATTERN_DATE_D.matcher( date ).matches()) {
		date = date.substring( 6 ) + '-' + date.substring( 3, 5 ) + '-' + date.substring( 0, 2 );
	}
	if (2 == date.indexOf( '-' )) {
		date = UtilMisc.prependCentury( date );
	}
	int add = 0;
	int tz10 = 0;
	if ((0 <= date.indexOf( 'T' )) || (0 <= date.indexOf( '.' ))) {
		int iHH = date.indexOf( 'T' );
		iHH = (0 <= iHH) ? iHH : date.indexOf( '.' );
		String hhmm = date.substring( iHH + 1 );
		date = date.substring( 0, iHH );
		if (2 <= hhmm.length()) {
			add += (hhmm.charAt( 0 ) & 0xf) * 600;
			add += (hhmm.charAt( 1 ) & 0xf) * 60;
			hhmm = hhmm.substring( (2 == hhmm.indexOf( ':' )) ? 3 : 2 );
			if (2 <= hhmm.length()) {
				add += (hhmm.charAt( 0 ) & 0xf) * 10;
				add += (hhmm.charAt( 1 ) & 0xf) * 1;
				iHH = hhmm.indexOf( '+' );
				iHH = (0 <= iHH) ? iHH : hhmm.indexOf( '-' );
				if ((0 <= iHH) & ((iHH + 5) <= hhmm.length())) {
					tz10 += (hhmm.charAt( iHH + 1 ) & 0xf) * 60;
					tz10 += (hhmm.charAt( iHH + 2 ) & 0xf) * 6;
					tz10 += (hhmm.charAt( iHH + 4 ) & 0xf) * 1;
					tz10 = tz10 & 0xff;
					tz10 = ('-' == hhmm.charAt( iHH )) ? (128 - tz10) : (128 + tz10);
					add += (tz10 - 128) * 10;
				}
			}
		}
	}
	date = date.replace( "-", "" );
	try {
		int n0 = Integer.parseInt( date );
		int month = (n0 / 100) % 100 - 1;
		int day = (n0 % 100 - 1) + toLeapMinute_months_tot[ month ];
		int year = n0 / 10000 - 1;
		if (neg) {
			year = -year - 1;
		}
		return ((((year * 366L) + day) * 24L * 60 + add) * 256L) + tz10;
	} catch (Exception e) {
	}
	return 0;
}

public static double leapMinute4Date( String xDate ) {
	return leapMinute256ForDate( xDate ) / 256.0;
}

public static String date4LeapMinute256( long xMinute256 ) {
	long minute = xMinute256 / 256L;
	minute -= (0 > minute) ? 1 : 0;
	int tz1280 = ((int) (xMinute256 - minute * 256L)) * 10;
	// Compensate 10 for potential rounding errors:
	minute -= (10 < tz1280) ? (tz1280 - 1280) : 0;
	if (0 == toDate4LeapMinute_monthsDelta[ 1 ]) {
		for (int month = 1; month < 12; ++ month) {
			toDate4LeapMinute_monthsDelta[ month ] = (byte) (month * 32 - toLeapMinute_months_tot[ month ]);
		}
	}
	long dayTot = minute / (24 * 60);
	minute = minute % (24 * 60);
	int year = (int) (dayTot / 366);
	int day = (int) (dayTot % 366);
	if (0 > minute) {
		-- day;
		minute += 24 * 60;
	}
	if (0 < day) {
		++ year;
	} else if (0 > day) {
		-- year;
		day += 366;
	} else if (0 <= minute) {
		++ year;
	}
	int month = day / 32;
	day -= month * 32;
	day += toDate4LeapMinute_monthsDelta[ month ];
	if (day >= toLeapMinute_months[ month ]) {
		day -= toLeapMinute_months[ month ];
		++ month;
	}
	++ month;
	++ day;
	String tzone = "";
	if (0 < tz1280) {
		tzone = "+";
		tz1280 -= 1280;
		if (0 > tz1280) {
			tzone = "-";
			tz1280 = -tz1280;
		}
		tzone += String.format( "%02d:%02d", (tz1280 / 60), tz1280 % 60 );
	}
	return String.format( "%d-%02d-%02dT%02d:%02d%s", year, month, day,//,
		minute / 60, minute % 60, tzone );
}

public static String date4LeapMinute( double xMinute ) {
	long mint256 = (long) (256.0 * xMinute);
	// Compensate for TZ rounding errors:
	mint256 = (0xff == (mint256 & 0xff)) ? (mint256 + 1) : mint256;
	return date4LeapMinute256( mint256 );
}

public static String base64X4Bitlists( int[] acBits, long... vals ) {
	int len = 0;
	for (int b0 : acBits) {
		len += b0 / 6;
	}
	char[] out = new char[ len ];
	int inx = len;
	for (int iv = vals.length - 1; iv >= 0; -- iv) {
		long val = vals[ iv ];
		for (int i0 = acBits[ iv ] / 6 - 1; i0 >= 0; -- i0) {
			int b64 = 0x3f & (int) val;
			-- inx;
			out[ inx ] = base64XChars[ b64 ];
			val = val >>> 6;
		}
	}
	if (0 != inx) {
		return null;
	}
	return new String( out );
}

public static short[] base64X4Bytes( byte[] string, int... pad ) {
	short[] out = new short[ string.length + string.length / 2 ];
	final int len = string.length - 3;
	int cnt = 0;
	int iStr = 0;
	for (; iStr < len; iStr += 3) {
		out[ cnt ++ ] = (short) ((string[ iStr ] & 0xff) >>> 2);
		out[ cnt ++ ] = (short) (((string[ iStr ] & 0x3) << 4) | ((string[ iStr + 1 ] & 0xff) >>> 4));
		out[ cnt ++ ] = (short) (((string[ iStr + 1 ] & 0xf) << 2) | ((string[ iStr + 2 ] & 0xff) >>> 6));
		out[ cnt ++ ] = (short) (string[ iStr + 2 ] & 0x3f);
	}
	if (iStr < len) {
		out[ cnt ++ ] = (short) ((string[ iStr ] & 0xff) >>> 2);
		out[ cnt ++ ] = (short) (((string[ iStr ] & 0x3) << 4) | ((0 < pad.length) ? (pad[ 0 ] & 0xf) : 0));
		++ iStr;
	}
	if (iStr < len) {
		out[ cnt ] |= ((string[ iStr + 1 ] & 0xff) >>> 4);
		out[ cnt ++ ] = (short) (((string[ iStr + 1 ] & 0xf) << 2) | ((0 < pad.length) ? ((pad[ 0 ] & 0xf) >>> 2) : 0));
	}
	return Arrays.copyOf( out, cnt );
}

private static String asciiCompressed4String_xBase64 = null;

public static byte[] asciiCompressed4Bytes( byte[] string ) {
	byte[] out = new byte[ 2 * string.length ];
	int cnt = 0;
	if (null == asciiCompressed4String_xBase64) {
		char[] ax = new char[ 62 ];
		for (char ch = ' '; cnt < 62; ch = (char) ((0x7e == ch) ? 0xdf : (ch + 1))) {
			if (0 > base64XString.indexOf( ch )) {
				ax[ cnt ++ ] = ch;
			}
		}
		asciiCompressed4String_xBase64 = new String( ax );
		cnt = 0;
	}
	for (int i0 = 0; i0 < string.length; ++ i0) {
		final byte v0 = string[ i0 ];
		int ix;
		if ((' ' <= v0) && (0 <= (ix = base64XString.indexOf( (char) v0 )))) {
			out[ cnt ++ ] = (byte) ix;
			continue;
		} else if ((' ' <= v0) && (v0 < 0x7f)) {
			ix = asciiCompressed4String_xBase64.indexOf( (char) v0 );
			out[ cnt ++ ] = (byte) (ix | 0x40);
			continue;
		} else if ((0xc2 == (v0 & 0xfe)) && ((i0 + 1) < string.length) && (0xc0 > (string[ i0 + 1 ] & 0xff))) {
			///// UTF-8.
			char cx = (char) (((v0 & 3) << 6) | (string[ i0 + 1 ] & 0x3f));
			if (0 <= (ix = asciiCompressed4String_xBase64.indexOf( cx ))) {
				out[ cnt ++ ] = (byte) (ix | 0x40);
				++ i0;
				continue;
			}
		}
		out[ cnt ++ ] = (byte) ((v0 >= 0) ? 0x7f : 0x7e);
		out[ cnt ++ ] = (byte) (v0 & 0x7f);
	}
	return Arrays.copyOf( out, cnt );
}

public static byte[] bytes4AsciiCompressed( byte[] ascii ) {
	byte[] out = new byte[ 2 * ascii.length ];
	int cnt = 0;
	if (null == asciiCompressed4String_xBase64) {
		asciiCompressed4Bytes( new byte[ 0 ] );
	}
	for (int i0 = 0; i0 < ascii.length; ++ i0) {
		final byte v0 = ascii[ i0 ];
		if (0x40 > v0) {
			out[ cnt ++ ] = (byte) base64XChars[ v0 & 0x3f ];
		} else if (0x7e > v0) {
			char cx = asciiCompressed4String_xBase64.charAt( v0 & 0x3f );
			if (0x80 > cx) {
				out[ cnt ++ ] = (byte) cx;
			} else {
				byte[] utf8 = UtilString.bytesUtf8( "" + cx );
				out[ cnt ++ ] = utf8[ 0 ];
				out[ cnt ++ ] = utf8[ 1 ];
			}
		} else {
			out[ cnt ++ ] = (byte) ((0x7f == v0) ? 0 : 0x80);
			++ i0;
			if (i0 < ascii.length) {
				out[ cnt - 1 ] |= ascii[ i0 ];
			}
		}
	}
	return Arrays.copyOf( out, cnt );
}

/** FNV-1a, also for short keys.
 * Cmp. http://www.isthe.com/chongo/tech/comp/fnv.
 * @param bits >= 16, <= 32
 */
public static int hash32_fnv1a( int bits, String... pieces ) {
//	final int prime = 0x1000193; // 16777619;
	final int offs = 0x811c9dc5; // (int) 2166136261L;
	int hash = offs;
	for (String dat : pieces) {
		final byte[] data = UtilString.bytesUtf8( dat );
		for (byte b0 : data) {
			hash ^= 0xff & b0;
			//hash *= prime;
			hash += (hash << 1) + (hash << 4) + (hash << 7) + (hash << 8) + (hash << 24);
		}
	}
	if (bits == 32) {
		return hash;
	}
	// XOR folding:
	return ((hash >>> bits) ^ hash) & ((1 << bits) - 1);
}

/** FNV-1a, also for short keys.
 * Cmp. http://www.isthe.com/chongo/tech/comp/fnv.
 * @param bits >= 32, <= 64
 */
public static long hash64_fnv1a( byte[] data, int bits ) {
//	final long prime = 0x100000001b3L; // 1099511628211L;
	final long offs = 0xcbf29ce484222325L; // any non-zero: 14695981039346656037
	long hash = offs;
	for (byte b0 : data) {
		hash ^= 0xff & b0;
		// hash *= prime;
		hash += (hash << 1) + (hash << 4) + (hash << 5) + (hash << 7) + (hash << 8) + (hash << 40);
	}
	if (bits == 64) {
		return hash;
	}
	// XOR folding:
	return ((hash >>> bits) ^ hash) & ((1L << bits) - 1L);
}

public static long hash32_partialString( String... parts ) {
	long hash = 0;
	for (String content : parts) {
		int len = content.length();
		int hash0 = (4096 > len) ? hash32_fnv1a( 32, content ) //,
			: hash32_fnv1a( 32, content.substring( 0, 1024 ), content.substring( len / 2, len / 2 + 1024 ),
				content.substring( len - 1024 ) );
		hash ^= hash0 & ((1L << 32) - 1);
	}
	return hash;
}

public static long getUidBase( long... optionalTime ) {
	String dateHour = date4Millis( true, optionalTime ).substring( 0, 15 );
	int year = (dateHour.charAt( 0 ) & 0xf) * 1000 + (dateHour.charAt( 1 ) & 0xf) * 100 + (dateHour.charAt( 2 ) & 0xf) * 10
		+ (dateHour.charAt( 3 ) & 0xf);
	int month = (dateHour.charAt( 5 ) & 0xf) * 10 + (dateHour.charAt( 6 ) & 0xf);
	int day2 = ((dateHour.charAt( 8 ) & 0xf) * 10 + (dateHour.charAt( 9 ) & 0xf)) << 1;
	int hour = ((dateHour.charAt( 11 ) & 0xf) * 10 + (dateHour.charAt( 12 ) & 0xf));
	day2 = day2 - ((12 > hour) ? 1 : 0);
//    hour2 += ('3' <= dateHour.charAt(14)) ? 1 : 0;
	int stamp = (((year - 2000) & 0xf0) << 14) | (((year - 2000) & 0xf) << 12) | (month << 6) | day2;
	if (stamp <= idStamp) {
		++ idCount;
	} else {
		idStamp = stamp;
		idCount = 1;
	}
	if ((1 << 30) < idCount) {
		// Wow :-)
		return ((long) stamp) << 32;
	}
	return (((long) stamp) << 32) | (idCount & ((1L << 32) - 1));
}

/** Create object ID (OID) for UID = source (e-mail address/ web site) + created OID.
 *  The created ID itself, whether created locally or elsewhere, has to stay unique
 *  for local storage and 'as unique as possible' for collaborative data
 *  stored on any device (for tracing changes) ==> conflict resolution mechanism
 *  ('eventual consistency').
 * @param content Text or data for hash value as part of ID (less conflicts).
 * @return 12/ 16/ 20 chars depending on length of sequence count.
 */
public static String createId( String content, long... optionalTime ) {
	long uid = getUidBase( optionalTime );
	int stamp = (int) (uid >>> 32);
	long hash = hash32_partialString( content );
	// 16 bits = 2 * byteLen = 3 * b64Len - 2:
	int lenCount = 18 - 2;
	// 24 bits = 3 * byteLen = 4 * b64Len: (special case: first bits at '111111' for conflict resolution)
	long countHigh = (idCount >> 4);
	long lowNHash2 = ((idCount & 0xff) << 2) | (hash >>> 30);
	if (idCount >= (1L << 38)) {
		lenCount += 48;
		// Keep sort order of first byte: keep first bits at '110':
		countHigh |= 6L << 57;
	} else if (idCount >= (1 << 15)) {
		lenCount += 24;
		// First bits at '10':
		countHigh |= 2L << 34;
	} // else: first bit at '0'
	qLastId = base64X4Bitlists( new int[] { 24, lenCount - 4, 6, 30 }, stamp, countHigh, lowNHash2, hash );
	return qLastId;
}

public static String createIdNext( String xOid ) {
	char[] out = xOid.toCharArray();
	for (int iChar = out.length - 1; iChar >= 0; -- iChar) {
		int i64 = 1 + Arrays.binarySearch( base64XChars, out[ iChar ] );
		if ((i64 > 0) && (i64 < base64XChars.length)) {
			out[ iChar ] = base64XChars[ i64 ];
			break;
		}
		out[ iChar ] = base64XChars[ 0 ];
	}
	return new String( out );
}

public static void writeFile( String path, byte[] dat, int offset, int length, byte[] xHeader ) throws Exception {
	RandomAccessFile file = new RandomAccessFile( path, "rw" );
	file.seek( 0 );
	if (xHeader != null) {
		file.write( xHeader );
	}
	file.write( dat, offset, length );
	file.setLength( file.getFilePointer() );
	file.close();
}

public static byte[] readFile( String xPath, int xExpectedFileStruc ) throws Exception {
	RandomAccessFile file = new RandomAccessFile( xPath, "r" );
	byte[] tlv = new byte[ 2 ];
	byte[] dat;
	int offs = 0;
	if (0 >= xExpectedFileStruc) {
		dat = new byte[ (int) file.length() ];
	} else {
		file.read( tlv );
		if (Dib2Constants.MAGIC_BYTES[ 0 ] == tlv[ 0 ]) {
			///// Old format.
			byte[] xlen = len4880( (int) file.length() );
			offs = xlen.length + 1;
			dat = new byte[ 1 + xlen.length + (int) file.length() ];
			dat[ 0 ] = (byte) Dib2Constants.RFC4880_EXP2;
			System.arraycopy( xlen, 0, dat, 1, xlen.length );
		} else {
			dat = new byte[ (int) file.length() ];
		}
		file.seek( 0 );
	}
	file.read( dat, offs, dat.length - offs );
	file.close();
	return dat;
}

/** OLD: little endian.
 * @param val >= 0
 * @return
 */
public static byte[] int2Tlv( int val ) {
	if (32 > val) {
		return new byte[] { (byte) val };
	}
	if (0x100 > val) {
		return new byte[] { 0x21, (byte) val };
	}
	byte[] little = new byte[] { (byte) (val & 0xff), (byte) ((val >>> 8) & 0xff), (byte) ((val >>> 16) & 0xff), (byte) (val >>> 24) };
	if (0x10000 > val) {
		return new byte[] { 0x22, little[ 0 ], little[ 1 ] };
	}
	if (0x1000000 > val) {
		return new byte[] { 0x23, little[ 0 ], little[ 1 ], little[ 2 ] };
	}
	return new byte[] { 0x24, little[ 0 ], little[ 1 ], little[ 2 ], little[ 3 ] };
}

public static int tlv2Len_OLD( byte[] packet, int offset ) {
	int pack0 = packet[ offset ] & 0xff;
	// Old format?
	if ((32 > pack0)) {
		return packet[ offset ];
	}
	if ((pack0 & 0x28) != 0x20) {
		// Unknown format.
		return -1;
	}
	// Old format.
	int len = packet[ offset ] & 0x7;
	int val = 0;
	for (int i0 = len - 1; i0 >= 0; -- i0) {
		val |= (packet[ offset + i0 + 1 ] & 0xff) << (i0 << 3);
	}
	return val;
}

public static byte[] len4880( int len ) {
	return (192 > len) ? new byte[] { (byte) len } //,
		: ((8283 >= len) ? new byte[] { (byte) ((((len - 192) >> 8) & 0xff) + 192), (byte) (len - 192) } //,
			: new byte[] { (byte) 0xff, (byte) (len >> 24), (byte) (len >> 16), (byte) (len >> 8), (byte) len });
}

/** RFC 4880: header len 2 if packet len < 192, 3 if <= 8283, 6 otherwise.
 * @param packet
 * @param offsetLv
 * @return
 */
public static int getPacketBodyLen( byte[] packet, int offsetLv ) {
	final byte[] p = packet;
	final int o = offsetLv;
	return (192 > (p[ o ] & 0xff)) ? (p[ o ] & 0xff) //,
		: ((223 >= (p[ o ] & 0xff)) ? (((((p[ o ] & 0xff) - 192) << 8) | (p[ o + 1 ] & 0xff)) + 192) //,
			: (((p[ o + 1 ] & 0xff) << 24) | ((p[ o + 2 ] & 0xff) << 16) | ((p[ o + 3 ] & 0xff) << 8) | (p[ o + 4 ] & 0xff)));
}

public static int getPacketHeaderLen( byte[] packet, int offsetTag ) {
	return (192 > (packet[ offsetTag + 1 ] & 0xff)) ? 2 //,
		: ((223 >= (packet[ offsetTag + 1 ] & 0xff)) ? 3 : 6);
}

/** Cmp. RFC 4880. E.g. for tag 11 (literal): one-byte format ('b' binary/ 'u' UTF8),
 * len-of-filename, filename, 4-byte time stamp, data.
 * @param xTag e.g. 0x80 | 0x40 | 11=literal, | 62=experimental2
 * @param xName4Literal (currently NULL)
 * @param xyData
 * @param xOffs4Reuse >2 for keeping data in xyData[], -1 for new byte[]
 * @return new data starting at 0 or at data[0]
 */
public static byte[] packet4880X( int xTag, byte[] xName4Literal, byte[] xyData, int xOffs4Reuse, int xEnd ) {
	// TODO: additional len if (xName4Literal != NULL)
	int len = ((0 > xEnd) ? xyData.length : xEnd) - xOffs4Reuse;
	byte[] qlen = len4880( len );
	byte[] out = (2 > xOffs4Reuse) ? new byte[ len + qlen.length + 1 ] : xyData;
	int offs = (2 > xOffs4Reuse) ? 0 : (xOffs4Reuse - qlen.length - 1);
	out[ 0 ] = (byte) offs;
	out[ offs ] = (byte) xTag;
	System.arraycopy( qlen, 0, out, offs + 1, qlen.length );
	if (2 > xOffs4Reuse) {
		System.arraycopy( xyData, xOffs4Reuse, out, 1 + qlen.length, len );
	}
	return out;
}

/** OLD: little endian.
 * @param len
 * @return
 */
public static byte[] lvLen( long len ) {
	// bits: 00xxxxxx/ 010xxxxx
	if (len <= ((3 << 5) - 1)) {
		return new byte[] { (byte) len };
	}
	// bits: 0xxxxxxx
	if (len <= ((1 << 15) - 2)) {
		return new byte[] { (byte) (len & 0x7f), (byte) (len >> 7) };
	}
	// bits: 10xxxxxx, 110xxxxx, ...
	int lead = 0x80;
	byte[] out = new byte[ 9 ];
	int inx = 8;
	for (int shift = 57; shift >= 22; shift -= 7) {
		out[ inx ] = (byte) ((len >>> shift) & 0xff);
		lead += lead >> 1;
	}
	out[ 0 ] |= lead;
	return out;
}

public static long long4Bytes( byte[] dat, int offs, int count ) {
	long out = 0;
	for (int i0 = 0; i0 < count; ++ i0) {
		out = (out << 8) | (dat[ offs + i0 ] & 0xff);
	}
	return out;
}

public static int bytes4Long( byte[] out, int offset, long val, int size ) {
	for (int i0 = size - 1; i0 >= 0; -- i0) {
		out[ i0 + offset ] = (byte) val;
		val >>>= 8;
	}
	return size;
}

/** Compress using Java's Deflater.
 * @param xyData data[offset] should contain part of magic byte (>= 0x30)
 * @return new data starting at 0 or at data[0]
 */
public static byte[] compress( int xTag, byte[] xyData, int xOffs4Reuse, int to ) {
	Deflater deflater = new Deflater( Deflater.DEFAULT_COMPRESSION, true );
	int len = to - xOffs4Reuse;
	byte[] full = len4880( len );
	deflater.setInput( xyData, xOffs4Reuse, len );
	deflater.finish();
	int outOffs = 22;
	byte[] out = new byte[ 2 * xyData.length + 2 * outOffs ];
	len = deflater.deflate( out, outOffs, out.length - outOffs );
	deflater.end();
	byte[] mb2 = Dib2Constants.MAGIC_BYTES;
	byte[] time = len4880( (int) (currentTimeMillisLinearized() / 1000) );
	byte[] mbNStamp = new byte[] { mb2[ 0 ], mb2[ 1 ], (byte) xTag, (byte) (full.length + time.length) };
	mbNStamp = Arrays.copyOf( mbNStamp, mbNStamp.length + mbNStamp[ 3 ] );
	System.arraycopy( full, 0, mbNStamp, 4, full.length );
	System.arraycopy( time, 0, mbNStamp, 4 + full.length, time.length );
	byte[] header = (1 < xTag) ? mbNStamp : new byte[ xTag ];
	if ((len >= (xyData.length - xOffs4Reuse)) && (0 < xOffs4Reuse)) {
		// No compression:
		out = Arrays.copyOfRange( xyData, xOffs4Reuse, to );
		len = out.length;
		full = new byte[] { 0 };
	}
	byte[] ll = len4880( len + header.length );
	if (1 == header.length) {
		// Old format (magic bytes not used).
		byte[] tlv0 = int2Tlv( to - xOffs4Reuse );
		byte[] tlv1 = (len >= 32) ? int2Tlv( len ) : new byte[] { 0x21, (byte) len };
		xyData[ 0 ] = (byte) xOffs4Reuse;
		System.arraycopy( tlv0, 0, xyData, xOffs4Reuse, tlv0.length );
		// 0x50 + 0x2x: marker for binary data + len-of-len:
		xyData[ xOffs4Reuse + tlv0.length ] = (byte) (0x50 + tlv1[ 0 ]);
		System.arraycopy( tlv1, 1, xyData, xOffs4Reuse + 1 + tlv0.length, tlv1.length - 1 );
		System.arraycopy( out, outOffs, xyData, xOffs4Reuse + tlv0.length + tlv1.length, len );
		return xyData; // tlv0.length + tlv1.length + len;
	} else if (0 < header.length) {
		int offs = (xOffs4Reuse - header.length - 1 - ll.length);
		if (0 <= offs) {
			System.arraycopy( out, outOffs, xyData, xOffs4Reuse, len );
			out = xyData;
			outOffs = xOffs4Reuse;
		} else {
			out = Arrays.copyOf( out, outOffs + len );
			offs = outOffs - header.length - 1 - ll.length;
		}
		out[ 0 ] = (byte) offs;
		out[ offs ++ ] = (byte) Dib2Constants.RFC4880_EXP2;
		System.arraycopy( ll, 0, out, offs, ll.length );
		System.arraycopy( header, 0, out, offs + ll.length, header.length );
	} else {
		if (10 < xOffs4Reuse) {
			System.arraycopy( out, outOffs, xyData, xOffs4Reuse, len );
			out = xyData;
			outOffs = xOffs4Reuse;
		}
		out[ 0 ] = (byte) (outOffs - ll.length - full.length);
		System.arraycopy( full, 0, out, outOffs - ll.length - full.length, full.length );
		System.arraycopy( ll, 0, out, outOffs - ll.length, ll.length );
	}
	return out;
}

public static byte[] decompress( byte[] xData, int from, int to ) {
	int offs = from;
	int full = 6 * (to - from);
	if (xData[ offs ] == (byte) Dib2Constants.RFC4880_EXP2) {
		offs += (192 > (xData[ offs + 1 ] & 0xff)) ? 2 //,
			: ((223 >= (xData[ offs + 1 ] & 0xff)) ? 3 : 6);
	}
	if (0x27 > (xData[ from ] & 0xff)) {
		// Old format.
		full = tlv2Len_OLD( xData, from );
		offs += 1 + (xData[ from ] & 0x7);
		// -1 for error or bad value?
		if (full < (to - from)) {
			return xData;
		}
		// Expect 0x7x as marker for compressed data:
		if ((xData[ offs ] & 0x78) != 0x70) {
			return xData;
		}
		offs += 1 + (xData[ offs ] & 0x7);
	} else if (Dib2Constants.MAGIC_BYTES[ 0 ] == xData[ offs ]) {
		// Skip markers:
		for (int i0 = 0; i0 <= Dib2Constants.MAGIC_BYTES.length; ++ i0, ++ offs) {
			if ('z' == xData[ offs ]) {
				++ offs;
				break;
			}
		}
		full = getPacketBodyLen( xData, offs + 1 );
		if (0 >= full) {
			return Arrays.copyOfRange( xData, from + 2, to );
		}
		offs += 1 + xData[ offs ];
	}
	Inflater inflater = new Inflater( true );
	inflater.setInput( xData, offs, to - offs );
	byte[] out = new byte[ full ];
	int done = 0;
	try {
		done = inflater.inflate( out );
		while (!inflater.finished()) {
			out = Arrays.copyOf( out, 2 * out.length );
			int add = inflater.inflate( out, done, out.length - done );
			if (0 >= add) {
				break;
			}
			done += add;
		}
	} catch (DataFormatException e) {
		return xData;
	}
	inflater.end();
//	if (done < (data.length / 2 - 32)) {
	return Arrays.copyOf( out, done );
}

//=====
}
