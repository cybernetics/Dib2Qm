// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.func;

import com.gitlab.dibdib.joined.util.Rfc1345;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.*;

/** Utilities for the String class and byte[]. Cmp. http://www.unicode.org, RFC 1345.
 */
public final class UtilString {
//=====

/** Assuming a 1:1 mapping for bytes ... :-( */
public static final Charset STR256 = Charset.forName( "ISO_8859_1" );
/** Trying to go for UTF-8 as much as possible ... :-( */
public static final Charset STRX16U8 = Charset.forName( "UTF-8" );

/* (Cmp. QStr!)
CTRL0 = \0..\1f
CTRL1 = \80..\9f
SHCHAR = \e000..\f7ff
PCHAR = others, except ' '
QMARKER = STX + ETX for quote, EOT(SLS) + ENQ(ELS) for sequence, SO + SI for TEMPL (DLE/DC1+QUOTE...)
XDELIM = \t \r \f \n
QDELIM = XDELIM + ' ' + SH + NBH + NBS + ZW
QCHAR = PCHAR + ' ', incl. PAR

STRING = String of CHARs
QQUOTE = quasi-quoted STRING (originally inside STX/ETX pair, removing only
one layer at a time: STX STX ... ENQ ENQ becomes quote with remaining pair)
QSTRING = String of QCHARs
WORD = (atomic) QSTRING of alphanumerics ('normal' words, numbers, ...)
BITLIST = (atomic) QSTRING with leading marker
SEParator = (atomic) QSTRING of others (", ", ..., " " (implied between WORDs!)) with marker
TEMPLate = SO QSTRING (name/ descr. for formating) SI
ATOM = WORD + TEMPL + SEP + QQUOTE + QDELIM + SLS/'[[' + ELS/']]'
LITERAL = sequence of ATOMs
QVAL = ATOM as numeric value (double + units)/ DATE (long)/ .../ seq. of QVALs,
       keeping original LITERAL

SHASH = sortable hash for QVAL: QVAL0 eq QVAL1 => SHASH(QVAL0) = SHASH(QVAL1)
QSEQ = sequence of QVALs
QFRAME = representation container

Parsing:
1. string4Mnemonics: '_' + ... + '_' > ...,
   quotes: '_{{' > STX, '}}_' > ETX,
   lists:  '_[[' > SLS, ']]_' > ELS,
   templ.: '_<'  > SO,   '>_' > SI,
2. atoms4String:
   balance quotes, transform into sequence of ATOMs
3. lists4Atoms:
   balance lists, remove list SEPs
   remove outer STX/ ETX pairs from ATOMs 
*/

///// Control char assignments: UI, markers

public static final char ESCAPE = '\u001b';
public static final char EOF = '\u001a';
public static final char PAR_X = '\u0014';
public static final char SECT_X = '\u0015';
public static final char CR = '\r';
public static final char FF = '\u000c';
public static final char ALT = '\u000b';
public static final char LF = '\n';
public static final char TAB = '\t';
public static final char BULLET_X = '\u0007'; // BELL
public static final char DELTA_X = '\u007f';

public static final char ZOOM_OUT = '\u001e';
public static final char ZOOM_IN = '\u001d';
public static final char DONE = EOF; // ==> re-draw
public static final char XCUT = '\u0019';
public static final char XCOPY = '\u0018';
public static final char XPASTE = '\u0017';
public static final char PSHIFT = '\u0013';
public static final char SHIFT = '\u0012';
public static final char SCROLL_LEFT = '\u0011';
public static final char SCROLL_RIGHT = '\u0010'; // DLE
public static final char SCROLL_DOWN = '\u000f'; // SI
public static final char SCROLL_UP = '\u000e'; //SO
public static final char BACKSP = '\u0008';
public static final char MOVE_LEFT = '\u0005';
public static final char MOVE_RIGHT = '\u0004';
public static final char MOVE_DOWN = '\u0003'; // ETX
public static final char MOVE_UP = '\u0002'; // STX
public static final char SEND = '\u0001';

public static final char LF0 = '\u2029';
public static final char BULLET = '\u2022';
public static final char NBH0 = '\u2010';
public static final char ZW0 = '\u200b';
public static final char PAR = '\u00b6';
public static final char SH0 = '\u00ad';
public static final char SECT = '\u00a7';
public static final char NBS0 = '\u00a0';

public static final char SH = '\u001f';
public static final char NBS = '\u001c';
public static final char NBH = '\u0016';
//public static final char TEMPLATE_END = SCROLL_RIGHT; // DLE 
public static final char TEMPLATE_END = SCROLL_DOWN; // SI (SO SI = stop last level's template)
public static final char TEMPLATE_START = SCROLL_UP; // SO (SO template SI = apply template)
public static final char ZW = '\u0006';
public static final char LIST_END = MOVE_LEFT;
public static final char LIST_START = MOVE_RIGHT; // (not within TEMPLATE OR QUOTE)
public static final char QUOTE_END = MOVE_DOWN; // ETX
public static final char QUOTE_START = MOVE_UP; // STX (not within TEMPLATE)

public static final String XCHARS = "" + NBS0 + SECT + SH0 + PAR + ZW0 + NBH0 + LF0 + DELTA_X;

///// 

private static final byte[] kAlpha1345Caps = (//-
/* ---!"#$%&'()*+,-./0123456789:;<=>?----- U:alpha, OO: blank, Ox: RFU                              
/* */"INMSPAYCZXHJFTV0123456789BRKEDQ").getBytes( UtilString.STR256 );
private static final char[] kAlpha1345Cap2Sym = new char[ 'Z' + 1 ];
private static final HashMap< String, Character > kRfc1345ToChar = new HashMap< String, Character >();
private static final HashMap< Character, String > kChar2Rfc1345 = new HashMap< Character, String >();

private final static char[] HEX = "0123456789ABCDEF".toCharArray();
private final static byte[] NIBBLE = { 9, 10, 11, 12, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, //-
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

/*
private static byte[] Pc8BordersOffsB0_2_UnicodePage25 = {
	0x91, 0x92, 0x93, 0x02, 0x24, 0x61, 0x62, 0x56,
	0x55, 0x63, 0x51, 0x57, 0x5d, 0x5c, 0x5b, 0x10,
	0x14, 0x34, 0x2c, 0x1c, 0x00, 0x3c, 0x5e, 0x5f,
	0x5a, 0x54, 0x69, 0x66, 0x60, 0x50, 0x6c, 0x67,
	0x68, 0x64, 0x65, 0x59, 0x58, 0x52, 0x53, 0x6b,
	0x6a, 0x18, 0x0c, 0x88, 0x84, 0x8c, 0x90, 0x80,
};
*/

private static char[] Cp125xOffs7f_2_Unicode = {
	0x0394, // 0x7F DELTA/ HOUSE
	0x20AC, // 0x80 EURO SIGN
	0x067E, // 0x81 A81
	0x201A, // 0x82 SINGLE LOW-9 QUOTATION MARK
	0x0192, // 0x83 LATIN SMALL LETTER F WITH HOOK
	0x201E, // 0x84 DOUBLE LOW-9 QUOTATION MARK
	0x2026, // 0x85 HORIZONTAL ELLIPSIS
	0x2020, // 0x86 DAGGER
	0x2021, // 0x87 DOUBLE DAGGER
	0x02C6, // 0x88 MODIFIER LETTER CIRCUMFLEX ACCENT
	0x2030, // 0x89 PER MILLE SIGN
	0x0160, // 0x8A LATIN CAPITAL LETTER S WITH CARON
	0x2039, // 0x8B SINGLE LEFT-POINTING ANGLE QUOTATION MARK
	0x0152, // 0x8C LATIN CAPITAL LIGATURE OE
	0x0686, // 0x8D A8D
	0x0698, // 0x8E A8E
	0x0688, // 0x8F A8F
	0x06AF, // 0x90 A90
	0x2018, // 0x91 LEFT SINGLE QUOTATION MARK
	0x2019, // 0x92 RIGHT SINGLE QUOTATION MARK
	0x201C, // 0x93 LEFT DOUBLE QUOTATION MARK
	0x201D, // 0x94 RIGHT DOUBLE QUOTATION MARK
	0x2022, // 0x95 BULLET
	0x2013, // 0x96 EN DASH
	0x2014, // 0x97 EM DASH
	0x02DC, // 0x98 SMALL TILDE
	0x2122, // 0x99 TRADE MARK SIGN
	0x0161, // 0x9A LATIN SMALL LETTER S WITH CARON
	0x203A, // 0x9B SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
	0x0153, // 0x9C LATIN SMALL LIGATURE OE
	0x200C, // 0x9D ZWNJ
	0x200D, // 0x9E ZWJ
	0x0178, // 0x9F LATIN CAPITAL LETTER Y WITH DIAERESIS
};

static {
	for (int i0 = kAlpha1345Caps.length - 1; i0 >= 0; -- i0) {
		kAlpha1345Cap2Sym[ kAlpha1345Caps[ i0 ] ] = (char) (0x21 + i0);
	}
	for (int i0 = Rfc1345.kRfc1345.length - 2; i0 >= 0; i0 -= 2) {
		kRfc1345ToChar.put( Rfc1345.kRfc1345[ i0 ], Rfc1345.kRfc1345[ i0 + 1 ].charAt( 0 ) );
		kChar2Rfc1345.put( Rfc1345.kRfc1345[ i0 + 1 ].charAt( 0 ), Rfc1345.kRfc1345[ i0 ] );
	}
}

private static int findChars( String chars, char[] in, int start ) {
	final int last = in.length - chars.length();
	final char ch = chars.charAt( 0 );
	for (int i0 = start; i0 <= last; ++ i0) {
		if (ch == in[ i0 ]) {
			if (1 >= chars.length()) {
				return i0;
			}
			int ix = 1;
			for (; ix < chars.length(); ++ ix) {
				if (chars.charAt( ix ) != in[ i0 + ix ]) {
					break;
				}
			}
			if (ix >= chars.length()) {
				return i0 + chars.length() - 1;
//			} else if (immediate) {
//				return -1;
			}
		}
	}
	return -1;
}

private static int findChar( char ch, char[] in, int start ) {
	final int len = in.length;
	for (int i0 = start; i0 < len; ++ i0) {
		if (ch == in[ i0 ]) {
			return i0;
		}
	}
	return -1;
}

public static char char4Rfc1345( String rfc ) {
	Character out = kRfc1345ToChar.get( rfc );
	return (null == out) ? (0 >= rfc.length() ? (char) 0 : rfc.charAt( rfc.length() - 1 )) : (char) out;
}

private static int group4Rfc1345_start = 0;
private static char[] group4Rfc1345_0x2600 = { 0x2606, 0x260f, 0x2610, 0x2611, 0x2612, 0x2620, 0x2622,
	0x262e, 0x262f, 0x2639, 0x263a, 0x2640, 0x2641, 0x2642, 0x2690, 0x2692, 0x2695, 0x2696,
};

public static String group4Rfc1345( char xChar ) {
	StringBuilder out = new StringBuilder( 128 + 10 );
	if (0 >= group4Rfc1345_start) {
		for (int i0 = 1; i0 < Rfc1345.kRfc1345.length; i0 += 2) {
			if (Rfc1345.kRfc1345[ i0 ].charAt( 0 ) == ' ') {
				group4Rfc1345_start = i0 - 1;
				break;
			}
		}
	}
	if (' ' < xChar) {
		char mnemonicFirst = 0;
		int i0 = group4Rfc1345_start + 1;
		if ('0' > xChar) {
			mnemonicFirst = xChar;
		} else {
			for (; i0 < Rfc1345.kRfc1345.length; i0 += 2) {
				final char c0 = Rfc1345.kRfc1345[ i0 ].charAt( 0 );
				if (c0 == xChar) {
					mnemonicFirst = Rfc1345.kRfc1345[ i0 - 1 ].charAt( 0 );
					break;
				}
			}
		}
		if (' ' < mnemonicFirst) {
			out.append( mnemonicFirst );
		}
		int loop = 1;
		if (('a' <= mnemonicFirst) && (mnemonicFirst <= 'z')) {
			loop = 2;
			out.append( (char) (mnemonicFirst - 0x20) );
		} else if (('A' <= mnemonicFirst) && (mnemonicFirst <= 'Z')) {
			loop = 2;
			out.append( (char) (mnemonicFirst + 0x20) );
		}
		if (0 < mnemonicFirst) {
			int stop = i0;
			int start = i0 + 1;
			for (; loop > 0; -- loop) {
				for (i0 = start; i0 < Rfc1345.kRfc1345.length; i0 += 2) {
					if ((Rfc1345.kRfc1345[ i0 ].charAt( 0 ) == mnemonicFirst) && (Rfc1345.kRfc1345[ i0 + 1 ].charAt( 0 ) != mnemonicFirst)) {
						out.append( Rfc1345.kRfc1345[ i0 + 1 ].charAt( 0 ) );
					}
				}
				for (i0 = group4Rfc1345_start + 2; i0 < stop; i0 += 2) {
					if ((Rfc1345.kRfc1345[ i0 ].charAt( 0 ) == mnemonicFirst) && (Rfc1345.kRfc1345[ i0 + 1 ].charAt( 0 ) != mnemonicFirst)) {
						out.append( Rfc1345.kRfc1345[ i0 + 1 ].charAt( 0 ) );
					}
				}
				mnemonicFirst += ('a' <= mnemonicFirst) ? -0x20 : 0x20;
			}
		}
	}
	char base = ' ';
	switch (xChar) {
		case ' ':
			base = ' ' + 1;
			break;
		case '.':
			base = 'Z' + 1;
			break;
		case '\\':
			for (char ch : group4Rfc1345_0x2600) {
				out.append( ch );
			}
			base = 0x25a0;
			break;
		default:
			if ('A' > xChar) {
				for (int i0 = 0; i0 < Rfc1345.kBasic80.length; ++ i0) {
					if (('0' > Rfc1345.kBasic80[ i0 ]) && ((xChar & 0xf) == (Rfc1345.kBasic80[ i0 ] & 0xf))) {
						base = Rfc1345.kBasic80[ i0 + 1 ];
						break;
					}
				}
			}
	}
	if ((' ' < base) || (0x80 > xChar)) {
		int count = 100 - out.length();
		for (; (count > 0) && (base < 0x80); -- count, ++ base) {
			switch (base) {
				case '0':
					base = '9' + 1;
					break;
				case 'A':
					base = 'Z' + 1;
					break;
				case 'a':
					base = 'z' + 1;
					break;
				case DELTA_X:
					base = NBS0 + 1;
					break;
//				case SH0:
//					base = SH0 + 1;
//					break;
//				case 0xc0:
//					base = 0xd7;
//					break;
//				case 0xd9:
//					base = 0xf7;
//					break;
//				case 0xf9:
//					base = 0x100;
//					break;
				default:
					;
			}
			out.append( base );
		}
		int i0 = 0;
		for (; i0 < Rfc1345.kBasic80.length; ++ i0) {
			if (base <= Rfc1345.kBasic80[ i0 ]) {
				i0 = (i0 < Rfc1345.kBasic80.length) ? i0 : 0;
				break;
			}
		}
		for (boolean all = false; count > 0; -- count) {
			if (0x80 <= Rfc1345.kBasic80[ i0 ]) {
				if (all) {
					all = false;
					for (char ch0 = (char) (Rfc1345.kBasic80[ i0 - 2 ] + 1); ch0 < Rfc1345.kBasic80[ i0 ]; ++ ch0) {
						out.append( ch0 );
						-- count;
					}
				}
				out.append( Rfc1345.kBasic80[ i0 ] );
			} else if (0 == Rfc1345.kBasic80[ i0 ]) {
				all = true;
			}
			++ i0;
			i0 = (i0 < Rfc1345.kBasic80.length) ? i0 : 0;
		}
	} else {
		base = (char) ((xChar + 1) & 0x3fff);
		base = (0x100 > base) ? (char) (0x1ff - base) : base;
		for (int count = out.capacity() - out.length(); count > 0; -- count, ++ base) {
			out.append( base );
		}
	}
	return out.toString();
}

private static char char4Rfc1345( char[] rfc, int start, int end ) {
	char[] tmp = new char[ end - start ];
	System.arraycopy( rfc, start, tmp, 0, end - start );
	return char4Rfc1345( new String( tmp ) );
}

private static int dropZeros( char[] str, int len ) {
	int i0 = 0;
	for (; i0 < len; ++ i0) {
		if (0 == str[ i0 ]) {
			break;
		}
	}
	int put = i0 ++;
	for (; i0 < len; ++ i0) {
		if (0 != str[ i0 ]) {
			str[ put ++ ] = str[ i0 ];
		}
	}
	return put;
}

private static int replaceRfc1345( char[] xyMnem, int start, int end ) {
	int out = start;
	for (int i0 = start; i0 < end; ++ i0) {
		if (' ' == xyMnem[ i0 ]) {
			final char ch0 = char4Rfc1345( xyMnem, out, i0 );
			if (0 == ch0) {
				out = i0;
			} else {
				xyMnem[ out ++ ] = ch0;
			}
			for (; out <= i0; ++ out) {
				xyMnem[ out ] = 0;
			}
		}
	}
	final char ch0 = char4Rfc1345( xyMnem, out, end );
	if (0 == ch0) {
		return end;
	}
	xyMnem[ out ] = ch0;
	return out + 1;
//	for (++ out; out <= end; ++ out) {
//		xyMnem[ out ] = 0;
//	}
}

private static int replaceAlpha1345( char[] sub, int start, int end ) {
	boolean replace = false;
	boolean alpha = false;
	int put = start;
	for (int i0 = start; i0 < end; ++ i0) {
		if ('_' == sub[ i0 ]) {
			if (replace) {
				sub[ start ] = (put <= start) ? (char) 0 : char4Rfc1345( sub, start, put );
				put = (put <= start) ? start : ++ start;
			} else if (((i0 + 1) < end) && ('O' == sub[ i0 + 1 ])) {
				++ i0;
				if ((++ i0) < end) {
					switch (sub[ i0 ]) {
						case 0:
							break;
						case 'O':
							sub[ put ++ ] = ' ';
							break;
						default:
							sub[ put ++ ] = '.';
					}
				}
			}
			replace = !replace;
			start = put;
		} else if (!replace) {
			sub[ put ++ ] = sub[ i0 ];
		} else if (alpha) {
			alpha = false;
			sub[ put ++ ] = sub[ i0 ];
		} else if ('U' == sub[ i0 ]) {
			alpha = true;
		} else if (('Z' >= sub[ i0 ]) && ('A' <= sub[ i0 ])) {
			sub[ put ++ ] = kAlpha1345Cap2Sym[ sub[ i0 ] ];
		} else {
			sub[ put ++ ] = sub[ i0 ];
		}
	}
	if (replace && (put > start)) {
		sub[ start ] = char4Rfc1345( sub, start, put );
		return start + 1;
//		put=++start;
	}
//	start=put;
//	for (; put < end; ++ put) {
//		sub[ put ] = 0;
//	}
//	return start;
	return put;
}

//private static String str4Alpha1345( char[] sub, int start, int end ) {
//	StringBuilder out = new StringBuilder( end - start );
//	StringBuilder tmp = new StringBuilder( 5 );
//	boolean alpha = false;
//	for (int i0 = start; i0 < end; ++ i0) {
//		if (alpha) {
//			alpha = false;
//			tmp.append( sub[ i0 ] );
//		} else if ('U' == sub[ i0 ]) {
//			alpha = true;
//		} else if ('O' == sub[ i0 ]) {
//			if (0 < tmp.length()) {
//				out.append( char4Rfc1345( tmp.toString() ) );
//				tmp = new StringBuilder( 5 );
//			}
//		} else if (('Z' >= sub[ i0 ]) && ('A' <= sub[ i0 ])) {
//			tmp.append( kCap2Sym[ sub[ i0 ] ] );
//		} else {
//			tmp.append( sub[ i0 ] );
//		}
//	}
//	if (0 < tmp.length()) {
//		out.append( char4Rfc1345( tmp.toString() ) );
//		tmp = new StringBuilder( 5 );
//	}
//	return out.toString();
//}

public static String string4Alpha1345( String sub ) {
	char[] a0 = sub.toCharArray();
	int len = replaceAlpha1345( a0, 0, a0.length );
//	int len = dropZeros( a0, a0.length );
	return new String( a0, 0, len );
}

private static final long REP_ZERO = 1;
private static final long REP_QUOTE = 2;
private static final long REP_SEQ = 4;
private static final long REP_LF = 8;
private static final long REP_PRINTABLE = 0x10;

private static void replaceCtrlNOld( char[] text, int start, int end, long flagsZeroQuoteSeqLfPrn ) {
	for (int i0 = start; i0 < end; ++ i0) {
		if (' ' <= text[ i0 ]) {
			if (0x7f > text[ i0 ]) {
				continue;
			} else if (ZW0 > text[ i0 ]) {
				if (0xA0 > text[ i0 ]) {
					text[ i0 ] = Cp125xOffs7f_2_Unicode[ text[ i0 ] - 0x7f ];
					continue;
				} else if ((SH0 != text[ i0 ]) && (NBS0 != text[ i0 ])) {
					continue;
				}
			} else if (LF0 < text[ i0 ]) {
				continue;
			}
		}
		switch (text[ i0 ]) {
			case 0:
				text[ i0 ] = (0 != (REP_ZERO & flagsZeroQuoteSeqLfPrn)) ? ' ' : (char) 0;
				break;
			case TAB:
				break;
			case CR: // TODO '\r\n' -> ' '
			case LF:
				text[ i0 ] = (0 != (REP_LF & flagsZeroQuoteSeqLfPrn)) ? ' ' : (char) 0;
			case FF:
			case LF0:
				text[ i0 ] = ' ';
				break;
			case BULLET_X:
				text[ i0 ] = BULLET;
				break;
			case PAR_X:
				text[ i0 ] = PAR;
				break;
			case SECT_X:
				text[ i0 ] = SECT;
				break;
			case DELTA_X:
				text[ i0 ] = Cp125xOffs7f_2_Unicode[ 0 ];
				break;
			case NBS:
			case NBS0:
				text[ i0 ] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? ' ' : NBS;
				break;
			case SH:
			case SH0:
				text[ i0 ] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? ZW0 : SH;
				break;
			case ZW:
			case ZW0:
				text[ i0 ] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? ZW0 : ZW;
				break;
			case NBH:
			case NBH0:
				text[ i0 ] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? '-' : NBH;
				break;
			case QUOTE_START:
				text[ i0 ] = (0 != (REP_QUOTE & flagsZeroQuoteSeqLfPrn)) ? '\u00a8' : text[ i0 ];
				break;
			case QUOTE_END:
				text[ i0 ] = (0 != (REP_QUOTE & flagsZeroQuoteSeqLfPrn)) ? '\u00a8' : text[ i0 ];
				break;
			case LIST_START:
				text[ i0 ] = (0 != (REP_SEQ & flagsZeroQuoteSeqLfPrn)) || (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? '\u00af'
					: text[ i0 ];
				break;
			case LIST_END:
				text[ i0 ] = (0 != (REP_SEQ & flagsZeroQuoteSeqLfPrn)) || (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? '\u00af'
					: text[ i0 ];
				break;
			case TEMPLATE_START:
				text[ i0 ] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? '\u00aa' : text[ i0 ];
				break;
			case TEMPLATE_END:
				text[ i0 ] = (0 != (REP_PRINTABLE & flagsZeroQuoteSeqLfPrn)) ? '\u00aa' : text[ i0 ];
				break;
			default:
				text[ i0 ] = (' ' <= text[ i0 ]) ? text[ i0 ] : '_';
		}
	}
}

public static String makePrintable( String data ) {
	int len = data.length();
	char[] out = data.toCharArray();
	replaceCtrlNOld( out, 0, len, 0xff );
	return new String( out, 0, len );
}

public static void replaceCtrlNOld( char[] text, int start, int end ) {
	replaceCtrlNOld( text, start, end, 0 );
}

public static boolean equalsRoughly( String str0, String str1 ) {
	if ((null == str0) || (null == str1)) {
		return str0 == str1;
	}
	int i0 = 0, i1 = 0;
	char[] tmp = new char[ 1 ];
	for (; (i0 < str0.length()) && (i1 < str1.length()); ++ i0, ++ i1) {
		final char ch0 = str0.charAt( i0 );
		final char ch1 = str1.charAt( i1 );
		if (ch0 == ch1) {
		} else if ((' ' >= ch0) && (' ' >= ch1)) {
		} else if ((0 <= XCHARS.indexOf( ch0 )) && (0 <= XCHARS.indexOf( ch1 ))) {
		} else if (0 <= XCHARS.indexOf( ch0 )) {
			if (' ' <= ch1) {
				break;
			}
		} else if (0 <= XCHARS.indexOf( ch1 )) {
			if (' ' <= ch0) {
				break;
			}
		} else if (' ' >= ch0) {
			tmp[ 0 ] = ch0;
			replaceCtrlNOld( tmp, 0, 1, ~0L );
			if (ch1 != tmp[ 0 ]) {
				-- i1;
			}
		} else if (' ' >= ch1) {
			tmp[ 0 ] = ch1;
			replaceCtrlNOld( tmp, 0, 1, ~0L );
			if (ch0 != tmp[ 0 ]) {
				-- i0;
			}
		} else {
			break;
		}
	}
	return !((i0 < str0.length()) && (i1 < str1.length()));
}

private static char[] char4Escape_map = new char[ 'Z' + 1 ];

public static char char4Escape( char c0, char c1 ) {
	char out = ('^' == c0) ? c1 : c0;
	if (('Z' >= out) && ('@' <= out)) {
		out = (char) (out - '@');
	} else if ('a' <= out) {
		if (10 != char4Escape_map[ 'N' ]) {
			char4Escape_map[ 'A' ] = 7;
			char4Escape_map[ 'B' ] = 8;
			char4Escape_map[ 'E' ] = 27;
			char4Escape_map[ 'F' ] = 12;
			char4Escape_map[ 'N' ] = 10;
			char4Escape_map[ 'R' ] = 13;
			char4Escape_map[ 'T' ] = 9;
			char4Escape_map[ 'V' ] = 11;
		}
		out = char4Escape_map[ out - ('a' - 'A') ];
	} else {
		switch (out) {
			case '{':
			case '}':
				out = 0x80;
				break;
			case '<':
				out = '{';
				break;
			case '>':
				out = '}';
				break;
			default:
				;
		}
	}
	return ('^' == c0) ? ((char) (0x80 | out)) : out;
}

private static int replaceMnemonicsRaw_OLD( char[] out, int len ) {
	///// QUOTE
	for (int i0 = 1; i0 < len; ++ i0) {
		if (('{' == out[ i0 ]) && ('{' == out[ i0 - 1 ])) {
			int level = 1;
			int start = i0;
			for (i0 += 2; i0 < len; ++ i0) {
				if (('{' == out[ i0 ]) && ('{' == out[ i0 - 1 ])) {
					++ level;
				} else if (('}' == out[ i0 ]) && ('}' == out[ i0 - 1 ])) {
					-- level;
					if (0 == level) {
						break;
					}
				}
			}
			replaceCtrlNOld( out, start + 1, i0 - 1, REP_SEQ );
			out[ start ] = 0;
			out[ start - 1 ] = QUOTE_START;
			if (i0 < len) {
				out[ i0 ] = QUOTE_END;
				out[ i0 - 1 ] = 0;
			}
		}
	}
	///// SEQ and others
	int lastQuote = 0;
	for (int i0 = 0; i0 < len; ++ i0) {
		if (QUOTE_START == out[ i0 ]) {
			replaceCtrlNOld( out, lastQuote, i0, 0 );
			for (; i0 < len; ++ i0) {
				if (QUOTE_END == out[ i0 ]) {
					break;
				}
			}
			lastQuote = i0 + 1;
			continue;
		} else if (0 <= "{}[]".indexOf( out[ i0 ] )) {
			if ((i0 + 1) >= len) {
				break;
			} else if (out[ i0 ] == out[ i0 + 1 ]) {
				switch (out[ i0 ]) {
					case '[':
						out[ i0 ] = LIST_START;
						break;
					case ']':
						out[ i0 ] = LIST_END;
						break;
					case '}':
						out[ i0 ] = QUOTE_END;
						break;
					default:
						;
				}
				out[ ++ i0 ] = 0;
			} else if ('_' == out[ i0 + 1 ]) {
				if (((i0 + 2) < len) && ('{' == out[ i0 ]) && ('{' == out[ i0 + 2 ])) {
					///// quasi-quote
					out[ i0 ] = 0;
					out[ ++ i0 ] = 0;
					out[ ++ i0 ] = QUOTE_START;
				}
			}
			continue;
		} else if (('_' != out[ i0 ]) || ((i0 + 1) >= len)) {
			continue;
		} else {
			///// Escape ('_')
			out[ i0 ++ ] = 0;
			if (' ' >= out[ i0 ]) {
				///// nil
				while ((' ' > out[ i0 ]) && ((i0 + 1) < len) && (' ' > out[ i0 + 1 ])) {
					///// CR-LF etc.
					out[ i0 ++ ] = 0;
				}
				out[ i0 ] = 0;
				continue;
			}
			switch (out[ i0 ]) {
				case '_': // escaped
				case '}':
					++ i0;
					i0 -= ((i0 < len) && ('_' == out[ i0 ])) ? 1 : 0;
					continue;
				case '{': // template
					out[ i0 ] = TEMPLATE_START;
					for (; (i0 < len) && ('}' != out[ i0 ]); ++ i0) {
					}
					if (i0 < len) {
						out[ i0 ] = TEMPLATE_END;
					}
					if (((i0 + 1) < len) && ('_' == out[ i0 + 1 ])) {
						out[ ++ i0 ] = 0;
					}
					continue;
				case '[': // RFC1345
					out[ i0 ] = 0;
					int start = ++ i0;
					for (; (i0 < len) && (']' != out[ i0 ]); ++ i0) {
					}
					if (i0 >= len) {
						// Next char could be RFU anyway.
						i0 = start;
						continue;
					}
					out[ i0 ] = 0;
					if ('_' == out[ start ]) {
						start = replaceAlpha1345( out, start, i0 );
					} else { // '*'
						out[ start ] = 0;
						start = replaceRfc1345( out, start + 1, i0 );
					}
					if (((i0 + 1) < len) && ('_' == out[ i0 + 1 ])) {
						++ i0;
					}
					for (; start <= i0; ++ start) {
						out[ start ] = 0;
					}
					continue;
				default:
					;
			}
			int lookahead = i0 + 1;
			for (; (lookahead <= (i0 + 7)) && (lookahead < len); ++ lookahead) {
				if ('_' == out[ lookahead ]) {
					lookahead = -lookahead;
					break;
				}
			}
			if (lookahead < len) {
				lookahead = -lookahead;
				if ((0 >= lookahead) || (lookahead >= len)) {
					continue;
				}
			} else if ((i0 + 1) >= len) {
				continue;
			}
			switch (out[ i0 ]) {
				case '#': // unicode hex
					int v0 = 0;
					out[ i0 ] = 0;
					for (++ i0; i0 < lookahead; ++ i0) {
						final int b0 = 0xff & NIBBLE[ out[ i0 ] & 0x1f ];
						v0 = (v0 << 4) | b0;
						out[ i0 ] = 0;
					}
					out[ lookahead - 1 ] = (char) v0;
					break;
				case '^': // control char
					out[ i0 ] = char4Escape( out[ i0 + 1 ], out[ lookahead - 1 ] );
					out[ ++ i0 ] = 0;
					// Potential override is okay:
					out[ lookahead - 1 ] = 0;
					out[ (lookahead >= len) ? i0 : lookahead ] = 0;
					continue;
				default:
					;
			}
			if (i0 < lookahead) {
				i0 = replaceRfc1345( out, i0, lookahead );
				for (; i0 < lookahead; ++ i0) {
					out[ i0 ] = 0;
				}
			}
			if (lookahead < len) {
				out[ lookahead ] = 0;
			}
		}
	}
//	replaceCtrlNOld( out, lastQuote, len, 0 );
	return dropZeros( out, len );
}

/** Normalize string with mnemonics. '_' rfc1345 (without '_') '_',
 * '__' -> '_', '_ ' -> '', '_[*' rfc1345 (any) ']_', '_[_' alpha1345 ']_'
 * '{{' quote '}}', '[[' sequence ']]', '{_{' quasi-quote '}}'
 * '_{' format/template '}_' for text with/ without '_{}_', '_}' => '}'
 * '_#' NN/NNNN unicode hex '_': , '_^' n/r/t/@/A/... control char  ('^^': +0x80) '_'
 * 
 * '{_'/ '[_': special char./ escape, 
 * '{_ {' => '{{'
 * '}_}' => "}}", ']_]' => "]]", '{__' => "{_",
 * 
 * // '{_{' => "{{", '[_[' => "[["
 * '{_<' format/template '>}' text with/ without '{_<>}'.
 * '{_nnnn' unicode hex '}', '{_n/r/@/A...}' escaped char ('^': + 0x80),
 * '{_/_' sub4rfc1345 '}', '{_/' rfc1345 '}'.
 * @param textWithMnem
 * @return
 */
public static String str4Mnemonics_OLD( String textWithMnem ) {
	int first = 0;
	int len = textWithMnem.length();
	if (!textWithMnem.contains( "{{" ) && !textWithMnem.contains( "[[" ) && (0 > textWithMnem.indexOf( '_' ))) {
		///// Fast lane?
		for (; first < len; ++ first) {
			final char ch0 = textWithMnem.charAt( first );
			if ((' ' > ch0) || (0x7f <= ch0)) {
				if ((0x100 <= ch0) || (SH0 == ch0) || (NBS0 >= ch0)) {
					break;
				}
			}
		}
		if (first >= len) {
			return textWithMnem;
		}
	}
	final char[] out = textWithMnem.toCharArray();
	len = replaceMnemonicsRaw_OLD( out, len );
	replaceCtrlNOld( out, 0, len, 0 );
	if (QUOTE_START == out[ 0 ]) {
		int level = 1;
		for (int i0 = 1; i0 < len; ++ i0) {
			if (QUOTE_START == out[ i0 ]) {
				++ level;
			} else if (QUOTE_END == out[ i0 ]) {
				-- level;
				if (0 >= level) {
					++ i0;
					if (i0 >= len) {
						return new String( out, 1, len - 2 );
					}
					break;
				}
			}
		}
	}
	return new String( out, 0, len );
}

/** Replace mnemonics in string. '_' rfc1345 (without '_') '_',
 * '__' -> '_', '_ ' -> '', '_[*' rfc1345 (any) ']_', '_[_' alpha1345 ']_',
 * '_//' quote '//_': for escaping mnemonics,
 * '_{{' quasi-quote '}}_' : '_{{' > STX, '}}_' > ETX
 * '_[[' list ']]_' : '_[[' > EOT, ']]_' > ENQ
 * '_<' template '>_' : '_<' > SO, '>_' > SI
 * '_#' NN/NNNN unicode hex '_': , '_^' n/r/t/@/A/... control char  ('^^': +0x80) '_'
 * Note: RFC1345 does not use '_' in combination with '{}[]()<>/' or '#...'.
*/
public static String string4Mnemonics( String mnem ) {
	if (0 > mnem.indexOf( '_' )) {
		return mnem;
	}
	final char[] out = mnem.toCharArray();
	int len = out.length;
	int nextQuotedChunk = -1;
	for (int i0 = 0; i0 < len; ++ i0) {
		if ('_' != out[ i0 ]) {
			continue;
		}
		if (i0 >= nextQuotedChunk) {
			len = mnem.indexOf( "_//", i0 ); // quote
			if (0 > len) {
				len = out.length;
			} else if (i0 < len) {
				++ len;
			} else {
				///// Skip the quote.
				out[ i0 ++ ] = 0;
				out[ i0 ++ ] = 0;
				out[ i0 ++ ] = 0;
				len = mnem.indexOf( "//_", i0 );
				if (i0 <= len) {
					i0 = len;
					out[ i0 ++ ] = 0;
					out[ i0 ++ ] = 0;
					out[ i0 ] = 0;
				}
				len = 1 + mnem.indexOf( "_//", i0 ); // next one
				if (0 >= len) {
					len = out.length;
				}
			}
			nextQuotedChunk = len;
		}
		if ((i0 + 1) >= out.length) {
			continue;
		}
		out[ i0 ++ ] = 0;
		int ix;
		int v0;
		switch (out[ i0 ]) {
			case '_':
				continue;
			case ' ':
				out[ i0 ] = 0;
				continue;
			case '#': // unicode hex
				out[ i0 ] = 0;
				if (0 > (ix = findChar( '_', out, i0 ))) {
					continue;
				} else if ((4 + 1) < (ix - i0)) {
					continue;
				}
				v0 = 0;
				for (++ i0; i0 < ix; ++ i0) {
					final int b0 = 0xff & NIBBLE[ out[ i0 ] & 0x1f ];
					v0 = (v0 << 4) | b0;
					out[ i0 ] = 0;
				}
				out[ i0 - 1 ] = (char) v0;
				out[ i0 ] = 0;
				continue;
			case '^': // control char
				out[ i0 ] = 0;
				if (0 > (ix = findChar( '_', out, i0 ))) {
					continue;
				}
				out[ i0 ] = char4Escape( out[ i0 + 1 ], out[ ix - 1 ] );
				out[ ++ i0 ] = 0;
				i0 = ix;
				// Potential override is okay:
				out[ i0 - 1 ] = 0;
				out[ i0 ] = 0;
				continue;
			case '{': // quote
//				if (0 > (ix = findChars( "}_", out, i0 ))) {
//					continue;
//				}
//				out[ i0 ] = TEMPLATE_START;
//				i0 = ix;
//				out[ i0 - 1 ] = TEMPLATE_END;
//				out[ i0 ] = 0;
//				continue;
			case '<': // template
			case '[': // RFC1345 or list
//				if (0 > (ix = findChars( "]_", out, i0 ))) {
				if (0 > (ix = findChars( "" + (char) (out[ i0 ] + 2) + "_", out, i0 ))) {
					continue;
				}
				if ((out[ i0 + 1 ] == out[ i0 ]) || ('<' == out[ i0 ])) {
					if (out[ ix - 2 ] == out[ ix - 1 ]) {
						out[ ix - 2 ] = 0;
					}
					// Might not be the matching one!:
					out[ ix - 1 ] = ('<' == out[ i0 ]) ? TEMPLATE_END
						: (('[' == out[ i0 ]) ? LIST_END : QUOTE_END);
					out[ ix ] = 0;
					if (out[ i0 + 1 ] == out[ i0 ]) {
						out[ i0 + 1 ] = 0;
					}
					out[ i0 ] = ('<' == out[ i0 ]) ? TEMPLATE_START
						: (('[' == out[ i0 ]) ? LIST_START : QUOTE_START);
					++ i0;
					continue;
				} else if ('[' != out[ i0 ]) {
//					|| (('_' != out[ i0 + 1 ]) && ((ix - i0) > 6))) {
					continue;
				}
				out[ ix -- ] = 0;
				out[ i0 ++ ] = 0;
				break;
			default:
				if (' ' > out[ i0 ]) {
					out[ i0 ] = 0;
					continue;
				}
				// RFC1345 wo '_'
				if (0 > (ix = findChar( '_', out, i0 ))) {
					continue;
				}
		}
		if ('_' == out[ i0 ]) {
			i0 = replaceAlpha1345( out, i0, ix );
		} else { // '*', or wo '_'
			i0 = replaceRfc1345( out, i0, ix );
		}
		for (; i0 < ix; ++ i0) {
			out[ i0 ] = 0;
		}
		out[ i0 ] = 0;
	}
	final int len2 = dropZeros( out, len );
	return new String( out, 0, len2 );
}

public static String mnemonics4String( String text, boolean leaveLfTab, boolean leaveExotic ) {
	int i0 = 0;
	int lenLine = 0;
	for (; i0 < text.length(); ++ i0, ++ lenLine) {
		final char ch = text.charAt( i0 );
		if ((78 < lenLine) && !leaveLfTab) {
			break;
		}
		if ((' ' > ch)) {
			if ('\n' == ch) {
				lenLine = -1;
			} else if ('\t' != ch) {
				break;
			}
			if (leaveLfTab) {
				continue;
			}
			// TODO: STX ETX SLS etc.
			break;
		} else if ((0x7f <= ch) || ('_' == ch)) { // (0 <= "_{}[]".indexOf( ch ))) {
			break;
		}
	}
	if (i0 >= text.length()) {
		return text;
	}
	char[] tmp = new char[ 1 ];
	StringBuilder out = new StringBuilder( text.length() * 2 );
	out.append( text.substring( 0, i0 ) );
	int last = i0;
	final int len = text.length();
	for (; i0 < len; ++ i0, ++ lenLine) {
		char ch = text.charAt( i0 );
		boolean breakLine = (77 < lenLine) && !leaveLfTab;
		if (!breakLine && (70 <= lenLine)) {
			breakLine = ((' ' > ch) || (0x7f <= ch) || ('_' == ch)); // (0 <= "_{}[]".indexOf( ch )));
		}
		if (breakLine) {
			for (int stop = ((i0 - 40) >= last) ? (i0 - 20) : last; i0 > stop; -- i0) {
				if (' ' >= ch) {
					break;
				}
			}
			out.append( text.substring( last, i0 ) );
			last = i0;
			out.append( '\n' );
			lenLine = -1;
			-- i0;
			continue;
		}
		if ((' ' > ch)) {
			if (('\n' == ch) || ('\t' == ch)) {
				if (leaveLfTab) {
					lenLine = ('\n' == ch) ? -1 : lenLine;
					continue;
				}
				out.append( text.substring( last, i0 ) );
				last = i0 + 1;
				if ('\n' == ch) {
					out.append( "_^n_\n" );
				} else if (40 > lenLine) {
					out.append( "_^t_" );
					lenLine += 3;
					continue;
				} else {
					out.append( "_^t_\n" );
				}
				lenLine = -1;
				continue;
			}
			if (!leaveExotic) {
				tmp[ 0 ] = ch;
				replaceCtrlNOld( tmp, 0, 1, 0x7 );
				if (' ' < tmp[ 0 ]) {
					ch = tmp[ 0 ];
				}
			}
		} else if (0x7f > ch) {
			if ('_' == ch) {
				out.append( text.substring( last, i0 + 1 ) );
				last = i0 + 1;
				++ lenLine;
				out.append( '_' );
				continue;
			}
			continue;
		}
		out.append( text.substring( last, i0 ) );
		last = i0 + 1;
		if (' ' < ch) {
			String repl = kChar2Rfc1345.get( ch );
			if ((null != repl) && (1 < repl.length()) && (0 > repl.indexOf( '_' ))) {
				out.append( '_' );
				out.append( repl );
				out.append( '_' );
				lenLine += 1 + repl.length();
				continue;
			}
		}
		out.append( "_#" );
		if (0x100 <= ch) {
			out.append( HEX[ (ch & 0xf000) >>> 12 ] );
			out.append( HEX[ (ch & 0xf00) >>> 8 ] );
		}
		out.append( HEX[ (ch & 0xf0) >>> 4 ] );
		out.append( HEX[ ch & 0xf ] );
		out.append( '_' );
		lenLine += (0x100 <= ch) ? 6 : 4;
	}
	out.append( text.substring( last, i0 ) );
	return out.toString();
}

/** Transform text string (with quote markers STX/ETX) into marked text atoms.
 */
public static String[] markedAtoms4String( String xStr ) {
	String str = xStr;
	String[] out = new String[ 10 + xStr.length() / 4 ];
	int cOut = 0;
	int level = -1;
	String combine = "";
	String[] chunks = str.split( "" + QUOTE_START, -1 );
	for (String chunk : chunks) {
		if ((cOut + 5) >= out.length) {
			out = Arrays.copyOf( out, 2 * out.length );
		}
		++ level;
		if (0 < level) {
			combine += QUOTE_START;
			int i0 = -1;
			while (0 < level) {
				i0 = chunk.indexOf( QUOTE_END, i0 + 1 );
				if (0 > i0) {
					combine += chunk;
					chunk = "";
					break;
				}
				-- level;
			}
			if (0 < level) {
				continue;
			}
			if (0 <= i0) {
				combine += chunk.substring( 0, i0 + 1 );
				chunk = chunk.substring( i0 + 1 );
			}
			out[ cOut ++ ] = combine;
			combine = "";
		}
		if (0 < chunk.length()) {
			out = QStr.splitTextAppend( out, cOut, chunk.replace( QUOTE_END, ' ' ) );
			if (out[ out.length - 1 ].startsWith( "\ue000" )) {
				cOut = out[ out.length - 1 ].charAt( 1 );
			}
		}
	}
	if (0 < combine.length()) {
		out[ cOut ++ ] = "" + combine + QUOTE_END;
	}
	return Arrays.copyOf( out, cOut );
}

/** Transform array of marked atoms into array with list markers:
 balance lists, remove list SEPs,
 remove outer STX/ ETX pairs from ATOMs. 
 * 
 * @param out
 * @return
 */
public static int lists4MarkedAtoms( String[] out ) {
	int iIn = 0;
	int iOut = 0;
	int level = 0;
	for (; iIn < out.length; ++ iIn) {
		final String in = out[ iIn ];
		if (0 >= in.length()) {
			continue;
		}
		switch (in.charAt( 0 )) {
			case QStr.SHASH_STRUC:
				if (0 < in.indexOf( LIST_START )) {
					++ level;
				} else if ((0 < in.indexOf( LIST_END )) && (0 < level)) {
					-- level;
				}
				out[ iOut ++ ] = in.substring( 1 );
				continue;
			case QStr.SHASH_PUNCT:
				if (0 < level) {
					// Drop it:
					continue;
				}
				out[ iOut ++ ] = in.substring( 1 );
				continue;
			case QStr.SHASH_TEMP_BITLIST:
			case QStr.SHASH_TEMP_NUM_STRING:
				out[ iOut ++ ] = in.substring( 1 );
				continue;
			default:
				break;
		}
		if (iOut != iIn) {
			out[ iOut ] = in;
		}
		++ iOut;
	}
	final int cOut = iOut;
	iOut = 0;
	for (iIn = 0; iIn < cOut; ++ iIn, ++ iOut) {
		final String outx = out[ iIn ];
		if ((QUOTE_START == outx.charAt( 0 )) && outx.endsWith( "" + QUOTE_END )) {
			out[ iOut ] = outx.substring( 1, outx.length() - 1 );
			if (0 >= out[ iOut ].length()) {
				-- iOut;
			}
		} else if (iOut != iIn) {
			out[ iOut ] = out[ iIn ];
		}
	}
	return iOut;
}

public static String mnemonics4String_OLD( String text, boolean leaveLfTab, boolean leaveExotic ) {
	int i0 = 0;
	int lenLine = 0;
	for (; i0 < text.length(); ++ i0, ++ lenLine) {
		final char ch = text.charAt( i0 );
		if ((78 < lenLine) && !leaveLfTab) {
			break;
		}
		if ((' ' > ch)) {
			if ('\n' == ch) {
				lenLine = -1;
			} else if ('\t' != ch) {
				break;
			}
			if (leaveLfTab) {
				continue;
			}
			break;
		} else if ((0x7f <= ch) || (0 <= "_{}[]".indexOf( ch ))) {
			break;
		}
	}
	if (i0 >= text.length()) {
		return text;
	}
	char[] tmp = new char[ 1 ];
	StringBuilder out = new StringBuilder( text.length() * 2 );
	out.append( text.substring( 0, i0 ) );
	int last = i0;
	for (; i0 < text.length(); ++ i0, ++ lenLine) {
		char ch = text.charAt( i0 );
		boolean breakLine = (77 < lenLine) && !leaveLfTab;
		if (!breakLine && (70 <= lenLine)) {
			breakLine = ((' ' > ch) || (0x7f <= ch) || (0 <= "_{}[]".indexOf( ch )));
		}
		if (breakLine) {
			for (int stop = ((i0 - 40) >= last) ? (i0 - 20) : last; i0 > stop; -- i0) {
				if (' ' >= ch) {
					break;
				}
			}
			out.append( text.substring( last, i0 ) );
			last = i0;
			out.append( '\n' );
			lenLine = -1;
			-- i0;
			continue;
		}
		if ((' ' > ch)) {
			if (('\n' == ch) || ('\t' == ch)) {
				if (leaveLfTab) {
					lenLine = ('\n' == ch) ? -1 : lenLine;
					continue;
				}
				out.append( text.substring( last, i0 ) );
				last = i0 + 1;
				if ('\n' == ch) {
					out.append( "_^n_\n" );
				} else if (40 > lenLine) {
					out.append( "_^t_" );
					-- lenLine;
				} else {
					out.append( "_^t_\n" );
				}
				lenLine += 4;
				continue;
			}
			if (!leaveExotic) {
				tmp[ 0 ] = ch;
				replaceCtrlNOld( tmp, 0, 1, 0x7 );
				if (' ' < tmp[ 0 ]) {
					ch = tmp[ 0 ];
				}
			}
		} else if (0x7f > ch) {
			if ('_' == ch) {
				out.append( text.substring( last, i0 + 1 ) );
				last = i0 + 1;
				++ lenLine;
				out.append( '_' );
			}
			if ((0 > "{}[]".indexOf( ch ))) {
				continue;
			}
		}
		String repl = kChar2Rfc1345.get( ch );
		out.append( text.substring( last, i0 ) );
		last = i0 + 1;
		out.append( '_' );
		if ((null != repl) && (1 < repl.length()) && (0 > repl.indexOf( '_' ))) {
			out.append( repl );
			out.append( '_' );
			lenLine += 1 + repl.length();
			continue;
		}
		out.append( '#' );
		if (0x100 <= ch) {
			out.append( HEX[ (ch & 0xf000) >>> 12 ] );
			out.append( HEX[ (ch & 0xf00) >>> 8 ] );
		}
		out.append( HEX[ (ch & 0xf0) >>> 4 ] );
		out.append( HEX[ ch & 0xf ] );
		out.append( '_' );
		lenLine += (0x100 <= ch) ? 6 : 4;
	}
	out.append( text.substring( last, i0 ) );
	return out.toString();
}

public static int binSearch( String[] sortedList, String key ) {
	int min = 0;
	int max = sortedList.length - 1;
	for (int i0 = (min + max) >> 1; min < max; -- max) {
		if (sortedList[ i0 ].compareTo( key ) <= 0) {
			min = i0;
		} else {
			max = i0;
		}
		i0 = (min + max) >> 1;
	}
	return min;
}

public static int indexOf( String[] list, String key ) {
	for (int i0 = 0; i0 < list.length; ++ i0) {
		if ((list[ i0 ] == key) || list[ i0 ].equals( key )) {
			return i0;
		}
	}
	return -1;
}

/** Convert text with newline formatting for automatic flow */
public static String flowText( String text ) {
	return text.replace( "\t", "    " ).replace( "\r", "" ).replaceAll( " *\\n *", "\t" )
		.replace( "\t\t", "\n\n" ).replace( "\n\t", "\n\n" ).replace( '\t', ' ' );
}

/** For sorted strings:
 * @param sorted0
* @param sorted1
* @return
 */
public static boolean containsAll( String[] sorted0, String... sorted1 ) {
	int i0 = sorted0.length - 1;
	int i1 = sorted1.length - 1;
	for (; (i0 >= 0) && (i1 >= 0); -- i0) {
		int cmp = (i0 > 0) ? 1 : 0;
		for (; cmp > 0; -- i0) {
			cmp = sorted1[ i1 ].charAt( 0 ) - sorted0[ i0 ].charAt( 0 );
		}
		cmp = (0 == cmp) ? sorted0[ i0 ].compareTo( sorted1[ i1 ] ) : cmp;
		if (0 > cmp) {
			return false;
		} else if (0 == cmp) {
			-- i1;
		}
	}
	return (i1 < 0);
}

/** For sorted strings:@param sorted0
* @param sorted1
* @return
 */
public static boolean containsOne( String[] sorted0, String... sorted1 ) {
	int i0 = sorted0.length - 1;
	int i1 = sorted1.length - 1;
	for (; (i0 >= 0) && (i1 >= 0); -- i0) {
		int cmp = (i0 > 0) ? 1 : 0;
		for (; cmp > 0; -- i0) {
			cmp = sorted1[ i1 ].charAt( 0 ) - sorted0[ i0 ].charAt( 0 );
		}
		cmp = (0 == cmp) ? sorted0[ i0 ].compareTo( sorted1[ i1 ] ) : cmp;
		if (0 > cmp) {
			++ i0;
			-- i1;
		} else if (0 == cmp) {
			return true;
		}
	}
	return false;
}

public static int indexOf( byte[] strUtf8, byte[] subStr, int offs ) {
	for (int i0 = offs; i0 < strUtf8.length; ++ i0) {
		if (strUtf8[ i0 ] == subStr[ 0 ]) {
			int i1 = 0;
			for (; i1 < subStr.length; ++ i1) {
				if (strUtf8[ i0 + i1 ] != subStr[ i1 ]) {
					break;
				}
			}
			if (i1 >= subStr.length) {
				return i0;
			}
		}
	}
	return -1;
}

public static int indexOf( byte[] strUtf8, byte[] subStr ) {
	return indexOf( strUtf8, subStr, 0 );
}

public static byte[] appendResize( byte[] xyArray, int[] xyPos, byte[] xToAppend ) {
	int newlen = xyArray.length;
	while ((xyPos[ 0 ] + xToAppend.length) >= newlen) {
		newlen *= 2;
	}
	if (newlen > xyArray.length) {
		xyArray = Arrays.copyOf( xyArray, newlen );
	}
	System.arraycopy( xToAppend, 0, xyArray, xyPos[ 0 ], xToAppend.length );
	xyPos[ 0 ] += xToAppend.length;
	xyArray[ xyPos[ 0 ] ++ ] = '\n';
	return xyArray;
}

public static byte[] appendClone( byte[] str, int offs, byte[] app ) {
	if ((str == null) || (str.length <= 0)) {
		return app;
	} else if ((app == null) || (app.length <= 0)) {
		return str;
	}
	offs = (offs >= 0) ? offs : str.length;
	byte[] out = new byte[ offs + app.length ];
	System.arraycopy( str, 0, out, 0, (offs < str.length) ? offs : str.length );
	System.arraycopy( app, 0, out, offs, app.length );
	return out;
}

public static byte[] replaceClone( byte[] strUtf8, byte chOld, byte chNew ) {
	int i0 = strUtf8.length - 1;
	for (; i0 >= 0; -- i0) {
		if (strUtf8[ i0 ] == chOld) {
			break;
		}
	}
	if (i0 < 0) {
		return strUtf8;
	}
	byte[] out = new byte[ strUtf8.length ];
	System.arraycopy( strUtf8, 0, out, 0, strUtf8.length );
	for (; i0 >= 0; -- i0) {
		if (out[ i0 ] == chOld) {
			out[ i0 ] = chNew;
		}
	}
	return out;
}

public static String toLowerCase( String str ) {
	boolean change = false;
	boolean ascii = true;
	int i0;
	for (i0 = str.length() - 1; i0 >= 0; -- i0) {
		final char c0 = str.charAt( i0 );
		if (('A' <= c0) && (c0 <= 'Z')) {
			change = true;
			break;
		} else if (0x80 <= c0) {
			ascii = false;
			break;
		}
	}
	if (!ascii) {
		return str.toLowerCase( Locale.ROOT );
	}
	if (change) {
		final char[] out = str.toCharArray();
		for (; i0 >= 0; -- i0) {
			final char c0 = str.charAt( i0 );
			out[ i0 ] = (('A' <= c0) && (c0 <= 'Z')) ? (char) (c0 | 0x20) : c0;
		}
		return new String( out );
	}
	return str;
}

public static String toUpperCase( String str ) {
	boolean change = false;
	boolean ascii = true;
	int i0;
	for (i0 = str.length() - 1; i0 >= 0; -- i0) {
		final char c0 = str.charAt( i0 );
		if (('a' <= c0) && (c0 <= 'z')) {
			change = true;
			break;
		} else if (0x80 <= c0) {
			ascii = false;
			break;
		}
	}
	if (!ascii) {
		return str.toUpperCase( Locale.ROOT );
	}
	if (change) {
		final char[] out = str.toCharArray();
		for (; i0 >= 0; -- i0) {
			final char c0 = str.charAt( i0 );
			out[ i0 ] = (('a' <= c0) && (c0 <= 'z')) ? (char) (c0 ^ 0x20) : c0;
		}
		return new String( out );
	}
	return str;
}

public static long long4String( String integer, long defaultValue ) {
	try {
		if (integer.startsWith( "0x" ) || integer.startsWith( "0X" )) {
			return Long.parseLong( integer.substring( 2 ), 16 );
		}
		return Long.parseLong( integer );
	} catch (Exception e) {
	}
	return defaultValue;
}

public static double double4String( String val, double defaultValue ) {
	try {
		if ((0 < val.indexOf( 'x' )) || (0 < val.indexOf( 'X' ))) {
			int exp = val.indexOf( 'p' );
			exp = (0 <= exp) ? exp : val.indexOf( 'P' );
			if (0 > exp) {
				exp = val.length();
				val += "p0";
			}
			if (0 > val.indexOf( '.' )) {
				val = val.substring( 0, exp ) + ".0" + val.substring( exp );
			}
		} else if (0 > val.indexOf( '.' )) {
			int exp = val.indexOf( 'e' );
			exp = (0 <= exp) ? exp : val.indexOf( 'E' );
			exp = (0 <= exp) ? exp : val.length();
			val = val.substring( 0, exp ) + ".0" + val.substring( exp );
		}
		return Double.parseDouble( val );
	} catch (Exception e) {
	}
	return defaultValue;
}

public static String string4Double( double val ) {
	if ((-1.0E16 < val) && (val < 1.0E16)) {
		long tmp = (long) val;
		if (val == tmp) {
			return "" + tmp;
		}
	}
	final String out = "" + val;
	return out.endsWith( ".0" ) ? out.substring( 0, out.length() - 2 ) : out;
}

public static String hex4Double( double v0, String separator ) {
	final boolean neg = (0.0 > v0);
	double vx = neg ? (-v0) : v0;
	String hex = Double.toHexString( vx );
	String hexx = null;
	if ((0x1.0p-16 <= vx) && (vx < 0x1.0p48)) {
		long vxx = (long) vx;
		hex = "0x" + Long.toHexString( vxx ); //.toLowerCase(Locale.ROOT);
		vx -= vxx;
		if (0.0 == vx) {
			if (neg) {
				vxx = (long) v0;
				hexx = Long.toHexString( vxx & Long.MAX_VALUE ); //.toLowerCase(Locale.ROOT);
				int i0 = 1;
				for (; (i0 < hexx.length()) && ('f' == hexx.charAt( i0 )); ++ i0) {
				}
				i0 -= 3;
				if (0 <= i0) {
					hexx = ".." + hexx.substring( i0 );
				}
				hexx = "0x" + hexx;
			}
		} else if ((50 / 4 + 5) >= hex.length()) {
			vx *= 1L << 48;
			vxx = (long) vx;
			String hex2 = Long.toHexString( vxx ); //.toLowerCase(Locale.ROOT);
			if (12 > hex2.length()) {
				hex2 = "000000000000".substring( hex2.length() ) + hex2;
			}
			hex += "." + hex2; // .replaceFirst( "0+$", "" );
			int lenx = hex.length() - 1;
			while ((lenx > 0) && ('0' == hex.charAt( lenx ))) {
				-- lenx;
			}
			hex = hex.substring( 0, lenx + 1 );
		}
	} else if (0.0 == vx) {
		hex = "0x0";
	}
	if ((null == separator) || !neg || (null == hexx)) {
		return neg ? ("-" + hex) : hex;
	}
	return "-" + hex + separator + hexx;
}

/** Poor Java, having fallen into both the localization trap and the UTF-16 trap ... :-(
 * Having char[] cloned is sad. And having to use 'ANSI' as a misnomer makes me really sad :-)
 * Hope for the best and use 'new String(byte[], UtilString.STR256)' instead of this !
 * @param iso_8859_1 This seems to be what Java refers to when all I need is a simple 1:1 byte mapping ...
 * @return
 */
public static String string4Ansi( byte[] iso_8859_1 ) {
	final int len = iso_8859_1.length;
	final char[] out = new char[ len ];
	for (int i0 = 0; i0 < len; ++ i0) {
		out[ i0 ] = (char) (iso_8859_1[ i0 ] & 0xff);
	}
	return new String( out );
}

/** Poor Java, having fallen into both the localization trap and the UTF-16 trap ... :-(
 * And having to use 'ANSI' as a misnomer makes me really sad :-)
 * Hope for the best and use '.getBytes(UtilString.STR256)' instead of this !
 * @param literal256 String of bytes/ 8-bit char values.
 * @return
 */
public static byte[] bytesAnsi( String literal256 ) {
	final int len = literal256.length();
	final byte[] out = new byte[ len ];
	for (int i0 = 0; i0 < len; ++ i0) {
		out[ i0 ] = (byte) literal256.charAt( i0 );
	}
	return out;
}

public static String string4Cp125x( byte[] ansi ) {
	final int len = ansi.length;
	final char[] out = new char[ len ];
	for (int i0 = 0; i0 < len; ++ i0) {
		final char ch = (char) (ansi[ i0 ] & 0xff);
		if ((0 <= ch) && (0x7f > ch)) {
			out[ i0 ] = ch;
			continue;
		} else if (0xa0 > ch) {
			out[ i0 ] = Cp125xOffs7f_2_Unicode[ ch - 0x7f ];
		} else {
			out[ i0 ] = ch;
		}
	}
	return new String( out );
}

/** For the sake of being less strict ...
 * @param utf8 UTF-8 bytes (possibly 'messed up' with CP125x) or null
 * @return literal (as much as possible) or null
 */
public static String string4Utf8( byte[] utf8 ) {
	if (null == utf8) {
		return null;
	}
	try {
		return new String( utf8, "UTF-8" );
	} catch (UnsupportedEncodingException e) {
		// Better not mess it up with any system localization coincidences ...
		// -- out = new String( utf8 );
	}
	///// Trying to make the most of it, assuming a dominance of some CP125x.
	final int len1 = utf8.length - 1;
	final char[] out = new char[ len1 + 1 ];
	final byte[] tmp = new byte[ 2 ]; // simplify
	int combined = 0;
	int i0 = 0;
	for (; i0 < len1; ++ i0) {
		if (0 <= utf8[ i0 ]) {
			// Ignoring 0x7F as special case - since things are messed up anyway.
			out[ i0 - combined ] = (char) utf8[ i0 ];
			continue;
		}
		final char ch = (char) (utf8[ i0 ] & 0xff);
		tmp[ 0 ] = utf8[ i0 ];
		tmp[ 1 ] = utf8[ i0 + 1 ];
		try {
			String str = new String( tmp, "UTF-8" );
			out[ i0 - combined ] = str.charAt( 0 );
			++ combined;
			++ i0;
		} catch (Exception e) {
			if (0xa0 > ch) {
				out[ i0 - combined ] = Cp125xOffs7f_2_Unicode[ ch - 0x7f ];
			} else {
				out[ i0 - combined ] = ch;
			}
		}
	}
	if (i0 == len1) {
		out[ len1 - combined ] = (char) (utf8[ i0 ] & 0xff);
	} else {
		-- combined;
	}
	return new String( out, 0, len1 + 1 - combined );
}

/** Just for the sake of having it - use '.getBytes(UtilString.STR16X)' instead of this !
 * @param literal Text string
/** @return UTF-8 encoding, as far as possible
 */
public static byte[] bytesUtf8( String literal ) {
	if (null == literal) {
		return null;
	}
	return literal.getBytes( UtilString.STRX16U8 );
}

/**
 * @param hexLiteral String of hex digits, optionally marked as X'...'
/** @return matching or empty byte array (not null)
 */
public static byte[] bytes4Hex( String hexLiteral ) {
	if ((null == hexLiteral) || (0 >= hexLiteral.length())) {
		return new byte[ 0 ];
	}
	// For "X'" as marker:
	int offs = ('X' == hexLiteral.charAt( 0 )) ? 1 : 0;
	// Excluding possible "'" at the end:
	byte[] out = new byte[ hexLiteral.length() / 2 - offs ];
	char[] arr = hexLiteral.toCharArray();
	int i0 = out.length - 1 + offs;
	for (int i1 = 2 * i0 + 1; i0 >= offs; -- i0) {
		byte b1 = NIBBLE[ arr[ i1 -- ] & 0x1f ];
		byte b0 = NIBBLE[ arr[ i1 -- ] & 0x1f ];
		out[ i0 - offs ] = (byte) ((b0 << 4) | b1);
	}
	return out;
}

public static byte[] bytes4Hex( byte[] hexAscii ) {
	if ((null == hexAscii) || (0 >= hexAscii.length)) {
		return new byte[ 0 ];
	}
	int i0 = -1 + (hexAscii.length + 1) / 2;
	byte[] out = new byte[ i0 + 1 ];
	for (int i1 = hexAscii.length - 1; i0 >= 0; -- i0, -- i1) {
		byte b1 = NIBBLE[ hexAscii[ i1 ] & 0x1f ];
		i1 = (i1 >>> 1) << 1;
		byte b0 = NIBBLE[ hexAscii[ i1 ] & 0x1f ];
		out[ i0 ] = (byte) ((b0 << 4) | b1);
	}
	return out;
}

public static String string4HexUtf8( String hex2 ) {
	return string4Utf8( bytes4Hex( hex2 ) );
}

public static String string4HexUtf16( char[] hex4, int start, int end ) {
	if ((null == hex4) || (end <= start)) {
		return "";
	}
	// For "X'" as marker:
	int offs = ('X' == hex4[ start ]) ? 1 : 0;
	// Excluding possible "'" at the end:
	char[] out = new char[ (end - start) / 4 - offs ];
	int i1 = ((end - start) / 4) * 4 + start + offs * 2 - 1;
	i1 = (i1 >= end) ? (end - 1) : i1;
	for (int i0 = out.length - 1; i0 >= 0; -- i0) {
		byte b3 = NIBBLE[ hex4[ i1 -- ] & 0x1f ];
		byte b2 = NIBBLE[ hex4[ i1 -- ] & 0x1f ];
		byte b1 = NIBBLE[ hex4[ i1 -- ] & 0x1f ];
		byte b0 = NIBBLE[ hex4[ i1 -- ] & 0x1f ];
		out[ i0 ] = (char) ((b0 << 12) | (b1 << 8) | (b2 << 4) | b3);
	}
	return new String( out );
}

public static String hex4Bytes( byte[] data, int offset, int end, boolean marked ) {
	if (null == data) {
		return "";
	}
	int offs2 = marked ? 2 : 0;
	char[] out = new char[ 2 * (end - offset) + (marked ? 3 : 0) ];
	if (marked) {
		out[ out.length - 1 ] = '\'';
		out[ 1 ] = '\'';
		out[ 0 ] = 'X';
	}
	for (int i0 = (end - offset) - 1; i0 >= 0; -- i0) {
		final byte v0 = data[ i0 + offset ];
		out[ i0 * 2 + offs2 ] = HEX[ (v0 & 0xf0) >>> 4 ];
		out[ i0 * 2 + 1 + offs2 ] = HEX[ v0 & 0xf ];
	}
	return new String( out );
}

public static String hex4Bytes( byte[] data, boolean marked ) {
	if (null == data) {
		return "";
	}
	return hex4Bytes( data, 0, data.length, marked );
}

public static byte[] hexAscii4Bytes( byte[] data ) {
	if (null == data) {
		return new byte[ 0 ];
	}
	byte[] out = new byte[ 2 * data.length ];
	int i0 = data.length - 1;
	for (int i1 = 2 * i0 + 1; i0 >= 0; -- i0) {
		final byte v0 = data[ i0 ];
		out[ i1 -- ] = (byte) HEX[ v0 & 0xf ];
		out[ i1 -- ] = (byte) HEX[ (v0 & 0xf0) >>> 4 ];
	}
	return out;
}

public static String hexUtf8( String literal, boolean marked ) {
	return hex4Bytes( bytesUtf8( literal ), marked );
}

public static String hexUtf16( String literal ) {
	char[] out = new char[ 4 * literal.length() ];
	for (int i0 = literal.length() - 1; i0 >= 0; -- i0) {
		final char v0 = literal.charAt( i0 );
		out[ 4 * i0 ] = HEX[ (v0 & 0xf000) >>> 12 ];
		out[ 4 * i0 + 1 ] = HEX[ (v0 & 0xf00) >>> 8 ];
		out[ 4 * i0 + 2 ] = HEX[ (v0 & 0xf0) >>> 4 ];
		out[ 4 * i0 + 3 ] = HEX[ v0 & 0xf ];
	}
	return new String( out );
}

public static String string4Array( String[] array, String separator ) {
	// Assuming '[...]' notation:
//	String out = array.toString();
//	return out.substring( 1, out.length() - 1 ).replace( ", ", separator );
	if (null == array) {
		return "";
	}
	StringBuilder out = new StringBuilder( array.length * 5 );
	String sep = "";
	for (String el : array) {
		out.append( sep );
		sep = separator;
		out.append( el );
	}
	return out.toString();
}

public static String nameNormalize( String str, long bExtendedPunctSlash_Blank ) {
	// Empty string not allowed:
	if (str.length() <= 0) {
		return ".";
	}
	// Basic case:
	if (str.matches( "[0-9A-Za-z\\._]+" ) && (0 != (0x2 & bExtendedPunctSlash_Blank))) {
		return str;
	}
	if (6 == bExtendedPunctSlash_Blank) {
		// Expected to be ASCII char's:
		if (str.matches( "[0-9A-Za-z\\._/]+" ) && !str.contains( "//" )) {
			return str;
		}
		String sx = str.trim();
		return sx.replace( ' ', '_' ).replace( ',', '/' ).replaceAll( "[^0-9A-Za-z\\._/]", "." ) //.
			.replaceAll( "[_\\./]+/[_\\./]*", "/" );
	} else if (2 == bExtendedPunctSlash_Blank) {
		// Expected to be ASCII char's:
		if (str.matches( "[0-9A-Za-z\\._]+" )) {
			return str;
		}
		String sx = str.trim();
		return sx.replace( ' ', '_' ).replaceAll( "[^0-9A-Za-z\\._]", "." );
	}
	int i0 = str.length() - 1;
	int found = 0;
	///// Initially, just check without changing it.
	for (; i0 >= 0; -- i0) {
		final char c0 = str.charAt( i0 );
		if (('0' <= c0) && (c0 <= 'z')) {
			if (('9' >= c0) || ('a' <= c0)) {
				continue;
			} else if (('A' <= c0) && (c0 <= 'Z')) {
				continue;
			}
			if (0 <= "!#$*<>?[]^{}".indexOf( c0 )) {
				break;
			}
			continue;
		} else if ((0x80 <= c0) && (0 != (0x1 & bExtendedPunctSlash_Blank))) {
			found |= 0x1;
			continue;
		} else if (' ' >= c0) {
			if ((' ' > c0) || (0 == (0x100 & bExtendedPunctSlash_Blank))) {
				break;
			}
			continue;
		} else if (0 <= "!#$*<>?[]^{}".indexOf( c0 )) {
			break;
		}
		found |= ('0' > c0) ? 0x2 : 0;
		found |= ('/' == c0) ? 0x4 : 0;
	}
	if ((0 > i0) && (found == (found & bExtendedPunctSlash_Blank))) {
		return str;
	}
	char[] build = str.toCharArray();
	for (i0 = 0; i0 < build.length; ++ i0) {
		final char c0 = str.charAt( i0 );
		if (('0' <= c0) && (c0 <= 'z')) {
			if (0 <= "!#$*<>?[]^{}".indexOf( c0 )) {
				build[ i0 ] = '.';
				continue;
			}
		} else if (0x80 <= c0) {
			build[ i0 ] = (0 == (0x1 & bExtendedPunctSlash_Blank)) ? '.' : build[ i0 ];
			continue;
		} else if (' ' >= c0) {
			if ((' ' > c0) || (0 == (0x100 & bExtendedPunctSlash_Blank))) {
				build[ i0 ] = '.';
			}
			continue;
		} else if (0 <= "!#$*<>?[]^{}".indexOf( c0 )) {
			build[ i0 ] = '.';
			continue;
		}
		if (('0' > c0) && (0 == (0x2 & bExtendedPunctSlash_Blank))) {
			build[ i0 ] = '.';
		} else if (('/' == c0) && (0 == (0x4 & bExtendedPunctSlash_Blank))) {
			build[ i0 ] = '.';
		}
	}
	return // (0 >= count) ? "." : 
	new String( build );
}

public static String csvField4Text( String str ) {
	if ((str.indexOf( '\t' ) < 0) && (str.indexOf( '\n' ) < 0)) {
		return str;
	}
	return str.replace( '\t', '.' ).replace( '\n', '.' );
}

//=====
}
