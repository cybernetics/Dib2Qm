// Copyright (C) 2016,2017,2018  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.func;

import java.util.Arrays;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.store.Codec;

/** Simple XOR codec for debugging */
public class CsvCodec1 extends CsvCodec0 {
//=====

public static final CsvCodec1 instance = new CsvCodec1();

@Override
public byte[] compress( byte[] xyData, int xOffset4Reuse, int to ) {
	return UtilMisc.compress( 0, xyData, xOffset4Reuse, to );
}

@Override
public byte[] decompress( byte[] xData, int len ) {
	return UtilMisc.decompress( xData, 0, len );
}

@Override
public byte[] encode( byte[] compressedData, int from, int to, byte[] key, byte[] iv16, int keyInfo, byte[] keyData, byte[] signatureKey )
	throws Exception {
//	byte[] out = encode4DerivedKey( compressedData, from, to, key );
//	return Arrays.copyOf( out, out.length );
	byte[] salt = Codec.createHeaderSaltIv16_OLD( Dib2Constants.MAGIC_BYTES, new byte[] { 1, 0, 0 }, this );
	for (int i0 = key.length - 1; i0 >= 4; -- i0) {
		key[ i0 ] ^= salt[ i0 % 16 ];
	}
	byte[] out = Arrays.copyOf( salt, to - from + salt.length );
	for (int i0 = to - from - 1; i0 >= 0; -- i0) {
		out[ salt.length + i0 ] = (byte) (key[ i0 % 32 ] ^ compressedData[ from + i0 ]);
	}
	return UtilMisc.packet4880X( Dib2Constants.RFC4880_EXP2, null, out, 0, out.length );
}

@Override
public byte[] decode( byte[] data, int offset, int len, byte[] keyOrPass, byte[] signatureKey ) throws Exception {
	int hdlen = UtilMisc.getPacketHeaderLen( data, offset );
	if (data[ hdlen + 3 ] != getMethodTag()) {
		return null;
	}
	byte[] key = Arrays.copyOf( keyOrPass, 32 );
	for (int i0 = key.length - 1; i0 >= 4; -- i0) {
		key[ i0 ] ^= data[ hdlen + (i0 % 16) ];
	}
	byte[] out = new byte[ len - 16 ];
	for (int i0 = len - 1; i0 >= 16; -- i0) {
		out[ i0 ] = (byte) (key[ i0 % 32 ] ^ data[ offset + hdlen + i0 ]);
	}
	return Arrays.copyOfRange( out, offset + hdlen + 16, offset + len );
}

@Override
public byte getMethodTag() {
	return '1'; // dummy - xor
}

@Override
public byte[] getInitialValue( int len ) {
	return Arrays.copyOf( ("" + (UtilMisc.currentTimeMillisLinearized() & 0xfffff) + "0123456789").getBytes( UtilString.STR256 ), len );
}

//=====
}
