// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.func;

import java.security.MessageDigest;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import net.sf.dibdib.config.*;

/** 'Quick' string utils: text segmentation and sorting (cmp UtilString!).
 * QString = sortable hash ('shash') + original string as required.
 */
/*
String handling, definitions:
'QString' as tokenized char string with shashes (sortable hashes):
qstring, packed: QX shash [QX shash]* QX QQ [ QX qelement [QX qelement]* QX]
string: token [ [separator/ indicator]+ token ]*
element: token/ separator/ indicator
qelement: token/ ([marker]* QM [spacer/label]* QM [separator/ indicator]+) 
(every spacer sequence becomes simple QX among shashes in qstring (for sorting),
indicator alone does not affect sorting)
shash: Q(normalize(token))
token: word (string of digits, ...) / float (date, ...)
sememe: predicate/ operator/ restrictor/ selector/ descriptor
expression: '(' operator [' ' term]* ')'
predication: '{' predicate [' ' term]+ '}'
substitution: '{' atom '}'
term: atom/ float/ group/ selector
group: descriptor [ '(' restrictor ')' ]
atom: word/ '<' word [' ' word]* '>'/ group '.' selector
word: [symbol/ alpha/ digit]* symbol/alpha/num [symbol/ alpha/ digit]*
restrictor/ descriptor/ ...: word
float: [d]+/ '0x' [d]+/ '0.' [d]+ ['e' ['+'/ '-'] d] ['~` d] ['_' [word '*' '/']+]/ ...
marker: (see below)

Float: normalized:
start with '0.' followed by '1'-'9' and more digits, plus exponent plus precision info plus unit
e.g.: 0.123e3~2_kg.m/s/s = (with last 2 digits plus binary rounding not reliable:) [123 - 64, 123 + 64] kg*m/sec^2.
Units as ASCII: 'm', 's', 'kg', ... with '$', pound -> '#', euro -> ',', yen -> '%', and ':' for date

Shash (sortable hash):
with collation key as bitlist for words ('a' is sorted like 'A' etc.: normalization form NFKC, strength PRIMARY)
plus special float encoding for numbers,
use Unicode private area (0xE100..0xF7FF):
0000 0000 000.....  control chars
0000 0000 00000001  - QQ
0000 0000 00000010  - QX
0000 0000 00000011  - QM
1110 0000 1.......  tag
1110 0001 0.......  format left (template)
1110 0001 1.......  format right
1110 0001 11111111  numeric with original string
1110 0010 00000000  separator (punctuation etc.) (original string appended)
1110 0010 000010..  struc basic
1110 0010 000011..  struc X
1110 0010 00110xxx  Bitlist (? ..due to surr. area only 8 per char)/ byte[] with x trailing 0 bits
1110 0010 0010xxxx  Bitlist (? 12 bits per char | 0xE000) with x trailing 0 bits
1110 0010 00111111  Bitlist with temporary format and preceding marker ('X', 'A', 'B', 'I', 'P')
1110 0010 11......  date as leap minutes << 8 + x (or other long: RFU) with 42 bits (-15000..15000)
1110 0011 ........  negative float with extended range (RFU)
1110 01.. ........  negative float (>= double precision), pos. exponent: IEEE bits toggled
1110 10.. ........  negative float >= -2 (bits toggled (+1) for sorting!)
1110 1100           NaN, sorted ahead of 0
1110 1100 00000000  0
1110 11.. ........  positive float with neg. or 0 exponent: IEEE bits
1111 00.. ........  positive float with pos. exponent
1111 0100 0.......  positive float with extended range (RFU)
1111 0100 1.......  float with units
1111 0101 ........  indicator for (condensed) collation key (depends on Java implementation)
1111 0101 0.......  - 7-bit encoded chars from 16 collation bits (first byte == 0x00)
1111 0101 10000101  - bytes from collation bits (first byte < 0x80, excluding 0x00)
1111 0101 10000110  - 16-bit uints (excl. 0) from 16 collation bits (first byte == 0x80)
1111 0101 10000111  - bytes from collation bits (first byte > 0x80, excluding 0x00)
1111 011. ........  9 significant bits of original char (e.g. control chars)  
0x0 as filler, subsequent chars have to be >= 0x20 !
(used chars must not overlap with control chars!)
0....... 1........  for subsequent 7-bit pieces
10...... .........  for subsequent 14-bit pieces (avoids surrogate area)
........ .........  (excluding 0x00.., 0x..00, 0xd...) for subsequent byte pieces

float with at least 2 bytes, value pieces encoded as IEEE floats with special toggling:
111. ..             (encoded bits for sign bit with 1 bit of exponent)
       .. ........  (remaining 10 bits of exponent for double precision, possibly toggled)
+ 10..............* (14 bits of value (significand), in several chunks as needed)
1111 0100 1.......  float with first 7 bits of unit
+0....... 1.......* (7 bit ASCII pieces from unit name or '.' for factor or '/')
+111..... ........  (exponent as above, followed by significand as above)
+01...... 0.......  (RFU)

e.g.:
E    type   exp  exp   val  val
1111 00 00 0000 0000 1 100 0000 0000 0000 = 1.1(2) * 2^1 = 11(2) = 3
1111 00 00 0000 0000 1 000 0000 0000 0000 = 1.0(2) * 2^1 = 2
1110 11 11 1111 1111 1 100 0000 0000 0000 = 1.1(2) * 2^0 = 1.1(2) = 1.5
1110 11 11 1111 1111 1 000 0000 0000 0001 = 1.000000000000001(2) * 2^0
1110 11 11 1111 1111 1 000 0000 0000 0000 1000 ... 0001 ~ 1.000...2
1110 11 11 1111 1111 1 000 0000 0000 0000 = 1.0(2) * 2^0 = 1
1110 11 00 0000 0001 1 000 0000 0000 0000 = 1.0(2) * 2^-1022
1110 11 00 0000 0000 1 000 0000 0000 0000 = 0
1110 10 00 0000 0001 1 000 0000 0000 0000 = -1
1110 10 00 0000 0000 1 000 0000 0000 0000 = -2

 */

/*
Cmp. icu-project.org:
-- Primary weights can be 1 to 4 bytes in length. (If a character has a 3-byte
CE primary weight, we'll call it a 3-byter, for example)
-- They have the restriction that no weight can be a proper initial sequence
of another. Eg, if X has weight 85 42, then no three or four byte weight
can start with 85 42.
-- It is important that the most frequent characters have short weights.
So, for example, in U5.2 we have {space, A-Z} being 1-byters.
-- The first bytes are important, and are allocated to special ranges.
[Java uses double bytes/ ...: Special case: 0x80..., trailing 0x00! for concatenation] 
 */

public final class QStr {
//=====

private static final String REGEX_SPACE_NL = "[\\s\\p{Z}]";
public static final Pattern PATTERN_SPACE_NL = Pattern.compile( REGEX_SPACE_NL );
public static final Pattern PATTERN_SPACE_NL_SEQ = Pattern.compile( REGEX_SPACE_NL + "+" );
private static final Pattern PATTERN_SPACE_BEGIN = Pattern.compile( "^\\s+" );
private static final Pattern PATTERN_SPACE_END = Pattern.compile( "\\s+$" );
public static final String REGEX_LINE_BREAK = "\\r?[\\n\\u0085\\u2028\\u2029]"; // "\\r?[\\n\\v\\u0085\\u2028\\u2029]";
public static final Pattern PATTERN_LINE_BREAK_TAB = Pattern.compile( "\\r?[\\n\u0085\u2028\u2029\\t]" ); // "\\r?[\\n\\v\\u0085\\u2028\\u2029\\t]"
public static final Pattern PATTERN_WORD_CONNECTOR = Pattern
	.compile( "[\\p{L}\\p{M}\\p{N}\\p{Pc}[\\p{InEnclosedAlphanumerics}&&\\p{So}]]+" );
public static final Pattern PATTERN_WORD_BASIC = Pattern.compile( "[\\p{L}\\p{M}\\p{N}]+" );
public static final Pattern PATTERN_WORD_SYMBOL = Pattern.compile( "[\\p{L}\\p{M}\\p{N}\\p{S}]+" );
public static final Pattern PATTERN_SYMBOLS = Pattern.compile( "\\p{S}+" );
//public static final Pattern PATTERN_LETTERS_CASED = Pattern.compile( "\\p{L&}+" );
public static final Pattern PATTERN_PUNCTUATION = Pattern.compile( "\\p{P}+" );
public static final Pattern PATTERN_CONTROLS_UNI = Pattern.compile( "\\p{Cc}+" );
public static final Pattern PATTERN_CONTROLS_ANSI = Pattern.compile( "\\p{Cntrl}+" );
public static final Pattern PATTERN_DIGITS = Pattern.compile( "\\p{Nd}+" );
public static final Pattern PATTERN_DIGITS_BASIC = Pattern.compile( "\\p{Digit}+" );
public static final Pattern PATTERN_HEXS = Pattern.compile( "\\p{XDigit}+" );
public static final Pattern PATTERN_NUMERICS = Pattern.compile( "\\p{N}+" );

//private static final String REGEX_CURRENCY = "\\p{Sc}";
private static final String REGEX_NUMBER = "([\\+\\-]?[1-9][0-9_\u00B7\\'\\.\\,]*[0-9])|([\\+\\-]?[0-9])";
private static final String REGEX_NUMBER_SEP = "[_\u00B7\\']";
private static final Pattern PATTERN_NUMBER_SEP = Pattern.compile( REGEX_NUMBER_SEP );
private static final String REGEX_UNIT_SUFFIX = "_[\\./A-z0-9\\p{Sc}]+";
private static final Pattern PATTERN_UNIT = Pattern.compile( REGEX_UNIT_SUFFIX );
private static final String REGEX_DIGITS_TEL_ETC = "[\\+\\#0][0-9\\-\\*]+\\#?";
private static final Pattern PATTERN_DIGITS_TEL_ETC = Pattern.compile( REGEX_DIGITS_TEL_ETC );
private static final String REGEX_OID_DIGITS_ETC = "\\%[0-9A-Za-z_\\^\\~]+";
private static final Pattern PATTERN_OID_DIGITS_ETC = Pattern.compile( REGEX_OID_DIGITS_ETC );
//private static final String REGEX_NUMBER_UNIT = REGEX_NUMBER + REGEX_UNIT_SUFFIX;
private static final Pattern PATTERN_NUMBER = Pattern.compile( REGEX_NUMBER );
private static final String REGEX_VALUE = "[\\+\\-]?[0-9_\u00B7\\'\\.\\,]*[0-9][eE][\\+\\-]?[0-9]+[\\~0-9]*" //,
; //+ REGEX_UNIT_SUFFIX;
private static final Pattern PATTERN_VALUE = Pattern.compile( REGEX_VALUE );
private static final String REGEX_VALUE_HEX = "[\\+\\-]?0[xX][0-9A-Fa-f_\u00B7\\'\\.\\,]*[0-9A-Fa-f]([pP][\\+\\-]?[0-9]+[\\~0-9]*)?"; //+ REGEX_UNIT_SUFFIX;
private static final Pattern PATTERN_VALUE_HEX = Pattern.compile( REGEX_VALUE_HEX );
private static final String REGEX_DATE = "\\-?[0-9]+\\-[0-9][0-9]\\-[0-9][0-9]T?[\\.0-9\\:\\+\\-]*";
private static final Pattern PATTERN_DATE = Pattern.compile( REGEX_DATE );
public static final Pattern PATTERN_DATE_D = Pattern.compile( "[0-9][0-9]\\.[0-9][0-9]\\.[12]?[0-9]?[0-9][0-9]" );
public static final Pattern PATTERN_TIME = Pattern.compile( "[0-9]+\\:[0-9][0-9](\\:[0-9][0-9])?" );
private static final String REGEX_WORD = "[\\w\\p{S}\\p{N}]*[\\p{L}\\p{S}][\\w\\p{S}\\p{N}]*";
private static final Pattern PATTERN_WORD = Pattern.compile( REGEX_WORD );
private static final String CHARS_NUM_FIRST = "+-0123456789#";
private static final String CHARS_STRUC = "()[]{}<>"; // (cmp. Unicode 'Ps'/'Pe' open/close)
//private static final String CHARS_STRUC_LINE = "./=;|-*:";
private static final String CHARS_QUOTE_LEFT = "'\"\u00ab\u2018\u201b\u201c\u201f\u2039"; // (cmp. Unicode 'Pi' initial)
//private static final String CHARS_QUOTE_RIGHT = "'\"\u00bb\u2019\u201d\u203a"; // (cmp. Unicode 'Pf' final)
private static final String CHARS_STRUC_MOD = "?%!&@"; // '$' moved to currency
private static final String CHARS_FORMAT = "*_+^~"; // markdown, '#' moved to numeric
private static final String CHARS_MARKER = CHARS_FORMAT + CHARS_STRUC + CHARS_STRUC_MOD;
private static final String CHARS_MARKER_PLUS_LEFT = ".:" + CHARS_MARKER;
private static final String CHARS_MARKER_DOLLAR = "$#" + CHARS_MARKER;

private static boolean isCurrency( char ch0 ) {
	return ('$' == ch0) || ((0xa2 <= ch0) && (ch0 <= 0xa5)) || ((0x20a0 <= ch0) && (ch0 < 0x20cf));
}

public static final char SHASH_QQ = (char) 1;
public static final char SHASH_QX = (char) 2;
public static final char SHASH_QM = (char) 3;
public static final char SHASH_FORMAT_LEFT = (char) 0xE100;
public static final char SHASH_FORMAT_RIGHT = (char) 0xE180;
public static final char SHASH_TEMP_NUM_STRING = (char) 0xE1ff;
public static final char SHASH_PUNCT = (char) 0xE200;
public static final char SHASH_STRUC = (char) 0xE208;
public static final char SHASH_BITLIST = (char) 0xE230;
public static final char SHASH_TEMP_BITLIST = (char) 0xE23f;
public static final char SHASH_DATE = (char) 0xE2C0 | ':';
public static final long SHASH_DATE_DELTA = 0x20000000000L;
public static final char SHASH_NEG = (char) 0xE400;
public static final char SHASH_POS = (char) 0xEC00;
public static final String SHASH_NAN = "" + (char) 0xEC00;
public static final char SHASH_NUM_UNIT = (char) 0xF480;
public static final char SHASH_LITERAL = (char) 0xF500;

private static Collator coll = null;
private static char[] collKey2Char = new char[ 0x100 ];
private static int[] collDelta = new int[ 0x100 ];
private static char[] collChar2Key = new char[ 0x100 ];

private static void populateCollData() {
	try {
		// Multilingual, using ICU's uca_rules.txt:
		RuleBasedCollator ucaDucet = new RuleBasedCollator( "& \\u0001 = \\u0002" );
		coll = ucaDucet;
	} catch (ParseException e) {
		coll = null;
	}
	if ((null == coll) || (null != Dib2Config.locale)) {
		coll = Collator.getInstance( (null != Dib2Config.locale) ? Dib2Config.locale : Locale.CANADA );
		// Case-independent:
		coll.setStrength( Collator.PRIMARY );
	}
	///// Override higher by lower values, map to lowercase.
	for (char c0 = (char) 0x1e; c0 != 0x1f; c0 -= (c0 != 1) ? 1 : -0xfe) {
		final String s0 = "" + c0;
		byte[] tmp = coll.getCollationKey( s0 ).toByteArray();
		if ((2 <= tmp.length) && ((0 == tmp[ 0 ]) || (0 == tmp[ 1 ]))) {
			final int key = tmp[ (0 == tmp[ 0 ]) ? 1 : 0 ] & 0xff;
			collKey2Char[ key ] = s0.toLowerCase( Locale.ROOT ).charAt( 0 );
			collChar2Key[ c0 ] = (char) key;
		}
	}
	for (int i0 = collKey2Char.length - 1; i0 >= 0; -- i0) {
		collKey2Char[ i0 ] = (0 >= collKey2Char[ i0 ]) ? collKey2Char[ '.' ] : collKey2Char[ i0 ];
	}
	byte[] tmp = coll.getCollationKey( "A" ).toByteArray();
	collChar2Key[ 0 ] = (char) ((0 == tmp[ 0 ]) ? 2 : 1);
	if ((1 == collChar2Key[ 0 ]) && (0x80 != tmp[ 0 ])) {
		collChar2Key[ 0 ] = (char) ((0x80 <= tmp[ 0 ]) ? 0xf587 : 0xf585);
	}
	///// Unicode blocks:
	for (int c0 = 0x100; c0 <= 0xff00; c0 += 0x100) {
		tmp = coll.getCollationKey( "" + (char) c0 ).toByteArray();
		collDelta[ c0 >>> 8 ] = -1;
		for (int i0 = 0; i0 < tmp.length; ++ i0) {
			if (tmp[ i0 ] == (byte) (c0 >>> 8)) {
				// tmp should have trailing '00' bytes.
				collDelta[ c0 >>> 8 ] = (c0 & 0xff) - (tmp[ i0 + 1 ] & 0xff);
				break;
			}
		}
	}
}

/** Calculate sortable hash (shash) string for value.
 * @param repr String representation, to be parsed (or null).
 * @param value Value or additional (fractional) value.
 * @return shash.
 */
public static String shash4Double( String repr, double value ) {
	String shash = null;
	try {
		double val = (null == repr) ? value : (UtilString.double4String( repr, 0.0 ) + value);
		long bits = Double.doubleToRawLongBits( val );
		if (0 > val) {
			bits = -bits;
		}
		int exp = (int) ((bits & 0x7ff0000000000000L) >>> 52);
		long significand = ((bits & 0x000fffffffffffffL) << (4 * 14 - 52));
		///// Stay away from surrogate area.
		char[] bits14 = new char[ 5 ];
		bits14[ 0 ] = (char) (((0 > val) ? SHASH_NEG : SHASH_POS) + exp);
		bits14[ 4 ] = (char) (0x8000 + (significand & 0x3fff));
		significand >>>= 14;
		bits14[ 3 ] = (char) (0x8000 + (significand & 0x3fff));
		significand >>>= 14;
		bits14[ 2 ] = (char) (0x8000 + (significand & 0x3fff));
		significand >>>= 14;
		bits14[ 1 ] = (char) (0x8000 + (significand & 0x3fff));
		shash = new String( bits14 );
		int count = 4;
		for (; count >= 2; -- count) {
			if (bits14[ count ] != 0x8000) {
				break;
			}
		}
		shash = shash.substring( 0, count + 1 );
	} catch (Exception e) {
	}
	return shash;
}

public static double double4ShashNum( String shash ) {
	if (0 >= shash.length()) {
		return 0;
	}
	if (SHASH_NEG > shash.charAt( 0 )) {
		return Double.NaN;
	} else if (SHASH_NUM_UNIT <= shash.charAt( 0 )) {
		///// Skip units
		int i0 = 1;
		for (; i0 < shash.length(); ++ i0) {
			if (0x8000 <= shash.charAt( i0 )) {
				break;
			}
		}
		if (i0 >= shash.length()) {
			return 0;
		}
		shash = shash.substring( i0 );
	}
	// Simplified:
	long bits = shash.charAt( 0 );
	boolean pos = (SHASH_POS <= shash.charAt( 0 ));
	bits -= pos ? SHASH_POS : SHASH_NEG;
	bits <<= 52;
	switch (shash.length()) {
		case 5:
			bits |= (shash.charAt( 4 ) & 0x3fffL) >>> 4;  // Fall through.
		case 4:
			bits |= (shash.charAt( 3 ) & 0x3fffL) << 10;
		case 3:
			bits |= (shash.charAt( 2 ) & 0x3fffL) << 24;
		case 2:
			bits |= (shash.charAt( 1 ) & 0x3fffL) << 38;
			break;
		default:
			return Double.NaN;
	}
	return Double.longBitsToDouble( pos ? bits : -bits );
}

public static String shash4LeapMin256( long mint256 ) {
	long minute256 = mint256 + SHASH_DATE_DELTA;
	if ((0xf >= minute256) || (minute256 >= 0x3ffffff0000L)) {
		// TODO Use ':' as unit for date
		return shash4Double( null, mint256 / 256.0 );
	}
	char[] shash = new char[ 4 ];
	shash[ 0 ] = SHASH_DATE;
	shash[ 1 ] = (char) (((minute256 >>> 28) & 0x3fff) | 0x8000);
	shash[ 2 ] = (char) (((minute256 >>> 14) & 0x3fff) | 0x8000);
	shash[ 3 ] = (char) ((minute256 & 0x3fff) | 0x8000);
	return new String( shash );
}

public static String shash4Date( String date ) {
	return shash4LeapMin256( UtilMisc.leapMinute256ForDate( date ) );
}

public static long leapMinutes256_4ShashDate( String shash ) {
	// TODO dates for extended range with ':' as unit
	char[] av = shash.toCharArray();
	if ((4 != av.length) || (av[ 0 ] != SHASH_DATE)) {
		return 0;
	}
	long minute256 = (av[ 1 ] & 0x3fffL) << 28;
	minute256 |= (av[ 2 ] & 0x3fff) << 14;
	minute256 |= av[ 3 ] & 0x3fff;
	return minute256 - SHASH_DATE_DELTA;
}

public static String date4ShashDate( String shash ) {
	if ((4 != shash.length()) || (shash.charAt( 0 ) != SHASH_DATE)) {
		return "" + double4ShashNum( shash ) + "_:";
	}
	return UtilMisc.date4LeapMinute256( leapMinutes256_4ShashDate( shash ) );
}

public static String shash4String( String piece, boolean xSha1 ) {
	String shash;
	if (null == coll) {
		populateCollData();
	}
	if ((null == piece) || (0 >= piece.length())) {
		return "";
//	} else if (xPacked && (0 <= piece.indexOf( SHASH_QM ))) {
//		return (piece.contains( "" + SHASH_QM + SHASH_QM )) ? "" : ("" + SHASH_QX);
	}
	boolean cut = (Dib2Constants.SHASH_MAX * 2) < piece.length();
	String px = cut ? piece.substring( 0, Dib2Constants.SHASH_MAX * 2 ) : piece;
	byte[] shashBytes = coll.getCollationKey( px ).toByteArray();
	int len = shashBytes.length;
	while ((0 < len) && (0 == shashBytes[ len - 1 ])) {
		-- len;
	}
	char[] shashChars = new char[ len * 2 + 3 ];
	int len2 = 1;
	///// Compress:
	if (1 >= len) {
//		len2 = 0;
		return "" + SHASH_PUNCT + ((3 < px.length()) ? px.substring( 0, 3 ) : px);
	} else if ((0x00 == shashBytes[ 0 ]) && (0 < shashBytes[ 1 ])) {
		byte[] pieces = new byte[ len * 2 + 3 ];
		shashChars[ 0 ] = (char) (0xf500 | shashBytes[ 1 ]);
		int i1 = 0;
		for (int i0 = 3; i0 < len; i0 += 2) {
			char val = (char) (((shashBytes[ i0 - 1 ] & 0xff) << 8) | (shashBytes[ i0 ] & 0xff));
			if (0 != val) {
				if (0x8000 <= val) {
					// Special Java marker?
					continue;
				} else if (0x7f <= val) {
					// Escaped?
					val += collDelta[ val >>> 8 ];
					pieces[ i1 ++ ] = (byte) ((val >>> 8) & 3);
				}
				pieces[ i1 ++ ] = (byte) (val & 0x7f);
			}
		}
		for (int i0 = 0; i0 < i1; i0 += 2) {
			int v0 = pieces[ i0 ] & 0x7f;
			int v1 = pieces[ i0 + 1 ] & 0x7f;
			shashChars[ len2 ++ ] = (char) (0x80 | (v0 << 8) | v1);
		}
	} else if ((shashBytes[ 0 ] == (byte) 0x80) && (0 == shashBytes[ 1 ])) {
		///// Special Java marker?
		shashChars[ 0 ] = 0xf586;
		for (int i0 = 1; i0 < len; i0 += 2) {
			int v0 = (((shashBytes[ i0 - 1 ] & 0xff) << 8) | (shashBytes[ i0 ] & 0xff));
			if ((0x8000 == v0) || (0 == v0)) {
				continue;
			}
			shashChars[ len2 ++ ] = (char) v0;
		}
	} else {
		shashChars[ 0 ] = (char) ((0x80 <= shashBytes[ 0 ]) ? 0xf587 : 0xf585);
		for (int i0 = 1; i0 < len; i0 += 2) {
			int v0 = (((shashBytes[ i0 - 1 ] & 0xff) << 8) | (shashBytes[ i0 ] & 0xff));
			if (0 == v0) { // (0x20 > v0)
				continue;
			}
			shashChars[ len2 ++ ] = (char) v0;
		}
	}
	shash = new String( Arrays.copyOf( shashChars, len2 ) );
	if (cut || (Dib2Constants.SHASH_MAX <= len2)) {
		if (xSha1) {
			shashChars = Arrays.copyOf( shashChars, Dib2Constants.SHASH_MAX );
			byte[] sha1;
			try {
				sha1 = MessageDigest.getInstance( "SHA-1" ).digest( piece.getBytes( "UTF-8" ) );
			} catch (Exception e) {
				sha1 = new byte[ 20 ];
			}
			shashChars[ Dib2Constants.SHASH_MAX - 11 ] = 0xffff;
			for (int i0 = 18; i0 >= 0; i0 -= 2) {
				shashChars[ Dib2Constants.SHASH_MAX - 10 + (i0 / 2) ] //-
				= (char) (((sha1[ i0 ] & 0xff) << 8) + (sha1[ i0 + 1 ] & 0xff));
			}
		} else {
			shashChars = Arrays.copyOf( shashChars, Dib2Constants.SHASH_MAX - 1 );
		}
		shash = new String( shashChars );
	}
	return shash;
}

public static String string4Shash( String shash ) {
	int len = shash.length();
	if (0 >= len) {
		return "";
	} else if (SHASH_LITERAL > shash.charAt( 0 )) {
		if (SHASH_NEG > shash.charAt( 0 )) {
			if (SHASH_DATE > shash.charAt( 0 )) {
				return (SHASH_PUNCT == shash.charAt( 0 )) ? shash.substring( 1 ) : ".";
			}
			return date4ShashDate( shash );
		}
		//TODO: Handle extra long floats.
		double val = double4ShashNum( shash );
		return (val == (long) val) ? ("" + (long) val) : ("" + val);
	} else if (0xf580 > shash.charAt( 0 )) {
		char last = shash.charAt( len - 1 );
		len = len * 2 - 1 - ((0 == (last & 0x7f)) ? 1 : 0);
		len = (0 >= len) ? 1 : len;
		char[] out = new char[ len ];
		out[ len - 1 ] = (char) (last & 0x7f);
		boolean high = false;
		for (int i0 = shash.length() - 2; i0 >= 0; -- i0) {
			out[ i0 * 2 + 1 ] = (char) ((shash.charAt( i0 + 1 ) >>> 8) & 0x7f);
			if (out[ i0 * 2 + 1 ] <= 3) {
				high = true;
			}
			out[ i0 * 2 ] = (char) (shash.charAt( i0 ) & 0x7f);
			if (out[ i0 * 2 ] <= 3) {
				high = true;
			}
		}
		if (!high) {
			for (int i0 = out.length - 1; i0 >= 0; -- i0) {
				out[ i0 ] = collKey2Char[ out[ i0 ] ];
			}
			return new String( out );
		}
		len = 0;
		for (int i0 = 0; i0 < (out.length - 1); ++ i0) {
			if (3 >= out[ i0 ]) {
				out[ len ++ ] = (char) (0x2000 | (out[ i0 ] << 8) | out[ i0 + 1 ]);
				++ i0;
			} else {
				out[ len ++ ] = collKey2Char[ out[ i0 ] ];
			}
		}
		return new String( Arrays.copyOf( out, len ) );
	} else if (0xf586 == shash.charAt( 0 )) {
		char[] out = new char[ shash.length() - 1 ];
		for (int i0 = shash.length() - 1; i0 > 0; -- i0) {
			char val = shash.charAt( i0 );
			out[ i0 - 1 ] = (val < 0x80) ? collKey2Char[ val ] : (char) (val + collDelta[ val >>> 8 ]);
		}
		return new String( out );
	} else if (0xf600 <= shash.charAt( 0 )) {
		return "" + (char) (shash.charAt( 0 ) & 0x1ff) + shash.substring( 1 );
	}
	char last = shash.charAt( len - 1 );
	len = (len - 1) * 2 - ((0 == (last & 0x7f)) ? 1 : 0);
	len = (0 >= len) ? 1 : len;
	char[] out = new char[ len ];
	out[ len - 1 ] = collKey2Char[ last & 0xff ];
	for (int i0 = shash.length() - 2; i0 > 0; -- i0) {
		out[ i0 * 2 ] = collKey2Char[ (shash.charAt( i0 + 1 ) >>> 8) & 0xff ];
		out[ i0 * 2 - 1 ] = collKey2Char[ shash.charAt( i0 ) & 0xff ];
	}
	out[ 0 ] = collKey2Char[ (shash.charAt( 1 ) >>> 8) & 0xff ];
	return new String( out );
}

/** For quick shash calculation: does not work for numbers, not for 'sharp S'.
 * @param str Is expected to contain basic ASCII/ ANSI char's.
 * @return Shash value.
 */
public static String shashAnsi( String str ) {
	if (0 == str.length()) {
		return "";
	}
	if (0 == collChar2Key[ 0 ]) {
		if (null == coll) {
			populateCollData();
		}
	}
	if ((1 == collChar2Key[ 0 ]) || (Dib2Constants.SHASH_MAX < str.length())) {
		// Bad luck ...
		return shash( str );
	}
	int offs = (0x80 < collChar2Key[ 0 ]) ? 1 : 0;
	int marker7Bit = (0x80 < collChar2Key[ 0 ]) ? 0 : 0x80;
	char[] out = new char[ 1 + (str.length() + offs) / 2 ];
	out[ out.length - 1 ] = (char) (marker7Bit | (collChar2Key[ 0xff & str.charAt( str.length() - 1 ) ] << 8));
	for (int i0 = 2 - offs; i0 < str.length(); i0 += 2) {
		out[ i0 / 2 ] = (char) (marker7Bit | ((collChar2Key[ 0xff & str.charAt( i0 - 1 ) ] << 8) | collChar2Key[ 0xff & str.charAt( i0 ) ]));
	}
	out[ 0 ] = (1 == offs) ? collChar2Key[ 0 ] : (char) (0xf500 | collChar2Key[ 0xff & str.charAt( 0 ) ]);
	return new String( out );

}

private static int outStruct( String[] out, int cOut, String struct ) {
	if (2 == struct.length()
		&& (' ' > struct.charAt( 1 ))
		&& (0 <= ("" + UtilString.QUOTE_START + UtilString.QUOTE_END
			+ UtilString.LIST_START + UtilString.LIST_END).indexOf(
				struct.charAt( 1 ) ))) {
		out[ cOut ++ ] = SHASH_STRUC + struct.substring( 1 );
		return cOut;
	}
	boolean struc0 = 0 <= (CHARS_STRUC + CHARS_STRUC_MOD).indexOf( struct.charAt( 1 ) );
	for (int i0 = 2; i0 < struct.length(); ++ i0) {
		boolean struc = 0 <= (CHARS_STRUC + CHARS_STRUC_MOD).indexOf( struct.charAt( i0 ) );
		if (struc != struc0) {
			if (struc) {
				out[ cOut ++ ] = struct.substring( 0, i0 );
				out[ cOut ++ ] = SHASH_STRUC + struct.substring( i0 );
				return cOut;
			}
			out[ cOut ++ ] = SHASH_STRUC + struct.substring( 1, i0 );
			out[ cOut ++ ] = "" + struct.charAt( 0 ) + struct.substring( i0 );
			return cOut;
		}
	}
	if (struc0) {
		out[ cOut ++ ] = SHASH_STRUC + struct.substring( 1 );
		return cOut;
	}
	out[ cOut ++ ] = struct;
	return cOut;
}

/** Split text into 'atoms' with simplified markers for numbers, bit lists, punctuation,
 * assuming '[[' and ']]' as markers for lists. Markers:
 * SHASH_STRUC, SHASH_PUNCT, SHASH_TEMP_BITLIST, SHASH_TEMP_NUM_STRING.
 * @param xText
 * @return
 */
public static String[] splitTextAppend( String[] out, int cOut, String xText ) {
//	String[] out = new String[ 2 + 3 * in.length ];
	String[] in = xText.split( " ", -1 );
	for (String slice : in) {
		if ((0 < cOut) && (null != out[ cOut - 1 ]) && (SHASH_PUNCT == out[ cOut - 1 ].charAt( 0 ))) {
			out[ cOut - 1 ] += ' ';
			if (0 >= slice.length()) {
				continue;
			}
		} else if (0 >= slice.length()) {
			out[ cOut ++ ] = "" + SHASH_PUNCT + ' ';
			continue;
		}
		int cut = 0;
		char ch0 = slice.charAt( 0 );
		for (; cut < slice.length(); ++ cut) {
			if (false //.
				|| (('a' <= ch0) && (ch0 <= 'z')) //.
				|| (('0' <= ch0) && (ch0 <= '9')) //.
				|| (('A' <= ch0) && (ch0 <= 'Z')) //.
				|| (' ' > ch0) //.
				|| (true //.
				&& !isCurrency( ch0 ) //.
				&& (0 > (CHARS_MARKER_PLUS_LEFT + CHARS_QUOTE_LEFT + '.').indexOf( slice.charAt( cut ) ))
				)) {
				break;
			}
		}
		if (cut >= slice.length()) {
			cOut = outStruct( out, cOut, SHASH_PUNCT + slice );
			continue;
		}
		for (String piece = slice; 0 < piece.length(); cut = 0) {
			String part = piece;
			String pre = "";
			if (0 < cut) {
				// Potential further splitting later on:
				pre = SHASH_PUNCT + piece.substring( 0, cut );
				part = piece.substring( cut );
				ch0 = part.charAt( 0 );
			}
			cut = -1;
			boolean num = false;
			if (0 < CHARS_NUM_FIRST.indexOf( ch0 )) {
				Matcher mx;
				if ((mx = PATTERN_VALUE_HEX.matcher( part )).find() && (0 == mx.start())) {
					cut = mx.end();
					num = true;
				} else if ((mx = PATTERN_DIGITS_TEL_ETC.matcher( part )).find() && (0 == mx.start())) {
					cut = mx.end();
				} else if ((mx = PATTERN_OID_DIGITS_ETC.matcher( part )).find() && (0 == mx.start())) {
					cut = mx.end();
				} else if ((mx = PATTERN_DATE.matcher( part )).find() && (0 == mx.start())) {
					cut = mx.end();
				} else if ((mx = PATTERN_VALUE.matcher( part )).find() && (0 == mx.start())) {
					cut = mx.end();
					num = true;
				} else if ((mx = PATTERN_NUMBER.matcher( part )).find() && (0 == mx.start())) {
					cut = mx.end();
					if ((mx = PATTERN_TIME.matcher( part )).find() && (0 == mx.start())) {
						cut = mx.end();
					} else {
						num = true;
					}
				}
				if (num && (mx = PATTERN_UNIT.matcher( part.substring( cut ) )).find() && (0 == mx.start())) {
					cut += mx.end();
				}
			} else if ((1 < part.length() && ('\'' == part.charAt( 1 )))) {
				switch (ch0) {
					case 'P': // packed base64x
					case 'I': // oid/ indirect
					case 'X': // octets
						cut = part.lastIndexOf( '\'' );
						if (1 >= cut) {
							cut = -1;
						} else {
							part = SHASH_TEMP_BITLIST + part;
						}
					default:
						;
				}
			}
			if ((cOut + 4) >= out.length) {
				out = Arrays.copyOf( out, 2 * out.length );
			}
			if (0 > cut) {
				Matcher mx = null;
				cut = (mx = PATTERN_WORD.matcher( part )).find() ? mx.start() : -1;
				if (0 > cut) {
					pre = (0 < pre.length()) ? pre : ("" + SHASH_PUNCT);
					pre += part;
					cOut = outStruct( out, cOut, pre );
					break;
				}
				if (0 < cut) {
					pre = (0 < pre.length()) ? pre : ("" + SHASH_PUNCT);
					pre += part.substring( 0, cut );
					part = part.substring( cut );
				}
				cut = mx.end() - cut;
			} else if (num && (0 < pre.length()) && (pre.endsWith( "+" ) || pre.endsWith( "-" ))) {
				part = pre.charAt( pre.length() - 1 ) + part;
				pre = pre.substring( 0, pre.length() - 1 );
			}
			if (num) {
				if ((0 < pre.length()) && isCurrency( pre.charAt( pre.length() - 1 ) )) {
					part = pre.charAt( pre.length() - 1 ) + part;
					pre = pre.substring( 0, pre.length() - 1 );
				}
				part = SHASH_TEMP_NUM_STRING + part;
				++ cut;
			}
			if (1 < pre.length()) {
				cOut = outStruct( out, cOut, pre );
				if (0 > cut) {
					cut = 0;
				}
			}
			if ((cut >= part.length()) || (0 >= cut)) {
				out[ cOut ++ ] = part;
				break;
			}
			piece = part.substring( cut );
//			String post = SHASH_PUNCT + part.substring( cut );
			if (0 < cut) {
				out[ cOut ++ ] = part.substring( 0, cut );
			}
//			if (1 < post.length()) {
//				cOut = outStruct( out, cOut, post );
//			}
		}
	}
	if (cOut < out.length) {
		out[ out.length - 1 ] = "\ue000" + (char) cOut;
	}
	return out;
}

/** 
 * Tokenize text into tokens plus formatting and structuring elements, based on AsciiDoc.
 * Special usage for '{{' as quote, '[[' as sequence, '_' as escape ...
 * @param xText plain text to be split
 * @return triples of data elements: format, token (normalized), quoted text part */
/*
formatLeft: ..' *'/ ..' _'/ ...:  *bold, _italic, +mono, ^sup, ~sub, #[attrib]#
(optional '.' after ' ' prevents using ' ' itself as separator)
formatRight: ..'* '/ ... / ..'# ' (succeeding ' ' or '\n' !)
special char: '#': after ' ' or '\n' as combiner (see below)
formatX: ' .'/ '\n<<<\n'=FF, '\n+\n' continuation with line break
struc: '\n\n' (explicit) overall end, [^ \n]'.'[^ \n] mid-num or selector,
'\n//' comment, \n==+ header, \n.[^ \n] or \n; title
'\n-* ' set, \n*+ list, \n.+ array, (these with optional label marker:) '::',
'\n| ' table row,
'[', ']', '<', ..., '{', '}', '${', '#{', ...
usage of '#' after ' ' and '\n' (#. erases preceding ' ' or '\n'):
#{ struc, #[ format/attrib, #[[ anchor/id, #( RFU, #< RFU,
'#'[0-9*#+-] marker for string of digits (e.g. telephone number),
#$ marker for float, #' marker for other string,
 */
public static String[] toElements_OLD( String xText ) {
	if ((null == xText) || (0 >= xText.length())) {
		return new String[ 0 ]; // new String[] { "" };
	}
	String text = xText;
	if (!Normalizer.isNormalized( text, Normalizer.Form.NFKC )) {
		text = Normalizer.normalize( text, Normalizer.Form.NFKC );
	}
	// Replace control char's to make room for own usage.
	if ((0 <= text.indexOf( '\u0000' )) || (0 <= text.indexOf( '\u0001' )) || (0 <= text.indexOf( '\u0002' ))
		|| (0 <= text.indexOf( '\u0003' ))) {
		char[] tx = text.toCharArray();
		for (int i0 = tx.length - 1; 0 <= i0; -- i0) {
			if (3 >= tx[ i0 ]) {
				tx[ i0 ] += 0xf600;
			}
		}
		text = new String( tx );
	}
	ArrayList< String > out = new ArrayList< String >( text.length() / 4 );
	Matcher MATCHER_LINE_BREAK_TAB = PATTERN_LINE_BREAK_TAB.matcher( text );
	String left = "";
	int suppressBlanks = 1;
	// Split by line breaks.
	for (int lineOffs = 0, lineEnd, lineNext; lineOffs < text.length() //- 
	; lineOffs = lineNext, left = text.substring( lineEnd, lineNext )) {
		lineEnd = MATCHER_LINE_BREAK_TAB.find() ? MATCHER_LINE_BREAK_TAB.start() : text.length();
		lineNext = (lineEnd < text.length()) ? MATCHER_LINE_BREAK_TAB.end() : lineEnd;
		String linex = text.substring( lineOffs, lineEnd );
		String line = PATTERN_SPACE_BEGIN.matcher( linex ).replaceFirst( "" );
		int pieceOffs = linex.length() - line.length();
		line = PATTERN_SPACE_END.matcher( line ).replaceFirst( "" );
		String struc = "";
		String label = "\n";
		if (line.isEmpty()) {
			// End of block.
			suppressBlanks = 0;
			out.add( "" + (char) (SHASH_FORMAT_RIGHT + '\n') );
			out.add( "\n" );
			out.add( left + linex );
			continue;
		} else if ("<<<".equals( line )) {
			// Page break.
			suppressBlanks = 0;
			out.add( "" );
			out.add( "\f" );
			out.add( left + linex );
			continue;
		} else if ("+".equals( line )) {
			// Continuation.
			suppressBlanks = 1;
			out.add( "" + (char) (SHASH_FORMAT_RIGHT + '\n') );
			out.add( "" );
			out.add( left + linex );
			continue;
		} else if (line.startsWith( "// " )) {
			// Comment.
			suppressBlanks = 1;
			out.add( "" );
			out.add( "" );
			out.add( left + linex );
			continue;
		} else if ((('.' == line.charAt( 0 )) || (';' == line.charAt( 0 ))) && (2 <= line.length()) && (' ' < line.charAt( 1 ))) {
			// Title.
			suppressBlanks = 1;
			++ pieceOffs;
			struc = "" + (char) (SHASH_FORMAT_RIGHT + '\n') + (char) (SHASH_STRUC + ';');
		} else if ((0 <= "=-*.|;".indexOf( line.charAt( 0 ) )) && (1 <= line.indexOf( ' ' ))) {
			// Header/ list/ table.
			out.add( "" + (char) (SHASH_FORMAT_RIGHT + '\n') );
			out.add( "\n" );
			label = "";
			out.add( left );
			left = "";
			for (++ pieceOffs; pieceOffs < linex.length(); ++ pieceOffs) {
				if (line.charAt( 0 ) != linex.charAt( pieceOffs )) {
					break;
				}
			}
			if (' ' == linex.charAt( pieceOffs )) {
				suppressBlanks = 1;
				int iLabel = pieceOffs + linex.indexOf( ':', pieceOffs );
				if ((iLabel < (lineNext - 1)) && (':' == linex.charAt( iLabel + 1 ))) {
					label = linex.substring( pieceOffs + 1, iLabel ).trim();
					label += "::";
					pieceOffs = iLabel + 2;
				}
				++ pieceOffs;
				struc = "" + (char) (SHASH_STRUC + line.charAt( 0 ));
			}
		}
		if ((0 < struc.length()) || (0 < lineOffs)) {
			out.add( struc );
			out.add( label );
			out.add( left + linex.substring( 0, pieceOffs ) );
		}
		left = "";
		line = linex;
		Matcher MATCHER_SPACE_NL_SEQ = PATTERN_SPACE_NL_SEQ.matcher( line );
		// Split by words.
		for (int pieceNext; pieceOffs < line.length(); pieceOffs = pieceNext) {
			int pieceEnd = MATCHER_SPACE_NL_SEQ.find() ? MATCHER_SPACE_NL_SEQ.start() : line.length();
			pieceNext = (pieceEnd < line.length()) ? MATCHER_SPACE_NL_SEQ.end() : pieceEnd;
			struc = "";
			// Catch up with offset:
			if (pieceOffs >= pieceEnd) {
				if (0 >= suppressBlanks) {
					out.add( " " );
					out.add( " " );
					out.add( "" );
				}
				suppressBlanks = 1;
				if (pieceNext < pieceOffs) {
					pieceNext = pieceOffs;
				}
				continue;
			}
			String piece = line.substring( pieceOffs, pieceEnd );
			int elOffs = 0;
			boolean forceFloat = false;
			boolean forceString = false;
			String format = "";
			String show = "";
			if (('#' == piece.charAt( 0 )) && ((1 >= pieceEnd) || (0 > CHARS_STRUC.indexOf( piece.charAt( 1 ) )))) {
				// Dangling '#'.
				if (piece.startsWith( "#." )) {
					suppressBlanks = 2;
					elOffs = 2;
				}
				forceString = true;
				struc = "" + (char) (SHASH_STRUC | '\'');
				if ((elOffs + 1) < pieceEnd) {
					++ elOffs;
					if ('$' == piece.charAt( elOffs )) {
						++ elOffs;
						forceString = false;
						forceFloat = true;
						struc = "" + (char) (SHASH_STRUC | '$');
					} else {
						elOffs += ('\'' == piece.charAt( elOffs )) ? 1 : 0;
					}
				}
			} else if ((0 <= (CHARS_MARKER_DOLLAR + '.').indexOf( piece.charAt( 0 ) ))) {
				// Find all markers.
				for (; elOffs < piece.length(); ++ elOffs) {
					if ('.' == piece.charAt( elOffs )) {
						suppressBlanks = 2;
					} else if ('#' == piece.charAt( elOffs )) {
						if (piece.substring( elOffs ).startsWith( "#[" ) && line.contains( "]#" )) {
							// Attribute/ anchor.
							int to = pieceOffs + line.indexOf( "]#" ) + 2;
							out.add( "" + (char) (SHASH_FORMAT_LEFT | '#') );
							out.add( line.substring( elOffs + 2, to - 2 ) );
							out.add( "" );
							piece = "";
							elOffs = to;
						} else {
							// Structural.
							show += piece.charAt( elOffs );
							struc += (char) (SHASH_STRUC | piece.charAt( elOffs )); // (piece.substring( elOffs ).startsWith( "#{" ) ? '#' : '^'));
						}
					} else if ((0 <= (CHARS_FORMAT + '#').indexOf( piece.charAt( elOffs ) ))) {
						format += ("" + (char) (SHASH_FORMAT_LEFT | piece.charAt( elOffs )));
					} else if (0 <= CHARS_MARKER_DOLLAR.indexOf( piece.charAt( elOffs ) )) {
						show += piece.charAt( elOffs );
						struc += (char) (SHASH_STRUC | piece.charAt( elOffs ));
					} else {
						break;
					}
				}
				if ((elOffs >= piece.length()) || (' ' >= piece.charAt( elOffs ))) {
					elOffs = 0;
				}
			}
			// Eject matching pieces.
			left += piece.substring( 0, elOffs );
			String blanks = (0 >= suppressBlanks) ? " " : "";
			if ((0 < format.length() || (0 < blanks.length()))) {
				out.add( "" + format );
				out.add( blanks );
				out.add( left );
				left = "";
			}
			if (0 < struc.length()) {
				out.add( struc );
				out.add( show );
				out.add( left );
				left = "";
			}
			if (0 < left.length()) {
				out.add( "" );
				out.add( "" );
				out.add( left );
				left = "";
			}
			suppressBlanks -= (0 < suppressBlanks) ? 1 : 0;
			// Find end of text pieces.
			int iMark = piece.length() - 1;
			if (!forceFloat && !forceString) {
				for (; iMark > elOffs; -- iMark) {
					if (0 > ((CHARS_FORMAT + '#') + CHARS_STRUC).indexOf( piece.charAt( iMark ) )) {
						break;
					}
				}
			}
			++ iMark;
			// Get text pieces.
			for (int next = elOffs; elOffs < iMark; elOffs = next) {

				//TODO Handle X'...' literal

//				next = elOffs;
				String part = piece.substring( elOffs, iMark );
				if (!forceString
					&& (forceFloat || ((0 <= CHARS_NUM_FIRST.indexOf( piece.charAt( elOffs ) ))
					&& !piece.substring( elOffs ).startsWith( "#{" )))) {
					boolean num = false;
					Matcher mx;
					if ((mx = PATTERN_VALUE_HEX.matcher( part )).find() && (0 == mx.start())) {
						next += mx.end();
						num = true;
					} else if ((mx = PATTERN_DIGITS_TEL_ETC.matcher( part )).find() && (0 == mx.start())) {
						next += mx.end();
					} else if ((mx = PATTERN_OID_DIGITS_ETC.matcher( part )).find() && (0 == mx.start())) {
						next += mx.end();
					} else if ((mx = PATTERN_DATE.matcher( part )).find() && (0 == mx.start())) {
						next += mx.end();
					} else if ((mx = PATTERN_VALUE.matcher( part )).find() && (0 == mx.start())) {
						next += mx.end();
						num = true;
					} else if ((mx = PATTERN_NUMBER.matcher( part )).find() && (0 == mx.start())) {
						int ex = mx.end();
						if ((mx = PATTERN_TIME.matcher( part )).find() && (0 == mx.start())) {
							next = iMark;
						} else {
							next += ex;
							num = true;
						}
					} else {
						for (++ next; next < iMark; ++ next) {
							if (0 > CHARS_NUM_FIRST.indexOf( piece.charAt( next ) )) {
								break;
							}
						}
					}
					if (num && (mx = PATTERN_UNIT.matcher( piece.substring( next ) )).find() && (0 == mx.start())) {
						next += mx.end();
					}
					if (elOffs < next) {
						out.add( "" );
						out.add( piece.substring( elOffs, next ) );
						out.add( piece.substring( elOffs, next ) );
					}
				} else {
					Matcher mx = null;
					int skip = (!forceString && (mx = PATTERN_WORD.matcher( part )).find()) ? mx.start() : -1;
					if (0 < skip) {
						out.add( "" );
						out.add( "" );
						out.add( part.substring( 0, skip ) );
					}
					next = (0 <= skip) ? mx.end() : part.length();
					skip = (0 > skip) ? 0 : skip;
					out.add( "" );
					out.add( part.substring( skip, next ) );
					out.add( part.substring( skip, next ) );
					next += elOffs;
				}
			}
			if (elOffs < iMark) {
				out.add( piece.substring( elOffs, iMark ) );
				out.add( "" );
				out.add( piece.substring( elOffs, iMark ) );
			}
			format = struc = show = "";
			if (iMark < piece.length()) {
				String lx = piece.substring( iMark );
				for (elOffs = iMark; elOffs < piece.length(); ++ elOffs) {
					if ((0 <= (CHARS_FORMAT + '#').indexOf( piece.charAt( elOffs ) ))) {
						format += ("" + (char) (SHASH_FORMAT_RIGHT | piece.charAt( elOffs )));
					} else {
						show += piece.charAt( elOffs );
						struc += (char) (SHASH_STRUC | (piece.charAt( elOffs ) & 0x7f));
					}
				}
				if (0 < format.length()) {
					out.add( "" + format );
					out.add( show );
					show = "";
					out.add( lx );
					lx = "";
				}
				if ((0 < struc.length()) || (0 < lx.length())) {
					out.add( struc );
					out.add( show );
					out.add( lx );
				}
			}
			left += line.substring( pieceEnd, pieceNext );
		}
		if (0 < left.length()) {
			out.add( "" );
			out.add( "" );
			out.add( left );
		}
	}
	if (0 < left.length()) {
		out.add( "" );
		out.add( "\n" );
		out.add( left );
	}
	return out.toArray( new String[ 0 ] );
}

public static String[] sortStd( String[] strings ) {
	CollationKey[] keys = new CollationKey[ strings.length ];
	for (int i0 = 0; i0 < strings.length; ++ i0) {
		keys[ i0 ] = coll.getCollationKey( strings[ i0 ] );
	}
	Arrays.sort( keys );
	String[] out = new String[ strings.length ];
	for (int i0 = 0; i0 < out.length; ++ i0) {
		out[ i0 ] = keys[ i0 ].getSourceString();
	}
	return out;
}

/** Calculate sortable hash (shash) values.
 * @param xSha1 for making it unique by appending SHA-1 value in case of long string
 * @param xaElements text or float or marker etc.
 * @return per element: up to (SHASH_MAX-2) chars or truncated (SHASH_MAX-1) chars or (SHASH_MAX-11) chars + X + SHA1
 */
public static String[] shash( boolean xSha1, String... xaElements ) {

	if (null == coll) {
		populateCollData();
	}
	String[] out = new String[ xaElements.length ];
	int inx = 0;
	boolean forceString = false;
	for (String piece : xaElements) {
		if ((null == piece) || (0 >= piece.length())) {
			out[ inx ++ ] = piece;
			continue;
//		} else if (xPacked && (0 <= piece.indexOf( SHASH_QM ))) {
//			forceString = (0 <= piece.indexOf( (char) (SHASH_STRUC | '\'') ));
//			out[ inx ++ ] = (piece.contains( "" + SHASH_QM + SHASH_QM )) ? "" : ("" + SHASH_QX);
//			continue;
		} else {
			forceString = false;
		}
		boolean num0 = !forceString && (0 <= CHARS_NUM_FIRST.indexOf( piece.charAt( 0 ) ));
		double frac = 0;
		String shash = "";
		// Units:
		String units = null;
		if (num0) {
			int iUnit = piece.lastIndexOf( '_' );
			if ((0 < iUnit) && (iUnit < (piece.length() - 1)) && (' ' < piece.charAt( iUnit + 1 ))) {
				if (PATTERN_UNIT.matcher( piece.substring( iUnit ) ).matches()
					&& !PATTERN_NUMBER.matcher( piece.substring( iUnit ) ).matches()) {
					units = piece.substring( iUnit + 1 );
					piece = piece.substring( 0, iUnit );
				}
			}
		}

		if (num0 && PATTERN_VALUE_HEX.matcher( piece ).matches()) {
			if ((0 > piece.indexOf( 'p' )) && (0 > piece.indexOf( 'P' ))) {
				piece += "p0";
			}
		} else if (num0 && PATTERN_DIGITS_TEL_ETC.matcher( piece ).matches()) {
			num0 = false;
		} else if (num0 && PATTERN_OID_DIGITS_ETC.matcher( piece ).matches()) {
			num0 = false;
		} else if (num0 && (PATTERN_DATE.matcher( piece ).matches()) || PATTERN_DATE_D.matcher( piece ).matches()) {
			shash = shash4Date( piece );
		} else if (num0 && PATTERN_VALUE.matcher( piece ).matches()) {
		} else if (num0 && PATTERN_NUMBER.matcher( piece ).matches()) {
		} else if ( // floatError || 
		piece.matches( REGEX_WORD )) {
			num0 = false;
		}
		if (num0) {
			if (0 >= shash.length()) {
				// German decimal point etc.:
				if (0 <= piece.indexOf( ',' )) {
					if (piece.indexOf( ',' ) != piece.lastIndexOf( ',' )) {
						piece = piece.replace( ",", "" );
					} else {
						piece = piece.replace( ".", "" ).replace( ",", "." );
					}
				}
				piece = PATTERN_NUMBER_SEP.matcher( piece ).replaceAll( "" );
				//TODO: Extra long floats.
				String shashTmp = shash4Double( piece, frac );
				shash = (null == shashTmp) ? shash : shashTmp;
				if (null == shashTmp) {
//				floatError = true;
					num0 = false;
					units = null;
				}
				if ((null != units) && (0 < units.length())) {
					//TODO: add precision info:
					char[] unit2 = new char[ units.length() / 2 + 1 ];
					int count = 0;
					unit2[ 0 ] = SHASH_NUM_UNIT;
					for (char unit : units.toCharArray()) {
						if (0 < unit2[ count ]) {
							unit2[ count ++ ] |= (0x80 | (unit & 0x7f));
						} else {
							unit2[ count ] = (char) (((unit & 0x7f) << 8) | 0x80);
						}
					}
					shash = new String( unit2 ) + shash;
				}
			}
		}
		if (!num0) {
			if (null != units) {
				piece += "_" + units;
			}
			shash = shash4String( piece, xSha1 );
		}
		out[ inx ++ ] = shash;
	}
	return out;
}

public static String shash( String... xStr ) {
	if (1 == xStr.length) {
		return shash( false, xStr )[ 0 ];
	}
	String[] out = shash( false, xStr );
	StringBuilder shash = new StringBuilder( Dib2Constants.SHASH_MAX * 2 );
	String sep = "";
	for (String piece : out) {
		shash.append( sep );
		sep = "" + (char) 1;
		shash.append( piece );
		if (Dib2Constants.SHASH_MAX <= shash.length()) {
			break;
		}
	}
	return (Dib2Constants.SHASH_MAX <= shash.length()) ? shash.substring( 0, Dib2Constants.SHASH_MAX - 1 ) : shash.toString();
}

//=====
}
