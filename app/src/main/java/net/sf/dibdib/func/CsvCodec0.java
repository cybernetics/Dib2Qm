// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.func;

import java.util.*;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.join.CsvCodecIf;

/** Basic methods plus dummy implementation of interface for debugging. */
public class CsvCodec0 implements CsvCodecIf {
//=====

public static final CsvCodec0 instance = new CsvCodec0();

@Override
public byte[] init( char platform, Object... parameters ) {
	return null;
}

@Override
public byte[] compress( byte[] xyData, int xOffset4Reuse, int to ) {
	return Arrays.copyOfRange( xyData, xOffset4Reuse, to );
}

@Override
public byte[] decompress( byte[] xData, int length ) {
	return Arrays.copyOf( xData, length );
}

@Override
public byte[] getKey( byte[] phrase, byte[] accessCode, byte[] salt, int iterations ) {
	return phrase;
}

@Override
public String getKeyInfo( HashSet< String > entries ) {
	return "(keyInfo)";
}

@Override
public byte[] encode( byte[] compressedData, int from, int to, byte[] key, byte[] iv16, int keyInfo, byte[] keyData, byte[] signatureKey )
	throws Exception {
//	byte[] out = encode4DerivedKey( compressedData, from, to, key );
//	return Arrays.copyOf( out, out.length );
	return Arrays.copyOfRange( compressedData, from, to );
}

@Override
/** For debugging: pass is not used */
public byte[] decode( byte[] data, int offset, int len, byte[] keyOrPass, byte[] signatureKey ) throws Exception {
	int offs = offset;
	if (data[ offs ] == (byte) Dib2Constants.RFC4880_EXP2) {
		offs += UtilMisc.getPacketHeaderLen( data, offs );
	} else {
		if ((data[ offs ] != Dib2Constants.MAGIC_BYTES[ 0 ]) || (data[ offs + Dib2Constants.MAGIC_BYTES.length + 1 ] != getMethodTag())) {
			return null;
		}
	}
	return Arrays.copyOfRange( data, offs + 16, offset + len );
}

@Override
public byte getMethodTag() {
	return '0'; // dummy - plain
}

@Override
public byte[] getInitialValue( int len ) {
	return Arrays.copyOf( "0123456789".getBytes( UtilString.STR256 ), len );
}

@Override
public byte[] encodePhrase( byte[] phrase, byte[] accessCode ) {
	return phrase;
}

@Override
public byte[] decodePhrase( byte[] encoded, byte[] accessCode ) {
	return encoded;
}

//=====
}
