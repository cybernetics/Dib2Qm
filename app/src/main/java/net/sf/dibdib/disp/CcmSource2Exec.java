// Copyright (C) 2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.disp;

import net.sf.dibdib.prcs.ExecuteCmd;
import net.sf.dibdib.struc.*;

public class CcmSource2Exec extends QDispatcher {
//=====

private ExecuteCmd mProcExec;

public void init( ExecuteCmd procExec ) {
	mProcExec = procExec;
}

public boolean takeCmd( CommandBaton cmd ) {
	// Do not pass it on immediately: keep it here as an indication for the other side.
	return 0 <= wxPlace.push( cmd );
}

@Override
public boolean take( QBaton main ) {
	return takeCmd( (CommandBaton) main );
}

@Override
public QBaton step() {
	QBaton out = wxPlace.pull();
	if (null != out) {
		mProcExec.takeCommand( (CommandBaton) out );
	}
	return out;
}

//=====
}
