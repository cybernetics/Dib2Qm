// Copyright (C) 2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.disp;

import net.sf.dibdib.config.Dib2Config;
import net.sf.dibdib.struc.QBaton;

public class CcmSink4External extends QDispatcher {
//=====

@Override
public boolean take( QBaton main ) {
	boolean out = super.take( main );
	// Notify the other side:
	Dib2Config.platform.invalidate();
	return out;
}

@Override
public QBaton step() {
	return wxPlace.pull();
}

//=====
}
