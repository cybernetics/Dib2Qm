// Copyright (C) 2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.disp;

import net.sf.dibdib.prcs.ExecSlow;
import net.sf.dibdib.struc.*;

public class ExecuteCmd2SlowSink extends QDispatcher {
//=====

private CcmSink4External mSink;
private ExecSlow mProcSlow;

public void init( ExecSlow procSlow, CcmSink4External sink ) {
	mSink = sink;
	mProcSlow = procSlow;
}

@Override
public boolean take( QBaton main ) {
	///// Pass it right on.
	if (!(main instanceof CommandBaton)) {
		return mSink.take( main );
	}
	return mProcSlow.take( main );
}

//=====
}
