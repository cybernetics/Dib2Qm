// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.disp;

import net.sf.dibdib.prcs.QPlacesTransitionsIf;
import net.sf.dibdib.struc.*;

/** At least one of the methods need to be overridden.
 */
public abstract class QDispatcher implements QPlacesTransitionsIf {
//=====

///// Threaded (in/ out/ trigger)

/** The one and only Place for dispatchers. */
public final QPlace wxPlace = new QPlace();

/////

/** Runs on (single) Baton (token) provider's thread. Needs to be complemented by more methods
 * for each provider -- or synchronized if used by multiple providers.
 * Subclasses may implement it so that the baton gets passed on directly.
 * @param main For Baton from main provider (preceding transition).
 * @return true if done
 */
@Override
public boolean take( QBaton main ) {
	return 0 <= wxPlace.push( main );
}

/** Runs on processing/ dispatching thread. The default implementation works only
 * if take() passes the token on directly instead of storing it.
 */
@Override
public QBaton step() {
	// This works only if take() had passed the Baton on.
	return null;
}

//=====
}
