// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.disp;

import java.io.File;
import net.sf.dibdib.config.*;
import net.sf.dibdib.func.UtilMisc;
import net.sf.dibdib.prcs.*;
import net.sf.dibdib.repr.UiCalcServer;
import net.sf.dibdib.store.*;
import net.sf.dibdib.struc.*;

import static net.sf.dibdib.store.UiSwitches.*;

/** For platform's worker thread (CCM net). */
public enum CcmRunner implements Runnable { //, QThreadProviderIf {
//=====

INSTANCE;

///// Threaded (in/ out/ trigger)

public CcmSource2Exec qGateIn = null;
public CcmSink4External qGateOut = null;
public static boolean wxSave = false;

/////

public static volatile Thread qActive = null;
public static volatile boolean qBusy = false;
private static long zLastSave = 0;
private static long zLastBackup = 0;

public ExecSlow qProcessSlow = null;
public ExecuteCmd qProcessFast = null;

public ExecuteCmd2SlowSink qDispFast2Slow = null;

public void init( boolean createGateways, boolean createNet ) {
	if (createGateways) {
		qGateIn = new CcmSource2Exec();
		qGateOut = new CcmSink4External();
	}
	if (createNet) {
		qProcessFast = new ExecuteCmd();
		qProcessSlow = new ExecSlow();
		qDispFast2Slow = new ExecuteCmd2SlowSink();
//		qDispSlow2Sink = new ExecSlow2Sink();
		qGateIn.init( qProcessFast );
		qProcessFast.init( qDispFast2Slow );
	}
}

public boolean isBusy() {
	return ((null != qActive) && qBusy) || !qGateIn.wxPlace.empty();
}

public static String check4Load() {
	File dir = Dib2Config.platform.getFilesDir( "safe" );
	File file = new File( dir, Dib2Config.dbFileName );
	String path = file.isFile() ? file.getAbsolutePath() : null;
	if (null != path) {
		return path;
	}
	path = UiCalcServer.findLatest( dir );
	if (null != path) {
		return path;
	}
	dir = Dib2Config.platform.getFilesDir( "external" );
	if ((null != dir) && (dir.exists())) {
		path = UiCalcServer.findLatest( dir );
	}
	return path;
}

public static boolean initLoad( boolean force ) {
	boolean done = false;
	if (!force && CsvDb.instance.isInitialized()) {
		UiSwitches.pathInitDataFile = null;
		UiSwitches.qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 999;
		return true;
	}
	// Check if proper phrase is available:
	final boolean dummy = !Codec.instance.setHexPhrase( "" );
	final byte[] pass = Codec.instance.getPassFull();
//	if ((100 > qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) && !CsvCodec0.setDummyPhrase( false )) {
	if (null != pass) {
		File dir = Dib2Config.platform.getFilesDir( "safe" );
		File file = new File( dir, Dib2Config.dbFileName );
		String path = file.isFile() ? file.getAbsolutePath() : null;
		if (null == path) {
			path = UiCalcServer.findLatest( dir );
		}
		if (null != path) {
			done = (0 <= CsvDb.instance.load( path ));
			if (done || (dummy && new File( path ).exists())) {
				// Do not pull old data if password had been set.
			} else if (new File( path + ".bak" ).isFile()) {
				done = done || (0 <= CsvDb.instance.load( path + ".bak" ));
				done = done || (0 <= CsvDb.instance.load( path + ".old" ));
			}
		} else {
			dir = Dib2Config.platform.getFilesDir( "external" );
			if ((null != dir) && (dir.exists())) {
				path = UiCalcServer.findLatest( dir );
				if (null != path) {
					done = done || (0 <= CsvDb.instance.load( new File( path ).getAbsolutePath() ));
				}
			}
		}
	}
	if (done) {
		UiSwitches.pathInitDataFile = null;
		UiSwitches.qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 999;
		if (!dummy) {
			File downloadFolder = Dib2Config.platform.getFilesDir( "external" );
			if (null != downloadFolder) {
				String name = UiCalcServer.getShortDay().substring( 4, 6 );
				name = Dib2Config.dbFileName.replace( ".dm", ".bak.dm" );
				zLastBackup = 1 + new File( downloadFolder, name ).lastModified();
			}
		}
	} else if (200 > UiSwitches.qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
		UiSwitches.qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 200;
	}
	return done;
}

public void addSamples() {
	CsvDb.instance.stackPush( new QVal( null, 3.0 ) );
	CsvDb.instance.stackPush( new QVal( null, 2.0 ) );
}

public void pushResult( QVal[] vals, int pos ) {
	if ((null == vals) || (0 >= vals.length)) {
		return;
	}
	CsvDb.instance.stackInsert( pos, vals );
}

private synchronized void queueMe() {
	if (null != qActive) {
		Thread.yield();
		for (int i0 = 0; (i0 <= 7) && (null != qActive); ++ i0) {
			if (7 == i0) {
				qActive.interrupt();
			}
			try {
				Thread.sleep( 70 );
			} catch (InterruptedException e) {
			}
		}
	}
	qActive = Thread.currentThread();
}

public static synchronized void saveAll( long bFlags_toDownloads_asBak ) {
	if (0 == bFlags_toDownloads_asBak) {
		Codec.instance.writePhrase();
	}
	File file = new File( Dib2Config.platform.getFilesDir( "safe" ), Dib2Config.dbFileName );
	String path = file.getAbsolutePath();
	if (file.exists()) {
		File old = new File( path + ((0 != bFlags_toDownloads_asBak) ? ".old" : ".bak") );
		if (old.exists()) {
			old.delete();
		}
		file.renameTo( old );
	}
	CsvDb.instance.save( path, true );
	zLastSave = UtilMisc.currentTimeMillisLinearized();
	if (0 != (1 & bFlags_toDownloads_asBak)) {
		File downloadFolder = Dib2Config.platform.getFilesDir( "external" );
		if (null != downloadFolder) {
			String name = UiCalcServer.getShortDay().substring( 4, 6 );
			name = Dib2Config.dbFileName.replace( ".dm", "."
				+ ((0 != (2 & bFlags_toDownloads_asBak)) ? "bak" : name)
				+ ".dm" );
			file = new File( downloadFolder, name );
			if ((3 != bFlags_toDownloads_asBak)
				|| ((file.lastModified() + 2 * 3600 * 1000) < UtilMisc.currentTimeMillisLinearized())) {
				path = file.getAbsolutePath();
				CsvDb.instance.save( path, true );
				zLastBackup = zLastSave;
			}
		}
	}
}

@Override
public void run() {
	queueMe();
//	Log.d( "run", "start " + QResult.getThreadIndex() ); // + Dib2UiP.INSTANCE.wPageLines[ 0 ] );
	QResult.getThreadIndex();
	try {
		if (1000 > qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
			qGateIn.wxPlace.flush( false );
			if (null != UiSwitches.pathInitDataFile) { // && !CsvDb.instance.isInitialized()) { // (0 > qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
				qBusy = true;
				UiCalcServer.INSTANCE.invalidate();
				if (initLoad( false )) {
					qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 990;
				} else if (200 > qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
					qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 200;
				}
			}
		} else {
			while (!UiCalcServer.wxExitRequest) {
				boolean active = false;
				active = (null != qGateIn.step()) || active;
				active = (null != qProcessFast.step()) || active;
				active = (null != qDispFast2Slow.step()) || active;
				active = (null != qProcessSlow.step()) || active;
				if (!active) {
					break;
				}
				qBusy = true;
				// Show progress (qActive != null !):
				UiCalcServer.INSTANCE.invalidate();
				Thread.yield();
			}
		}
	} catch (Exception e) {
		//Log.d( "CcmRunner", "exception " + e );
	}
//	Log.d( "run", "end " + QResult.getThreadIndex() ); // + Dib2UiP.INSTANCE.wPageLines[ 0 ] );
	qActive = null;
	qBusy = false;
	QResult.drop8Pool();
	// Final refresh
	UiCalcServer.INSTANCE.invalidate();
	if (1000 < qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
		if (wxSave) {
			wxSave = false;
			CcmRunner.saveAll( 0 );
		} else if (((zLastBackup + Dib2Constants.MAX_DELTA_ACCESS_CHECK) < UtilMisc.currentTimeMillisLinearized())
			&& (0 < zLastBackup)) {
			if (zLastSave > 1000) {
				CcmRunner.saveAll( 3 );
			}
		}
	}
}

//=====
}
