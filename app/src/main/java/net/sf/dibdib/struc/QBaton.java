// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.struc;

import java.lang.reflect.Array;
import java.util.Arrays;
import net.sf.dibdib.func.UtilMisc;

/** For token flow model, to be extended. Basic functions also work with other Objects.
 * Based on Petri net (Arc weight = 1) with typed Places for data tokens (Baton).
 * (Places impose guards on their preceding Transitions):
 * Outgoing token batons are collected in arrays. Once the receiving elements are ready,
 * they take the whole array.
 * Elements of net ('Context'):
 * - Process (synchronized split): incoming Places with single Arcs + single Transition.
 * - Dispatcher (merged choice): single (unguarded) incoming Place + identity Transitions.
 * - Store (merged source): single guarded Place + copy Transitions with returning Arc.
 * - Plus Terminators (source, sink) for workflow: used for gateways/ bridges/ external transitions.
 */
public class QBaton {
//=====

//Token-Oriented-Programming (TOP):
//
//IDEA
//
//Petri Arcs are implemented as simple pipes with a buffering end-point and a referencing start-point.
//Petri Places and Transitions are 'normalized' by specific element groups: Transposers and Stores.
//Transposers = active elements: Process, Dispatcher/ trigger, Terminator (sink/ source).
//Transposers have buffered ports for their incoming batons (= arguments),
//plus virtual ports for their connections (as callable functions) to Stores (= parameters).
//Outgoing ports (= results) are implemented as a simple reference to the peer's in-port buffer.
//==> No queue or extra container for Places: avoids internal race conditions.
//==> Baton values as arguments, Store values as parameters for Transposers.
//
//MODEL
//
//PortBuffer 'x': buffering endpoint of Petri Arc for Petri Place 'O':
//- Process fires if all in-ports/ places have data.
//- Dispatcher collects data (unguarded) and passes it on according to Transition's guards.
//- Store is implemented as container with synchronized methods
//('put' for ports, 'pick' for Transitions).
//
//Process;               Dispatcher:            Store ('synchronized', feeding data tokens)
//---x O -\             ---x\      /-|--->          <----.
//         \|--->            \    /            --x  v   /|--->
//---x O ---|           ----x O <              ---x O -<
//         /|--->            /    \            --x  ^   \|--->
//---x O -/             ---x/      \-|--->          <----'
//
//Error handling: asst() for assertions logs ERROR line.
//==> Use special sink for errors and status messages, use error baton for flushing pipes.
//==> OR: Baton may get swallowed, Terminator checks with popLog().
//
//IMPLEMENTATION
//
//Transposers and Stores are implemented within Contexts (containers), which are connected by
//Gateways (bridges): Sources and Sinks as Context gates are the end-points for the Gateways.
//Batons do not carry the 'full load' of data from the model above, because the data
//gets stored in stateful data containers, i.e. a container implements the behavior of a Store.
//Stores become stateful (e.g. backed by files), Transposers are stateless!
//Batons and the Container entries have a timestamp, i.e. containers keep latest versions.
//Batons are used for presentation data (e.g. filtered data), Stores can also be used as
//optional read-only gates of the Context (e.g. for dumping all data).
//Consistency of the data is possible using the timestamps: only if all tokens with a certain
//timestamp have entered a sink and no token with a lower timestamp is left,
//then the Context data with that timestamp is considered done.
//If necessary, a rollback to that state of the Context data is possible.
//Due to parallel processing of tokens the timestamped data values may have 'glitches'
//=> Transposers and Stores use the timestamps to minimize non-deterministic values.
;

/** msec value (0 if trashed), lowest 2 bits may be adjusted according to 'TIME_SHIFTED'. */
public long timeStamp;

/** Each Baton has its own stack for processing */
//public Object[] stack;

/////

public static final QBaton DUMMY = new QBaton();
public static final QBaton EMPTY = new QBaton();
public static final QBaton PENDING = new QBaton();

/////

public void setTimeStamp() {
	timeStamp = UtilMisc.currentTimeMillisLinearized();
}

///// OLD

// Assertion:
// crash == true ==> on
// crash == false ==> log, remove token
// method commented ==> off
@SuppressWarnings( "rawtypes" )
public static boolean asst( boolean assertion, String error, Class... affected ) {
	//TODO
	return assertion;
}

/** Check if not ready for processing. This method may be called by owning thread and by
 * other threads! => Race condition might cause temporary delays, but consistency remains.
 * @param xIncomingBatonPlaces
 * @param xInternalBatonPlaces
 * @return
 */
public static boolean lacking( Object[] xIncomingBatonPlaces, Object[] xInternalBatonPlaces ) {
	boolean out = false;
	for (int i1 = xIncomingBatonPlaces.length - 1; i1 >= 0; -- i1) {
		out = out || ((null == xIncomingBatonPlaces[ i1 ]) && (null == xInternalBatonPlaces[ i1 ]));
	}
	return out;
}

public static boolean pullBatons( boolean yield, Object[] xyIncomingBatonPlaces, Object[] xyInternalBatonPlaces ) {
	boolean neg = false;
	for (int step = 0; step < 3; ++ step) {
		for (int i0 = xyIncomingBatonPlaces.length - 1; i0 >= 0; -- i0) {
			if ((null != xyInternalBatonPlaces[ i0 ]) && (null == ((Object[]) xyInternalBatonPlaces[ i0 ])[ 0 ])) {
				int cnt = 0;
				for (int ix = 0; ix < ((Object[]) xyInternalBatonPlaces[ i0 ]).length; ++ ix) {
					if (null != ((Object[]) xyInternalBatonPlaces[ i0 ])[ ix ]) {
						((Object[]) xyInternalBatonPlaces[ i0 ])[ cnt ++ ] = ((Object[]) xyInternalBatonPlaces[ i0 ])[ ix ];
					}
				}
				if (0 >= cnt) {
					xyInternalBatonPlaces[ i0 ] = null;
				}
			}
			if (null == xyInternalBatonPlaces[ i0 ]) {
				if (null != xyIncomingBatonPlaces[ i0 ]) {
					xyInternalBatonPlaces[ i0 ] = xyIncomingBatonPlaces[ i0 ];
					xyIncomingBatonPlaces[ i0 ] = null;
				} else {
					neg = true;
				}
			}
		}
		if (!neg || !yield) {
			break;
		}
		Thread.yield();
	}
	return !neg;
}

/**
 * @param yield true = let others pick up their batons, false = return quickly.
 * @param xyOutgoingTokenArrays Array of baton arrays => [][].
 * @param xaiExternalPlaces Each one's index of matching baton array on the other side.
 * @param xyExternalTokenPlacesArray The other side's incoming baton arrays => [][][].
 * @return false if blocked
 */
public static boolean pushBatons( boolean yield, Object[] xyOutgoingBatonArrays, int[] xaiExternalPlaces,
	Object[]... xyExternalTokenPlacesArray ) {
	boolean out = true;
	for (int step = 0; step < 3; ++ step) {
		for (int i0 = xyOutgoingBatonArrays.length - 1; i0 >= 0; -- i0) {
			if (null == xyExternalTokenPlacesArray[ i0 ][ xaiExternalPlaces[ i0 ] ]) {
				xyExternalTokenPlacesArray[ i0 ][ xaiExternalPlaces[ i0 ] ] = xyOutgoingBatonArrays[ i0 ];
				xyOutgoingBatonArrays[ i0 ] = null;
			} else if (null != xyOutgoingBatonArrays[ i0 ]) {
				out = false;
			}
		}
		if (out || !yield) {
			break;
		}
		Thread.yield();
	}
	return out;
}

@SuppressWarnings( "unchecked" )
public static < T > boolean prepareBatonArray( Object[] xyOutgoingBatonArrays, int xiInternalPlace, T xrBaton ) {
	if (null == xyOutgoingBatonArrays[ xiInternalPlace ]) {
		xyOutgoingBatonArrays[ xiInternalPlace ] = Array.newInstance( xrBaton.getClass(), 5 ); //, 1 ); // (T[]) (new Object[] { xrToken });
		((T[]) xyOutgoingBatonArrays[ xiInternalPlace ])[ 0 ] = xrBaton;
	} else {
		T[] arr = (T[]) xyOutgoingBatonArrays[ xiInternalPlace ];
		int len = arr.length - 1;
		for (; (0 <= len) && (null == arr[ len ]); -- len)
			;
		++ len;
		if (len >= arr.length) {
			arr = Arrays.copyOf( arr, 2 * arr.length + 1 );
		}
		arr[ len ] = xrBaton;
		xyOutgoingBatonArrays[ xiInternalPlace ] = arr;
	}
	return true;
}

//=====
}
