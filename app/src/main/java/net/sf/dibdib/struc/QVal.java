// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.struc;

import net.sf.dibdib.func.*;

/** Convenience class for values and handles ==> lazy evaluation.
 * Text in its original form is usually a literal (String) consisting of 'pieces' (semantic elements).
 * The literal gets tokenized (and possibly stored via handles) into atomic typed values (cmp. QStr):
 * numbers, values, bit strings, values with units, dates, words or 'morphemic' literals.
 * Contents of arrays: not to be changed, neither inside nor outside! 'value' is
 * -- String for literal (WORD) or SEP or BITLIST (original and/or shash),
 * -- null for date token as LeapMinutes (with shash[0]==SHASH_DATE)
 * -- null for numeric token as double (original with or without units, units in shash),
 * -- QVal[] for list of values.
 * -- long[] for handles. 
 * -- (RFU:) String[] for bit list or byte[]
 */
public final class QVal {
//=====

// Encoding with reversed TLV/TCV, cmp. https://github.com/msgpack/msgpack
// Values are big-endian !
/** NUM (0,1,2..127): positive fixint 7, 0x00 - 0x7f */
public static final short TAG_NUM0_POS = 0;
/** MAP (0,2,4..30 elements): fixmap C4*2, 0x80 - 0x8f */
public static final short TAG_MAP_C0 = 0x80;
/** ARR (0,1,2..15 elements): fixarray C4, 0x90 - 0x9f */
public static final short TAG_ARR_C0 = 0x90;
/** STR (len<32): fixbytes L5, 0xa0 - 0xbf */
public static final short TAG_BYTES_L0 = 0xa0;
/** nil, 0xc0 */
public static final byte TAG_nil = (byte) 0xc0;
/** ARR2SET/INT2FALSE (indicator): false, 0xc2 */
public static final byte TAG_ARR2SET_INT2FALSE = (byte) 0xc2;
/** ARR2TRIPLES/INT2TRUE (indicator): true, 0xc3 */
public static final byte TAG_ARR2TRIPLES_INT2TRUE = (byte) 0xc3;
/** BIGINT (len<=0xff): bin L8, 0xc4 + L */
public static final byte TAG_BIGINT_L1 = (byte) 0xc4;
/** BIGFLOAT (len<=0xffff): bin L16, 0xc5 + L1 + L0 */
public static final byte TAG_BIGFLOAT_L2 = (byte) 0xc5;
/** NUM (float): float 64, 0xcb */
public static final byte TAG_NUM64 = (byte) 0xcb;
/** MSEC (timeStamp): uint 64, 0xcf */
public static final byte TAG_MSEC = (byte) 0xcf;
/** LMIN (leapMinutes): int 64, 0xd3 */
public static final byte TAG_LEAPMIN = (byte) 0xd3;
/** STR (len<=0xff): bytes L8, 0xd9 + L */
public static final byte TAG_BYTES_L1 = (byte) 0xd9;
/** STR (len<=0xffff): bytes L16, 0xda + L1 + L0 */
public static final byte TAG_BYTES_L2 = (byte) 0xda;
/** STR (len>0xffff): bytes L32, 0xdb + L3 + L2 + L1 + L0 */
public static final byte TAG_BYTES_L4 = (byte) 0xdb;
/** ARR (count<=0xffff): array L16, 0xdc .. */
public static final byte TAG_ARR_C2 = (byte) 0xdc;
/** ARR (count>0xffff): array L32, 0xdd .. */
public static final byte TAG_ARR_C4 = (byte) 0xdd;
/** MAP (count<=0xffff): map L16, 0xde .. */
public static final byte TAG_MAP_C2 = (byte) 0xde;
/** MAP (count>0xffff): map L32, 0xdf .. */
public static final byte TAG_MAP_C4 = (byte) 0xdf;
/** NUM (-32..-1): negative fixint 5, 0xe0 - 0xff */
public static final short TAG_NUM0_NEG = 0xe0;

/** The original String if available as such. */
private final String zOriginal;
/** The value or null (if numeric). Original Literal/ QUOTE (TODO: OID, digit strings) may contain TEMPLATES. */
public final Object value;
private final double zNumeric;
private final long zLeapMinutes256;
// private String[] zTokens;
private String zString;
private String zShash;
//TODO errorVal

public static final QVal EMPTY = new QVal( "", true );
public static final QVal NaN = new QVal( null, Double.NaN );
public static final QVal V_0 = new QVal( null, 0.0 );
public static final QVal FALSE = V_0;
public static final QVal V_1 = new QVal( null, 1.0 );
public static final QVal NEG_1 = new QVal( null, -1.0 );
public static final QVal TRUE = NEG_1;
/** Special value for indicating delayed processing/ lazy evaluation. */
public static final QVal IN_PROCESS = new QVal( "(in process ...)", Double.NEGATIVE_INFINITY );

public QVal( String value, boolean forceText ) {
	zOriginal = value;
	zShash = forceText ? null : QStr.shash( value );
	zNumeric = (null == zShash) || (QStr.SHASH_LITERAL <= zShash.charAt( 0 )) //,
		|| (QStr.SHASH_DATE >= zShash.charAt( 0 )) //,
	? Double.NaN : QStr.double4ShashNum( zShash );
	zLeapMinutes256 = ((null == zShash) || (QStr.SHASH_DATE != zShash.charAt( 0 )) ? 0 : QStr.leapMinutes256_4ShashDate( zShash ));
	this.value = ((null == zShash) ? value
		: ((QStr.SHASH_DATE == zShash.charAt( 0 )) ? null
			: ((QStr.SHASH_LITERAL <= zShash.charAt( 0 )) ? value : null)));
}

public QVal( String value, String shash ) {
	final char v0 = (0 < shash.length()) ? shash.charAt( 0 ) : QStr.SHASH_LITERAL;
	this.value = (QStr.SHASH_LITERAL <= v0) ? value : null;
	zOriginal = value;
	zShash = shash;
	this.zNumeric = ((null != this.value) || (QStr.SHASH_DATE == v0)) ? Double.NaN : QStr.double4ShashNum( zShash );
	zLeapMinutes256 = (QStr.SHASH_DATE == v0) ? QStr.leapMinutes256_4ShashDate( shash ) : 0;
}

public QVal( String optionalLiteral, double value, String... shash ) {
	this.value = null;
	zOriginal = optionalLiteral;
	zShash = (0 == shash.length) ? null : shash[ 0 ];
	zLeapMinutes256 = 0;
	zNumeric = value;
}

public QVal( long leapMin256, String... optionalLiteral ) {
	this.value = null;
	zOriginal = ((null == optionalLiteral) || (0 >= optionalLiteral.length)) ? null : optionalLiteral[ 0 ];
	zShash = QStr.shash4LeapMin256( leapMin256 );
	zLeapMinutes256 = leapMin256;
	zNumeric = Double.NaN;
}

public QVal( String optionalLiteral, QVal[] list, String... shash ) {
	this.value = list;
	zOriginal = optionalLiteral;
	zShash = (0 == shash.length) ? null : shash[ 0 ];
	zLeapMinutes256 = 0;
	zNumeric = Double.NaN;
}

public QVal( String optionalLiteral, long[] externalHandles, String shash ) {
	this.value = externalHandles;
	zOriginal = optionalLiteral;
	zShash = shash;
	zLeapMinutes256 = 0;
	zNumeric = Double.NaN;
}

public static QVal qVal4Shash( String shash ) {
	int len = shash.length();
	if (0 >= len) {
		return EMPTY;
	} else if (QStr.SHASH_LITERAL > shash.charAt( 0 )) {
		if (QStr.SHASH_NEG > shash.charAt( 0 )) {
			if (QStr.SHASH_DATE > shash.charAt( 0 )) {
				return new QVal( (QStr.SHASH_PUNCT == shash.charAt( 0 )) ? shash.substring( 1 ) : ".", shash );
			}
			return new QVal( QStr.leapMinutes256_4ShashDate( shash ) );
		}
		//TODO: Handle extra long floats.
		double val = QStr.double4ShashNum( shash );
		return new QVal( null, val, shash );
	}
	return new QVal( QStr.string4Shash( shash ), shash );
}

public boolean isNumeric() {
	return (null == value);
}

public boolean isDate() {
	return (null == value) && Double.isNaN( zNumeric )
		&& (null != zShash) && (QStr.SHASH_DATE == zShash.charAt( 0 ));
}

public boolean isList() {
	return (value instanceof QVal[]) || (value instanceof int[]) || (value instanceof long[]);
}

@Override
public String toString() {
	if (null != zOriginal) {
		return zOriginal;
	} else if (null != zString) {
		return zString;
	} else if (null == value) {
		if ((null != zShash) && (QStr.SHASH_DATE == zShash.charAt( 0 ))) {
			zString = UtilMisc.date4LeapMinute256( zLeapMinutes256 );
			return zString;
		}
		zString = UtilString.string4Double( zNumeric );
//	} else if (value instanceof int[]) {
//		zText = "" + ((0 == ((long[]) value).length) ? "" : QMap.main.readObject( ((int[]) value)[ 0 ] ).toString()) //,
//			+ "...";
	} else if (value instanceof QVal[]) {
		zString = ((0 == ((QVal[]) value).length) ? "" : ((QVal[]) value)[ 0 ].toString()) + "...";
	}
	return zString;
}

public String toShash() {
	if (null != zShash) {
		return zShash;
	} else if (null == value) { // (!Double.isNaN( zNumeric )) {
		zShash = QStr.shash4Double( null, zNumeric );
	} else if (value instanceof String) {
		zShash = QStr.shash( (String) value );
//	} else if (!(value instanceof long[])) {
//		QVal[] vals = (value instanceof int[]) ? QMap.main.readQVals( (int[]) value ) : (QVal[]) value;
//		StringBuilder out = new StringBuilder( Dib2Constants.SHASH_MAX * 2 );
//		for (QVal val0 : vals) {
//			out.append( val0.toShash() );
//			if (Dib2Constants.SHASH_MAX >= out.length()) {
//				break;
//			}
//		}
//		zShash = out.toString();
	}
	return zShash;
}

public double toDouble() {
	if (!isDate()) {
		return zNumeric;
	}
	return (zLeapMinutes256 / 256.0);
}

@Override
public int hashCode() {
	return toShash().hashCode();
}

public String format( String pre, String sep, boolean hex ) {
	if ((null != zOriginal) && !hex) {
		return pre + sep + zOriginal;
	}
	if (!isNumeric()) {
		final String out = toString();
		if (!hex || (99 <= out.length())) {
			return pre + sep + out;
		}
		if ((null != zOriginal) && !zOriginal.equals( out )) {
			return pre + sep + zOriginal + sep + UtilString.hexUtf8( out, true ) + sep + out;
		}
		return pre + sep + out + sep + UtilString.hexUtf8( out, true );
	} else if (isDate()) {
		final String date = UtilMisc.date4LeapMinute256( zLeapMinutes256 );
		if ((null != zOriginal) && !zOriginal.equals( date )) {
			return pre + sep + zOriginal + sep + UtilString.hex4Double(
				UtilMisc.millis4Date( date ), null ) + sep + date;
		}
		return pre + sep + date + (!hex ? "" : (sep + UtilString.hex4Double(
			UtilMisc.millis4Date( date ), null )));
	}
	String num = UtilString.string4Double( zNumeric );
	if ((null != zOriginal) && !zOriginal.equals( num )) {
		return pre + sep + zOriginal + sep + UtilString.hex4Double( zNumeric, sep ) + sep + UtilString.string4Double( zNumeric );
	}
	return pre + sep + num + (!hex ? "" : (sep + UtilString.hex4Double( zNumeric, sep )));
}

public static long getTcvOffsetLength( byte[] data, int start, int iTag, int count ) {
	int cC = 0;
	int cV = 0;
	for (; count > 0; -- count, -- iTag) {
		if (iTag < start) {
			return -1;
		}
		cC = 0;
		cV = 0;
		if (TAG_NUM0_NEG < data[ iTag ]) {
			continue;
		} else if (0xc0 > (data[ iTag ] & 0xff)) {
			final int t0 = data[ iTag ] & 0xff;
			if (TAG_BYTES_L0 <= t0) {
				cV = (t0 & 0x1f);
				iTag -= cV;
			} // else // TODO
			continue;
		}
		switch (data[ iTag ]) {
			case TAG_nil:
				break;
			case TAG_BYTES_L1:
				cC = 1;
				break;
			case TAG_BYTES_L2:
				cC = 2;
				break;
			case TAG_BYTES_L4:
				cC = 4;
				break;
			// case ... TODO
			default:
				;
		}
		if (iTag < cC) {
			return -1;
		}
		if (0 < cC) {
			cV = data[ iTag - 1 ] & 0xff;
		}
		if (1 < cC) {
			cV = (cV << 8) | (data[ iTag - 2 ] & 0xff);
		}
		if (2 < cC) {
			cV = (cV << 16) | ((data[ iTag - 3 ] & 0xff) << 8) | (data[ iTag - 4 ] & 0xff);
		}
		iTag -= cV + cC;
	}
	return ((cV & -1L) << 32) | ((iTag + 1) & -1L);
}
//=====
}
