// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.struc;

import net.sf.dibdib.store.*;

/** Base class for mappings etc.
 */
public class OidBaton extends QBaton {
//=====

/** Object ID (OID) for import, export, and versioning:
 * locally unique, eventual consistency,
 * (short OIDs for temporary mappings (e.g. 'X', 'Y', ...) ) */
public String oid;

/** Informative: msec delta for time zone info. */
public int timeStampDeltaZone;

/** Required number of data elements for packing. */
public static final int PACK_ADD = 3;

// For related batons and values => enable atomic transactions
// public int groupStamp;

public OidBaton() {
}

public OidBaton( String xOid, long timestamp, int timeStampDeltaZone ) {
	this.oid = xOid;
	this.timeStamp = timestamp;
	this.timeStampDeltaZone = timeStampDeltaZone;
}

/**
 * @param xExistingOid
 * @param mapped
 * @return count for contained data.
 */
public int init( String xExistingOid, long[] mapped, int offset ) {
	oid = xExistingOid;
	if (null != mapped) {
		timeStamp = (long) (QMap.handleNum2Double( mapped[ offset ] ) * 60000);
		final long low = (long) QMap.handleNum2Double( mapped[ offset + 1 ] );
		timeStamp += low >>> 32;
		timeStampDeltaZone = (int) low;
	}
	if (0 == timeStamp) {
		timeStamp = 1;
	}
	return 3;
}

public long[] pack( long tag, QMap xMap, /*h*/long[] xyahData ) {
	int len = xyahData.length;
	// To minutes:
	xyahData[ len - PACK_ADD ] = QMap.getHandle( timeStamp / 60000 );
	xyahData[ len - PACK_ADD + 1 ] = QMap.getHandle( ((timeStamp % 60000) << 32) | timeStampDeltaZone );
	xyahData[ len - 1 ] = tag; // QMap.getHandle( xMarker );
	return xyahData;
}

public long store( QResult zTmp, long[] mapped, QMap xMap, String shash ) {
	long out = xMap.add( zTmp, mapped, false, shash );
	xMap.addKey( zTmp, out, 1, oid );
	return out;
}

//=====
}
