// Copyright (C) 2016,2017,2018,2019 Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.struc;

import java.util.*;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.func.*;
import net.sf.dibdib.store.QMap;

/** Conceptual element/ Convenience class for QMap/ ...:
 * Element (record) of categorical database (currently saved as CSV line), also used as Token.
 * Contains:
 * name (atomic) + propositional main category (+ secondary categories)
 * + time stamp + source of information
 * + (propositional) data.
 * (Constraint logic, name as partial ID)
 * Prolog interpretation: (CATEGORY ATOM), (CATEGORY.DATA_LABEL ATOM DATA_ENTRY).
 */
public class Mapping extends OidBaton {
//=====

//public static final String version = "0.5"; //"0.2";
public static final String fieldNames = //,
	"" // OID
		+ "\tNAME" //,
		+ "\tCATS" //,
		+ "\tTIME" //,
		+ "\tCONTRIB" //,
		+ "\tSRCS" //,
		+ "\tRECV" //,
		+ "\tDAT1" //,
		+ "\tDAT2" //,
		+ "\tDAT3" //,
		+ "\tDAT4" //,
		+ "\tDAT5" //,
		+ "\tDAT6" //,
		+ "\tDAT7" //,
		+ "\tDAT8" //,
		+ "\tDAT9";

public final static int kiOid = 0;
public final static int kiLabel = 1;
public final static int kiCats = 2;
public final static int kiTime = 3;
public final static int kiContrib = 4;
public final static int kiSrcs = 5;
public final static int kiRecv = 6;
public final static int kiData = 7;

public static enum Pack {
NAME, CATS, CONTRIB, SRCS, RECV, DAT, Q_COUNT,
};

public static final String qRootDummyAddress = "0@0";

/** Main OID, to be used for PREF "." and in combination with qRootAddress for main CONTACT.
 * Bootstrapped: generated OID for dummy address, OID for PREF "email_address",
 * OID in combination with qRootAddress from CONTACT. Change cautiously: some Mapping
 * objects have this value for contributor or refer to this with contributor == "".  */
public static/*h*/long qhRootOid = QMap.main.makeHandle( new QResult(), UtilMisc.createId( qRootDummyAddress ) );

/** To be used in combination with qhRootOid: target value for PREF "." and "email_address". */
public static String qRootAddress = null;

public static final char TAG = 'M';

///// -- Immutable - 'as if' (whenever not enforced by Java)

/** 'Name': sememe/ name of (quasi-atomic) element, e.g. 'Charlie Brown' or 'Brown.Charlie' */
public final String sLabel;

/** Shash of label, for faster access */
public final String labelShash;

/** All category sememes */
public final String[] asCategories;

/** Flags for pre-defined categories */
public final long bCategories;

// Nickname not stored here.
// Time stamp below.

/** OID of contributor, "" iff it is the user/ root */
public final String uContributor;

public final String[] asSources;
public final String[] auShareReceivers;
//TODO: split element strings into sememes => long[][]
/** Use hex strings for PREF values! */
public final String[] dataElements;

//public final String[] listShashes;

///// -- Mutable (for ease of Baton handling etc.)

// (parent: public String oid, public long timeStamp)
// ...

//=====

/** Pre-defined categories */
public static enum Cats {
//=====

///// Singular
PREF( 1 ),
VAR( 2 ),

///// Special
OTHERS( 1L << 8 ),
TRASH( 1L << 9 ),
DONE( 1L << 10 ),

/////
NOTE( 1L << 16 ),
EVENT( 1L << 17 ),
CONTACT( 1L << 18 ),
GROUP( 1L << 19 ),
MSG( 1L << 20 ),
//
_MAX( 1L << 21 ),
//
;

public final long handle;
public final long handleFallback;
public final long flag;
public final long[] ahAsList;
public static final Cats DEFAULT = NOTE;

private static HashMap< Long, Cats > map = null;

private Cats( long xFlag ) {
	flag = xFlag;
	final QResult tmp = new QResult();
	handle = QMap.main.makeHandle( tmp, this.name() );
	handleFallback = ((1 << 16) > xFlag) ? QMap.main.makeHandle( tmp, "X" + this.name() ) : QMap.NONE;
	ahAsList = new long[] { handle };
}

public static Cats valueOf( long handle ) {
	if (null == map) {
		map = new HashMap< Long, Cats >();
		for (Cats cat : Cats.values()) {
			map.put( cat.handle, cat );
		}
	}
	return map.get( handle );
}

public static Cats[] list4Flags( long flags ) {
	Cats[] out = new Cats[ 32 ];
	int count = 0;
	for (Cats cat : Cats.values()) {
		if ((cat.flag & flags) != 0) {
			out[ count ++ ] = cat;
		}
	}
	return Arrays.copyOf( out, count );
}

public static long[] normalize( int xLen, long[] xyHandles ) {
	if (1 >= xLen) {
		if ((1 == xLen) && (OTHERS.handle == xyHandles[ 0 ])) {
			return new long[ 0 ];
		}
		return Arrays.copyOf( xyHandles, xLen );
	}
	Arrays.sort( xyHandles, 0, xLen );
	// Populates 'map':
	boolean otherMarked = (OTHERS == valueOf( xyHandles[ xLen - 1 ] ));
	boolean other = !map.containsKey( xyHandles[ xLen - 1 ] );
	boolean conflict = false;
	if (!other) {
		Cats cat = map.get( xyHandles[ xLen - 1 ] );
		if (QMap.NONE != cat.handleFallback) {
			///// Not singular
			xyHandles[ xLen - 1 ] = cat.handleFallback;
		}
	}
	for (int i0 = xLen - 2; i0 >= 0; -- i0) {
		if (xyHandles[ i0 ] >= xyHandles[ i0 + 1 ]) {
			conflict = true;
		}
		Cats cat = map.get( xyHandles[ i0 ] );
		other = other || (null == cat);
		otherMarked = otherMarked || (OTHERS == valueOf( xyHandles[ i0 ] ));
		if ((null != cat) && (QMap.NONE != cat.handleFallback)) {
			///// Not singular
			xyHandles[ i0 ] = cat.handleFallback;
		}
	}
	conflict = conflict || (other != otherMarked);
	if (!conflict) {
		return xyHandles;
	}
	///// Conflict
	long[] out = xyHandles;
	if (other && !otherMarked) {
		if (xLen >= xyHandles.length) {
			out = Arrays.copyOf( xyHandles, xLen + 1 );
		}
		out[ xLen ] = OTHERS.handle;
	}
	int cnt = (other || (OTHERS.handle != out[ 0 ])) ? 1 : 0;
	for (int i0 = 1; i0 < xLen; ++ i0) {
		if ((out[ i0 - 1 ] != out[ i0 ]) && (other || (OTHERS.handle != out[ i0 ]))) {
			out[ cnt ++ ] = out[ i0 ];
		}
	}
	out = Arrays.copyOf( out, cnt );
	Arrays.sort( out );
	return out;
}

public static long[] handles4Flags( long flags ) {
	long[] out = new long[ 32 ];
	int count = 0;
	for (Cats cat : Cats.values()) {
		if ((cat.flag & flags) != 0) {
			out[ count ++ ] = cat.handle;
		}
	}
	return normalize( count, out );
}

public static long toFlags( long[] handles ) {
	long out = 0;
	if (null == handles) {
		return 0;
	}
	if (null == map) {
		// Populate map:
		valueOf( 1 );
	}
	for (long handle : handles) {
		Cats cat = map.get( handle );
		if (null == cat) {
			out |= OTHERS.flag;
		} else {
			out |= cat.flag;
		}
	}
	return out;
}

public static long[] toCategories( QResult zTmp, String xCategories ) {
	final int len = xCategories.length();
	if (100 <= len) {
		// Error.
		return new long[] { Cats.OTHERS.handle };
	}
	long[] handles = new long[ 2 + (len >> 2) ];
	int cnt = 0;
	boolean boundary = false;
	int i0 = 0;
	for (int i1 = 0; i1 < len; ++ i1) {
		if (boundary != ('0' > xCategories.charAt( i1 ))) {
			boundary = !boundary;
			if (boundary) {
				if (handles.length >= cnt) {
					handles = Arrays.copyOf( handles, 2 * handles.length );
				}
				if (10 <= cnt) {
					// Error.
					handles[ cnt ++ ] = Cats.OTHERS.handle;
					break;
				}
				handles[ cnt ++ ] = QMap.main.makeHandle( zTmp, xCategories.substring( i0, i1 ) );
			}
			i0 = i1;
		}
	}
	if (!boundary) {
		handles[ cnt ++ ] = QMap.main.makeHandle( zTmp, xCategories.substring( i0, len ) );
	}
	return normalize( cnt, handles );
}
//=====
}

//=====

/** Return handle for Mapping object with OID as key. */
public static long handleByOid( QResult zTmp, String oid ) {
	long[] mapped = QMap.main.readHandles( zTmp, oid, Dib2Constants.MAPPING_KEY_TYPE_OID, false );
	return ((null == mapped) || (0 == mapped.length)) ? QMap.NONE : mapped[ 0 ];
}

public Mapping( String oid, /*h*/long[] mapped ) {
	//NAME, CATS, CONTRIB, SRCS, RECV, DAT,
	super.init( oid, mapped, Pack.Q_COUNT.ordinal() );
	sLabel = QMap.main.readStrings( oid, mapped[ Pack.NAME.ordinal() ] )[ 0 ];
	labelShash = QStr.shash( sLabel );
	long[] cats = QMap.main.readStruct( mapped[ Pack.CATS.ordinal() ] );
	cats = ((null == cats) || (0 >= cats.length)) ? Cats.DONE.ahAsList : cats;
	asCategories = QMap.main.readStrings( "", cats );
	bCategories = Cats.toFlags( cats );
	uContributor = QMap.main.readStrings( "", mapped[ Pack.CONTRIB.ordinal() ] )[ 0 ];
//	long[] dat = QMap.main.readStruct( mapped[ Pack.DAT.ordinal() ] );
	dataElements = QMap.main.readLiterals( mapped[ Pack.DAT.ordinal() ] );
	//Arrays.copyOfRange( mapped, offs + Pack.DAT.ordinal(), mapped.length ) );
	asSources = QMap.main.readStrings( "", mapped[ Pack.SRCS.ordinal() ] );
	auShareReceivers = QMap.main.readStrings( "", mapped[ Pack.RECV.ordinal() ] );
}

/** Label (without '*') + timeStamp/-1 + OID/null + Cats + contributor/0 + values. */
public Mapping( String xLabel, long xTimeStamp, String xObjectId, long[] cats, long xhhContributor, String... xmValues ) {
	timeStamp = (0 <= xTimeStamp) ? xTimeStamp : UtilMisc.currentTimeMillisLinearized();
	cats = ((null == cats) || (0 >= cats.length)) ? Cats.DONE.ahAsList : cats;
//	long[] cats = Cats.toCategories( xCategories );
	asCategories = QMap.main.readStrings( "", cats );
	bCategories = Cats.toFlags( cats );
	String label = UtilString.nameNormalize( xLabel, 0xffff );
	label = (0 == (Cats.MSG.flag & bCategories)) ? label //,
		: (label + '*' + UtilMisc.dateShort4Millis( timeStamp ));
	sLabel = label;
	labelShash = QStr.shash( label );
	oid = (null != xObjectId) ? xObjectId : UtilMisc.createId( label, timeStamp );
	//bVersioned = ...;
	long hh = (0 != xhhContributor) && (null == QMap.main.existsAsSpecialObject( xhhContributor )) ? 0 : xhhContributor;
	uContributor = (qhRootOid == hh) ? "" : QMap.main.readStrings( "N.N.", hh )[ 0 ];
	dataElements = xmValues;
	asSources = new String[ 0 ];
	auShareReceivers = new String[ 0 ];
}

public Mapping( String[] csvFields, int iOid, int flagsMarkAdjust4Time ) {
	final QResult pooled = QResult.get8Pool();
	long time = (6 > csvFields[ 3 ].length())
		? UtilMisc.currentTimeMillisLinearized() : UtilMisc.millis4Date( csvFields[ 3 ] );
	if (0 != (flagsMarkAdjust4Time & 1)) {
		time = time & ~Dib2Constants.TIME_SHIFTED;
	}
	long min = (0 != (flagsMarkAdjust4Time & 2)) ? 1 : time;
	timeStamp = UtilMisc.alignTime( time, min );

	long[] cats = Cats.toCategories( pooled, csvFields[ 2 + iOid ] );
	cats = ((null == cats) || (0 >= cats.length)) ? Cats.DONE.ahAsList : cats;
	asCategories = QMap.main.readStrings( "", cats );
	bCategories = Cats.toFlags( cats );
	String label = UtilString.nameNormalize( csvFields[ 1 + iOid ], 0xffff );
	label = (0 == (Cats.MSG.flag & bCategories)) ? label //,
		: (label + '*' + UtilMisc.dateShort4Millis( timeStamp ));
	sLabel = label;
	labelShash = QStr.shash( label );
	oid = ((0 <= iOid) && (10 <= csvFields[ iOid ].length()))
		? csvFields[ iOid ] : UtilMisc.createId( label, timeStamp );
	//bVersioned = ...;

	// As OID:
	String ctrb = csvFields[ 4 + 2 * iOid ];
	if (QMap.main.readStrings( "N.N.", qhRootOid )[ 0 ].equals( ctrb )) {
		ctrb = "";
	} else if (10 > ctrb.length()) {
		ctrb = "";
	}
	uContributor = ctrb;
	dataElements = ((7 + 3 * iOid) >= csvFields.length) ? new String[ 0 ] //,
		: Arrays.copyOfRange( csvFields, 7 + 3 * iOid, csvFields.length );
	String srcs = (5 >= csvFields.length) || (null == csvFields[ 5 ]) || (0 >= csvFields[ 5 ].length()) ? "" : csvFields[ 5 ];
	asSources = (0 < srcs.length()) ? srcs.split( " // " ) : new String[ 0 ];
	auShareReceivers = new String[ 0 ];
}

/** Create simple Mapping based on String values.
 * @param name Name or label.
 * @param cat Category (default: DONE).
 * @param ctrbOid "" for main user/ root.
 * @param data Mapped data
 * @return Created Mapping object.
 */
public static Mapping make( String name, Cats cat, String ctrbOid, String data ) {
	String oid = UtilMisc.createId( ".".equals( name ) ? data : name );
	cat = (null == cat) ? Cats.DONE : cat;
	String time = UtilMisc.date4Millis( false );
	String[] a0 = new String[] { oid, name, cat.name(), time, ctrbOid, "", "", data };
	return new Mapping( a0, 0, 0 );
}

public static Mapping make( String name, String cats, String date, int offsetData, String... data ) {
	String[] a0 = new String[ 7 + data.length - offsetData ];
	a0[ 0 ] = UtilMisc.createId( ".".equals( name ) ? data[ 0 ] : name );
	a0[ 1 ] = name;
	a0[ 2 ] = cats;
	a0[ 3 ] = (null == date) ? "" : date; //((null == date) || (6 > date.length())) ? UtilMisc.date4Millis() : date;
	a0[ 4 ] = "";
	a0[ 5 ] = "";
	a0[ 6 ] = "";
	for (int i0 = 7; i0 < a0.length; ++ i0) {
		a0[ i0 ] = data[ i0 - 7 + offsetData ];
	}
	return new Mapping( a0, 0, 0 );
}

public static Mapping make( String xExistingOid, int instance ) {
	final QResult pooled = QResult.get8Pool();
	long[] mapped = QMap.main.readHandles( pooled, xExistingOid, 1, false );
	if ((null != mapped) && (mapped.length >= instance)) {
		mapped = QMap.main.readStruct( mapped[ instance ] );
		return new Mapping( xExistingOid, mapped );
	}
	return null;
}

@Override
public String toString() {
	return sLabel + '@' + UtilMisc.dateShort4Millis( timeStamp ) //,
		// '[' + QMap.main.handlesStruct2String( 
		+ asCategories[ 0 ] //, 0, 0 ) //, 
		// "]: " 
		+ ((0 < dataElements.length) ? dataElements[ 0 ] : "");
}

public String toTextLine( int maxLen ) {
	StringBuilder out = new StringBuilder( 64 * dataElements.length );
	if (0 >= maxLen) {
		out.append( UtilMisc.dateShort4Millis( timeStamp ).substring( 0, 6 ) );
		out.append( ' ' );
	}
	out.append( sLabel );
	boolean elDone = (1 >= dataElements.length);
	if (0 < dataElements.length) {
		out.append( '\t' );
		elDone = (20 >= dataElements[ 0 ].length());
		out.append( elDone ? dataElements[ 0 ]
			: (dataElements[ 0 ].substring( 0, 20 ) + "...") );
		elDone = elDone && (1 >= dataElements.length);
	}
	if (0 >= maxLen) {
		out.append( "\t[" );
		out.append( oid );
		out.append( ']' );
		for (String cat : asCategories) {
			out.append( ' ' );
			out.append( cat );
		}
	}
	if (!elDone) {
		for (String el : dataElements) {
			out.append( '\t' );
			out.append( el );
		}
	}
	return ((0 >= maxLen) || (maxLen >= out.length())) ? out.toString() : out.substring( 0, maxLen );
}

public String getContributorOid( String xRootOid ) {
	if (0 == uContributor.length()) {
		return (null == xRootOid) ? QMap.main.readStrings( "N.N.", qhRootOid )[ 0 ] : xRootOid;
	}
	return uContributor;
}

public String getData( int iElement ) {
	return (iElement >= dataElements.length) ? null : dataElements[ iElement ];
}

public byte[] getData4Pref() {
	String val = getData( 0 );
	if (null == val) {
		return new byte[ 0 ];
	}
	return UtilString.bytes4Hex( val );
}

public long[] pack() {
	//NAME, CATS, CONTRIB, SRCS, RECV, DAT,
	final QResult pooled = QResult.get8Pool();
	long[] mpg = new long[ Pack.Q_COUNT.ordinal() + OidBaton.PACK_ADD ];
	mpg[ Pack.NAME.ordinal() ] = QMap.main.makeHandle( pooled, sLabel );
	long[] cats = QMap.main.makeHandles( pooled, asCategories );
	mpg[ Pack.CATS.ordinal() ] = QMap.main.add( pooled, cats, false );
	mpg[ Pack.CONTRIB.ordinal() ] = QMap.main.makeHandle( pooled, uContributor );
	long[] srcs = QMap.main.makeHandles( pooled, asSources );
	mpg[ Pack.SRCS.ordinal() ] = QMap.main.add( pooled, srcs, false );
	long[] recv = QMap.main.makeHandles( pooled, auShareReceivers );
	mpg[ Pack.RECV.ordinal() ] = QMap.main.add( pooled, recv, false );
	mpg[ Pack.DAT.ordinal() ] = QMap.main.addLiterals( pooled, dataElements );
	long tag = QMap.getHandleTag( TAG, (int) bCategories );
	super.pack( tag, QMap.main, mpg );
	return mpg;
}

private String shash4Pack( long[] packed ) {
	String shash = QMap.main.handle2Shash( packed[ Pack.NAME.ordinal() ] );
	shash += (char) 1;
	shash += (1 < packed.length) ? QMap.main.handle2Shash( packed[ Pack.CATS.ordinal() ] ) : "";
	shash += (char) 1;
	shash += QStr.shash( oid );
	if (Dib2Constants.SHASH_MAX <= shash.length()) {
		shash = shash.substring( 0, Dib2Constants.SHASH_MAX - 1 );
	}
	return shash;
}

public static Mapping search4Oid( String oid ) {
	final QResult pooled = QResult.get8Pool();
	long handle = handleByOid( pooled, oid );
	Object o0 = QMap.main.existsAsSpecialObject( handle );
	if ((null != o0) && (o0 instanceof long[])) {
		return new Mapping( oid, (long[]) o0 );
	}
	return null;
}

public static Mapping search4Label( long preferredCats, String label ) {
	final QResult pooled = QResult.get8Pool();
	final String shash4Label = QStr.shash( label ) + (char) 1;
	final long[] handles = QMap.main.traverse4Struct( pooled, shash4Label, 0, 0 );  //traverse4Struct_OLD( shash4Label, 0, shash4Label.length() );
	Mapping out = null;
	if (null == handles) {
		return null;
	}
	for (long handle : handles) {
		Object o0 = QMap.main.existsAsSpecialObject( handle );
		if ((null != o0) && (o0 instanceof long[])) {
			String oid = QMap.main.readKey( handle, 1 );
			if (null == oid) {
				continue;
			}
			Mapping mpg = new Mapping( oid, (long[]) o0 );
			if (label.equals( mpg.sLabel )) {
				out = mpg;
				if (preferredCats == (mpg.bCategories & preferredCats)) {
					break;
				}
			}
		}
	}
	return out;
}

public static Mapping search4Label( String label, int index ) {
	final QResult pooled = QResult.get8Pool();
	final String shash4Label = QStr.shash( label ) + (char) 1;
	final long[] handles = QMap.main.traverse4Struct( pooled, shash4Label, 0, 0 );  //traverse4Struct_OLD( shash4Label, 0, shash4Label.length() );
	if (null == handles) {
		return null;
	}
	int i0 = 0;
	for (long handle : handles) {
		Object o0 = QMap.main.existsAsSpecialObject( handle );
		if ((null != o0) && (o0 instanceof long[])) {
			String oid = QMap.main.readKey( handle, 1 );
			if (null == oid) {
				continue;
			}
			Mapping mpg = new Mapping( oid, (long[]) o0 );
			if (label.equals( mpg.sLabel )) {
				if (i0 == index) {
					return mpg;
				}
				++ i0;
			}
		}
	}
	return null;
}

public static Mapping searchNextLabel4Oid( String xOid ) {
	final QResult pooled = QResult.get8Pool();
	Mapping mpg = search4Oid( xOid );
	if (null == mpg) {
		return null;
	}
	final String label = mpg.sLabel;
	final String shash4Label = QStr.shash( label ) + (char) 1;
	final long[] handles = QMap.main.traverse4Struct( pooled, shash4Label, 0, 0 );  //traverse4Struct_OLD( shash4Label, 0, shash4Label.length() );
	if (null == handles) {
		return null;
	}
	boolean found = false;
	for (long handle : handles) {
		Object o0 = QMap.main.existsAsSpecialObject( handle );
		if ((null != o0) && (o0 instanceof long[])) {
			String oid = QMap.main.readKey( handle, 1 );
			if (!found) {
				if (xOid.equals( oid )) {
					found = true;
				}
			} else {
				mpg = new Mapping( oid, (long[]) o0 );
				if (label.equals( mpg.sLabel )) {
					return new Mapping( oid, (long[]) o0 );
				}
			}
		}
	}
	return null;
}

/** Create 2 handles based on OID and label, make sure that OID stays unique.
 * @return handle based on OID
 */
public long put2QStruct() {
	final QResult pooled = QResult.get8Pool();
	long[] packed = pack();
	for (; true; oid = UtilMisc.createIdNext( oid )) {
		long handle = handleByOid( pooled, oid );
		Object o0 = QMap.main.existsAsSpecialObject( handle );
		if (null == o0) {
			break;
		} else if ((o0 instanceof long[])) { // && Arrays.equals( packed, (long[]) o0 )) {
			Mapping mpg = new Mapping( oid, (long[]) o0 );
			if (mpg.sLabel.equals( sLabel ) && (mpg.dataElements.length == dataElements.length)) {
				int i0 = dataElements.length - 1;
				for (; i0 >= 0; -- i0) {
					if (!mpg.dataElements[ i0 ].trim().equals( dataElements[ i0 ].trim() )) {
						break;
					}
				}
				//TODO CATs
				if (0 > i0) {
					return handle;
				}
			}
		}
	}
	String shash = shash4Pack( packed );
	return store( pooled, packed, QMap.main, shash );
}

public static String toCsvLine( String oid, String label, long timeStamp, String cat, String dat ) {
	StringBuilder out = new StringBuilder( 100 + dat.length() );
	out.append( oid );
	String time = UtilMisc.date4Millis( false, timeStamp );
	out.append( '\t' ).append( (null == label) ? time : label );
	out.append( '\t' ).append( cat );
	out.append( '\t' ).append( time );
	out.append( '\t' ).append( "" ); // contributorOid
	out.append( '\t' ).append( "" ); // sources
	out.append( '\t' ).append( "" ); // shareReceivers, " " ) );
	out.append( '\t' ).append( dat.replace( '\n', '\t' ) );
	return out.toString();
}

/**
 * @param xUserOid null for root user (qhRootOid).
 * @return
 */
public String toCsvLine( String xUserOid ) {
	StringBuilder out = new StringBuilder( 100 + 10 * dataElements.length );
	out.append( oid );
	String label = sLabel;
	label = (0 < label.indexOf( '*' )) ? label.substring( 0, label.indexOf( '*' ) ) : label;
	out.append( '\t' ).append( label );
	out.append( '\t' ).append( UtilString.string4Array( asCategories, " " ) );
	out.append( '\t' ).append( UtilMisc.date4Millis( false, timeStamp ) );
	out.append( '\t' ).append( getContributorOid( xUserOid ) );
	out.append( '\t' ).append( UtilString.string4Array( asSources, " // " ) );
	out.append( '\t' ).append( UtilString.string4Array( auShareReceivers, " " ) );
	for (String el : dataElements) {
		out.append( '\t' ).append( el.replace( '\n', '\t' ) );
	}
	return out.toString();
}

//=====
}
