// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.struc;

import net.sf.dibdib.func.QCalc;

public final class CommandBaton extends QBaton {
public QCalc operator;
//public String uiCommand;
public char uiKeyOrButton;
public String uiParameter;
public QVal[] vals;
//public String error;
}