// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.store;

import java.util.Arrays;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.func.*;
import net.sf.dibdib.struc.*;

/** Use 'shash' (sortable hash) values of QVal objects 
 * for creating data handles in a map. The objects may also be associated
 * with other kinds of keys (e.g. object ID (oid)).
 * Note: shash values are not unique, just like hash values.
 * TODO linked deletion
 */
public final class QMap {
//=====
/*
Handle bit values (long!, cmp. QStr):

.......... .   .... .... . (may turn negative!)
shash << 3 f1  sub  inx  f0 

f0: 0 = value, 1 = indexed
f1: if indexed: 0 = string, 1 = struct
string index = inx << 1 + f0
shash index = inx << 1
struct index = inx
struct shash = shash of first elements + 2 bits type info f2/f3
sub: subtable (>0: with hash+x as index, 0: spill list)
f3/f2: type (implicitly packaged, unpackaged counterparts ('bunch'
for set, 'sequence' for list) are stored in the same way and need
to be handled differently outside of this class): 00 = list (basic aggregation),
01 = set (predication/denotation: (ODD 3)), 02 = map (proposition/connotation: (SUCC 3 4)),
03 = triples (association/mediation: (SUB 2 3 1))

value, last bits:
0: value == handle2Double()/Str() (lowercase/num)
2: RFU
4: value == handle2Str()/ 1st uppercase
6: value == handle2Str()/ all uppercase

0 = empty string
-2 (-1) = error

 */

public static final QMap main = new QMap( Dib2Constants.MAPPING_KEY_TYPES );

private static final int BOX_SHIFT = 14;
private static final int BOX_SIZE = 1 << BOX_SHIFT;
private static final int BOX_MASK = (1 << BOX_SHIFT) - 1;
private static final String MARKER_DELETED = "" + (char) 1;

public static final int COMBINED_BITS__28 = 2 * BOX_SHIFT;
public static final int FLAG_INDEXED__1 = 1;
/** Bit is 0 for strings/ literals, 1 for lists, maps etc. */
public static final long FLAG_STRUCT = 1L << COMBINED_BITS__28;

private static final int COMBINED_MASK = (1 << COMBINED_BITS__28) - 1;

private static final long FLAG_VALUE_CAPS__4 = 4;
private static final long FLAG_VALUE_CAPS_ALL__6 = 6;
private static final long FLAG_VALUE_CAPS_FIRST__4 = 4;

public static final long STRUC_ERROR = -4;
public static final long NONE = -2;
public static final long EMPTY_STRING = 0;
public static final long EMPTY_LIST = FLAG_STRUCT | FLAG_INDEXED__1;

/** To be handled externally: Internally, all STRUCTs are treated as LISTs, i.e.
 * a SET and a LIST/ MAP etc. with the same data elements are the same inside QMap,
 * but possibly different outside. 3 bits are used if the lowest of them is 1.
 */
public static final long FLAGS_STRUCT_TYPE = 7L << (COMBINED_BITS__28);
public static final int LIST = 1;
public static final int SET = 3;
public static final int MAP = 5;
public static final int ASSOCIATION = 7;

/////

private final Trie[] heads;
/** Object is String for shash or String[] for shash plus oids/ other keys, followed by
 * String for tokenized pieces of literals or long[] for literals and structs. */
private Object[][] values = new Object[][] { new Object[ BOX_SIZE ] };
/** >= 100 => stop counting */
private byte[][] refCount = new byte[][] { new byte[ BOX_SIZE / 2 ] };
//Skip values[0][0]:
private short[] sizes = new short[] { 2 };
private int hDeletedHead = -1;
private int hDeletedTail = -1;
private int hDeletedTemp = -1;

private static final long[] mCharHandleCache = new long[ 128 ];

//=====
private static final class Trie {
//=====
private char[] mKeyBits;
/** int[] for refs to values (with trailing 0s), Trie for next key bits */
private Object[] mRefs;
private int mCount;

Trie( int size ) {
	mKeyBits = new char[ (size / 16 + 1) * 16 ];
	mRefs = new Object[ (size / 16 + 1) * 16 ];
	mCount = 0;
}

/** Get element or next.
 * @param index Negative for next.
 * @return
 */
Object getElement( int index ) {
	if ((0 <= index) && (index < mCount)) {
		return mRefs[ index ];
	}
	index = (-index - 1);
	if ((0 <= index) && (index < mCount)) {
		return mRefs[ index ];
	}
	return null;
}

synchronized int search( QResult yResult, char xKeyBits ) {
	int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, xKeyBits );
	if (0 > iRef) {
		yResult.object0 = null;
		return iRef;
	}
	yResult.object0 = mRefs[ iRef ];
	return iRef;
}

int searchSubTrieIndex( Trie xTrie ) {
	Object[] refs = mRefs;
	for (int i0 = 0; i0 < refs.length; ++ i0) {
//		if (refs[ i0 ] instanceof Trie) {
		if (xTrie == refs[ i0 ]) {
			return i0;
		}
	}
	return -1;
}

int nextKeyBits( int xiRef, int comparison ) {
	char[] keyBits = mKeyBits;
	xiRef += (0 >= comparison) ? 1 : -1;
	return ((0 > xiRef) || (xiRef >= mCount)) ? -1 : keyBits[ xiRef ];
}

synchronized int searchNext( QResult yResult, char xKeyBits ) {
	int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, xKeyBits );
	iRef = (0 > iRef) ? (-iRef - 1) : (iRef + 1);
	yResult.object0 = (iRef >= mRefs.length) ? null : mRefs[ iRef ];
	return iRef;
}

synchronized int searchPrevious( QResult yResult, char xKeyBits ) {
	int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, xKeyBits );
	iRef = (0 > iRef) ? (-iRef - 2) : (iRef - 1);
	yResult.object0 = (0 > iRef) ? null : mRefs[ iRef ];
	return iRef;
}

synchronized Trie split( int offs, int iRef, int[] handles, String[] keys ) {
	if ((1 >= mCount) && ((256 + offs) >= keys.length)) {
		return this;
	}
	mRefs[ iRef ] = new Trie( handles.length );
	Trie trie = (Trie) mRefs[ iRef ];
	int from = 0;
	char keyChar = 0;
	int to = 0;
	for (; to < handles.length; ++ to) {
//		int hx = handles[ to ];
		final String key = keys[ to ]; // valShash( hx );
		final char kx = (offs >= key.length()) ? 0 : key.charAt( offs );
		if ((kx != keyChar) && (to > from)) {
			trie.mKeyBits[ trie.mCount ] = keyChar;
			trie.mRefs[ trie.mCount ++ ] = Arrays.copyOfRange( handles, from, to );
			from = to;
		}
		keyChar = kx;
	}
	if ((to > from) && (0 < handles[ from ])) {
		trie.mKeyBits[ trie.mCount ] = keyChar;
		trie.mRefs[ trie.mCount ++ ] = Arrays.copyOfRange( handles, from, to );
	}
	return trie;
}

synchronized int[] adjustHandleArrayNAddKeyChar( int len, int iRef, int keyChar ) {
	//if (mKeyBits[ iRef ] > 0)/ ...:
	if ((iRef >= mRefs.length) || (mKeyBits[ iRef ] != (char) keyChar)) {
		++ mCount;
		if (mCount >= mKeyBits.length) {
			mKeyBits = Arrays.copyOf( mKeyBits, mCount * 2 );
			mRefs = Arrays.copyOf( mRefs, mCount * 2 );
		}
		System.arraycopy( mKeyBits, iRef, mKeyBits, iRef + 1, mCount - iRef - 1 );
		System.arraycopy( mRefs, iRef, mRefs, iRef + 1, mCount - iRef - 1 );
		mKeyBits[ iRef ] = (char) keyChar;
		mRefs[ iRef ] = new int[ 0 ];
	}
	int[] handles = (int[]) mRefs[ iRef ];
	mRefs[ iRef ] = Arrays.copyOf( handles, len + 1 );
	return (int[]) mRefs[ iRef ];
}

private synchronized boolean removeKey( char keyChar, int index, int len, int handle ) {
	int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, keyChar );

	///// Check for race conditions.
	if (0 > iRef) {
		return false;
	}
	final Object next = mRefs[ iRef ];
	if (!(next instanceof int[])) {
		return false;
	}
	final int[] handles = (int[]) next;
	if ((len < handles.length) && (0 != handles[ len ])) {
		return false;
	}
	if (handle != handles[ index ]) {
		return false;
	}

	if (1 >= len) {
		-- mCount;
		System.arraycopy( mKeyBits, iRef + 1, mKeyBits, iRef, mCount - iRef );
		System.arraycopy( mRefs, iRef + 1, mRefs, iRef, mCount - iRef );
		mKeyBits[ mCount ] = 0;
		mRefs[ mCount ] = null;
		if ((16 < mCount) && (mCount < mKeyBits.length / 3)) {
			mKeyBits = Arrays.copyOf( mKeyBits, mKeyBits.length / 2 );
			mRefs = Arrays.copyOf( mRefs, mRefs.length / 2 );
		}
	} else {
		System.arraycopy( handles, index + 1, handles, index, len - index - 1 );
		handles[ len - 1 ] = 0;
		if ((4 < len) && (len < handles.length / 3)) {
			mRefs[ iRef ] = Arrays.copyOf( handles, handles.length / 2 );
		}
	}
	return true;
}

Trie removeKeyBits( String key, int offs, long xHandle ) {
	final int handle = (int) xHandle & COMBINED_MASK;
	char keyChar = (offs >= key.length()) ? 0 : key.charAt( offs );
	// Try again in case of race condition:
	while (true) {
		boolean done = true;
		int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, keyChar );
		if (0 > iRef) {
			return this;
		}
		final Object next = mRefs[ iRef ];
		if (next instanceof int[]) {
			final int[] handles = (int[]) next;
			int len = handles.length;
			for (int ix = handles.length - 1; ix >= 0; -- ix) {
				if (0 == handles[ ix ]) {
					len = ix;
				} else if (handle == handles[ ix ]) {
					done = removeKey( keyChar, ix, len, handle );
					break;
				}
			}
			if (done) {
				return null;
			}
		}
		if (done) {
			return (Trie) next;
		}
	}
}
//=====
}

//=====

public QMap( int cKeyTypes ) {
	///// 0 is empty String
	refCount[ 0 ][ 0 ] = 100;
	values[ 0 ][ 0 ] = "";
	values[ 0 ][ 1 ] = "";
	heads = new Trie[ cKeyTypes ];
	for (int i0 = cKeyTypes - 1; i0 >= 0; -- i0) {
		heads[ i0 ] = new Trie( 64 );
	}
}

public boolean isFresh() {
	return (1 == sizes.length) && (2 >= sizes[ 0 ]) && (0 == heads[ 0 ].mCount);
}

public static boolean isVoid( /*h*/long handle ) {
	return (NONE == handle) || (STRUC_ERROR == handle) || (-1 == handle);
}

public static boolean isVoidOrEmpty( /*h*/long handle ) {
	return (0 == handle) || (EMPTY_LIST == handle) || (NONE == handle) || (STRUC_ERROR == handle) || (-1 == handle);
}

public void idle() {
	hDeletedTemp = hDeletedTail;
}

private synchronized Object readStoredValue( long handle ) {
	final int high = BOX_MASK & ((int) handle >>> BOX_SHIFT);
	final int low = (int) handle & BOX_MASK;
	if ((high >= refCount.length) || (0 >= refCount[ high ][ low / 2 ])) {
		return null;
	}
	return values[ high ][ low ];
}

private synchronized String readStoredKeyNValue( QResult yResult, int handle, int iKeyType ) {
	final int high = BOX_MASK & (handle >>> BOX_SHIFT);
	final int low = handle & (BOX_MASK & ~1);
	if ((high >= refCount.length) || (0 >= refCount[ high ][ low / 2 ])) {
		return null;
	}
	final Object keys = values[ high ][ low ];
	String out;
	if (keys instanceof String) {
		if (iKeyType != 0) {
			return null;
		}
		out = (String) keys;
	} else {
		if (iKeyType >= ((String[]) keys).length) {
			return null;
		}
		out = ((String[]) keys)[ iKeyType ];
	}
	if (out.startsWith( MARKER_DELETED )) {
		return null;
	}
	if (null != yResult) {
		yResult.object0 = values[ high ][ low | 1 ];
	}
	return out;
}

private synchronized long handle4BoxElement( QResult zTmp, int xHandle ) {
	final String shash = readStoredKeyNValue( zTmp, xHandle, 0 );
	if (null == shash) {
		return 0;
	}
	Object o0 = zTmp.object0;
	zTmp.object0 = null;
	return (shash2handleOffset( shash ) & ~(long) COMBINED_MASK)
		| ((o0 instanceof long[]) ? FLAG_STRUCT : 0) | xHandle;
}

private synchronized long handle4BoxElement( QResult zTmp, int xiBox, int xiElement ) {
	return handle4BoxElement( zTmp, (xiBox << BOX_SHIFT) | xiElement );
}

private int traverseTries( QResult yResult, Trie xCurrent, int xIndex, int xDirection, Trie xParent ) {
	assert 0 != xDirection;
	Object out = xCurrent.getElement( xIndex );
	if (null == out) {
		if (null == xParent) { // || (0 == xDirection)
			return -1;
		}
		int inx = xParent.searchSubTrieIndex( xCurrent );
		if (0 > inx) {
			return -1;
		}
		inx += xDirection;
		if (0 > inx) {
			return -1;
		}
		return traverseTries( yResult, xParent, inx, xDirection, null );
	} else if (out instanceof int[]) {
		yResult.object0 = xCurrent;
		return xIndex;
	}
	return traverseTries( yResult, (Trie) out, (0 <= xDirection) ? 0 : -1, xDirection, xCurrent );
}

private int traverseTries( QResult yResult, Trie xCurrent, int xiLevel, String xKey, int xiKeyType, int xiStep, Trie xParent ) {
	Trie current = (null == xCurrent) ? heads[ xiKeyType ] : xCurrent;
	final char keyChar = (xiLevel >= xKey.length()) ? 0 : xKey.charAt( xiLevel );
	int iRef = current.search( yResult, keyChar );
	Object out = yResult.object0;
	yResult.object0 = null;
	if (null == out) {
		if (0 == xiStep) {
			return -1;
		}
		iRef = -iRef - 1 - ((0 > xiStep) ? 1 : 0);
		if (0 <= iRef) {
			out = current.getElement( iRef );
		}
		if (null == out) {
			if ((null == xParent) || (0 == xiStep)) {
				return -1;
			}
			iRef = xParent.searchSubTrieIndex( current );
			iRef += xiStep;
			if (0 > iRef) {
				return -1;
			}
			return traverseTries( yResult, xParent, iRef, xiStep, null );
		} else if (out instanceof int[]) {
			yResult.object0 = current;
			return iRef;
		}
	} else if (out instanceof int[]) {
		if (0 == xiStep) {
			yResult.object0 = current;
			return iRef;
		}
		iRef += xiStep;
//		iStep = -iStep;
		if (0 > iRef) {
			return -1;
		}
		iRef = traverseTries( yResult, current, iRef, xiStep, null );
		current = (Trie) yResult.object0;
		out = null;
	}
	if (null != out) {
		iRef = traverseTries( yResult, (Trie) out, xiLevel + 1, xKey, xiKeyType, xiStep, current );
		current = (Trie) yResult.object0;
	}
	if ((null != current) || (null == xParent)) {
		yResult.object0 = current;
		return iRef;
	}
	iRef = xParent.searchSubTrieIndex( xCurrent );
	iRef += xiStep;
	if (0 > iRef) {
		return -1;
	}
	return traverseTries( yResult, xParent, iRef, xiStep, null );
}

/** Split if handles[] is full and 'large': assuming no trailing 0s.
 * @param iKeyType
 * @param trie
 * @param offs
 * @param iRef
 * @param handles
 */
private synchronized void splitTrie( int iKeyType, Trie trie, int offs, int iRef, int[] handles ) {
	String[] keys = new String[ handles.length ];
	for (int to = handles.length - 1; to >= 0; -- to) {
		int hx = handles[ to ];
		final String key = readStoredKeyNValue( null, hx, iKeyType );
		keys[ to ] = key;
	}
	trie.split( offs, iRef, handles, keys );
}

/** Add to reference count. Needs to be released for GC.
 * @param handle
 * @return handle, NONE if failed
 */
public synchronized/*h*/long takeHandle( /*h*/long handle ) {
	final int high = BOX_MASK & ((int) handle >>> BOX_SHIFT);
	final int low = (int) handle & BOX_MASK;
	// Deleted?
	if ((high >= refCount.length) || (0 >= refCount[ high ][ low / 2 ]) || (MARKER_DELETED.equals( values[ high ][ low & ~1 ] ))) {
		return NONE;
	}
	refCount[ high ][ low / 2 ] += (refCount[ high ][ low / 2 ] <= 100) ? 1 : 0;
	return handle;
}

/** Having found the data, do the synchronized process.
 * @param object Data if iKeyType == 0 (shash), otherwise handle for secondary key.
 * @param iKeyType 0 = shash, otherwise secondary key (e.g. 1 = OID).
 * @param key
 * @param offs
 * @param trie
 * @param lenHandles
 * @param iRef
 * @param insert
 * @return
 */
private synchronized int addKeyCharNHandle( QResult zTmp, Object object, int iKeyType, String key,
	int offs, Trie trie, int lenHandles, int iRef, int insert ) {
	char keyChar = (offs >= key.length()) ? 0 : key.charAt( offs );

	///// Check for race conditions.
	int ix = trie.search( zTmp, keyChar );
	final Object next = zTmp.object0;
	zTmp.object0 = null;
	ix = (0 > ix) ? (-ix - 1) : ix;
	if (iRef != ix) {
		return 0;
	}
	if (next instanceof int[]) {
		final int[] handles = (int[]) next;
		if (lenHandles > handles.length) {
			return 0;
		} else if ((lenHandles < handles.length) && (0 != handles[ lenHandles ])) {
			return 0;
		} else if (insert > lenHandles) {
			return 0;
		} else if (insert < lenHandles) {
			int handle = handles[ insert ];
			final String keyx = readStoredKeyNValue( null, handle, iKeyType );
			final int cmp = key.compareTo( keyx );
			if (0 < cmp) {
				return 0;
			} else if (0 > cmp) {
				if (0 < insert) {
					if (0 >= key.compareTo( readStoredKeyNValue( null, handles[ insert - 1 ], iKeyType ) )) {
						return 0;
					}
				}
			}
		}
	}
	int[] handles = trie.adjustHandleArrayNAddKeyChar( lenHandles, iRef, ((offs < key.length()) ? keyChar : 0) ); //-1) );
	if (lenHandles > insert) {
		System.arraycopy( handles, insert, handles, insert + 1, lenHandles - insert );
	}
	int handle;
	if (0 == iKeyType) {
		if (sizes[ values.length - 1 ] >= values[ values.length - 1 ].length) {
			values = Arrays.copyOf( values, values.length + 1 );
			values[ values.length - 1 ] = new Object[ BOX_SIZE ];
			sizes = Arrays.copyOf( sizes, values.length );
			refCount = Arrays.copyOf( refCount, values.length );
			refCount[ refCount.length - 1 ] = new byte[ BOX_SIZE / 2 ];
		}
		values[ values.length - 1 ][ sizes[ values.length - 1 ] ++ ] = key;
		refCount[ values.length - 1 ][ sizes[ values.length - 1 ] / 2 ] = 1;
		handle = ((values.length - 1) << BOX_SHIFT) | sizes[ values.length - 1 ];
		values[ values.length - 1 ][ sizes[ values.length - 1 ] ++ ] = object;
	} else {
		handle = ((Integer) object) & COMBINED_MASK;
	}
	handles[ insert ] = handle;
	// Some keys, especially short keys, e.g. for punctuation, may end up in having long lists.
	// This is okay if the lists are sorted and the strings are short. Otherwise, we rather
	// split the search tree:
	if ((offs < key.length()) && (lenHandles >= handles.length) && (16 <= lenHandles) && (0 != key.charAt( offs ))) {
		++ offs;
		splitTrie( iKeyType, trie, offs, iRef, handles );
	}
	return handle;
}

private int add( QResult zTmp, int iKeyType, Object object, String key ) {
	//TODO Use hDeleted...
	int iRef = 0;
	int offs = 0;
	int insert = 0;
	int len = 0;
	char keyChar = 0;
	Trie trie = heads[ iKeyType ];
	// Try again in case of race condition:
	while (true) {
		for (; offs <= key.length(); ++ offs) {
			keyChar = (offs >= key.length()) ? 0 : key.charAt( offs );
			iRef = trie.search( zTmp, keyChar );
			final Object next = zTmp.object0;
			if (0 > iRef) {
				iRef = -iRef - 1;
				break;
			}
			if (next instanceof int[]) {
				final int[] handles = (int[]) next;
				len = handles.length;
				//TODO speed up
				for (insert = 0; insert < len; ++ insert) {
					int handle = handles[ insert ];
					if (0 == handle) {
						// Trailing 0.
						len = insert;
						break;
					}
					final String keyx = readStoredKeyNValue( null, handle, iKeyType );
					final int cmp = key.compareTo( keyx );
					if (0 > cmp) {
						break;
					} else if (0 == cmp) {
						if ((object instanceof Integer) && (handle != (Integer) object)) {
							break;
						} else if ((object instanceof String) &&
							!((String) object).equals( values[ BOX_MASK & (handle >>> BOX_SHIFT) ][ handle & BOX_MASK ] )) {
							break;
						} else if (object instanceof long[]) {
							Object vx = values[ BOX_MASK & (handle >>> BOX_SHIFT) ][ handle & BOX_MASK ];
							if (!(vx instanceof long[]) || !Arrays.equals( (long[]) object, (long[]) vx )) {
								break;
							}
						}
						takeHandle( handle );
						return handle;
					}
				}
				break;
			}
			trie = (Trie) next;
			iRef = 0;
		}
		zTmp.object0 = null;
		int handle = addKeyCharNHandle( zTmp, object, iKeyType, key, offs, trie, len, iRef, insert );
		if (0 != handle) {
			return handle;
		}
	}
}

/** Secondary keys, e.g. OID, for stored objects. Does not work for simple values. */
public synchronized long addKey( QResult zTmp,/*h*/long handle, int iKeyType, String secondaryKey ) {
	final int high = BOX_MASK & ((int) handle >>> BOX_SHIFT);
	final int low = (int) handle & BOX_MASK;
	if ((high >= refCount.length) || (0 >= refCount[ high ][ low / 2 ])) {
		return NONE;
	}
	final Object kx = values[ high ][ low & ~1 ];
	String[] keys = (kx instanceof String[]) ? (String[]) kx : new String[] { (String) kx };
	if ((null != keys) && !keys[ 0 ].startsWith( MARKER_DELETED )) {
		if ((iKeyType < keys.length) && (secondaryKey.equals( keys[ iKeyType ] ))) {
			return NONE;
		} else if (iKeyType >= keys.length) {
			keys = Arrays.copyOf( keys, iKeyType + 1 );
		}
		keys[ iKeyType ] = secondaryKey;
		values[ high ][ low & ~1 ] = keys;
		add( zTmp, iKeyType, (int) handle, secondaryKey );
		return handle;
	}
	return NONE;
}

private synchronized String removeKey( int level, /*h*/long handle, String key ) {
	final int high = BOX_MASK & ((int) handle >>> BOX_SHIFT);
	final int low = (int) handle & BOX_MASK;
	if ((high >= refCount.length) || (0 >= refCount[ high ][ low / 2 ])) {
		return null;
	}
	final String[] keys = (String[]) values[ high ][ low & ~1 ];
	String out = null;
	if ((null != keys) && (level < keys.length)) {
		if (key.equals( keys[ level ] )) {
			values[ high ][ low & ~1 ] = Arrays.copyOf( keys, level );
			out = key;
		}
	}
	for (int iKeyType = level; iKeyType < keys.length; ++ iKeyType) {
		int offs = 0;
		for (Trie trie = heads[ iKeyType ]; offs <= key.length(); ++ offs) {
			Trie old = trie;
			trie = trie.removeKeyBits( keys[ iKeyType ], offs, handle );
			if (null == trie) {
				if (iKeyType == level) {
					out = key;
				}
				break;
			} else if (old == trie) {
				break;
			}
		}
		if (0 != level) {
			break;
		}
	}
	return out;
}

private static long toHandle4ShortString( String str, String shash ) {
	if (null == shash) {
		shash = QStr.shash( str );
	}
	long offs = shash2handleOffset( shash );
	if (0 == (offs & FLAG_INDEXED__1)) {
		// Expected to be lowercase:
		String cmp = QStr.string4Shash( shash );
		if ((str.length() == cmp.length()) && ((str.charAt( 0 ) & 0x1f) == (cmp.charAt( 0 ) & 0x1f))) {
			if (str.equals( cmp )) {
				return offs;
			} else if ((str.charAt( 0 ) + 0x20) == cmp.charAt( 0 )) {
				if (str.substring( 1 ).equals( cmp.substring( 1 ) )) {
					return offs | FLAG_VALUE_CAPS_FIRST__4;
				} else if (str.equals( UtilString.toUpperCase( cmp ) )) {
					return offs | FLAG_VALUE_CAPS_ALL__6;
				}
			}
		}
	}
	return STRUC_ERROR;
}

private static synchronized void fillCharHandleCache() {
	if (0 == mCharHandleCache[ 0 ]) {
		for (int i0 = 127; i0 >= 0; -- i0) {
			mCharHandleCache[ i0 ] = toHandle4ShortString( "" + (char) i0, null );
//			if (STRUC_ERROR == mCharHandles[ i0 ]) {
//				mCharHandles[ i0 ] = 0;
//			}
		}
		mCharHandleCache[ 0 ] = (0 == mCharHandleCache[ 0 ]) ? STRUC_ERROR : mCharHandleCache[ 0 ];
	}
}

private static String handleBasic2Shash( /*h*/long handle ) {
	if (0 == handle) {
		return "";
	} else if (isVoidOrEmpty( handle )) {
		return "" + QStr.SHASH_PUNCT;
	} else if (0 == (handle & FLAG_INDEXED__1)) {
		char[] shash = new char[ 4 ];
		long inBytes = handle >>> 3;
		for (int i0 = 3; i0 >= 0; -- i0) {
			shash[ i0 ] = (char) inBytes;
			inBytes >>>= 16;
		}
		int len = 4;
		for (; len > 0; -- len) {
			if (0 != shash[ len - 1 ]) {
				break;
			}
		}
		shash[ 0 ] |= 0xe000;
		return new String( Arrays.copyOf( shash, len ) );
//	} else if (1 == (FLAG_STRUCT & handle)) {
	}
	return null;
}

private static String handleShortString2String( /*h*/long handle ) {
	String out = QStr.string4Shash( handleBasic2Shash( handle & ~FLAG_INDEXED__1 ) );
	if ((0 == (handle & FLAG_VALUE_CAPS__4)) || (0 >= out.length()) || isVoid( handle )) {
		return out;
	}
	return (FLAG_VALUE_CAPS_ALL__6 == (handle & FLAG_VALUE_CAPS_ALL__6)) ? UtilString.toUpperCase( out ) //,
		: ("" + (char) (out.charAt( 0 ) - 0x20) + out.substring( 1 ));
}

//-> toHandleOffset4Oid
/**
* @param shash
* @return handle offset, i.e. without index and without FLAG_STRUCT
*/
public static/*h*/long shash2handleOffset( String shash ) {
	final int len = shash.length();
	if (0 >= len) {
		return EMPTY_STRING;
	}
	long offs = (shash.charAt( 0 ) & 0x1fffL) << (3 + 48);
	offs |= (1 >= len) ? 0 : ((long) shash.charAt( 1 ) << (3 + 32));
	if (4 >= len) {
		offs |= (2 >= len) ? 0 : ((long) shash.charAt( 2 ) << (3 + 16));
		offs |= (3 >= len) ? 0 : (shash.charAt( 3 ) << 3);
		// Last bit is 0:
		return offs;
	}
	// Keep 29 bits for index + 2nd flag:
	offs |= ((long) shash.charAt( 2 ) & 0xfc00) << (3 + 16);
	return offs | FLAG_INDEXED__1;
}

private static/*h*/long toHandleOffset4ShortString( String[] xyStr ) {
	String str = xyStr[ 0 ];
	xyStr[ 0 ] = null;
	if (0 == str.length()) {
		return EMPTY_STRING;
	} else if ((1 == str.length()) && (128 > str.charAt( 0 ))) {
		if (0 == mCharHandleCache[ 0 ]) {
			fillCharHandleCache();
		}
		if ((0 != mCharHandleCache[ 0 ]) && (0 != mCharHandleCache[ str.charAt( 0 ) ])) {
			return mCharHandleCache[ str.charAt( 0 ) ];
		}
	}
	String shash = QStr.shash( str );
	long offs = shash2handleOffset( shash );
	if (0 == (offs & FLAG_INDEXED__1)) {
		long hx = toHandle4ShortString( str, shash );
		if (STRUC_ERROR != hx) {
			return hx;
		}
	}
	xyStr[ 0 ] = shash;
	return offs;
}

///// PUBLIC

//-> toHandle...
public static/*h*/long makeHandleAsciiShort( String str ) {
	if (0 == str.length()) {
		return 0;
	} else if ((1 == str.length()) && (STRUC_ERROR != mCharHandleCache[ 0 ]) && (0 != mCharHandleCache[ 0 ]) && (128 > str.charAt( 0 ))) {
		return mCharHandleCache[ str.charAt( 0 ) ];
	}
	String shash = QStr.shashAnsi( str );
	long offs = shash2handleOffset( shash );
	if (0 == (offs & FLAG_INDEXED__1)) {
		// Expected to be lowercase:
		String cmp = QStr.string4Shash( shash );
		if ((str.length() == cmp.length()) && ((str.charAt( 0 ) & 0x1f) == (cmp.charAt( 0 ) & 0x1f))) {
			if (str.equals( cmp )) {
				return offs;
			} else if ((str.charAt( 0 ) + 0x20) == cmp.charAt( 0 )) {
				if (str.substring( 1 ).equals( cmp.substring( 1 ) )) {
					return offs | FLAG_VALUE_CAPS_FIRST__4;
				} else if (str.equals( UtilString.toUpperCase( cmp ) )) {
					return offs | FLAG_VALUE_CAPS_ALL__6;
				}
			}
		}
	}
	return STRUC_ERROR;
}

public static double handleNum2Double( /*h*/long handle ) {
	if (isVoid( handle )) {
		return Double.NaN;
	} else if (0 != (handle & FLAG_INDEXED__1)) {
		String shash = handleBasic2Shash( handle );
		return (null == shash) ? Double.NaN : QStr.double4ShashNum( shash );
	}
	long inBytes = handle >>> 3;
	long bits = 0xe000 | (inBytes >>> 48);
	if (bits >= QStr.SHASH_NUM_UNIT) {
		inBytes = (inBytes & 0xffffffffffffL) << 16;
		while (((inBytes >>> 48) < 0x8000) && (0L < inBytes)) {
			inBytes = (inBytes & 0xffffffffffffL) << 16;
		}
		bits = (inBytes >>> 48);
	}
	boolean pos = (QStr.SHASH_POS <= bits);
	bits -= pos ? QStr.SHASH_POS : QStr.SHASH_NEG;
	bits <<= 52;
	bits |= (inBytes & 0x3fffL) << 10;
	bits |= ((inBytes >>> 16) & 0x3fffL) << 24;
	bits |= ((inBytes >>> 32) & 0x3fffL) << 38;
	return Double.longBitsToDouble( pos ? bits : -bits );
}

private QVal readQVal4Stored( QResult zTmp, int handle ) {
	final String shash = readStoredKeyNValue( zTmp, handle, 0 );
	if (null == shash) {
		return null;
	}
	final Object o0 = zTmp.object0;
	zTmp.object0 = null;

	if (o0 instanceof String) {
		return new QVal( (String) o0, shash );
	}
	return new QVal( null, (long[]) o0, shash );
}

private Object readValue( long handle, boolean preferString ) {
	if (0 == handle) {
		return "";
	} else if (isVoid( handle )) {
		return null;
	} else if (0 == (handle & FLAG_INDEXED__1)) {
		String shash = handle2Shash( handle & ~FLAG_INDEXED__1 );
		if ((null == shash) || (0 >= shash.length())) {
			return "";
		} else if (QStr.SHASH_LITERAL >= shash.charAt( 0 )) {
			if (QStr.SHASH_NEG > shash.charAt( 0 )) {
				if (QStr.SHASH_DATE > shash.charAt( 0 )) {
					return (QStr.SHASH_PUNCT == shash.charAt( 0 )) ? shash.substring( 1 ) : ".";
				}
				return QStr.date4ShashDate( shash );
			} else if (!preferString) {
				return QStr.double4ShashNum( shash );
			}
		}
		return handleShortString2String( handle );
	}
	return readStoredValue( handle );
}

public Object readValue( long handle ) {
	return readValue( handle, false );
}

//-> readQVal
public QVal handle2QVal( /*h*/long handle ) {
	if (0 == handle) {
		return QVal.EMPTY;
	} else if (isVoid( handle )) {
		return QVal.NaN;
	}
	if (0 == (handle & FLAG_INDEXED__1)) {
		String shash = handle2Shash( handle & ~FLAG_INDEXED__1 );
		if ((null == shash) || (0 >= shash.length())) {
			return QVal.EMPTY;
		} else if (QStr.SHASH_LITERAL >= shash.charAt( 0 )) {
			if (QStr.SHASH_NEG > shash.charAt( 0 )) {
				if (QStr.SHASH_DATE > shash.charAt( 0 )) {
					return new QVal( (QStr.SHASH_PUNCT == shash.charAt( 0 )) ? shash.substring( 1 ) : ".", shash );
				}
				return new QVal( QStr.date4ShashDate( shash ), shash );
			} else {
				return new QVal( null, QStr.double4ShashNum( shash ), shash );
			}
		}
		return new QVal( handleShortString2String( handle ), true );
	}
	final QResult pooled = QResult.get8Pool();
	return readQVal4Stored( pooled, (int) handle );
}

public Object existsAsSpecialObject( long handle ) {
	return readStoredValue( handle );
}

public String readString( long handle ) {
	if (isVoid( handle )) {
		return null;
	} else if (isVoidOrEmpty( handle )) {
		return "";
	}
	Object o0 = readValue( handle, true );
	if (o0 instanceof long[]) {
		long[] ah = (long[]) o0;
		if (0 == ah.length) {
			return "";
		} else if (1 == ah.length) {
			o0 = readStoredValue( ah[ 0 ] );
		}
	} else if (null == o0) {
		return null;
	}
	return (o0 instanceof String) ? (String) o0 : o0.toString();
}

public String[] readStrings( String fallback, long... handles ) {
	if (null == handles) {
		return new String[ 0 ];
	}
	String[] out = new String[ handles.length ];
	int count = 0;
	for (long handle : handles) {
		if (isVoid( handle )) {
			out[ count ++ ] = fallback;
			continue;
		} else if (isVoidOrEmpty( handle )) {
			out[ count ++ ] = "";
			continue;
		}
		final Object o0 = readValue( handle, true );
		if (o0 instanceof long[]) {
			long[] ah = (long[]) o0;
			StringBuilder sx = new StringBuilder( 5 * ah.length );
			for (long hx : ah) {
				String s0 = readString( hx );
				sx.append( (null == s0) ? "" : s0 );
			}
			out[ count ++ ] = sx.toString();
		} else if (null == o0) {
			out[ count ++ ] = fallback;
		} else {
			out[ count ++ ] = (o0 instanceof String) ? (String) o0 : o0.toString();
		}
	}
	return out;
}

public boolean containedHandle( long handle, long... arraysToSearch ) {
	for (long toSearch : arraysToSearch) {
		if (toSearch == handle) {
			return true;
		}
		Object arr = existsAsSpecialObject( toSearch );
		if ((null != arr) && (arr instanceof long[])) {
			if (containedHandle( handle, (long[]) arr )) {
				return true;
			}
		}
	}
	return false;
}

public long[] readStruct( long handle ) {
	if (0 == (FLAG_INDEXED__1 & handle)) {
		return new long[] { handle };
	}
	final Object o0 = readValue( handle, false );
	return o0 instanceof long[] ? (long[]) o0 : ((null == o0) ? null : new long[] { handle });
}

public Object readStructElement( long handle, int index ) {
	long[] ah = readStruct( handle );
	return ((null == ah) || (ah.length <= index)) ? null : readValue( ah[ index ], false );
}

private static final String readLiteral_fallback = "?";

public String readLiteral( long[] handles ) {
	StringBuilder out = new StringBuilder( 5 * handles.length );
	boolean punct = true;
	for (long handle : handles) {
		String s0 = readString( handle );
		s0 = (null == s0) ? readLiteral_fallback : s0;
		if ((0 == s0.length()) || QStr.PATTERN_SPACE_NL.matcher( "" + s0.charAt( 0 ) ).matches()) {
			punct = true;
		} else {
			if (!punct) {
				out.append( ' ' );
			}
			punct = false;
		}
		out.append( s0 );
	}
	return out.toString();
}

public String[] readLiterals( long xHandle ) {
	{
		final Object o0 = readValue( xHandle, true );
		if (o0 instanceof String) {
			return new String[] { (String) o0 };
		}
	}
	long[] handles = readStruct( xHandle );
	if ((null == handles) || (0 >= handles.length)) {
		return new String[ 0 ];
	}
	if ((0 == (FLAG_STRUCT & xHandle)) && (readValue( handles[ 0 ], true ) instanceof String)) {
		return new String[] { readLiteral( handles ) };
	}
	String[] out = new String[ handles.length ];
	int count = 0;
	for (long handle : handles) {
		if (isVoid( handle )) {
			out[ count ++ ] = readLiteral_fallback;
			continue;
		} else if (isVoidOrEmpty( handle )) {
			out[ count ++ ] = "";
			continue;
		}
		final Object o0 = readValue( handle, true );
		if (o0 instanceof long[]) {
			long[] ah = (long[]) o0;
			out[ count ++ ] = readLiteral( ah );
		} else if (null == o0) {
			out[ count ++ ] = readLiteral_fallback;
		} else {
			out[ count ++ ] = (o0 instanceof String) ? (String) o0 : o0.toString();
		}
	}
	return out;
}

private int[] findSet( QResult zTmp, String xKey, int xiKeyType, int comparison ) {
	String key = xKey;
	int offs = 0;
	if (key.startsWith( MARKER_DELETED )) {
		return null;
	}
	for (Trie trie = heads[ xiKeyType ]; offs <= key.length(); ++ offs) {
		final char keyChar = (offs >= key.length()) ? 0 : key.charAt( offs );
		int iRef;
		if ((0 < comparison) && (0 == keyChar)) {
			iRef = trie.searchPrevious( zTmp, keyChar );
		} else {
			iRef = trie.search( zTmp, keyChar );
		}
		Object next = zTmp.object0;
		zTmp.object0 = null;
		if (null == next) {
			if (0 < comparison) {
				iRef = trie.searchPrevious( zTmp, keyChar );
			} else if (0 > comparison) {
				iRef = trie.searchNext( zTmp, keyChar );
			}
			next = zTmp.object0;
			zTmp.object0 = null;
			if (null == next) {
				return null;
			}
		}
		if (next instanceof int[]) {
			int[] handles = (int[]) next;
			int count = 0;
			int[] out = new int[ handles.length ];
			//TODO speed up
			for (int handle : handles) {
				if (0 == handle) {
					// Trailing 0.
					break;
				}
				final String keyx = readStoredKeyNValue( null, handle, xiKeyType );
				if ((0 == comparison) ? key.equals( keyx )
					: ((comparison < 0) == (key.compareTo( keyx ) < 0))) {
					out[ count ++ ] = handle;
				}
			}
			if ((0 == count) && (0 != comparison)) {
				int ch = trie.nextKeyBits( iRef, comparison );
				if (0 <= ch) {
					key = key.substring( 0, offs ) + (char) ch;
					-- offs;
					continue;
				}
			}
			return Arrays.copyOf( out, count );
		}
		trie = (Trie) next;
	}
	return null;
}

public static long getHandleTag( int tag, int flags ) {
	return (((long) (0x800000 | (char) tag)) << 32L) | (flags << 1);
}

public static/*h*/long getHandle( double value ) {
	return shash2handleOffset( QStr.shash4Double( null, value ) );
}

// -> takeHandle()
public/*h*/long getHandle( QResult zTmp, String str ) {
	String[] io = new String[] { str };
	long offs = toHandleOffset4ShortString( io );
	String shash = io[ 0 ];
	// Not stored?
	if (shash == null) {
		return offs;
	}
	offs &= ~(long) COMBINED_MASK;
	int[] handles = findSet( zTmp, shash, 0, 0 );
	zTmp.object0 = null;
	if (null == handles) {
		return NONE;
	}
	for (int handle : handles) {
		if (0 == handle) {
			// Trailing 0.
			break;
		}
		final Object o0 = readStoredValue( handle );
		if ((o0 instanceof String) && ((String) o0).equals( str )) {
			takeHandle( handle );
			// FLAG_STRUCT == 0:
			return offs | handle;
		}
	}
	return STRUC_ERROR;
}

//-> toShash4Handle()
public String handle2Shash( /*h*/long handle ) {
	String out = handleBasic2Shash( handle );
	if (null != out) {
		return out;
	}
	return readStoredKeyNValue( null, (int) handle, 0 );
}

public String shash4Handles( long[] pieces ) {
	StringBuilder out = new StringBuilder( Dib2Constants.SHASH_MAX * 2 );
	boolean first = true;
	for (long piece : pieces) {
		if (first) {
			first = false;
		} else {
			out.append( "" + (char) 1 );
		}
		out.append( handle2Shash( piece ) );
		if (Dib2Constants.SHASH_MAX <= out.length()) {
			out = out.delete( Dib2Constants.SHASH_MAX - 1, out.length() );
			break;
		}
	}
	return (0 >= out.length()) ? ("" + QStr.SHASH_STRUC) : out.toString();
}

/** Read OID or secondary key value of Object.
 * @param handle
 * @return
 */
public String readKey( /*h*/long handle, int iKeyType ) {
	if (0 == (FLAG_INDEXED__1 & handle)) {
		return null;
	}
	final int high = BOX_MASK & ((int) handle >>> BOX_SHIFT);
	final int low = (int) handle & (BOX_MASK & ~1);
	if ((high >= refCount.length) || (0 >= refCount[ high ][ low / 2 ])) {
		return null;
	}
	final Object keys = values[ high ][ low ];
	if ((0 == iKeyType) && (keys instanceof String)) {
		return (String) keys;
	}
	return (keys instanceof String[]) && (iKeyType < ((String[]) keys).length)
		? ((String[]) keys)[ iKeyType ] : null;
}

private QVal[] readQVals( QResult zTmp, int[] handles ) {
	QVal[] out = new QVal[ handles.length ];
	int count = 0;
	for (int handle : handles) {
		if (0 == handle) {
			// Trailing 0.
			break;
		}
		QVal v0 = readQVal4Stored( zTmp, handle );
		if (null != v0) {
			out[ count ++ ] = v0;
		}
	}
	if (out.length > count) {
		return Arrays.copyOf( out, count );
	}
	return out;
}

public QVal[] readQVals( /*h*/long... handles ) {
	QVal[] out = new QVal[ handles.length ];
	int count = 0;
	for (long handle : handles) {
		QVal v0 = handle2QVal( handle );
		if (null != v0) {
			out[ count ++ ] = v0;
		}
	}
	if (out.length > count) {
		return Arrays.copyOf( out, count );
	}
	return out;
}

//-> readQVals
public QVal[] findQVals( QResult zTmp, String shash ) {
	int[] out = findSet( zTmp, shash, 0, 0 );
	zTmp.object0 = null;
	if (null == out) {
		return null;
	}
	return readQVals( zTmp, out );
}

public long[] readHandles( QResult zTmp, String key, int iKeyType, boolean asLiteral ) {
	int[] handles = findSet( zTmp, key, iKeyType, 0 );
	zTmp.object0 = null;
	if (null == handles) {
		return null;
	}
	long[] out = new long[ handles.length ];
	int count = 0;
	for (int handle : handles) {
		final long hx = handle4BoxElement( zTmp, handle );
		if (0 != hx) {
			out[ count ++ ] = hx;
		}
	}
	return out;
}

public String removeSecondaryKey( /*h*/long handle, String key, int iKeyType ) {
	return removeKey( iKeyType, handle, key );
}

public synchronized Object release( /*h*/long handle, boolean force ) {
	final int high = BOX_MASK & ((int) handle >>> BOX_SHIFT);
	final int low = (int) handle & BOX_MASK;
	if ((high >= refCount.length) || (0 >= refCount[ high ][ low / 2 ])) {
		return null;
	}
	final Object v0 = values[ high ][ low ];
	final Object kx = values[ high ][ low & ~1 ];
	final String[] keys = (kx instanceof String[]) ? (String[]) kx : new String[] { (String) kx };
	if (keys[ 0 ].startsWith( MARKER_DELETED ) || (null == v0)) {
		///// Should not happen.
		refCount[ high ][ low / 2 ] = 0;
		return null;
	}
	Object out = null;
	if (force || (1 == refCount[ high ][ low / 2 ])) {
		out = v0;
		refCount[ high ][ low / 2 ] = 0;
		if ((1 << COMBINED_BITS__28) > handle) {
			if (0 < hDeletedTail) {
				values[ hDeletedTail >>> BOX_SHIFT ][ hDeletedTail & (BOX_MASK & ~1) ] =
					MARKER_DELETED
						+ (char) (0x8000 | high) + (char) (0x8000 | low);
			} else {
				hDeletedHead = (high << BOX_SHIFT) | low;
			}
			hDeletedTail = (high << BOX_SHIFT) | low;
		}
		removeKey( 0, handle, keys[ 0 ] );
		values[ high ][ low ] = null;
		values[ high ][ low & ~1 ] = null;
		if (sizes[ high ] <= (low + 1)) {
			int sx = low - 1;
			while ((sx >= 0) && (null == values[ high ][ sx ])) {
				-- sx;
			}
			sizes[ high ] = (short) (sx + 1);
		}
	}
	refCount[ high ][ low / 2 ] -= (0 < refCount[ high ][ low / 2 ]) && (refCount[ high ][ low / 2 ] < 100) ? 1 : 0;
	return out;
}

/** Create handle for String, or as actually intended, tokenized piece of String. */
public long makeHandle( QResult zTmp, String token ) {
	String[] io = new String[] { token };
	long offs = toHandleOffset4ShortString( io );
	String shash = io[ 0 ];
	if (shash == null) {
		if (token.equals( handleShortString2String( offs ) )) {
			return offs;
		}
		shash = QStr.shash( token );
	}
	int handle = add( zTmp, 0, token, shash );
	return (0 == handle) ? NONE : ((offs & ~(long) COMBINED_MASK) | handle);
}

/** Create handles for token pieces of String. */
public long[] addLiteralTokens( QResult zTmp, String[] elements ) {
	long[] out = new long[ 2 * elements.length ];
	int count = 0;
	boolean toSkip = false;
	boolean skipped = false;
	for (int i0 = 2; i0 < elements.length; i0 += 3) {
		final String el = elements[ i0 ]; // (0 < elements[ i0 ].length()) ? elements[ i0 ] : elements[ i0 - 2 ];
		if (0 >= el.length()) {
			continue;
		}
		if (el.equals( " " ) && toSkip) {
			skipped = true;
			toSkip = false;
			continue;
		}
		toSkip = (0 != el.length()) && !QStr.PATTERN_SPACE_NL.matcher( "" + el.charAt( 0 ) ).matches();
		if (skipped && !toSkip) {
			out[ count ++ ] = toHandle4ShortString( " ", null );
		} else if (!skipped && toSkip && (0 < count)) {
			out[ count ++ ] = EMPTY_STRING;
		}
		skipped = false;
		out[ count ++ ] = makeHandle( zTmp, el );
	}
	return Arrays.copyOf( out, count );
}

/**
 * Split string into tokens and return their handles.
 * @param literal
 * @return Token handles
 */
public long[] addLiteral( QResult zTmp, String literal ) {
	// TODO
	String[] elements = QStr.toElements_OLD( literal );
	return addLiteralTokens( zTmp, elements );
}

public long[] makeHandles( QResult zTmp, String[] tokens ) {
	long[] out = new long[ tokens.length ];
	for (int i0 = out.length - 1; i0 >= 0; -- i0) {
		out[ i0 ] = makeHandle( zTmp, tokens[ i0 ] );
	}
	return out;
}

public long add( QResult zTmp, long[] literalOrStruct, boolean asLiteral, String... shash ) {
	if (null == literalOrStruct) {
		return NONE;
	} else if ((1 >= literalOrStruct.length) && ((null == shash) || (0 == shash.length))) {
		if (0 >= literalOrStruct.length) {
			return EMPTY_LIST;
		}
		long out = literalOrStruct[ 0 ];
		if (0 == (FLAG_INDEXED__1 & out)) {
			return out;
		}
		out &= ~FLAG_STRUCT;
		if (!asLiteral) {
			Object o0 = readValue( out );
			out |= (o0 instanceof long[]) ? FLAG_STRUCT : 0;
		}
		return out;
	}
	String shx = ((null == shash) || (0 >= shash.length)) ? shash4Handles( literalOrStruct ) : shash[ 0 ];
	if ((null == shx) || (0 >= shx.length())) {
		return (0 >= literalOrStruct.length) ? EMPTY_LIST : NONE;
	}
	int handle = add( zTmp, 0, literalOrStruct, shx );
	long offs = shash2handleOffset( shx ) & ~(long) COMBINED_MASK;
	return (0 == handle) ? NONE : (offs | (asLiteral ? 0 : FLAG_STRUCT) | handle);
}

/**
 * Split strings into tokens and return handle for the list of literals.
 * @param literals
 * @return handle
 */
public long addLiterals( QResult zTmp, String[] literals ) {
	if ((null == literals) || (0 == literals.length)) {
		return EMPTY_STRING;
	} else if (1 == literals.length) {
		long[] ah = addLiteral( zTmp, literals[ 0 ] );
		if ((null == ah) || (0 == ah.length)) {
			return EMPTY_STRING;
		} else if (1 == ah.length) {
			return ah[ 0 ];
		}
		return add( zTmp, ah, true );
	}
	long[] out = new long[ literals.length ];
	for (int i0 = 0; i0 < out.length; ++ i0) {
		long[] ah = addLiteral( zTmp, literals[ i0 ] );
		out[ i0 ] = add( zTmp, ah, true );
	}
	return add( zTmp, out, false );
}

/** Create handle for String or list of handles. */
public long makeHandle( QResult zTmp, QVal val ) {
	if (val.value instanceof String) {
		return makeHandle( zTmp, (String) val.value );
	} else if (val.value instanceof long[]) {
		return add( zTmp, (long[]) val.value, false, val.toShash() );
	}
	return 0;
}

//final Object[] dumpStruct_yRead = new Object[ 1 ];

/**
 * Dump data as STRUCTs, without recognizing literals.
 * @param prevHandle Start with 0, then use long[length-1].
 * @param tag (or 0)
 * @param maxItems
 * @return
 */
public long[] dumpStruct( QResult zTmp,/*h*/long prevHandle, int tag, int maxItems ) {
//	assert null == dumpStruct_yRead[ 0 ] : "Recursion.";
	int high = BOX_MASK & ((int) prevHandle >>> BOX_SHIFT);
	int low = (int) prevHandle & BOX_MASK;
	if ((high >= refCount.length) || (0 >= refCount[ high ][ low / 2 ])) {
		return null;
	}
	final long[] out = new long[ maxItems ];
	low = (low | 1) + 2;
	int count = 0;
	for (; high < refCount.length; ++ high) {
		for (; (low < sizes[ high ]) && (count < maxItems); low += 2) {
			if (0 >= refCount[ high ][ low / 2 ]) {
				continue;
			}
			Object o0 = values[ high ][ low ];
			if (!(o0 instanceof long[])) {
				continue;
			}
			long[] struct = (long[]) o0;
			if (0 < tag) {
				if ((0 >= struct.length) || (tag != (((struct[ struct.length - 1 ] >>> 32) & 0xfffff)))) {
					continue;
				}
			}
			final long hx = handle4BoxElement( zTmp, high, low );
			if (0 != hx) {
				out[ count ++ ] = hx;
			}
		}
//		dumpStruct_yRead[ 0 ] = null;
		low = 1;
	}
	if (count == maxItems) {
		return out;
	}
	return (0 < count) ? Arrays.copyOf( out, count ) : null;
}

public long[] traverse4Struct( QResult zTmp, String xKey, int xiKeyType, int xiStep ) {
	int inx = traverseTries( zTmp, null, 0, xKey, xiKeyType, xiStep, null );
	if (0 > inx) {
		return null;
	}
	Trie trie = (Trie) zTmp.object0;
	zTmp.object0 = null;
	if (null == trie) {
		return null;
	}
	Object el = trie.getElement( inx );
	if (el instanceof int[]) {
		int[] handles = (int[]) el;
		long[] out = new long[ handles.length ];
		int count = 0;
		for (int handle : handles) {
			if (0 == handle) {
				// Trailing 0.
				break;
			}
			final long hx = handle4BoxElement( zTmp, handle );
			if (0 != hx) {
				out[ count ++ ] = hx;
			}
		}
		return out;
	}
	return null;
}

/**
 * Extract elements for display etc.
 * @param ixyThread For using long[] (in QResult) and returning the selected element's index.
 * @param xKey Key (Shash/ OID/ ...).
 * @param xiKeyType Its type.
 * @param xNumber 0 or index for multiple hits.
 * @return Length plus QResult values.
 */
public int extract4Struct( QResult xyResult, String xKey, int xiKeyType, int xNumber ) {
	String key = xKey;
	if (key.startsWith( MARKER_DELETED )) {
		return 0;
	}
	int count = 0;
	int inx = traverseTries( xyResult, heads[ xiKeyType ], 0, 1, null );
	if (0 > inx) {
		return 0;
	}
	Trie trie = (Trie) xyResult.object0;
	xyResult.object0 = null;
	if (null == trie) {
		return 0;
	}
	// TODO
//	int offs = 0;
//	int out = 0;
//	yHandles[count++] = trie.getElement( extract4Struct_yIndex[0] );
//	for (trie = heads[ xiKeyType ]; offs <= key.length(); ++ offs) {
//		final char keyChar = (offs >= key.length()) ? 0 : key.charAt( offs );
//		int iRef = trie.search( keyChar, extract4Struct_ySearch );
//		Object next = extract4Struct_ySearch[ 0 ];
//		extract4Struct_ySearch[ 0 ] = null;
//		if (null == next) {
//			iRef = trie.searchNext( keyChar, extract4Struct_ySearch );
//			next = extract4Struct_ySearch[ 0 ];
//			extract4Struct_ySearch[ 0 ] = null;
//			if (null == next) {
//				break;
//			}
//		}
//		yHandles[count++] = trie.getElement( iRef );
//		yHandles[count++] = trie.getElement( -1 );
//		if (next instanceof int[]) {
//			..
//		}
//		trie = (Trie) next;
//	}
	return count;
}

//=====
}
