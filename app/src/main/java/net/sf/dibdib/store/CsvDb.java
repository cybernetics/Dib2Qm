// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.store;

import java.io.*;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import net.sf.dibdib.config.*;
import net.sf.dibdib.func.*;
import net.sf.dibdib.struc.*;

/**
 * ==> CSV with tabs (TSV, commas if tabs missing) as single line (trailing '\n' as '\t').
 * (for UI's worker thread or dedicated thread ...)
 */
public/*final*/class CsvDb {
//=====

public static CsvDb instance = new CsvDb();

protected/* static */long zLastSave = 0;
protected/* static */boolean zLoadSuccess = false;
protected/* static */boolean zStray = false;
/** To be saved as hex literals! (with or without leading "X'" */
protected/* static */ConcurrentHashMap< String, byte[] > zPrefs = null;
/** X/Y/Z ==> stack, T=Term, M=Memory */
protected/* static */ConcurrentHashMap< String, QVal > zVariables = null;
protected/* static */long[] zahContactsNGroups = new long[ 100 ];
protected/* static */int zcContactsNGroups = 0;
protected/* static */QVal[] zStack = null;
protected/* static */int zcStack = 0;

static {
//	zPrefs = new ConcurrentHashMap< String, byte[] >();
//	zVariables = new ConcurrentHashMap< String, QVal >();
//	zStack = new QVal[ 100 ];
//	zPrefs.put( "pub", "".getBytes( UtilString.STR16X ) );
//	zPrefs.put( "sec", "".getBytes( UtilString.STR16X ) );
}

public CsvDb() {
	zPrefs = new ConcurrentHashMap< String, byte[] >();
	zVariables = new ConcurrentHashMap< String, QVal >();
	zStack = new QVal[ 100 ];
	zPrefs.put( "pub", "".getBytes( UtilString.STR256 ) );
	zPrefs.put( "sec", "".getBytes( UtilString.STR256 ) );
}

public boolean isInitialized() {
	return 1000 < zLastSave;
}

public byte[] preference_get( String key ) {
	key = key.replace( '\t', ' ' ).replace( '\n', ' ' );
	if (".".equals( key )) {
		// Even if qRootAddress will later be used as value:
		return UtilString.bytesUtf8( QMap.main.readStrings( null, Mapping.qhRootOid )[ 0 ] );
	} else if ("email_address".equals( key )) {
		return UtilString.bytesUtf8( Mapping.qRootAddress );
	} else if ("lastId".equals( key )) {
		return UtilMisc.createId( key ).getBytes( UtilString.STR256 );
	}
	byte[] out = zPrefs.get( key );
	return (out == null) ? null : out.clone();
}

public String preference_getHex( String key, boolean marked ) {
	key = key.replace( '\t', ' ' ).replace( '\n', ' ' );
	if (".".equals( key ) || "email_address".equals( key ) || "lastId".equals( key )) {
		return UtilString.hex4Bytes( preference_get( key ), marked );
	}
	byte[] out = zPrefs.get( key );
	return (null == out) ? "" : UtilString.hex4Bytes( out, marked );
}

public synchronized void preference_set( String key, byte[] value, String oid4ForcingInitial ) {
	key = key.replace( '\t', ' ' ).replace( '\n', ' ' ).trim();
	if (null == value) {
		zPrefs.remove( key );
	} else if (".".equals( key ) || "email_address".equals( key )) {
		if (((null == Mapping.qRootAddress) || (null != oid4ForcingInitial)) && (6 <= value.length)) {
			Mapping.qRootAddress = UtilString.string4Utf8( value );
			// Expect Mapping objects to have 0 as contributor OID, so that this change
			// does not 'hurt':
			Mapping.qhRootOid = QMap.main.makeHandle( QResult.get8Pool(),
				(null != oid4ForcingInitial)
					? oid4ForcingInitial : UtilMisc.createId( UtilString.string4Utf8( value ) ) );
		}
		if (null != oid4ForcingInitial) {
			if (".".equals( key )) {
				Mapping.qhRootOid = QMap.main.makeHandle( QResult.get8Pool(), oid4ForcingInitial );
				if (!UtilString.string4Utf8( value ).equalsIgnoreCase( Mapping.qRootAddress )) {
					//TODO
				}
			} else if ("email_address".equals( key )) {
				// For new root OID or keep OID for new address!
				String email = UtilString.string4Utf8( value );
				Mapping.qRootAddress = email;
				///// Has to match main CONTACT
				Object dat0 = QMap.main.readStructElement( Mapping.qhRootOid, 0 );
				if ((null == dat0) || !(dat0 instanceof String) || !((String) dat0).contains( email )) { //&& (mpg instanceof Mapping)) {
					//TODO search CONTACTs for e-mail address, or add or replace
				}
			}
		}
	} else if ("lastId".equals( key )) {
		if (8 < value.length) {
			UtilMisc.initLastId( new String( value, UtilString.STR256 ) );
		}
	} else if (0 < key.length()) {
		zPrefs.put( key, value.clone() ); //UtilMisc.toHexLiteral( value, true ) );
	}
//	write();
}

public synchronized void preference_setHex( String key, String hexLiteral, String oid4ForcingInitial ) {
	byte[] val = null;
	if (null == hexLiteral) {
	} else if (0 >= hexLiteral.length()) {
		val = new byte[ 0 ];
	} else {
		char ch = hexLiteral.charAt( 0 );
		if (('X' == ch) && hexLiteral.startsWith( "X'" )) {
			val = UtilString.bytes4Hex( hexLiteral );
		} else if (((('0' <= ch) && (ch <= '9')) || (('A' <= ch) && (ch <= 'F'))) && hexLiteral.matches( "[0-9A-F ]+" )) {
			// Bad luck if it is meant to be a decimal number.
			val = UtilString.bytes4Hex( hexLiteral );
		} else {
			// Not hex after all.
			val = UtilString.bytesUtf8( hexLiteral );
		}
	}
	preference_set( key, val, oid4ForcingInitial );
}

public synchronized void preference_remove( String key ) {
	Dib2Config.log( "preference_remove", key );
	zPrefs.remove( key );
}

public QVal variable_get( String name ) {
	return zVariables.get( name );
}

/**
 * Set variable. Name is expected to have more than 1 char for user variables.
 * @param name Name of variable.
 * @param value null iff variable is to be removed.
 */
public synchronized void variable_set( String name, QVal value ) {
	name = UtilString.nameNormalize( name, 0xff ); // name.replace( '\t', ' ' ).replace( '\n', ' ' );
	///// Reserved?
	if (1 >= name.length()) {
		//e.g. '.' for ROOT
		//if (('X' <= name.charAt( 0 )) && ('Z' >= name.charAt( 0 ))) {
		return;
	} else if (('X' <= name.charAt( 0 )) && ('Z' >= name.charAt( 0 )) && (name.substring( 1 ).matches( "[0-9]+" ))) {
		return;
	}
	if (null == value) {
		zVariables.remove( name );
	} else {
		zVariables.put( name, value );
	}
}

public synchronized void variable_remove( String name ) {
	name = UtilString.nameNormalize( name, 0xff );
	zVariables.remove( name );
}

public void variable_force( String name, QVal value ) {
	name = UtilString.nameNormalize( name, 0xff );
	int iTo = -1;
	if (1 >= name.length()) {
		if (0 >= name.length()) {
			return;
		} else if (('X' <= name.charAt( 0 )) && ('Z' >= name.charAt( 0 ))) {
			iTo = name.charAt( 0 ) - 'X';
		} else if (name.equals( "." )) {
			return;
		}
	} else if (name.substring( 1 ).matches( "[0-9]+" )) {
		if (('X' <= name.charAt( 0 )) && ('Y' >= name.charAt( 0 ))) {
			return;
		} else if (('Z' == name.charAt( 0 ))) {
			iTo = Integer.parseInt( name.substring( 1 ) ) + 2;
			if (iTo >= (zStack.length - 1)) {
				return;
			}
		}
	}
	if (0 <= iTo) {
		int e0 = iTo + 1 - zcStack;
		if (0 < e0) {
			System.arraycopy( zStack, 0, zStack, e0, zcStack );
			for (int ix = e0 - 1; ix >= 0; -- ix) {
				zStack[ ix ] = QVal.EMPTY;
			}
			zcStack = iTo + 1;
		}
		zStack[ zcStack - 1 - iTo ] = value;
		return;
	}
	zVariables.put( name, value );
}

public int stackSize() {
	return zcStack;
}

public synchronized int stackPush( QVal value ) {
	if (zcStack >= zStack.length) {
		zStack = Arrays.copyOf( zStack, 2 * zStack.length );
	}
	zStack[ zcStack ++ ] = value;
	return zcStack - 1;
}

public int stackReplace( int pos, QVal value ) {
	if ((0 <= pos) && (pos < zcStack)) {
		zStack[ pos ] = value;
		return pos;
	}
	return -1;
}

public synchronized int stackRemove( int pos ) {
	if ((0 <= pos) && (pos < zcStack)) {
		-- zcStack;
		System.arraycopy( zStack, pos + 1, zStack, pos, zcStack - pos );
		return pos;
	}
	return -1;
}

public synchronized int stackInsert( int pos, QVal... values ) {
	final int len = values.length;
	if ((pos > zcStack) || (0 > pos)) {
		pos = zcStack;
	}
	zcStack += len;
	while (zcStack >= zStack.length) {
		zStack = Arrays.copyOf( zStack, 2 * zStack.length );
	}
	if ((pos + 1) < (zcStack - len)) {
		System.arraycopy( zStack, pos, zStack, pos + len, zcStack - len );
	}
	System.arraycopy( values, 0, zStack, pos, len );
	return pos;
}

public synchronized void stackClear( boolean clearMemVariables ) {
	zcStack = 0;
	if (clearMemVariables) {
		for (char iMemory = '0'; iMemory <= 'z'; ++ iMemory) {
			variable_remove( "M" + iMemory );
		}
	}
}

public QVal stackPeek() {
	final QVal[] stack = zStack;
	final int cnt = zcStack;
	return ((0 >= cnt) || (stack.length <= cnt)) ? null : stack[ cnt - 1 ];
}

public synchronized QVal[] stackPop( int cArgs, QCalc cmd ) {
	if (zcStack < cArgs) {
		return null;
	}
	if (null != cmd) {
		StringBuilder term = new StringBuilder();
		for (int i0 = zcStack - cArgs; i0 < zcStack; ++ i0) {
			term.append( zStack[ i0 ] ).append( " " );
		}
		term.append( cmd.getOperatorOrName() );
		variable_force( "T", new QVal( term.toString(), true ) );
	}
	QVal[] out = Arrays.copyOfRange( zStack, zcStack - cArgs, zcStack );
	zcStack -= cArgs;
	if ((100 <= zcStack) && (zStack.length / 2 > zcStack)) {
		zStack = Arrays.copyOf( zStack, zcStack + 10 );
	}
	return out;
}

public synchronized int stackCopy( QVal[] yData, int offset, boolean partial ) {
	if (!partial && (zcStack > (yData.length - offset))) {
		return -zcStack - offset;
	}
	int to = offset;
	for (int next = zcStack - 1; (next >= 0) && (to < yData.length); -- next, ++ to) {
		yData[ to ] = zStack[ next ];
	}
	if (to < yData.length) {
		yData[ to ] = null;
	}
	return to;
}

public synchronized int stackRead( long xbHexMemory, String[] yLines, int offset, boolean partial ) {
	if (!partial && (zcStack > (yLines.length - offset))) {
		return -zcStack - offset;
	}
	int to = offset;
	int ceil = zcStack + offset; //stackCopy( yLines, offset + 1, partial );
	for (int next = zcStack - 1; (next >= 0) && (to < yLines.length); -- next, ++ to) {
		if (((yLines.length - 1) == to) && ((ceil - 1) > to)) {
			yLines[ to ] = "...";
			continue;
		}
		int i0 = to - offset - 2;
		String key = (-2 == i0) ? "X" : ((-1 == i0 ? "Y" : ((0 == i0 ? "Z" : ("Z" + i0)))));
		if (null == zStack[ next ]) {
			yLines[ to ] = "";
		} else {
			yLines[ to ] = zStack[ next ].format( key, "\t", (0 != (1 & xbHexMemory)) );
		}
	}
	if (0 != (2 & xbHexMemory)) {
		if ((10 > zVariables.size()) && ((yLines.length - ceil) > zVariables.size())) {
			for (String var : zVariables.keySet()) {
				yLines[ to ++ ] = var + '\t' + zVariables.get( var );
			}
		} else {
			char iMemory = '0';
			for (; (iMemory <= 'z') && (to < yLines.length); ++ iMemory) {
				if (zVariables.containsKey( "M" + iMemory )) {
					yLines[ to ++ ] = "M" + iMemory + '\t' + zVariables.get( "M" + iMemory );
				}
			}
		}
	}
	if (to < yLines.length) {
		yLines[ to ] = null;
	}
	return to;
}

public synchronized/*h*/long add( Mapping.Cats cat, Mapping mpg ) {
	if (Mapping.Cats.PREF == cat) {
		String val = (0 >= mpg.dataElements.length) ? "" : mpg.dataElements[ 0 ];
		zPrefs.put( mpg.sLabel, UtilString.bytes4Hex( val ) );
		return QMap.NONE;
	}
	if (null == cat) {
		cat = Mapping.Cats.list4Flags( mpg.bCategories )[ 0 ];
	}
	if (0 == (cat.flag & mpg.bCategories)) {
		cat = null;
	}
	long handle = mpg.put2QStruct();
	if ((Mapping.Cats.CONTACT == cat) || (Mapping.Cats.GROUP == cat)) {
		if (zcContactsNGroups >= zahContactsNGroups.length) {
			zahContactsNGroups = Arrays.copyOf( zahContactsNGroups, 2 * zahContactsNGroups.length );
		}
		zahContactsNGroups[ zcContactsNGroups ++ ] = handle;
	}
	return handle;
}

public synchronized void remove( String xOid ) {
	final QResult pooled = QResult.get8Pool();
	long hOld = Mapping.handleByOid( pooled, xOid );
	final Object old = QMap.main.existsAsSpecialObject( hOld );
	if (!(old instanceof long[])) {
		return;
	}
	final long[] ahOld = (long[]) old;
	QMap.main.release( hOld, true );
//	QMap.main.removeSecondaryKey( hOld, xOid, Dib2Constants.MAPPING_KEY_TYPE_OID );
	if ((Mapping.Pack.CATS.ordinal() >= ahOld.length)
		|| !QMap.main.containedHandle( Mapping.Cats.CONTACT.handle, ahOld )
		|| !QMap.main.containedHandle( Mapping.Cats.GROUP.handle, ahOld )) {
		return;
	}
	int iOld = zcContactsNGroups - 1;
	for (; iOld >= 0; -- iOld) {
		if (zahContactsNGroups[ iOld ] == hOld) {
			break;
		}
	}
	if (0 <= iOld) {
		zahContactsNGroups[ iOld ] = zahContactsNGroups[ zcContactsNGroups - 1 ];
		-- zcContactsNGroups;
	}
}

public synchronized/*h*/long update( Mapping xMpg ) {
	final QResult pooled = QResult.get8Pool();
	if (0 != (Mapping.Cats.PREF.flag & xMpg.bCategories)) {
		String val = (0 >= xMpg.dataElements.length) ? "" : xMpg.dataElements[ 0 ];
		zPrefs.put( xMpg.sLabel, UtilString.bytes4Hex( val ) );
		return QMap.NONE;
	}
	long hOld = Mapping.handleByOid( pooled, xMpg.oid );
	Object old = QMap.main.existsAsSpecialObject( hOld );
	if (!(old instanceof long[])) {
		return QMap.NONE;
	}
	//TODO keep old version
	remove( xMpg.oid );
	int iOld = zcContactsNGroups - 1;
	for (; iOld >= 0; -- iOld) {
		if (zahContactsNGroups[ iOld ] == hOld) {
			break;
		}
	}
	if (0 == ((Mapping.Cats.CONTACT.flag | Mapping.Cats.GROUP.flag) & xMpg.bCategories)) {
		if (0 <= iOld) {
			zahContactsNGroups[ iOld ] = zahContactsNGroups[ zcContactsNGroups - 1 ];
			-- zcContactsNGroups;
		}
	} else if (0 > iOld) {
		if (zcContactsNGroups >= zahContactsNGroups.length) {
			zahContactsNGroups = Arrays.copyOf( zahContactsNGroups, 2 * zahContactsNGroups.length );
		}
		zahContactsNGroups[ zcContactsNGroups ++ ] = hOld;
	}
	return add( null, xMpg );
}

public synchronized void save( String path, boolean immediately ) {
	write( path, immediately, true, false );
}

public synchronized int write( String path, boolean immediately, boolean backupOld, boolean includeTrash ) {
	byte[] phrase = Codec.instance.getPassFull();
	if (null == phrase) {
		return -1;
	}
	if (!immediately && ((zLastSave + 60 * 1000) >= UtilMisc.currentTimeMillisLinearized())) {
		return -1;
	}
	Dib2Config.log( "save", " .. " + zPrefs.size() );
	File pathFTemp = new File( path + ".tmp" );
	if (pathFTemp.isFile()) {
		pathFTemp.delete();
	}
	zLastSave = UtilMisc.currentTimeMillisLinearized();
	byte[] dat = toCsv( null, 0, ~0, includeTrash ? (~0) : (~2) );
	Dib2Config.log( "exportLines", "ok " + dat.length );

	int len = Codec.instance.writePacked( dat, 0, dat.length, pathFTemp.getAbsolutePath() );
	Dib2Config.log( "save", "ok? " + len );
	if (0 <= len) {
		File pathFNew = new File( path );
		if (pathFNew.isFile()) {
			if (backupOld) {
				path = pathFNew.getAbsolutePath();
				File old = new File( path + (immediately ? ".old" : ".bak") );
				if (old.exists()) {
					if (!immediately || !zLoadSuccess) {
						old = new File( path + ".bak" );
					}
					old.delete();
				}
				pathFNew.renameTo( old );
			} else {
				pathFNew.delete();
			}
		}
		pathFTemp.renameTo( pathFNew );
	}
	zLastSave = UtilMisc.currentTimeMillisLinearized();
	return len;
}

public synchronized int load( String path ) {
	int out = -1;
	byte[] phrase = Codec.instance.getPassFull();
	if (null == path) {
		zLastSave = UtilMisc.currentTimeMillisLinearized();
		return (null == phrase) ? -1 : 0;
	}
	if (null == phrase) {
		return -1;
	}
	Dib2Config.log( "load", " .. " + zPrefs.size() );
	out = importFile( path, true );
	if (0 <= out) {
		zLastSave = UtilMisc.currentTimeMillisLinearized();
	}
	return out;
}

/**
 * Import encoded file.
 * @param filePath Path with name of file
 * @param phrase Pass phrase
 * @param replace true for overriding everything
 * @return number of imported records or -1
 */
public synchronized int importFile( String filePath, boolean replace ) {
//	phrase = (null == phrase) ? getPassFull() : phrase;
	byte[] header = new byte[ 8 ];
	byte[] dat = Codec.instance.readPacked( filePath, header, null, null );
	if (null == dat) {
		Dib2Config.log( "import", "read/decode failed." );
		return -1;
	}
//	int version = header[ UtilMisc.getPacketHeaderLen( header, 0 ) + Dib2Constants.MAGIC_BYTES.length ] & 0xff;
	int version = header[ 2 ] & 0xff;
	version = (6 >= version) ? (10 * version) : header[ 4 ];
	int count = 0;
	int flagsMarkAdjusttimeKeyhex = (replace ? 0 : 2) | ((30 >= version) ? 1 : 0);
	flagsMarkAdjusttimeKeyhex |= (Dib2Constants.FILE_STRUC_VERSION_MIN > version) ? 4 : 0;
	try {
		count = importCsv( dat, replace, flagsMarkAdjusttimeKeyhex ); // importLines( dat, replace, version );
	} catch (Exception e) {
		Dib2Config.log( "import", e.getMessage() );
		return -1;
	}
	Dib2Config.log( "import", "" + count );
	return count;
}

private Object importCsv( byte[] csvData, boolean directly, boolean replace, int flagsMarkAdjusttimeKeyHex ) {
	final QResult pooled = QResult.get8Pool();
	if (csvData.length <= 2) {
		return directly ? 0 : new Mapping[ 0 ];
	}
	int i1 = 1;
	int iOut = 0;
	int count = 0;
	int i0 = 0;
	int iOid = 0;
	boolean oidFound = false;
	boolean hasTimeStamp = true;
	Mapping[] out = new Mapping[ 24 ];
	if ((csvData[ 0 ] == Dib2Constants.MAGIC_BYTES[ 0 ]) && (csvData[ 1 ] == Dib2Constants.MAGIC_BYTES[ 1 ])) {
		// Skip header:
		i0 = 1 + UtilString.indexOf( csvData, new byte[] { '\n' } );
		iOid = (csvData[ 2 ] == '\t') ? -1 : 0;
	} else {
		iOid = -2;
		i0 = 1 + UtilString.indexOf( csvData, new byte[] { '\n' } );
		hasTimeStamp = new String( Arrays.copyOf( csvData, i0 ), UtilString.STR256 ).contains( "\tTIME\t" );
		if ((' ' >= csvData[ 0 ]) || hasTimeStamp) {
			// Skip header
		} else {
			i0 = 0;
		}
	}
	for (; i0 < csvData.length; i0 = i1 + 1) {
		i1 = UtilString.indexOf( csvData, new byte[] { '\n' }, i0 );
		if (i1 < 0) {
			i1 = csvData.length;
		}
		String line;
		try {
			line = new String( csvData, i0, i1 - i0, "UTF-8" );
		} catch (UnsupportedEncodingException e) {
			line = new String( csvData, i0, i1 - i0 );
		}
		if ((line.indexOf( "\t" ) < 0) && (line.indexOf( "," ) > 0)) {
			line = line.replaceAll( "\"? *, *\"?", "\t" );
		}
		String[] a0 = line.split( "\t" ); //, 6 + iOid );
		if ((5 + iOid) > a0.length) {
			continue;
		}
		try {
			if (iOut >= out.length) {
				out = Arrays.copyOf( out, 2 * iOut );
			}
			final Mapping mpg = (-2 < iOid) ? new Mapping( a0, iOid, flagsMarkAdjusttimeKeyHex )
				: Mapping.make( a0[ 0 ], a0[ 1 ], (hasTimeStamp ? a0[ 2 ] : ""), (hasTimeStamp ? 3 : 2), a0 );
			out[ iOut ] = mpg;
			if (directly) {
				if (0 != ((Mapping.Cats.PREF.flag | Mapping.Cats.VAR.flag) & mpg.bCategories)) {
					///// Pairs that are stored separately (PREF, VAR, ...).
					final String key = mpg.sLabel;
					if (0 >= key.length()) {
						continue;
					}
					final String value = (0 >= mpg.dataElements.length) ? "" : mpg.dataElements[ 0 ];
					///// Do not override current entries when importing older data.
					if ((0 != (Mapping.Cats.PREF.flag & mpg.bCategories)) && (1 <= key.length())) {
						///// 'value' as hexstring!
						if (replace || !zPrefs.containsKey( key ) || (null == zPrefs.get( key ))
							|| (0 >= zPrefs.get( key ).length)) {
							if (0 != (flagsMarkAdjusttimeKeyHex & 4) && key.startsWith( "KEY" )
								&& (value.length() > 3 * "CAAU".length()) && value.matches( "3.3.3.3.*" )) {
								preference_setHex( key, UtilString.string4HexUtf8( value ), null );
							} else {
								preference_setHex( key, value, replace ? mpg.oid : null );
							}
						}
					} else {
						if (replace || !zVariables.containsKey( key ) || (null == zVariables.get( key ))
							|| (0 >= zVariables.get( key ).toString().length())) {
							if (key.equals( "." )) {
								if (replace) {
									// Wrong placement ...
									Mapping.qRootAddress = (4 <= value.length()) ? value : Mapping.qRootAddress;
									if (4 <= value.length()) {
										Mapping.qRootAddress = (value.startsWith( "X" ) || ('7' >= value.charAt( 0 )))
											? UtilString.string4HexUtf8( value ) : value;
									}
									Mapping.qhRootOid = QMap.main.makeHandle( pooled, mpg.oid );
								}
							} else {
								variable_force( key, new QVal( value, false ) );
							}
						}
					}
				} else {
					///// Note: Not duplicating values if they are used as PREF or VAR.
					if (0 != (Mapping.Cats.CONTACT.flag & mpg.bCategories)) {
						// Expected as first part:
						String email = (0 >= mpg.dataElements.length) ? "" : mpg.dataElements[ 0 ];
						email = email.substring( 1 + email.indexOf( ' ' ) ).trim();
						if (3 <= email.length() && (0 < email.indexOf( '@' ))) {
							if (email.equalsIgnoreCase( Mapping.qRootAddress )) {
								if (replace || !oidFound) {
									Mapping.qhRootOid = QMap.main.makeHandle( pooled, mpg.oid );
									oidFound = true;
								}
							} else if (null == Mapping.qRootAddress) {
								Mapping.qRootAddress = email;
							}
						}
					}
					mpg.put2QStruct();
				}
			} else {
				++ iOut;
			}
			++ count;
		} catch (Exception e) {
			Dib2Config.log( "Csv ", "Import failed/ " + count + ": " + e );
			return directly ? 0 : null;
		}
	}
	return directly ? (Integer) count : Arrays.copyOf( out, iOut );
}

public synchronized Mapping[] fromCsv( byte[] csvData, int flagsMarkShift4Time ) {
	return (Mapping[]) importCsv( csvData, false, false, flagsMarkShift4Time );
}

public synchronized int importCsv( byte[] csvData, boolean replace, int flagsMarkShift4Time ) {
	return (Integer) importCsv( csvData, true, replace, flagsMarkShift4Time );
}

/** Create encoded list of mappings (header: "dm(TTT)N.N" (time TTT, version N.N).)
 * -- "dm(..)N.N" starts plain-text CSV with header row (version N.N).
 * -- "dm^Cxxx" precedes encoded container as salt value.
 * @param xzMap
 * @param cMap
 * @param catFilter
 * @param bFlagsInclude_prefs_trash_stack
 * @return
 */
public synchronized byte[] toCsv( Mapping[] xzMap, int cMap, long catFilter, long bFlagsInclude_prefs_trash_stack ) {
	final QResult pooled = QResult.get8Pool();
	catFilter = (0 == catFilter) ? ~0 : catFilter;
	if (null == xzMap) {
		xzMap = new Mapping[ zPrefs.size() + 1000 ];
		cMap = 0;
		if (0 == (catFilter & Mapping.Cats.PREF.flag)) {
		} else if (null == Mapping.qRootAddress) {
			xzMap[ cMap ++ ] = new Mapping( ".", -1, QMap.main.readStrings( null, Mapping.qhRootOid )[ 0 ], //,
				Mapping.Cats.PREF.ahAsList, 0, "00" );
		} else {
			xzMap[ cMap ++ ] = new Mapping( ".", -1, QMap.main.readStrings( null, Mapping.qhRootOid )[ 0 ], //,
				Mapping.Cats.PREF.ahAsList, 0, UtilString.hexUtf8( Mapping.qRootAddress, true ) );
			// For backwards compatibility:
			xzMap[ cMap ++ ] = new Mapping( "email_address", -1, null, Mapping.Cats.PREF.ahAsList, 0, //,
				UtilString.hexUtf8( Mapping.qRootAddress, true ) );
			final byte[] lastId = preference_get( "lastId" );
			if ((null != lastId) && (8 < lastId.length)) {
				xzMap[ cMap ++ ] = new Mapping( "lastId", -1, null, Mapping.Cats.PREF.ahAsList, 0,
					UtilString.hex4Bytes( lastId, true ) );
			}
		}
	}
	if (0 != (1 & bFlagsInclude_prefs_trash_stack)) {
		for (String key : zPrefs.keySet()) {
			xzMap = (cMap >= xzMap.length) ? Arrays.copyOf( xzMap, 2 * xzMap.length ) : xzMap;
			String val = UtilString.hex4Bytes( zPrefs.get( key ), true );
			if (null == val) {
				continue;
			}
			xzMap[ cMap ++ ] = new Mapping( key, -1, null, Mapping.Cats.PREF.ahAsList, 0, val );
		}
	}
	if (0 != (4 & bFlagsInclude_prefs_trash_stack)) {
		for (int iElement = 0; iElement < zcStack; ++ iElement) {
			xzMap = (cMap >= xzMap.length) ? Arrays.copyOf( xzMap, 2 * xzMap.length ) : xzMap;
			String val = zStack[ iElement ].toString();
			int inx = zcStack - iElement - 3;
			String key = (-2 == inx) ? "X" : ((-1 == inx ? "Y" : ((0 == inx ? "Z" : ("Z" + inx)))));
			if ((null == val) || (0 >= val.length())) {
				continue;
			}
			xzMap[ cMap ++ ] = new Mapping( key, -1, null, Mapping.Cats.VAR.ahAsList, 0, val );
		}
	}
	if (0 != (catFilter & Mapping.Cats.VAR.flag)) {
		for (String key : zVariables.keySet()) {
			xzMap = (cMap >= xzMap.length) ? Arrays.copyOf( xzMap, 2 * xzMap.length ) : xzMap;
			String val = zVariables.get( key ).toString();
			if (null == val) {
				continue;
			}
			xzMap[ cMap ++ ] = new Mapping( key, -1, null, Mapping.Cats.VAR.ahAsList, 0, val );
		}
	}
	String header = new String( Dib2Constants.MAGIC_BYTES, UtilString.STR256 );
	// old: header += "\t" + UtilMisc.toDate4Millis() + ... + '\n';
	header += "(" + UtilMisc.dateShort4Millis() + ')' + Dib2Constants.FILE_STRUC_VERSION_STR
		+ Dib2Config.moduleShort + Mapping.fieldNames + '\n';
	StringBuilder out = new StringBuilder( 100 * xzMap.length );
	out.append( header );
	for (Mapping entry : xzMap) {
		if (0 >= cMap) {
			break;
		}
		-- cMap;
		out.append( entry.toCsvLine( null ) );
		out.append( '\n' );
	}
	long handle = 0;
	long[] ahEntries;
	while (null != (ahEntries = QMap.main.dumpStruct( pooled, handle, Mapping.TAG, 4096 ))) {
		for (long hx : ahEntries) {
			handle = hx;
			String oid = QMap.main.readKey( hx, Dib2Constants.MAPPING_KEY_TYPE_OID );
			long[] ahMpg = QMap.main.readStruct( hx );
			if ((null == oid) || (null == ahMpg) || (Mapping.Pack.Q_COUNT.ordinal() >= ahMpg.length)) {
				continue;
			}
			Mapping mpg = new Mapping( oid, ahMpg );
			if (((bFlagsInclude_prefs_trash_stack & 2) != 0) || (1 < mpg.timeStamp)) {
				if ((mpg.bCategories & catFilter) != 0L) {
					out.append( mpg.toCsvLine( null ) );
					out.append( '\n' );
				}
			}
		}
		if (4096 > ahEntries.length) {
			break;
		}
	}
	return out.toString().getBytes( UtilString.STRX16U8 );
}

public synchronized Mapping[] toList( long catFilter, int maxSize ) {
	final QResult pooled = QResult.get8Pool();
	Mapping[] xzMap = new Mapping[ maxSize ];
	int count = 0;
	catFilter = (0 == catFilter) ? ~0 : catFilter;
	int size = (4096 > maxSize) ? maxSize : 4096;
	long handle = 0;
	long[] ahEntries;
	while (null != (ahEntries = QMap.main.dumpStruct( pooled, handle, Mapping.TAG, size ))) {
		for (long hx : ahEntries) {
			if (count >= maxSize) {
				break;
			}
			handle = hx;
			String oid = QMap.main.readKey( hx, Dib2Constants.MAPPING_KEY_TYPE_OID );
			long[] ahMpg = QMap.main.readStruct( hx );
			if ((null == oid) || (null == ahMpg) || (Mapping.Pack.Q_COUNT.ordinal() >= ahMpg.length)) {
				continue;
			}
			Mapping mpg = new Mapping( oid, ahMpg );
			if (0 != (mpg.bCategories & catFilter)) {
				xzMap[ count ++ ] = mpg;
			}
		}
		if ((size > ahEntries.length) || (count <= maxSize)) {
			break;
		}
	}
	return Arrays.copyOf( xzMap, count );
}

//=====
}
