// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.config;

import java.util.Locale;
import localhost.dibdib.util.CsvCodecAes;
import net.sf.dibdib.func.*;
import net.sf.dibdib.join.*;
import net.sf.dibdib.store.Codec;

public class Dib2Config implements Dib2Constants {
//=====

///// Fixed values:

public static final long TIME_MIN_2017_01_01 = 1483228800000L;
public static final long TIME_MAX = 2 * TIME_MIN_2017_01_01;

/** First one is default for encoding. */
public static final CsvCodecIf[] csvCodecs = new CsvCodecIf[] {
	CsvCodecAes.instance,
	CsvCodec1.instance,
	CsvCodec0.instance,
};

public static final Locale locale = null; // null for multilingual texts with CLDR (UCA DUCET) sorting

///// To be initialized:

public static char platformMarker = 0;
public static String moduleShort = "";
///** Use this cautiously: potential threading issues. */
//public static ContextIf qMainContext = null;
public static PlatformIf platform;

/** Referenced value (to be set in Main): true if processor does fast multiplication, compared to shifting and adding. */
public static boolean[] useFastMultiplication = new boolean[] { false };

/** Option: use linked list instead of array for TfPortBuffer etc. */
public static boolean useLinkedList = false;

public static String dbFileName = null;

/** General initialization here, specific values at caller. */
public static void init( char xPlatformMarker, String xModuleShort, String xDbFileName, PlatformIf xPlatform ) {
	moduleShort = xModuleShort;
	dbFileName = xDbFileName; //qMainContext.init( xPlatform );
	platform = xPlatform;
	if (0 != platformMarker) {
		return;
	}
	platformMarker = xPlatformMarker;
	switch (platformMarker) {
		case '0': {
			// Assuming i686 or amd64 or similar processor:
			useFastMultiplication = new boolean[] { true };
			break;
		}
		default:
			;
	}
	UtilMisc.timeZoneDone = false;
	Codec.instance.init( platformMarker );
}

/** Enable early logging as much as possible */
public static void log( String... aMsg ) {
	UtilMisc.keepLog( aMsg[ 0 ], aMsg[ 1 ] );
	if (null != platform) {
		platform.log( aMsg );
	}
}

//=====
}
