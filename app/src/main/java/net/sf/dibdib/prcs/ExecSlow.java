// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.prcs;

import net.sf.dibdib.func.QCalc;
import net.sf.dibdib.struc.*;

/** Long-running process. */
public final class ExecSlow implements QPlacesTransitionsIf { //QThreadUserIf {
//=====

///// Threaded (in/ out/ trigger)

public final QPlace wxCommands = new QPlace();

/////

private CommandBaton mIn;
private CommandBaton mOut = null;
private double[] mSlowIntermediate;
private long[] mSlowParameters;

public boolean takeCommand( CommandBaton cmd ) {
	return 0 <= wxCommands.push( cmd );
}

@Override
public boolean take( QBaton main ) {
	return takeCommand( (CommandBaton) main );
}

private boolean start() {
	mOut = null;
	CommandBaton out = null;
	try {
		boolean delay = true; //(0 >= msecLimit);
		///// For the fun of it ...
		double factArg = ((QCalc.FACTORIAL == mIn.operator) && (null != mIn.vals) //,
		&& (mIn.vals[ 0 ].isNumeric())) ? mIn.vals[ 0 ].toDouble() : 0.0;
		delay = delay || ((100.0 < factArg) && (factArg < 170.0)); // && (200 > msecLimit));
		if (delay) {
			mSlowParameters = new long[ 2 ];
			mSlowIntermediate = new double[ 1 ];
			mSlowParameters[ 0 ] = 1;
			mSlowParameters[ 1 ] = (long) factArg;
			mSlowParameters[ 1 ] = (170 < mSlowParameters[ 1 ]) ? 170 : mSlowParameters[ 1 ];
			mSlowIntermediate[ 0 ] = 1.0;
			return true;
		} else {
			final QVal[] result = mIn.operator.calc( mIn.vals );
			out = new CommandBaton();
			out.vals = result;
		}
	} catch (Exception e) {
		return false;
	}

	mOut = out;
	return true;
}

@Override
public QBaton step() {
	CommandBaton out = mOut;
	mOut = null;
	if (null == out) {
		if (null == mIn) {
			mIn = (CommandBaton) wxCommands.pull();
			start();
			if (null == mIn) {
				return mOut;
			}
		}
		if (QCalc.FACTORIAL == mIn.operator) {
			// for testing
			try {
				Thread.sleep( 200 );
			} catch (InterruptedException e) {
			}
			for (long to = mSlowParameters[ 0 ] + 10; (mSlowParameters[ 0 ] < to) && (mSlowParameters[ 0 ] <= mSlowParameters[ 1 ]); ++ mSlowParameters[ 0 ]) {
				mSlowIntermediate[ 0 ] *= mSlowParameters[ 0 ];
			}
			if (mSlowParameters[ 0 ] < mSlowParameters[ 1 ]) {
				return QBaton.PENDING;
			}
			out = new CommandBaton();
			out.vals = new QVal[] { new QVal( null, mSlowIntermediate[ 0 ] ) };
		}
	}
	// TODO push
	return out;
}

//=====
}
