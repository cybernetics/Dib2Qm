// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.prcs;

import net.sf.dibdib.struc.QBaton;

/** Common interface for QDispatcher (single Place with typed Transitions)
 * and QProcess (Places with single Transition).
 */
public interface QPlacesTransitionsIf {
//=====

/** Drop (main) QBaton (data token) in incoming Place and/ or do some initial processing.
 * Runs on Baton (token) provider's thread. Needs to be complemented by more methods
 * for each provider -- or synchronized if used by multiple providers.
 * @param main For Baton from main provider (preceding transition). 
 * @return true iff successful.
 */
public boolean take( QBaton main );

/** Perform small processing step. If processing is finished, drop outgoing QBatons in
 * receiver's Place(s) and return main one.
 * @return (main) QBaton if finished, PENDING if still processing,
 * null if terminated/ nothing changed.
 */
public QBaton step();

//=====
}
