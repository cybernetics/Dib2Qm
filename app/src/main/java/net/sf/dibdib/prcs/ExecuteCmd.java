// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.prcs;

import java.io.File;
import java.util.Arrays;
import net.sf.dibdib.config.*;
import net.sf.dibdib.disp.ExecuteCmd2SlowSink;
import net.sf.dibdib.func.*;
import net.sf.dibdib.store.*;
import net.sf.dibdib.struc.*;

public final class ExecuteCmd implements QPlacesTransitionsIf {
//=====

///// Threaded (in/ out/ trigger)

public final QPlace wxCommands = new QPlace();

/////

private ExecuteCmd2SlowSink mDispResult;

public void init( ExecuteCmd2SlowSink dispatcher ) {
	mDispResult = dispatcher;
}

private QVal fastExec( CommandBaton cmd ) {
	QVal result = null;
	try {
		final QCalc op = cmd.operator;
		final QVal[] args = cmd.vals;
		String str;
		switch (op) {
			case MC:
				str = args[ 0 ].toString();
				if ((null != args) && (null != str) && (0 < str.length())) {
					if (1 == str.length()) {
						str = "M" + str.charAt( 0 );
					}
					CsvDb.instance.variable_set( str, null );
				}
				break;
			case CLRALL:
				CsvDb.instance.stackClear( true );
				break;
			case CLR1:
			case CLEAR:
				//CsvDb.instance.stackPop( 0 );
				break;
			case CLR2:
				CsvDb.instance.stackPop( 1, null );
				break;
			case CLR3:
				CsvDb.instance.stackPop( 2, null );
				break;
			case DATE:
				long date = UtilMisc.leapMinute256ForDate( UtilMisc.date4Millis( true ) );
				result = new QVal( date );
				break;
			case NOP:
				if ((null != cmd.uiParameter) && (0 < cmd.uiParameter.length())) {
					result = new QVal( cmd.uiParameter, false );
				}
				break;
			case TICK:
				result = new QVal( null, UtilMisc.currentTimeMillisLinearized() );
				break;
			default:
				cmd = null;
				return null;
		}
	} catch (Exception e) {
		result = null;
	}
	return result;
}

private QVal MOVE_ME_TO_SLOW__Exec( CommandBaton cmd ) {
	QVal result = null;
	try {
		final QResult pooled = QResult.get8Pool();
		final QCalc funct = cmd.operator;
		final QVal[] args = cmd.vals;
		String str;
		byte[] dat;
		boolean ok;
		switch (funct) {
			case FDECR:
				ok = false;
				if ((null != args) && (0 < args[ 0 ].toString().length())) {
					try {
						String file = args[ 0 ].toString();
						if (0 > file.indexOf( '/' )) {
							file = new File( Dib2Config.platform.getFilesDir( "external" ), file ).getAbsolutePath();
						}
						dat = Codec.instance.readPacked( file + ".dib", null, null, null );
						UtilMisc.writeFile( file, dat, 0, dat.length, null );
					} catch (Exception e) {
					}
				}
				result = ok ? QVal.TRUE : QVal.FALSE;
				break;
			case FENCR:
				ok = false;
				if ((null != args) && (0 < args[ 0 ].toString().length())) {
					try {
						String file = args[ 0 ].toString();
						if (0 > file.indexOf( '/' )) {
							file = new File( Dib2Config.platform.getFilesDir( "external" ), file ).getAbsolutePath();
						}
						dat = UtilMisc.readFile( file, 0 );
						ok = (0 < Codec.instance.writePacked( dat, 0, dat.length, file + ".dib" ));
					} catch (Exception e) {
					}
				}
				result = ok ? QVal.TRUE : QVal.FALSE;
				break;
			case EXPALL:
			case EXPORT:
				ok = false;
				if ((null != args) && (0 < args[ 0 ].toString().length())) {
					// Do not expose key values etc. unless really requested
					dat = CsvDb.instance.toCsv( null, 0, ~0, (funct == QCalc.EXPALL) ? (~0) : 4 );
					try {
						String file = args[ 0 ].toString();
						if (0 > file.indexOf( '/' )) {
							file = new File( Dib2Config.platform.getFilesDir( "external" ), file ).getAbsolutePath();
						}
						UtilMisc.writeFile( file, dat, 0, dat.length, null );
						ok = true;
					} catch (Exception e) {
					}
				}
				result = ok ? QVal.TRUE : QVal.FALSE;
				break;
			case IMPORT:
				ok = false;
				if ((null != args) && (0 < args[ 0 ].toString().length())) {
					try {
						String file = args[ 0 ].toString();
						if (0 > file.indexOf( '/' )) {
							file = new File( Dib2Config.platform.getFilesDir( "external" ), file ).getAbsolutePath();
						}
						dat = UtilMisc.readFile( file, 0 );
						if (Dib2Constants.MAGIC_BYTES.length < dat.length) {
							if ((byte) Dib2Constants.RFC4880_EXP2 == dat[ 0 ]) {
								ok = 0 < CsvDb.instance.importFile( file, false );
							} else if (Arrays.equals( Dib2Constants.MAGIC_BYTES, Arrays.copyOf( dat, Dib2Constants.MAGIC_BYTES.length ) )) {
								ok = 0 < CsvDb.instance.importCsv( dat, false, 0 );
							} else {
								///// Expecting simple entries or a header line.
								boolean found = false;
								for (int i0 = 0; (i0 < 100) && (i0 < dat.length); ++ i0) {
									if ('\t' == dat[ i0 ]) {
										found = true;
										break;
									}
								}
								for (int i0 = 0; (i0 < 1000) && (i0 < dat.length); ++ i0) {
									if ('\n' == dat[ i0 ]) {
										found = found && true;
										break;
									}
								}
								if (found) {
									ok = 0 < CsvDb.instance.importCsv( dat, false, 0 );
								}
							}
						}
					} catch (Exception e) {
					}
				}
				result = ok ? QVal.TRUE : QVal.FALSE;
				break;
			case LOAD:
				str = args[ 0 ].toString();
				if (1 == str.length()) {
					final char ch = str.charAt( 0 );
					switch (ch) {
						case '!':
							str = "T";
							break;
						default:
							str = "M" + ch;
					}
				}
				result = CsvDb.instance.variable_get( str );
				result = (null == args[ 0 ]) ? QVal.NaN : result;
				break;
			case QCAT: {
				Mapping mpg = Mapping.search4Oid( args[ 0 ].toString() );
				if (null == mpg) {
					result = QVal.NaN;
				} else {
					CsvDb.instance.update( new Mapping( mpg.sLabel,
						-1, mpg.oid, Mapping.Cats.toCategories( pooled, args[ 1 ].toString() ), 0, mpg.dataElements ) );
					result = args[ 0 ];
				}
			}
				break;
			case QDEL: {
				long handle = Mapping.handleByOid( pooled, args[ 0 ].toString() );
				Object ah = QMap.main.existsAsSpecialObject( handle );
				if (!(ah instanceof long[])) {
					result = QVal.FALSE;
				} else if (QMap.isVoid( handle )) {
					result = QVal.FALSE;
				} else {
					CsvDb.instance.remove( args[ 0 ].toString() );
					result = QVal.TRUE;
				}
			}
				break;
			case QLOAD: {
				str = null;
				Mapping mpg = Mapping.search4Oid( args[ 0 ].toString() );
				if (null != mpg) {
					str = mpg.toTextLine( 1024 ).replace( "\t", " // " );
					if (1020 <= str.length()) {
						str = str.substring( 0, 1020 ) + "...";
					}
				}
				result = (null != str) ? new QVal( str, false ) : QVal.NaN;
			}
				break;
			case QOID: {
				str = null;
				Mapping mpg = Mapping.search4Label( args[ 0 ].toString(), 0 );
				if (null != mpg) {
					str = mpg.oid;
				}
				result = (null != str) ? new QVal( str, true ) : QVal.NaN;
			}
				break;
			case QNEXT: {
				str = null;
				Mapping mpg = Mapping.searchNextLabel4Oid( args[ 0 ].toString() );
				if (null == mpg) {
					str = null;
				} else {
					str = mpg.oid;
				}
				result = (null != str) ? new QVal( str, true ) : QVal.NaN;
			}
				break;
			case QQL: {
				str = null;
				long cats = UiSwitches.qSwitches[ UiSwitches.SWI_MAPPING_CATS ] & ((1L << 32) - 1L);
				Mapping mpg = Mapping.search4Label( cats, args[ 0 ].toString() );
				if (null == mpg) {
					str = null;
				} else {
					str = mpg.toTextLine( 1024 ).replace( "\t", " // " );
					if (1020 <= str.length()) {
						str = str.substring( 0, 1020 ) + "...";
					}
				}
				result = (null != str) ? new QVal( str, false ) : QVal.NaN;
			}
				break;
			case QS: {
				long[] cats = Mapping.Cats.handles4Flags( UiSwitches.qSwitches[ UiSwitches.SWI_MAPPING_CATS ] );
				cats = (0 == cats.length) ? new long[] { Mapping.Cats.DEFAULT.handle } : cats;
				new Mapping( args[ 1 ].toString(), -1, null, cats, 0, args[ 0 ].toString() )
					.put2QStruct();
				result = (null != args) ? args[ 1 ] : QVal.NaN;
			}
				break;
			case QSTORE: {
				final long[] cats = Mapping.Cats.toCategories( pooled, args[ 1 ].toString() );
				new Mapping( args[ 2 ].toString(), -1, null, cats, 0, args[ 0 ].toString() )
					.put2QStruct();
				result = (null != args) ? args[ 2 ] : QVal.NaN;
			}
				break;
			case QUP: {
				Mapping mpg = Mapping.search4Oid( args[ 0 ].toString() );
				if (null == mpg) {
					result = QVal.NaN;
				} else {
					CsvDb.instance.update( new Mapping( mpg.sLabel,
						-1, mpg.oid, QMap.main.makeHandles( pooled, mpg.asCategories ), 0, args[ 1 ].toString() ) );
					result = args[ 0 ];
				}
			}
				break;
			case SAVTO:
				ok = false;
				if ((null != args) && (0 < args[ 0 ].toString().length())) {
					String file = args[ 0 ].toString();
					if (0 > file.indexOf( '/' )) {
						file = new File( Dib2Config.platform.getFilesDir( "external" ), file ).getAbsolutePath();
					}
					ok = 0 < CsvDb.instance.write( file, true, false, true );
				}
				result = ok ? QVal.TRUE : QVal.FALSE;
				break;
			case STORE:
				str = args[ 1 ].toString();
				result = QVal.NaN;
				if ((null != args) && (null != str) && (0 < str.length())) {
					if (1 == str.length()) {
						str = "M" + str.charAt( 0 );
					}
					CsvDb.instance.variable_set( str, args[ 0 ] );
					result = args[ 0 ];
				}
				break;
			default:
				return null;
		}
	} catch (Exception e) {
		result = null;
	}
//	cmd = null;
	return result;
}

public boolean takeCommand( CommandBaton cmd ) {
	return 0 <= wxCommands.push( cmd );
}

@Override
public boolean take( QBaton main ) {
	return takeCommand( (CommandBaton) main );
}

//TODO Make use of PENDING/ ExecSlow
@Override
public QBaton step() {
	CommandBaton cmd = (CommandBaton) wxCommands.pull();
	if (null == cmd) {
		return null;
	}

//	cmd.operator = QCalc.getOperator( cmd.uiCommand );
	if (null == cmd.operator) {
		return QBaton.DUMMY;
	}
	int cArgs = cmd.operator.cArgs;
	if (0 > cArgs) {
		CsvDb.instance.stackPush( new QVal( "(not implemented yet)", true ) );
		return QBaton.EMPTY;
	}
	cmd.vals = CsvDb.instance.stackPop( cArgs, cmd.operator );
	if (null == cmd.vals) {
		return QBaton.DUMMY;
	}

	QVal result = fastExec( cmd );

	// TODO move it
	if (null == result) {
		result = MOVE_ME_TO_SLOW__Exec( cmd );
	}

	if (null != result) {
		CsvDb.instance.stackPush( result );
		mDispResult.take( QBaton.EMPTY );
		return QBaton.EMPTY;
	} else if (null != cmd.vals) {
		final QVal[] vals = cmd.operator.calc( cmd.vals );
		if (null != vals) {
			CsvDb.instance.stackInsert( -1, vals );
			return QBaton.EMPTY;
		}
	}
	return QBaton.DUMMY;
}

//=====
}
