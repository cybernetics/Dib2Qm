// Copyright (C) 2016,2017,2018  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.vreeken.quickmsg;

// import net.vreeken.quickmsg.*;

import com.gitlab.dibdib.dib2qm.*;

import localhost.dibdib.chat.*;

import net.sf.dibdib.config.Dib2Config;

public class preferences extends quickmsg_db {
//=====

public preferences( Object context ) {
	super( context );
//	if (null == Dib2Config.qContextQm) {
	Dib2Config.init( 'A', "qm", qDbFileName, this );
	Pgp.privProvs = (null == Pgp.privProvs) //,
	? new PrivProvIf[] { new EcDhQm() /*net.vreeken.quickmsg.pgp*/} //,
		: Pgp.privProvs;
}

//=====
}
