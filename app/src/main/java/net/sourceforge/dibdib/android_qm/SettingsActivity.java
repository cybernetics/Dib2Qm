// Copyright (C) 2018  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android_qm;

import com.gitlab.dibdib.dib2qm.quickmsg_db;

public final class SettingsActivity extends net.vreeken.quickmsg.SettingsActivity_0 {
//=====

@Override
public void onPause() {
	quickmsg_db db = new quickmsg_db( this );
	db.save();
	super.onPause();
}

//=====
}
