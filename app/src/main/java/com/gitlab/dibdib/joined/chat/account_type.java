// Copyright (C) 2016 Roland Horsch and others:
// -- Changes:  Copyright (C) 2016,2017,2019  Roland Horsch <gx work s{at}g mail.c om>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// All under GNU license:
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.joined.chat;

import net.sf.dibdib.join.ContextIf_OLD;

import net.sf.dibdib.func.UtilString;


public class account_type {
//=====

String _name;
String _username_help;
String _username_remove; /* what should be removed from address to get username*/
String _imap_server;
String _smtp_server;
String _imap_port;
String _smtp_port;

public account_type( String name, String username_help, String username_remove, String imap_server, String imap_port, String smtp_server,
	String smtp_port )
{
	_name = name;
	_username_help = username_help;
	_username_remove = username_remove;
	_imap_server = imap_server;
	_imap_port = imap_port;
	_smtp_server = smtp_server;
	_smtp_port = smtp_port;
}

public String name_get()
{
	return _name;
}

private static void set( ContextIf_OLD preferences, String key, String value ) {
	preferences.set( key, UtilString.bytesUtf8( value ) );
}

public void preferences_set( ContextIf_OLD preferences, String name, String address, String pass )
{
	String username;

	if (_username_remove != null) {
		username = address.replace( _username_remove, "" );
	} else
		username = address;

	set( preferences, "display_name", name );
	set( preferences, "email_address", address );
	set( preferences, "imap_user", username );
	set( preferences, "smtp_user", username );
	set( preferences, "imap_pass", pass );
	set( preferences, "smtp_pass", pass );

	set( preferences, "imap_server", _imap_server );
	set( preferences, "imap_port", _imap_port );
	set( preferences, "smtp_server", _smtp_server );
	set( preferences, "smtp_port", _smtp_port );
//	preferences.db2pref();
}

//=====
}
