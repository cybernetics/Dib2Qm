// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// This part is new code for working with QuickMSG, based on the
// corresponding API from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.dib2qm;

import net.vreeken.quickmsg.attachment;

public interface PrivProvIf {
//=====

public void init( net.sf.dibdib.join.ContextIf_OLD app_context );

public attachment pgpmime_id();

public attachment encrypt_sign( attachment unenc, String to );

public attachment key_attachment( String memberAddr );

public boolean load_keys();

/**
 * @param xInputStream
/** @param xyKeyFound
/** @return Fallback key value, null if error or if value has not changed
 */
public byte[] public_keyring_add_key( byte[] xInputStream, String[] xyKeyFound );

public attachment decrypt_verify( attachment attachment );

public String fingerprint( String addr );

public void public_keyring_remove_by_address( String addr );

//=====
}
