// Copyright (C) 2016,2017,2018  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.dib2qm;

import android.content.Context;
import android.util.Log;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.IMAPProtocol;
import java.io.File;
import java.util.List;
import javax.mail.Store;
import net.sf.dibdib.config.Dib2Config;
import net.sourceforge.dibdib.android_qm.background;
import net.vreeken.quickmsg.attachment;

public class mail extends net.vreeken.quickmsg.mail_0 {
//=====

//DIFF (_0):
//public static boolean imap_connected = false;
//public static ...
//public List<attachment> multipart_get_attachments(attachment mpa)
//intent.setAction("net.sf.dibdib.android.update_ui");
//Keep track of background.unread.

public int sent = -1;
public static volatile boolean toFlush = false;

@Override
public Store open_imap( Context context )
{
	try {
		return super.open_imap( context );
	} catch (Exception e2) {
		Log.e( "open_imap", "connect imap: " + e2.getMessage() );
	}
	return null;
}

@Override
public String send( Context context, String to, List< attachment > attachments,
	String subtype ) {
	boolean local = to.indexOf( '@' ) < 0;
	if (local) {
		return null;
	}
	return send( true, context, to, attachments, subtype );
}

@Override
public String send( boolean immediately, Context context, String to, List< attachment > attachments, String subtype ) {
	++ sent;
	background.unread = (0 == background.unread) ? -1 : background.unread;
	if (0 >= sent) {
		immediately = false;
	}
	// Breaking IMAP's idle loop could be good at this point, but not from the main thread.
//	imap_close();
	String out = super.send( immediately, context, to, attachments, subtype );
	if (null == out) {
		// Not queued.
		local_message.lastMsg = "Message sent.";
		local_message.send_connection( context, true );
	} else {
		toFlush = true;
	}
	return out;
}

public boolean queue_check( Context c )
{
	toFlush = false;
	String dir = get_mqueue_dir( c );
	if (dir == null)
		return false;
	File[] files = new File( dir ).listFiles();
	if (files == null)
		return false;
	boolean out = false;
	for (File f : files) {
		if (f.isFile()) {
			if (f.length() > 0) {
				out = true;
				break;
			}
		}
	}
	toFlush = out;
	return out;
}

@Override
public void noop( IMAPFolder folder ) {
//	background.idleTimeNet = 20;
	if (folder == null) {
//		idleListenerDone = true;
		folder = mail_folder;
		if (folder == null) {
			imap_close();
			return;
		}
	} else if (folder != mail_folder) {
		imap_close();
		return;
	}
	try {
		if (folder.isOpen()) {
			folder.doCommand( new IMAPFolder.ProtocolCommand() {
				@Override
				public Object doCommand( IMAPProtocol p ) throws ProtocolException {
					p.simpleCommand( "NOOP", null );
					return null;
				}
			} );
			if (folder.isOpen()) {
				return;
			}
		}
	} catch (Exception e) { // (MessagingException e) {
		// Possible race condition.
		Dib2Config.log( "noop", "exception: " + e );
	}
	imap_close();
}

//=====
}
