// Copyright (C) 2016,2017,2018  Roland Horsch <gx work s{at}g mail.c om>.
// See LICENSE file (dibdib.sourceforge.net:LICENSE or assets/*.txt) for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.dib2qm;

import android.app.*;
import android.content.*;
import android.graphics.BitmapFactory;
import android.net.*;
import android.os.SystemClock;
import com.gitlab.dibdib.dib2qm_joined.*;
import com.sun.mail.imap.IMAPFolder;
import java.lang.Thread.State;
import javax.mail.Store;
import net.sf.dibdib.config.Dib2Config;
import net.sf.dibdib.func.UtilMisc;
import net.sourceforge.dibdib.android.dib2qm.R;
import net.vreeken.quickmsg.preferences;

public class background extends com.gitlab.dibdib.dib2qm_joined.background_1 {
//=====

// android.support.v4.app.NotificationCompat: NotificationCompatImplBase:
// 'Notification result = (Notification) b...' --> 'return b.getNotification();'

/** 2 force offline, 1 severe error, 0 temporary/ stopping thread, -1 go online */
protected static volatile int keepOffline = 2;
protected static volatile boolean dependsActivity = false;
protected static volatile Thread pollthread;
/** continuously doubled */
protected static volatile long lastLoop;

private static AlarmManager alarmMgr;
private static Intent intent;
private static PendingIntent alarmIntent;

/** 2 force offline, 1 severe error, 0 depending on Activity, -1 go online */
public static void setOffline( int offline ) {
	dependsActivity = (0 > offline) ? false : true;
	keepOffline = ((keepOffline >= 2) && (offline > 0)) ? keepOffline : ((0 == offline) ? -1 : offline);
}

public static int isOffline() {
	return keepOffline;
}

/**
 * Depending on 'pollthread' and 'keepOffline': Stop thread or have it move on within loop.
 */
public static void checkThreads() {
	idleTimeNet = 20;
	keepOffline = (0 >= keepOffline) ? -1 : keepOffline;
	new Thread( new Runnable() {
		@Override
		public void run() {
			mail.imap_close();
		}
	} ).start();
	if (null != pollthread) {
		pollthread.interrupt();
	}
}

private void doStartFg4HiddenActivity() {
	Intent notificationIntent = new Intent( this, ListActivity.class );
	PendingIntent pendingIntent = PendingIntent.getActivity( this, 0,
		notificationIntent,
		Intent.FLAG_ACTIVITY_NEW_TASK );

	notification = new Notification.Builder( this )
		.setSmallIcon( R.drawable.ic_stat_qm )
		.setContentText( getString( R.string.app_name ) )
		.setContentIntent( pendingIntent )
		//	.setPriority( Notification.PRIORITY_DEFAULT )
		.setLargeIcon( BitmapFactory.decodeResource( svcContext.getResources(),
			R.drawable.ic_launcher ) )
		.setOngoing( true );
	startForeground( notification_id, notification.getNotification() );
}

@Override
public void onCreate()
{
	super.onCreate();
	svcContext = this;
	doStartFg4HiddenActivity();
	prefs = new preferences( svcContext );
	db = prefs;
	alarmMgr = (AlarmManager) svcContext.getSystemService( Context.ALARM_SERVICE );
	intent = new Intent( svcContext, net.sourceforge.dibdib.android_qm.background.class );
	alarmIntent = PendingIntent.getService( svcContext, 0, intent, 0 );
//	Dib2Config.log( "bg", "onCreate" );
}

private NetworkInfo getNetworkInfo() {
	final String email = prefs.getLiteral( "email_address", "" );
	ConnectivityManager cm = (ConnectivityManager) svcContext.getSystemService( Context.CONNECTIVITY_SERVICE );
	NetworkInfo info = (null == cm) ? null : cm.getActiveNetworkInfo();
	if ((0 >= email.length()) || (info == null) || !info.isConnected()) {
		Dib2Config.log( "network", "no email/info" );
		return null;
	}
	return info;
}

public static void setIdleAlarm() {
	if ((null != alarmMgr) && (0 >= keepOffline)) {
		long jitter = ((lastLoop >>> 10) % MIN_IDLE_TIME);
		long idle = ((STD_IDLE_TIME > idleTimeNet) ? MIN_IDLE_TIME : STD_IDLE_TIME) + jitter;
		Dib2Config.log( "bg", "idleAlarm " + idle / 1000 );
		alarmMgr.set( AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() //,
			+ idle, alarmIntent );
	}
}

//Due to old naming of classes.
@SuppressWarnings( "static-access" )
@Override
public int onStartCommand( Intent intent, int flags, int startId )
{
	int state = 1;
	svcContext = this;
	// 'T' network okay, 'F' no network, 't' try to connect, 'f' pause.
	String msg = "...";
	if (intent != null) {
		msg = intent.getStringExtra( "state" );
		msg = ((null == msg) || (0 >= msg.length())) ? "T" : msg;
		switch (msg.charAt( 0 )) {
			case 'F':
				msg = "FNo network.";
				state = 0;
				break;
			case 'T':
				msg = "FChecking.";
				break;
			case 'f':
				msg = "F";
				state = 0;
				break;
			case 't':
				msg = "T";
				break;
			default:
				state = 0;
		}
	}
	final long delta = UtilMisc.currentTimeMillisLinearized() - lastLoop;
	final boolean stop = (state == 0) || (0 < keepOffline) //,
		|| ( //,
		(alarmDone || (dependsActivity && (0 > unread))) && !quickmsg_activity_1.isActive //,
		);

	if (null == notification) {
		doStartFg4HiddenActivity();
	}
	Dib2Config.log( "background", "onStartCommand() " + msg + keepOffline + ' ' + !stop + ' ' + mail.sent + dependsActivity
		+ unread //,
		+ ' ' + delta / 1000 + pollthread );
	if ((null != ListActivity_1.toast) || (null == quickmsg_activity_1.pgp)) {
		Dib2Config.log( "background", "waiting 1: " + ListActivity_1.toast );
		new Thread( new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep( 700 );
				} catch (InterruptedException e) {
				}
				if (null != ListActivity_1.toast) {
					Dib2Config.log( "background", "waiting 2: " + ListActivity_1.toast );
					ListActivity_1.toast = "";
					try {
						Thread.sleep( 500 );
					} catch (InterruptedException e) {
					}
				}
				local_message.send_statusMsg( svcContext, "?" );
			}
		} ).start();
		return START_REDELIVER_INTENT;
	}

	Thread poll = pollthread;
	if (stop) {
		Dib2Config.log( "background", "stopping/ " + keepOffline + dependsActivity + quickmsg_activity_1.isActive + (null == poll) );
		alarmMgr.cancel( alarmIntent );
		keepOffline = (0 < keepOffline) ? keepOffline : 0;
		if (null != pollthread) {
			checkThreads();
			Thread.yield();
		}
		if ((0 < keepOffline) || (dependsActivity && !quickmsg_activity_1.isActive && (0 > unread))) {
			local_message.send_statusMsg( svcContext, msg );
			Dib2Config.log( "background", "stopping2/ " + msg + (null == pollthread) );
			stopForeground( true );
			if (null == pollthread) {
				stopSelf();
				return START_NOT_STICKY;
			} else {
				pollthread = null;
				// Next time we will stop:
				local_message.send_background( svcContext, false );
			}
		}
		return START_REDELIVER_INTENT;
	}
	update_ui( false );
//	startForeground( notification_id, notification.getNotification() ); // build() );

	// As fallback:
	alarmMgr.set( AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() //,
		+ STD_IDLE_TIME, alarmIntent );
	if ((null != pollthread) && (mail.receive || ((pollthread.getState() == State.BLOCKED) && (2 * STD_IDLE_TIME > delta)))) {
//		Dib2Config.log( "bg", "blocked/ " + delta / 1000 );
		if (STD_IDLE_TIME < delta) {
			mail.receive = false;
		}
		return START_STICKY;
	}
	if ((null != pollthread) && (MAX_IDLE_TIME > delta) && (0 >= keepOffline)
		&& ((pollthread.getState() == State.RUNNABLE) || (pollthread.getState() == State.TIMED_WAITING))) {
		if ((STD_IDLE_TIME / 2 <= delta) && quickmsg_activity_1.isActive) {
			checkThreads();
		}
		return START_STICKY;
	}
	++ keepOffline;
	if ((1 < keepOffline) || (null != pollthread)) {
		poll = pollthread;
		pollthread = null;
		if (1 >= keepOffline) {
			// Thread died? IMAP? sleep()-cut-off? -- Wait for AlarmManager.
			local_message.send_statusMsg( svcContext, "FCut-off" );
			keepOffline = 0;
		} else {
			keepOffline = (2 < keepOffline) ? 2 : 1;
		}
		// Close IMAP in extra thread:
		checkThreads();
		poll.interrupt();
		return START_STICKY;
	}
	// The following part with (null == pollthread) is indirectly synchronized.
	// Otherwise we could have a race condition while starting the new pollthread.
	keepOffline = -1;
	lastLoop = UtilMisc.currentTimeMillisLinearized();

	///// Tries to keep IMAP's idle loop active by an extra polling loop.
	poll = new Thread( new Runnable() {
		@Override
		public void run() {
			idleTimeNet = (quickmsg_activity_1.isActive || (MIN_IDLE_TIME > idleTimeNet)) ? MIN_IDLE_TIME : idleTimeNet;
			long jitter = MIN_IDLE_TIME + ((lastLoop >>> 10) % MIN_IDLE_TIME);
			alarmMgr.set( AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() //,
				+ jitter + STD_IDLE_TIME + idleTimeNet, alarmIntent );
			local_message.send_statusMsg( svcContext, "TConnecting" );
			pollLoop:
			while ((0 > keepOffline) && (Thread.currentThread() == pollthread)) {
				long delta = UtilMisc.currentTimeMillisLinearized() - lastLoop;
				lastLoop += delta;
				boolean ok = false;
				try {
					while (quickmsg_activity_1.isActive) {
						if (3 * STD_IDLE_TIME <= delta) {
							Dib2Config.log( "mail idle", "loop " + mail.sent + " " + delta / 1000 + " " + idleTimeNet );
						}
						if (0 <= mail.sent) {
							mail.sent = -1;
							// Give the sending process some time:
							Thread.sleep( 200 );
//							mail.imap_close();
						}
						Thread.sleep( 50 );
						if (0 > mail.sent) {
							// Sending is temporarily disabled.
							break;
						}
					}
					while (true) {
						if (null == getNetworkInfo()) {
							break;
						}
						Store store = mail.open_imap( svcContext );
						if (null == store) {
							Dib2Config.log( "mail idle", "store null" );
							break;
						}
						IMAPFolder folder = mail.open_folder( svcContext, store );
						if (null == folder) {
							Dib2Config.log( "mail idle", "folder null" );
							break;
						}
						ok = folder.isOpen();
						// No additional delays if (ok == true).
						local_message.send_connection( background.getSvcContext(), ok );
						if (!ok) {
							break;
						}
						mail.receive = true;
						mail.recv( svcContext );
						ok = !mail.receive;
						mail.receive = false;
						if (!ok) {
							break;
						}
						// Got something?
						if (null == mail.mail_folder) {
							local_message.send_statusMsg( background.getSvcContext(), "TData" );
							break;
						}
						if (mail.toFlush) {
							mail.flush( svcContext );
							if (!mail.queue_check( svcContext )) {
								local_message.send_statusMsg( background.getSvcContext(), "TSent." );
							}
						}
						// In case idle() had returned too quickly:
						if ((1500 > delta) && quickmsg_activity_1.isActive) {
							Thread.sleep( 1200 );
						}
						if (0 <= findUnread()) {
							if (!alarmDone) {
								// Update UI, handle alarm:
								local_message.send_background( svcContext, true );
								idleTimeNet = MIN_IDLE_TIME;
							} else if (!quickmsg_activity_1.isActive) {
								// User has notification, we can slow down.
								idleTimeNet = MAX_IDLE_TIME;
							}
						}
						ok = folder.isOpen();
						if (!ok) {
							Dib2Config.log( "mail idle", "folder closed" );
							local_message.send_connection( background.getSvcContext(), ok );
							break;
						}
						// Enable sending:
						mail.sent = 0;
						if (quickmsg_activity_1.isActive) {
							local_message.send_statusMsg( svcContext, "TON." );
							alarmMgr.set( AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() //,
								+ jitter + (2 * STD_IDLE_TIME), alarmIntent );
							mail.idle( svcContext, STD_IDLE_TIME );
//							Dib2Config.log( "bg", "return from idle" );
							if ((-1 <= findUnread()) && !alarmDone) {
								// Update UI, handle alarm:
								local_message.send_background( svcContext, true );
							}
						}
						break;
					}
					long nap = idleTimeNet;
					idleTimeNet = (quickmsg_activity_1.isActive || (MIN_IDLE_TIME > idleTimeNet)) ? MIN_IDLE_TIME : (2 * idleTimeNet);
					idleTimeNet = (MAX_IDLE_TIME <= idleTimeNet) ? MAX_IDLE_TIME : idleTimeNet;
					if (!quickmsg_activity_1.isActive) {
						if (null == getNetworkInfo()) {
							Dib2Config.log( "bg poll", "network: cut-off" );
							// Use STD_IDLE_TIME from above:
							break pollLoop;
						}
						if (nap > delta) {
							// After second round.
							nap = (STD_IDLE_TIME / 2 < idleTimeNet) ? idleTimeNet : (STD_IDLE_TIME / 2);
							alarmMgr.set( AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() //,
								+ jitter + nap, alarmIntent );
							Dib2Config.log( "bg", "waiting for alarm " + keepOffline + ' ' + nap / 1000 );
							keepOffline = (0 > keepOffline) ? 0 : keepOffline;
							break pollLoop;
						}
						// Start second round: Do not get caught with sleep():
						continue;
					}
					local_message.send_connection( background.getSvcContext(), false );
					mail.imap_close();
					Thread.sleep( ok ? 20 : nap );
				} catch (InterruptedException e) {
					Dib2Config.log( "bg poll", "interrupt " + idleTimeNet + pollthread + mail.mail_folder );
				} catch (Exception e) {
					Dib2Config.log( "bg poll", "error: " + e.getMessage() + mail.mail_folder );
				}
			}
			local_message.send_statusMsg( svcContext, "F" );
			Dib2Config.log( "bg poll", "stop " + pollthread + mail.mail_folder );
			if (Thread.currentThread() == pollthread) {
				if (null != mail.mail_folder) {
					mail.imap_close();
				}
				pollthread = null;
			}
		}
	} );

	final Thread poll2 = poll;

	if (null == pollthread) {
		if ((0 >= keepOffline) && (null != poll2)) {
			pollthread = poll2;
			keepOffline = -1;
			poll2.start();
		}
	}

	return START_STICKY;
}

@Override
public void onDestroy()
{
	Dib2Config.log( "background", "onDestroy" );
	if (null != pollthread) {
		keepOffline = 1;
		try {
			checkThreads();
		} catch (Exception e) {
		}
	}
	alarmMgr.cancel( alarmIntent );
	notification = null;
}

//=====
}
